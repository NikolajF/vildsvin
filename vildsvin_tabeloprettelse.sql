﻿-- NB: tabeller med serial skal have sat værdien

create schema vildsvin;
set search_path = vildsvin, public;

--drop table o_fundtype;
create table o_fundtype
(
  id integer NOT NULL,
  tekst character varying,
  proever character varying,
  indsamling character varying,
  CONSTRAINT pk_o_fundtype PRIMARY KEY (id)
);

-- proever bestemmer hvad der videre skal ske
-- n er at de skal indsamles / nedlægges af naturstyrelsen
-- t er trikiner, ø de øvrige tre prøver

insert into o_fundtype(id, tekst, proever,indsamling) values
(1,'Levende', null, 'n'),
(10, 'Spor', null, 'n'),
(20, 'Nedlagt','tø', 'v'),
(21, 'Nedlagt, under hegn','tø', 'v'),
(30, 'Fundet død','ø', 'v'),
(40, 'Drevet i land, død', null, 'v');

--update o_fundtype set proever =null where proever =''


create table o_resultat
(
  id serial NOT NULL,
  tekst character varying,
  CONSTRAINT pk_o_resultat PRIMARY KEY (id)
);

insert into o_resultat(id, tekst) values
(0, 'Ikke relevant'),
(1, 'Ikke testet'),
(2, 'Positiv'),
(3, 'Negativ'),
(4, 'Test ikke mulig');

create table o_status
(
  id serial NOT NULL,
  tekst character varying,
  gruppe character varying,
  CONSTRAINT pk_o_status PRIMARY KEY (id)
);

--truncate o_status;
insert into o_status(id, tekst) values
(0, '1a. Afventer visitation'),
(1, '1c. Visiteret til indsamler'),
(2, '2a. Tildelt opsamlingshold'),
(3, '2e. Opsamling udført'),
(4, '2f. Afsendt til laboratorium'),
(5, '3a. Modtaget på laboratorium'),
(6, '3d. Undersøgt på laboratorium'),
(7, '3b. Kasseret af laboratorium'),
(90, '1b. Frasorteret ved visitation'),
(91, '2c. Ikke fundet af opsamlingshold'),
(92, '2b. Kasseret af opsamlingshold'),
(93, '3c. Ikke testet på laboratorium');
(94, '2d. Skal ikke indsamles'),

update o_status set gruppe ='Visit' where id in(0,1,90);
update o_status set gruppe ='Saml' where id in(2,3,4,91,92,94);
update o_status set gruppe ='Lab' where id in(5,6,7,93);



create table o_skjul
(
  id serial NOT NULL,
  tekst character varying,
  CONSTRAINT pk_o_skjul PRIMARY KEY (id)
);

insert into o_skjul(id, tekst) values
(0, 'Aktiv'),
(1, 'Gammel'),
(2, 'Dublet'),
(3, 'Test'),
(4, 'Fejlindtastning');


CREATE TABLE indberetninger
(
  id serial,
  registrator integer,
  afd integer,
  an_navn character varying,
  an_telefon character varying,
  an_email character varying,
  an_vejnavn character varying,
  an_husnr character varying,
  an_postnr character varying,
  an_by character varying,
  setdato character varying,
  setx integer,
  sety integer,
  setpostnr character varying,
  setbeskriv character varying,
  status integer DEFAULT 0,
  setvejnavn character varying,
  sethusnr character varying,
  setby character varying,
  afhentningsadresse character varying DEFAULT 'Samme som fundadressen'::character varying,
  fv_region integer,
  fundsted character varying,
	visitator integer DEFAULT 0,
	visiteret_dato character varying(8),
	visiteret_tid timestamp without time zone,
  pdf boolean DEFAULT false,
  an_tid timestamp without time zone,
  set_tid timestamp without time zone,
  skjul integer DEFAULT 0,
  geom geometry(Point,25832),
  registrator_ret integer DEFAULT 0,
  rettet_tid timestamp without time zone,
  kopimail character varying(30),
  fundkoordinater integer,
  indberetter_id character varying(36),
  setafstand integer NOT NULL DEFAULT 0,
  reserveret_af_id integer DEFAULT 0,
  afh_type integer DEFAULT 1,
  billede character varying,
  kommentar character varying,
  fundtype integer DEFAULT 0,
  antal integer DEFAULT 0,
  CONSTRAINT pk_indberetninger PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

/*
alter TABLE vildsvin.indberetninger
  drop column ancpr,
  drop column andato;
  
  alter TABLE vildsvin.indberetninger RENAME COLUMN annavn TO an_navn;
  alter TABLE vildsvin.indberetninger RENAME COLUMN antelefon TO an_telefon;
  alter TABLE vildsvin.indberetninger RENAME COLUMN anemail TO an_email;
  alter TABLE vildsvin.indberetninger RENAME COLUMN anvejnavn TO an_vejnavn;
  alter TABLE vildsvin.indberetninger RENAME COLUMN anhusnr TO an_husnr;
  alter TABLE vildsvin.indberetninger RENAME COLUMN anpostnr TO an_postnr;
  alter TABLE vildsvin.indberetninger RENAME COLUMN anby TO an_by;
*/  

-- i og med at tip-app skal have id på sete dyr retur, så man kan bruge numrene som labels, skal vi nok springe indberetninger_vildsvin over og gå direkte til vildsvin
-- også spor skal oprettes - men altid kun en



CREATE TABLE vildsvin
(
  id serial,
  indberetning integer DEFAULT 0,
  x integer,
  y integer,
  x_33 double precision,
  y_33 double precision,
  postnr character varying(4),
  kommune character varying,
  afd integer DEFAULT 0,
  status integer DEFAULT 1, -- samlet
  skjul integer DEFAULT 0,
  sidst_rettet timestamp without time zone DEFAULT now(),
  fundtype integer DEFAULT 0,
  tid_indsamlet timestamp without time zone, 
  journalnr_1 character varying,
  status_1 integer,
  tid_lab_1 timestamp without time zone,
  tid_slut_1 timestamp without time zone,
  journalnr_2 character varying,
  status_2 integer,
  tid_lab_2 timestamp without time zone,
  tid_slut_2 timestamp without time zone,
  asf integer DEFAULT 0,
  csf integer DEFAULT 0,
  aujeszkys integer DEFAULT 0,
  trikiner integer DEFAULT 0,
  kommentar character varying,
  geom geometry(Point,25832),
  tid_slut timestamp without time zone,
  tid_lab timestamp without time zone,
  resultat_tekst character varying,
  pos smallint DEFAULT 0,
  CONSTRAINT pk_vildsvin PRIMARY KEY (id)
)
WITH (
  OIDS=TRUE
);

/*
alter TABLE vildsvin.vildsvin RENAME COLUMN journalnr1 TO journalnr_1;
alter TABLE vildsvin.vildsvin RENAME COLUMN status1 TO status_1;
alter TABLE vildsvin.vildsvin RENAME COLUMN tid_lab1 TO tid_lab_1;
alter TABLE vildsvin.vildsvin RENAME COLUMN tid_slut1 TO tid_slut_1;
alter TABLE vildsvin.vildsvin RENAME COLUMN journalnr2 TO journalnr_2;
alter TABLE vildsvin.vildsvin RENAME COLUMN status2 TO status_2;
alter TABLE vildsvin.vildsvin RENAME COLUMN tid_lab2 TO tid_lab_2;
alter TABLE vildsvin.vildsvin RENAME COLUMN tid_slut2 TO tid_slut_2;
Alter TABLE vildsvin.vildsvin add column tid_lab timestamp without time zone
Alter TABLE vildsvin.vildsvin add column resultat_tekst character varying
Alter TABLE vildsvin.vildsvin add column pos smallint DEFAULT 0
*/
--select kode, tekst from vildsvin.opslag where gruppe like 'status%' order by 1



CREATE TABLE vildsvin.log
(
  id serial,
  ip character varying(20),
  tid timestamp without time zone,
  brugerid integer DEFAULT 0,
  godkendt smallint,
  script character varying(50),
  vars text,
  cookie character varying,
  CONSTRAINT pk_log PRIMARY KEY (id)
);



CREATE TABLE vildsvin.log_sql
(
  id serial,
  log integer DEFAULT 0,
  tabel character varying(30),
  post integer DEFAULT 0,
  poster integer DEFAULT 0,
  tekst text,
  resultat character varying,
  CONSTRAINT log_sql_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=TRUE
);
ALTER TABLE vildsvin.log_sql
  OWNER TO postgres;

-- Index: vildsvin.log_sql_log

-- DROP INDEX vildsvin.log_sql_log;

CREATE INDEX log_sql_log
  ON vildsvin.log_sql
  USING btree
  (log);

-- Index: vildsvin.log_sql_post

-- DROP INDEX vildsvin.log_sql_post;

CREATE INDEX log_sql_post
  ON vildsvin.log_sql
  USING btree
  (post);

-- Index: vildsvin.log_sql_tabel

-- DROP INDEX vildsvin.log_sql_tabel;

CREATE INDEX log_sql_tabel
  ON vildsvin.log_sql
  USING btree
  (tabel COLLATE pg_catalog."default");


CREATE TABLE vildsvin.log_stat
(
  id serial NOT NULL,
  cookie character varying,
  starttid timestamp without time zone,
  sluttid timestamp without time zone,
  bruger integer,
  firma integer,
  firmatype integer,
  kort_filter character varying,
  CONSTRAINT log_stat_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE vildsvin.log_stat
  OWNER TO postgres;

-- Index: vildsvin.log_stat_cookie

-- DROP INDEX vildsvin.log_stat_cookie;

CREATE INDEX log_stat_cookie
  ON vildsvin.log_stat
  USING btree
  (cookie COLLATE pg_catalog."default");



CREATE TABLE vildsvin.indstillinger_brugere
(
  id bigserial NOT NULL,
  side bigint,
  kolonne bigint,
  bruger integer,
  bredde integer NOT NULL DEFAULT 0,
  hidden boolean NOT NULL DEFAULT false,
  CONSTRAINT indstillinger_brugere_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE vildsvin.indstillinger_brugere
  OWNER TO postgres;

CREATE TABLE vildsvin.indstillinger_grids
(
  id bigserial NOT NULL,
  side bigint,
  grid character varying,
  raekkefoelge integer,
  titel character varying,
  opdateret boolean NOT NULL DEFAULT true,
  CONSTRAINT indstillinger_grids_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE vildsvin.indstillinger_grids
  OWNER TO postgres;
  

CREATE TABLE vildsvin.indstillinger_kolonner
(
  id bigserial NOT NULL,
  side bigint,
  grid bigint,
  raekkefoelge integer,
  dataindex character varying,
  titel character varying,
  bredde integer NOT NULL DEFAULT 0,
  hidden boolean NOT NULL DEFAULT false,
  opdateret boolean NOT NULL DEFAULT true,
  CONSTRAINT indstillinger_kolonner_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE vildsvin.indstillinger_kolonner
  OWNER TO postgres;


CREATE TABLE vildsvin.indstillinger_sider
(
  id bigserial NOT NULL,
  jsfil character varying,
  titel character varying,
  raekkefoelge integer,
  firma integer NOT NULL DEFAULT 0,
  udelad boolean NOT NULL DEFAULT false,
  opdateret boolean NOT NULL DEFAULT true,
  bredde integer NOT NULL DEFAULT 600,
  kort boolean NOT NULL DEFAULT true,
  kommando character varying,
  CONSTRAINT indstillinger_sider_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE vildsvin.indstillinger_sider
  OWNER TO postgres;


-------------------- EFTERRETTELSER --------------------------------
UPDATE vildsvin.o_fundtype set tekst =  'Ilanddrevet, død' where id= 40;

delete from vildsvin.o_skjul where id in (1,3);

--------------------   views til geoserver  -------------------- 
--drop view vildsvin.v_levende 
create view vildsvin.v_levende as 
select id as aid, fundtype, (reserveret_af_id > 0) as noteret, geom from vildsvin.indberetninger where fundtype <20;

-- tilføj manglende geometri i vildsvin
update vildsvin.vildsvin a set geom =b.geom from vildsvin.indberetninger b where b.id=a.indberetning;

create view vildsvin.v_vildsvin_doede as 
select id as aid, fundtype, status, geom from vildsvin.vildsvin;

create view vildsvin.v_vildsvin_proevesvar as 
select id as aid, fundtype, status, geom from vildsvin.vildsvin where fundtype <40 ;

create view vildsvin.v_vildsvin_off as 
select -id as aid, fundtype, 0 as pos, geom from vildsvin.indberetninger where fundtype < 20 or fundtype=40
union 
select id as aid, fundtype, pos, geom from vildsvin.vildsvin where fundtype <40 and status =6;

-- nye navne til statusværdier

-- 0, 90,94 skal udgå ???
delete from o_status where id in (0,90, 93, 94);
update o_status set tekst = 'Afventer indsamling' where id=1;
update o_status set tekst = 'Reserveret af indsamler' where id=2;
update o_status set tekst = 'Indsamling udført' where id=3;
update o_status set tekst = substring(tekst from 5) where id >4;
update o_status set tekst = 'Bortskaffet' where id = 7;
update o_status set tekst = 'Ikke fundet' where id = 91;
update o_status set tekst = 'Kasseret' where id = 92;

-- og testdata opdateres
update vildsvin set status = 3 where status in (1,2) and fundtype in (20,21);
update indberetninger set reserveret_af_id = 0 where fundtype in (20,21);


-- nst afdelinger
drop table vildsvin.nst_afdelinger
create table vildsvin.nst_afdelinger
(
id serial,
navn character varying, 
vej character varying, 
postnr integer, 
postby character varying, 
email character varying, 
telefon character varying,
geom geometry(Point,25832),
xy  character varying,
CONSTRAINT pk_nst_afdelinger PRIMARY KEY (id)
);



insert into vildsvin.nst_afdelinger(navn, vej, postnr, postby, email, telefon, xy) values
('Naturstyrelsen Blåvandshuk',
'Ålholtvej 1',
'6840',
'Oksbøl',
'BLH@nst.dk',
'72 54 35 11',
'449060 6178594'),

('Naturstyrelsen Bornholm',
'Rømersdal, Ekkodalsvejen 2',
'3720',
'Aakirkeby',
'bon@nst.dk',
'72 54 30 02',
'876042.4 6126600.8'),

('Naturstyrelsen Fyn',
'Sollerupvej 24, Sollerup',
'5600',
'Faaborg',
'fyn@nst.dk',
'72 54 35 52',
'587581.6 6135228'),

('Naturstyrelsen Himmerland',
'Møldrupvej 26',
'9520',
'Skørping',
'HIM@nst.dk',
'7254 3900',
'548388 6305109.6'),

('Naturstyrelsen Hovedstaden',
'Boveskovgård, Dyrehaven 6',
'2930',
'Klampenborg',
'HST@nst.dk',
'7254 3151',
'723537.3119574449 6175786.5773305055'),

('Naturstyrelsen Kronjylland',
'Vasevej 7',
'8920',
'Randers NV',
'KJY@nst.dk',
'72 54 39 20',
'571940 6256725.6'),

('Naturstyrelsen Midtjylland',
'Feldborggård, Bjørnkærvej 18',
'7540',
'Haderup',
'mjy@nst.dk',
'72 54 36 90',
'502410.4 6251400.8'),

('Naturstyrelsen Nordsjælland',
'Ostrupgård, Gillelejevej 2B',
'3230',
'Græsted',
'NSJ@nst.dk',
'Tlf.: 72 54 32 00',
'702849.3926898474 6213246.718838067'),

('Naturstyrelsen Storstrøm',
'Hannenovvej 22',
'4800',
'Nykøbing F.',
'STS@nst.dk ',
'54 43 90 13',
'683735.2 6109013.6'),

('Naturstyrelsen Søhøjlandet',
'Vejlbo, Vejlsøvej 12',
'8600',
'Silkeborg',
'SHL@nst.dk',
'72 54 39 51',
'559754.4 6209314.4'),

('Naturstyrelsen Sønderjylland',
'Felstedvej 14',
'6300',
'Gråsten',
'SDJ@nst.dk',
'72 54 35 00',
'538378.4 6101180'),

('Naturstyrelsen Thy',
'Søholt, Søholtvej 6, Vester Vandet',
'7700',
'Thisted',
'THY@nst.dk',
'72 54 39 65',
'482135.2 6312072.8'),

('Naturstyrelsen Trekantsområdet',
'Gjøddinggård, Førstballevej 2',
'7183',
'Randbøl',
'tre@nst.dk',
'72 54 35 85',
'535127.2 6172706.4'),

('Naturstyrelsen Vadehavet',
'Skovridervej 3, Arnum',
'6510',
'Gram',
'vad@nst.dk',
'72 54 33 31',
'487664.8 6111215.2'),

('Naturstyrelsen Vendsyssel',
'Sct. Laurentii Vej 148',
'9990',
'Skagen',
'VSY@nst.dk',
'72 54 36 53',
'557399.2 6364706.4'),

('Naturstyrelsen Vestjylland',
'Sønderby, Gl. Landevej 35, Fabjerg',
'7620',
'Lemvig',
'VJY@nst.dk ',
'72 54 36 64',
'451927.2 6247304.8'),

('Naturstyrelsen Vestsjælland',
'Mantzhøj, Ulkerupvej 1',
'4500',
'Nykøbing Sj.',
'vsj@nst.dk',
'72 54 32 88',
'660490.4 6172194.4'),

('Naturstyrelsen Østsjælland',
'Syvstjernen, Fægyden 1',
'3500',
'Værløse',
'OSJ@nst.dk',
'72 54 33 35',
'705904.8 6177493.6');

-- og så geometrikolonnen
update vildsvin.nst_afdelinger set geom=st_pointfromtext('POINT(' || xy || ')',25832)
alter table vildsvin.nst_afdelinger add column firmaid integer


insert into brugere.firmatype (tekst, vildsvin) values('Naturstyrelsen',true);

insert into brugere.firma (navn, firmatype, indsamlergruppe, email)
select regexp_replace(navn, 'Naturstyrelsen', 'NST'), 11, 'NST', email from vildsvin.nst_afdelinger

update vildsvin.nst_afdelinger a set firmaid = b.id from brugere.firma b where regexp_replace(a.navn, 'Naturstyrelsen', 'NST') = b.navn

-- rettelse 09-09-2018 - skal kun køres ved opdatering, create table har den nye kolonne
alter TABLE vildsvin.indstillinger_brugere add column side bigint;


------------------------------------------- samme ændring i fugle, dog gridi stedet for side -----------------------------------------------------------------
-- alter TABLE fugle.indstillinger_brugere add column grid bigint;
-- update fugle.indstillinger_brugere a set grid = b.grid from fugle.indstillinger_kolonner b where a.kolonne= b.id;


----------------------------------------- indhold til kolonnebredde-tabeller



