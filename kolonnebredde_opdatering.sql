--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.6
-- Dumped by pg_dump version 9.6.6

-- Started on 2018-10-03 03:45:18

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = vildsvin, pg_catalog;

--
-- TOC entry 4985 (class 0 OID 49077582)
-- Dependencies: 664
-- Data for Name: indstillinger_grids; Type: TABLE DATA; Schema: vildsvin; Owner: postgres
--
truncate indstillinger_grids ;
INSERT INTO indstillinger_grids VALUES (1, 2, 'Brugere_grid', 1, NULL, true);
INSERT INTO indstillinger_grids VALUES (8, 3, 'FindVildsvinegrid', 1, NULL, true);
INSERT INTO indstillinger_grids VALUES (3, 5, 'Indsamlinger_grid', 1, NULL, true);
INSERT INTO indstillinger_grids VALUES (4, 6, 'LabListe_grid', 1, NULL, true);
INSERT INTO indstillinger_grids VALUES (5, 7, 'ana_grid', 1, NULL, true);
INSERT INTO indstillinger_grids VALUES (6, 8, 'LevendeGrid', 1, NULL, true);

--
-- TOC entry 4994 (class 0 OID 0)
-- Dependencies: 663
-- Name: indstillinger_grids_id_seq; Type: SEQUENCE SET; Schema: vildsvin; Owner: postgres
--

SELECT pg_catalog.setval('indstillinger_grids_id_seq', 8, true);


--
-- TOC entry 4987 (class 0 OID 49077594)
-- Dependencies: 666
-- Data for Name: indstillinger_kolonner; Type: TABLE DATA; Schema: vildsvin; Owner: postgres
--

truncate indstillinger_kolonner;
INSERT INTO indstillinger_kolonner VALUES (2, 2, 1, 2, 'navn', 'Navn', 120, false, true);
INSERT INTO indstillinger_kolonner VALUES (3, 2, 1, 3, 'email', 'Email', 80, false, true);
INSERT INTO indstillinger_kolonner VALUES (4, 2, 1, 4, 'aktiv', 'Status', 40, false, true);
INSERT INTO indstillinger_kolonner VALUES (5, 2, 1, 5, 'firmanavn', 'Firmanavn', 60, false, true);
INSERT INTO indstillinger_kolonner VALUES (6, 2, 1, 6, 'firmaaktiv', 'Firma status', 40, false, true);
INSERT INTO indstillinger_kolonner VALUES (47, 3, 8, 1, 'indberetning', 'Indberetning', 80, false, true);
INSERT INTO indstillinger_kolonner VALUES (48, 3, 8, 2, 'vildsvin_id', 'Vildsvin ID', 80, false, true);
INSERT INTO indstillinger_kolonner VALUES (49, 3, 8, 3, 'journalnr_2', 'FVST journalnr.', 120, false, true);
INSERT INTO indstillinger_kolonner VALUES (50, 3, 8, 4, 'journalnr_1', 'LAB journalnr.', 120, false, true);
INSERT INTO indstillinger_kolonner VALUES (10, 5, 3, 1, 'id', 'ID', 40, true, true);
INSERT INTO indstillinger_kolonner VALUES (11, 5, 3, 2, 'afd', 'Afdeling', 120, false, true);
INSERT INTO indstillinger_kolonner VALUES (12, 5, 3, 3, 'opsamler', 'Opsamler', 80, false, true);
INSERT INTO indstillinger_kolonner VALUES (13, 5, 3, 4, 'postnr', 'Postnr', 60, true, true);
INSERT INTO indstillinger_kolonner VALUES (14, 5, 3, 5, 'indberetning', 'Indberetning', 40, false, true);
INSERT INTO indstillinger_kolonner VALUES (15, 5, 3, 6, 'fundtype', 'Fundtype', 100, false, true);
INSERT INTO indstillinger_kolonner VALUES (16, 5, 3, 7, 'set_tid', 'Set dato', 80, false, true);
INSERT INTO indstillinger_kolonner VALUES (17, 5, 3, 8, 'status', 'Status', 80, false, true);
INSERT INTO indstillinger_kolonner VALUES (18, 5, 3, 9, 'skjul', 'Vis / Skjul', 60, true, true);
INSERT INTO indstillinger_kolonner VALUES (19, 6, 4, 1, 'id', 'ID', 40, true, true);
INSERT INTO indstillinger_kolonner VALUES (20, 6, 4, 2, 'afd', 'Indsamler', 120, false, true);
INSERT INTO indstillinger_kolonner VALUES (21, 6, 4, 3, 'indberetning', 'Indberetning', 40, false, true);
INSERT INTO indstillinger_kolonner VALUES (22, 6, 4, 4, 'tid_lab', 'Modtaget dato', 80, true, true);
INSERT INTO indstillinger_kolonner VALUES (23, 6, 4, 5, 'fundtype', 'Fundtype', 100, false, true);
INSERT INTO indstillinger_kolonner VALUES (24, 6, 4, 6, 'journalnr_1', 'Lab journalnr.', 100, false, true);
INSERT INTO indstillinger_kolonner VALUES (25, 6, 4, 7, 'journalnr_2', 'FVST journalnr.', 100, false, true);
INSERT INTO indstillinger_kolonner VALUES (26, 6, 4, 8, 'status', 'Status', 80, false, true);
INSERT INTO indstillinger_kolonner VALUES (27, 6, 4, 9, 'resultat_tekst', 'Resultat', 100, false, true);
INSERT INTO indstillinger_kolonner VALUES (28, 6, 4, 10, 'skjul', 'Vis / Skjul', 60, true, true);
INSERT INTO indstillinger_kolonner VALUES (1, 2, 1, 1, 'id', 'ID', 40, true, true);
INSERT INTO indstillinger_kolonner VALUES (29, 7, 5, 1, 'id', 'Vildsvin ID', 40, true, true);
INSERT INTO indstillinger_kolonner VALUES (30, 7, 5, 2, 'indberetning', 'Indberetning', 60, false, true);
INSERT INTO indstillinger_kolonner VALUES (35, 7, 5, 3, 'set_tid', 'Funddato', 80, false, true);
INSERT INTO indstillinger_kolonner VALUES (31, 7, 5, 4, 'fundtype', 'Fundtype', 100, false, true);
INSERT INTO indstillinger_kolonner VALUES (36, 7, 5, 5, 'resultat', 'Resultat', 120, false, true);
INSERT INTO indstillinger_kolonner VALUES (32, 7, 5, 6, 'journalnr_1', 'LAB Journalnr', 120, false, true);
INSERT INTO indstillinger_kolonner VALUES (33, 7, 5, 7, 'journalnr_2', 'FVST Journalnr', 120, false, true);
INSERT INTO indstillinger_kolonner VALUES (34, 7, 5, 8, 'kommune', 'Kommune', 120, false, true);
INSERT INTO indstillinger_kolonner VALUES (37, 8, 6, 1, 'id', 'Indberetning', 40, false, true);
INSERT INTO indstillinger_kolonner VALUES (38, 8, 6, 2, 'antal', 'Antal', 30, false, true);
INSERT INTO indstillinger_kolonner VALUES (39, 8, 6, 3, 'fundtype', 'Fundtype', 80, false, true);
INSERT INTO indstillinger_kolonner VALUES (40, 8, 6, 4, 'afd', 'Registrator', 100, true, true);
INSERT INTO indstillinger_kolonner VALUES (41, 8, 6, 5, 'status', 'Enhed', 100, false, true);
INSERT INTO indstillinger_kolonner VALUES (42, 8, 6, 6, 'reserveret_af_id', 'Noteret', 50, false, true);
INSERT INTO indstillinger_kolonner VALUES (43, 8, 6, 7, 'setpostnr', 'Postnr', 40, false, true);
INSERT INTO indstillinger_kolonner VALUES (44, 8, 6, 8, 'set_tid', 'Fund dato', 80, false, true);
INSERT INTO indstillinger_kolonner VALUES (45, 8, 6, 9, 'skjul', 'Vis / Skjul', 60, true, true);


--
-- TOC entry 4995 (class 0 OID 0)
-- Dependencies: 665
-- Name: indstillinger_kolonner_id_seq; Type: SEQUENCE SET; Schema: vildsvin; Owner: postgres
--

SELECT pg_catalog.setval('indstillinger_kolonner_id_seq', 50, true);


-- Completed on 2018-10-03 03:45:18

--
-- PostgreSQL database dump complete
--

