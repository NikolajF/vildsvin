﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LE34.Data.Access;
using System.IO;
using System.Configuration;
using System.Data;


namespace Vildsvin
{
    class Program
    {
        static DataBase db = new DataBase("", ConfigurationManager.ConnectionStrings["vildsvin_npgsql"].ConnectionString);
        static string progbib = System.Reflection.Assembly.GetEntryAssembly().Location;

        static void Main(string[] args)
        {
            // find formularmappen
            progbib = progbib.Substring(0, progbib.IndexOf("kolonneskriver")) + @"Vildsvin\formularer\";

            opret_database();
            skriv_js();
        }

        static void skriv_js()
        {
            db.SetSQLStatement("select * from indstillinger_sider where udelad =false order by raekkefoelge");
            db.Query();
            foreach (DataRow r in db.Result.Rows)
            {
                string[] linjer = null;
                using (StreamReader sr = new StreamReader(progbib + r["jsfil"].ToString()))
                {
                    string ind = sr.ReadToEnd();
                    linjer = ind.Replace("\r", "").Split('\n');
                }
                string side = r["jsfil"].ToString().Replace(".js", "");
                StringBuilder b = new StringBuilder();
                string tilstand = "";
                string grid = "";
                string kol = "";
                foreach (string lin in linjer)
                {
                    switch (tilstand)
                    {
                        case "": // intet sat endnu
                            if (lin.Contains("Ext.grid.Panel"))
                            {
                                tilstand = "grid";
                                int bg = lin.IndexOf("var ") + 4;
                                int slut = lin.IndexOf("=", bg);
                                grid = lin.Substring(bg, slut - bg).Trim();
                            }
                            b.AppendLine(lin);
                            break;
                        case "grid": //grid fundet, find kolonner
                            if (lin.Contains("columns: {"))
                            {
                                tilstand = "kol";
                                kol = "";
                            }
                            b.AppendLine(lin);
                            break;
                        case "kol": // i gang med kolonner, hold øje med gridkol_slut og opdeling i enkelte kolonner
                            if (lin.Contains("dataIndex:"))
                            {
                                kol = findKol(lin, "dataIndex", "text");
                                b.AppendLine(lin);
                                string indryk = lin.Substring(0, lin.IndexOf("dataIndex"));
                                b.AppendLine(indryk + "width: " + side + "." + grid + "." + kol + ".width,");
                                b.AppendLine(indryk + "hidden: " + side + "." + grid + "." + kol + ".hidden,");
                            }
                            else if (lin.Contains("width:") || lin.Contains("hidden:"))
                            {
                                // skriv ikke disse linjer
                            }
                            else
                            {
                                b.AppendLine(lin);
                                if (lin.Contains("gridkol_slut"))
                                    tilstand = "";
                            }
                            break;

                    }
                }
                using (StreamWriter w = new StreamWriter(progbib + side + "_ny.js", false, Encoding.UTF8))
                {
                    w.Write(b.ToString());
                }
            }
        }
        static void opret_database()
        {
            Console.WriteLine("Nulstiller tabeller");
            db.SingleSelect<long>("update indstillinger_sider set opdateret =false");
            db.SingleSelect<long>("update indstillinger_grids set opdateret =false");
            db.SingleSelect<long>("update indstillinger_kolonner set opdateret =false");



            string[] filer = Directory.GetFiles(progbib);
            foreach (string fil in filer)
            {
                string filnavn = fil.Substring(fil.LastIndexOf("\\") + 1);
                Console.WriteLine(filnavn);
                long? sidetal = db.SingleSelect<long>("update indstillinger_sider set opdateret = true where jsfil='" + filnavn + "' returning case when udelad = true then -id else id end as id");
                if (sidetal == null || sidetal == 0)
                    sidetal = db.SingleSelect<long>("insert into indstillinger_sider(jsfil) values('" + filnavn + "') returning id");
                if (sidetal > 0) // skal ikke springes over
                {
                    int slut = 0;
                    int bg = 0;
                    int koltal = 0;
                    // åbn filen
                    using (StreamReader sr = new StreamReader(fil))
                    {
                        int rk = 0;
                        string ind = sr.ReadToEnd();
                        // og så grids
                        slut = 0;
                        bg = 0;
                        do
                        {
                            bg = ind.IndexOf("Ext.grid.Panel'", slut + 1);
                            if (bg > 0)
                            {
                                rk++;
                                // gå til begyndelsen af linjen og find gridnavnet - der står altid var foran
                                int vbg = ind.LastIndexOf("var ", bg) + 4;
                                string gridnavn = ind.Substring(vbg, ind.IndexOf(" ", vbg) - vbg);
                                Console.WriteLine("    " + gridnavn);
                                long? gridnr = db.SingleSelect<long>("update indstillinger_grids set opdateret =true, raekkefoelge = " + rk + " where side = " + sidetal + " and grid ='" + gridnavn + "' returning id");
                                if (gridnr == null || gridnr == 0)
                                    gridnr = db.SingleSelect<long>("insert into indstillinger_grids(side, grid, raekkefoelge) values(" + sidetal + ", '" + gridnavn + "', " + rk + ") returning id");
                                bg = ind.IndexOf("columns: {", bg) + 10;
                                slut = ind.IndexOf("gridkol_slut", bg);
                                string[] kolonner = ind.Substring(bg, slut - bg).Split('{');
                                koltal = 0;

                                // hvad hvis der er slettet kolonner siden sidst? alle kolonner skal have en gl-markør sat inden de nye kolonner indlæses
                                // nye værdier, inklusive rækkefølge, overskriver altid gamle
                                // og til sidst slettes de kolonner, der ikke længere findes, både her og i indstillinger_bruger
                                foreach (string kol in kolonner)
                                {
                                    // for hver kolonne skal vi have titel, dataindex, bredde og hidden - det klares med en funktion, der blot tager en søgeværdi
                                    string dataindex = findKol(kol, "dataIndex", "text");
                                    // i første omgang skriver vi bare hvad vi kan, men der skal måske laves yderligere tjek af at det er en gyldig kolonne
                                    if (dataindex != "")
                                    {
                                        string titel = findKol(kol, "text", "text");
                                        string bredde = findKol(kol, "width", "number");
                                        string hidden = findKol(kol, "hidden", "bool");
                                        koltal++;
                                        long? kolnr = db.SingleSelect<long>("select id from indstillinger_kolonner where side = " + sidetal + " and grid = " + gridnr + " and dataindex = '" + dataindex + "'");
                                        if (kolnr == null || kolnr == 0)
                                            kolnr = db.SingleSelect<long>("insert into indstillinger_kolonner(side, grid, raekkefoelge, dataindex, titel, bredde, hidden) values(" + sidetal + ", " + gridnr + ", " + koltal + ", '" + dataindex + "', '" + titel + "', " + bredde + ", " + hidden + ") returning id");
                                        else
                                            kolnr = db.SingleSelect<long>("update indstillinger_kolonner set raekkefoelge = " + koltal + ", titel ='" + titel + "', bredde=" + bredde + ", hidden= " + hidden + ", opdateret=true where id =" + kolnr + " returning id");

                                        // og skriv så variablene i js-filen

                                    }

                                }

                            }
                        } while (bg > 0);
                    }
                }
            }
        }

        static string findKol(string kol, string krit, string type)
        {
            string svar = (type == "bool" ? "false" : "");

            kol = kol.Replace("\"", "'");
            int bg = kol.IndexOf(krit);
            int slut = 0;
            if (bg > 0)
            {
                if (type == "text")
                {
                    bg = kol.IndexOf("'", bg) + 1;
                    slut = kol.IndexOf("'", bg);
                    svar = kol.Substring(bg, slut- bg);
                }
                else //if (type == "number") fælles med bool
                {
                    bg = kol.IndexOf(":", bg) + 1;
                    slut = kol.IndexOf("\r", bg);
                    svar = kol.Substring(bg, slut - bg);
                    // og så skal vi slette mellemrum og komma
                    svar = svar.Replace(" ", "").Replace(",", "");
                }
            }

            return svar;
        }
        }
    }
