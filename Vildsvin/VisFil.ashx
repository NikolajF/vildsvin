﻿<%@ WebHandler Language="C#" Class="Vildsvin.VisFil" %>

using System;
using System.Configuration;
using System.Web;
using System.Diagnostics;
using System.Collections.Generic;
//using Vildsvin.Webservices;
using System.Data;
using LE34.Data.Access;

namespace Vildsvin
{
    public class VisFil : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //bool OK = true;
            string type = "";
            string id = "";
            string fileName = "";
            //string indhold = "";
            svin_util ut = new svin_util(true);
            //byte[] buf = null;
            try
            {
                type = context.Request.QueryString["type"].ToString();
                id = context.Request.QueryString["id"].ToString();
                string logintype = "tjekLogin";
                string brugerID = ut.tjekBruger(logintype , "id=" + id + "\r\ntype=" + type, "", "");
                // ??? hvordan skal der fejles hvis brugeren ikke er logget ind
                switch (type)
                {
                    case "pdf":
                        fileName = "Køreseddel_" + id + ".pdf";
                        logintype = "tjek";
                        break;
                    case "xl_levende": // Levende som Excel-fil
                        fileName = "vildsvin_levende_" + ut.nu.ToString("yyyyMMdd_HHmmss") + ".xlsx";
                        ut.ExcelRapportLevende(context.Request.QueryString["gridfilter"].ToString(), System.Configuration.ConfigurationManager.AppSettings["beskyttet_upload"] + fileName);
                        break;
                    case "xl_indsaml": // Indsamlinger som Excel-fil
                        fileName = "vildsvin_indsaml_" + ut.nu.ToString("yyyyMMdd_HHmmss") + ".xlsx";
                        ut.ExcelRapportIndsaml(context.Request.QueryString["gridfilter"].ToString(), System.Configuration.ConfigurationManager.AppSettings["beskyttet_upload"] + fileName);
                        break;
                    case "xl_find": // Find vildsvin som Excel-fil
                        fileName = "find_vildsvin_" + ut.nu.ToString("yyyyMMdd_HHmmss") + ".xlsx";
                        ut.ExcelRapportFind(context.Request.QueryString["value"].ToString(), context.Request.QueryString["felt"].ToString(), System.Configuration.ConfigurationManager.AppSettings["beskyttet_upload"] + fileName);
                        break;
                    case "xl_lab": // LAB udtræk som Excel-fil
                        fileName = "vildsvin_lab_" + ut.nu.ToString("yyyyMMdd_HHmmss") + ".xlsx";
                        ut.ExcelRapportLab(context.Request.QueryString["gridfilter"].ToString(), System.Configuration.ConfigurationManager.AppSettings["beskyttet_upload"] + fileName);
                        break;
                    case "xl_brugere":
                        fileName = "vildsvin_brugere_" + ut.nu.ToString("yyyyMMdd_HHmmss") + ".xlsx";
                        ut.ExcelRapportBrugere(context.Request.QueryString["gridfilter"].ToString(), System.Configuration.ConfigurationManager.AppSettings["beskyttet_upload"] + fileName);
                        break;
                    case "xl_off": // offentligt udtræk som Excel-fil
                        fileName = "vildsvin_off_" + ut.nu.ToString("yyyyMMdd_HHmmss") + ".xlsx";
                        ut.ExcelRapportOff(context.Request.QueryString["aar"].ToString(), System.Configuration.ConfigurationManager.AppSettings["beskyttet_upload"] + fileName);
                        break;
                }
                // Set the filename
                context.Response.AddHeader("content-disposition", "attachment;filename=\"" + fileName + "\"");

                //try
                //{
                // Stream the file to the client
                switch (type)
                {
                    case "pdf":
                        context.Response.ContentType = "application/pdf";
                        context.Response.TransmitFile(System.Configuration.ConfigurationManager.AppSettings["beskyttet_upload"] + fileName);
                        break;

                    case "xl_levende": // Levende som Excel-fil
                    case "xl_indsaml": // Indsamlinger som Excel-fil
                    case "xl_lab": // LAB udtræk som Excel-fil
                    case "xl_find": // Find fugl som Excel-fil, intet filter
                    case "xl_brugere":
                    case "xl_off":
                        context.Response.ContentType = "application/octetstream";
                        //context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        context.Response.TransmitFile(System.Configuration.ConfigurationManager.AppSettings["beskyttet_upload"] + fileName);
                        break;
                }

            }
            catch (Exception ex)
            {
                //OK = false;
                // set content type to text
                context.Response.ContentType = "text/html";

                // output content
                context.Response.Write("Ugyldigt kald. " + ex.Message);
                context.Response.StatusCode = 500;
            }
            //}
            //catch (Exception e)
            //{
            //	System.Diagnostics.Debug.Print(e.Message);
            //	// set content type to text
            //	context.Response.ContentType = "text/html";

            //	// output content
            //	context.Response.Write("Ugyldigt kald. " + e.Message);
            //	//context.Response.Status = "Ugyldigt kald.";
            //	context.Response.StatusCode = 500;

            //}
            //			}
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}
