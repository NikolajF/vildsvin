﻿
using System;
using System.Data;
using System.Data.OleDb;
//using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Xml;
//using System.Web.Services;
using System.IO;
//using System.Net;
using System.Text;
//using System.Web.UI;
//using System.Web.UI.WebControls;
using System.Web.Security;
using System.Reflection;  // reflection namespace

using LE34.Data.Access;

namespace Vildsvin
{
	public class svin_util
	{

		public bool erSendt = false;
        // slet herover

        public BrugerInfo brugerinfo = new BrugerInfo();
        public DateTime nu { get; set; }
        public string nuDB = "";
        public string ProgMappe = "";

		public string svin_cookie { get; set; }
        public string logid = "0";
        public string PDFMappe = System.Configuration.ConfigurationManager.AppSettings["beskyttet_upload"];


        public bool _epidemi = bool.Parse(ConfigurationManager.AppSettings["epidemi"]); 
         
        public svin_util(bool http) // initiering af klassen
		{
			// find sessionID en gang for alle
			HttpCookie cookie;
			try
			{
				if (http)
				{
					cookie = HttpContext.Current.Request.Cookies["svin_ticket"];
					svin_cookie = cookie.Value;
					ProgMappe = HttpContext.Current.Server.MapPath("~/");// +"\\";

				}
				Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-GB");
				nu = DateTime.Now;
				nuDB = nu.ToString("yyyy-MM-dd HH:mm:ss");
			}
			catch
			{
				throw new HttpException(403,"Du har ingen rettigheder til at lave dette kald.");
			}
            brugerinfo.epidemi = _epidemi;

        }

        #region "Database"
        public DataBase getDB(string sql)
        {
            return new DataBase(sql, ConfigurationManager.ConnectionStrings["svin_npgsql"].ConnectionString);
        }

        public void db_BeginTransaction(DataBase t)
        {
            t.BeginTransaction();
            DataBase db = getDB("insert into log_sql(log, tekst) values('" + logid + "', 'BeginTransaction')");
            db.Execute(true);

        }

        public void db_CommitTransaction(DataBase t)
        {
            try
            {
                t.CommitTransaction();
                db_exec("insert into log_sql(log, tekst) values('" + logid + "', 'CommitTransaction')", "", "0");
            }
            catch
            {
                throw;
            }
        }

        public void db_RollbackTransaction(DataBase t)
        {
            t.RollbackTransaction();
            db_exec("insert into log_sql(log, tekst) values('" + logid + "', 'RollbackTransaction')", "", "0");
        }

        public string db_exec_t(string sql, string tabel, string id, DataBase t)
        {
            string svar = "";
            string fejl = "";

            try
            {
                t.SetSQLStatement(sql);
                svar = t.Execute().ToString();
            }
            catch (Exception ex)
            {
                fejl = ex.Message;
            }
            finally
            {
                if (tabel != "")
                {
                    if (svar == null || svar == "")
                        svar = "0";
                    DataBase db = getDB("insert into log_sql(log, tabel, post, poster, tekst, resultat) values('" + logid + "', '" + tabel + "'," + id + ", " + svar + ", " + anf(sql, true) + ", " + anf(fejl, true) + ")");
                    db.Execute(true);
                }
            }
            if (fejl != "")
                throw new Exception("Der er sket en fejl ved skrivning i databasen:\r\n" + fejl);
            return svar;
        }

        public string db_exec(string sql, string tabel, string id)
        {
            string svar = "";
            string fejl = "";
            DataBase db = getDB(sql);
            try
            {
                svar = db.Execute(false).ToString();
            }
            catch (Exception ex)
            {
                fejl = ex.Message;
            }
            finally
            {
                if (tabel != "")
                {
                    if (svar == null || svar == "")
                        svar = "0";
                    db.SetSQLStatement("insert into log_sql(log, tabel, post, poster, tekst, resultat) values('" + logid + "', '" + tabel + "'," + id + ", " + svar + ", " + anf(sql, true) + ", " + anf(fejl, true) + ")");
                    db.Execute();
                }
                db.Close();
            }
            if (fejl != "")
                throw new Exception("Der er sket en fejl ved skrivning i databasen:\r\n" + fejl);
            return svar;
        }

        public DataBase db_query(string sql, string tabel, string id)
        {
            string fejl = "";
            string svar = "0";
            DataBase db = getDB(sql);
            try
            {
                db.Query(true);
                if (tabel != "")
                {
                    if (db.Result != null)
                    {
                        svar = db.Result.Rows.Count.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                fejl = ex.Message;
            }
            finally
            {
                if (tabel != "")
                {
                    if (svar == null || svar == "")
                        svar = "0";
                    else if (tabel == "label_id_seq" && db.Result.Rows.Count > 0)
                        id = db.Result.Rows[0]["nextval"].ToString();
                    db_exec("insert into log_sql(log, tabel, post, poster, tekst, resultat) values('" + logid + "', '" + tabel + "'," + id + ", " + svar + ", " + anf(sql, true) + ", " + anf(fejl, true) + ")", "", "0");
                }
            }
            if (fejl != "")
                throw new Exception("Der er sket en fejl ved læsning i databasen:\r\n" + fejl);
            return db;
        }

        public DataRow db_enkeltrække(string sql, string tabel, string id)
        {
            string fejl = "";
            string svar = "0";
            DataBase db = getDB(sql);
            DataRow rk = null;
            try
            {
                db.Query(true);
                if (tabel != "")
                {
                    if (db.Result != null)
                    {
                        svar = db.Result.Rows.Count.ToString();
                        if (db.Result.Rows.Count > 0)
                            rk = db.Result.Rows[0];


                    }
                }
            }
            catch (Exception ex)
            {
                fejl = ex.Message;
            }
            finally
            {
                if (tabel != "")
                {
                    if (svar == null || svar == "")
                        svar = "0";
                    db_exec("insert into log_sql(log, tabel, post, poster, tekst, resultat) values('" + logid + "', '" + tabel + "'," + id + ", " + svar + ", " + anf(sql, true) + ", " + anf(fejl, true) + ")", "", "0");
                }
            }
            if (fejl != "")
                throw new Exception("Der er sket en fejl ved læsning i databasen:\r\n" + fejl);
            return rk;
        }

        public string db_enkeltopslag(string sql, string tabel, string felt, string id)
        {
            string svar = "0";
            string antal = "0";
            string fejl = "";
            DataBase db = getDB(sql);
            try
            {
                db.Query(true);
                // ??? hvis der ikke findes poster, fejler næste linje
                if (db.Result != null && db.Result.Rows.Count > 0)
                {
                    svar = db.Result.Rows[0][0].ToString();
                    // ??? burde det ikke være svar, der skrives i log_sql, snarere end antal - det er i hvert fald det interessante i forbindelse med alle opslag om en post findes i forvejen - at der er en række med antallet 0 er ikke relevant
                    // måske kan det klares ved at kontrollere om der er count( i sql
                    antal = db.Result.Rows.Count.ToString();
                }
            }
            catch (Exception ex)
            {
                svar = ex.Message;
                fejl =svar;
            }
            finally
            {
                if (tabel != "")
                {
                    if (svar == null || svar == "")
                        svar = "0";
                    db_exec("insert into log_sql(log, tabel, post, poster, tekst, resultat) values('" + logid + "', '" + tabel + "'," + id + ", " + antal + ", " + anf(sql, true) + ", " + anf(svar, true) + ")", "", "0");
                }
            }
            if (fejl != "")
                throw new Exception("Der er sket en fejl ved læsning i databasen:\r\n" + fejl);
            return svar;
        }
        #endregion

        #region "Login"
        public string tjekBruger(string LoginType, string urlParameters, string email, string kendeord)
		{
			string svar = "0";
			string fejl = "";
            DataBase db = getDB("");
            logid = db.GetNextval("log_id_seq").ToString();

            string url_anf = anf(urlParameters, true);
            switch (LoginType)
            {
                case "sys": // systemopdateringerne skal have en særlig logintype=0 - og intet af det øvrige skal køres
                    svar = "-1";
                    db.SetSQLStatement("insert into log(id, tid, brugerid, script, vars) values(" + logid + ", '" + nu + "'," + svar + ",'systemopdatering', " + url_anf + ")");
                    db.Execute(false);
                    return svar;
                case "start":
                    // opret en ny post med cookie-value, starttid, bruger =0, firma=0, sider =1
                    //svar = Guid.NewGuid().ToString();
                    db.SetSQLStatement("insert into log_stat(cookie, starttid, sluttid) values(:cookie, '" + nu + "', '" + nu + "')");
                    db.Add("cookie", kendeord, NpgsqlTypes.NpgsqlDbType.Varchar);
                    db.Execute(true);
                    return svar;
                case "login": 
                    // ??? dette skal vel skrives i sql log - kan man adde parametre inden sqlstatementet sættes ?
                
                    // i første omgang slår den bare en bruger op og sætter værdierne
                    db.SetSQLStatement(@"select b.id, b.navn, b.firmaid, f.firmatype, f.navn as firmanavn 
						from brugere.brugere b inner join brugere.firma f on b.firmaid=f.id inner join brugere.firmatype t on f.firmatype=t.id
						WHERE lower(b.email)=:email and b.kendeord=:kendeord and b.aktiv = true and f.aktiv = true and t.vildsvin=true");
					db.Add("email", email.ToLower(), NpgsqlTypes.NpgsqlDbType.Varchar);
					db.Add("kendeord", kendeord, NpgsqlTypes.NpgsqlDbType.Varchar);
                    db.Query(false);
                    if (db.Result == null || db.Result.Rows.Count == 0)
                    {
                        db.Close();
                        throw new Exception("Beklager, brugernavnet og / eller adgangskoden er forkert."); 
                    }
                    else
                    { 
                        DataRow r = db.Result.Rows[0];
                        // sæt rettighederne
                        brugerinfo.brugernavn = r["navn"].ToString();
                        brugerinfo.brugerid = r["id"].ToString();
                        brugerinfo.firma = r["firmaid"].ToString();
                        brugerinfo.firmatype = r["firmatype"].ToString();
						brugerinfo.firmanavn = r["firmanavn"].ToString();
						brugerinfo.epidemi = _epidemi; 
						// skriv session
                        db_exec("update log_stat set bruger =" + brugerinfo.brugerid + ", firma=" + brugerinfo.firma + ", firmatype=" + brugerinfo.firmatype + ", sluttid='" + nu + "' where cookie ='" + svin_cookie + "'", "", "0");
						svar = brugerinfo.brugerid;
                    //    return svar;
						break;
                    }
                case "tjek": // slå brugeren op fra sessionid og returner et brugerid
                case "tjekLogin": // slå brugeren op fra sessionid og returner et brugerid
                    svar = db_enkeltopslag("select bruger from log_stat where cookie='" + svin_cookie + "'", "log_stat", "bruger", "0"); // order by seneste_kald desc limit 1
                    if (svar == "") svar = "0";
                    if (svar == "0") // ikke logget ind (længere)
                    {
                        if (LoginType == "tjekLogin")
                        {

                            Debug.Print(urlParameters);
                            throw new Exception("Beklager, du er nødt til at logge ind igen, inden du fortsætter.");
                        }
                    }
                    else
                    {
                        DateTime sidst = DateTime.Parse(db_enkeltopslag("select sluttid from log_stat where cookie='" + svin_cookie + "'", "log_stat", "sluttid", "0")); // order by seneste_kald desc limit 1
                        sidst = sidst.AddHours(18);
                        if (DateTime.Compare(sidst, nu) < 0)
                            fejl = "Dit login er udløbet";
                        else
                            db_exec("update log_stat set sluttid ='" + nu + "' where cookie='" + svin_cookie + "'", "log_stat", svar); // and brugerid=" + svar

                        fyldBrugerInfo(svar);
                    }
                    break;
				case "logaf": // slå brugeren op fra sessionid og returner et brugerid
                    db_exec("update log_stat set sluttid ='" + nu + "', bruger=null, firma=null, firmatype=null, kort_filter=null where cookie='" + svin_cookie + "'", "log_stat", "0");
					svar = "0";
					fyldBrugerInfo(svar);
					break;
				case "": // intet login, blot skrivning af log
					svar = db_enkeltopslag("select bruger from log_stat where cookie='" + svin_cookie + "'", "log_stat", "bruger", "0"); // order by seneste_kald desc limit 1
					if (svar == "") svar = "0";
						db_exec("update log_stat set sluttid ='" + nu + "' where cookie='" + svin_cookie + "'", "log_stat", svar); // and brugerid=" + svar
					fyldBrugerInfo(svar);
					break;
				case "email": // send email
                    break;
            }
			// gem de rå inddata
			StackTrace stackTrace = new StackTrace();
			// get calling method name
			string scriptnavn = stackTrace.GetFrame(1).GetMethod().Name;
			string ip = System.Web.HttpContext.Current.Request.UserHostAddress;
			db.SetSQLStatement("insert into log(id, ip, tid, brugerid, script, vars, cookie) values(" + logid + ", '" + ip + "', '" + nu + "'," + svar + ",'" + scriptnavn + "', " + url_anf + ",'" + svin_cookie + "')");
            db.Execute(true);

			if (fejl != "")
				throw new Exception(fejl);

            // find rettighederne
            //int svartal = int.Parse(svar);
            return svar;
		}
        public bool fyldBrugerInfo(string brugerid)
        {
            DataBase db = getDB("select b.id, b.navn, b.firmaid, f.firmatype, f.navn as firmanavn from brugere.brugere b inner join brugere.firma f on b.firmaid=f.id WHERE b.id = " + brugerid + " and b.aktiv = true and f.aktiv = true");
            db.Query(true);
            if (db.Result == null || db.Result.Rows.Count == 0)
                return false;
            else
            { 
                DataRow r = db.Result.Rows[0];
                // sæt rettighederne
                brugerinfo.brugernavn = r["navn"].ToString();
                brugerinfo.brugerid = r["id"].ToString();
                brugerinfo.firma = r["firmaid"].ToString();
                brugerinfo.firmatype = r["firmatype"].ToString();
				brugerinfo.firmanavn = r["firmanavn"].ToString();
                //brugerinfo.epidemi = epidemi;  //bool.Parse(ConfigurationManager.AppSettings["epidemi"]); 
                return true;
            }
        }

        #endregion

        #region "util"
        public string findXML(string xdoc, string tag)
		{
			string svar = "";
			try
			{
				int bg = xdoc.IndexOf("<" + tag + ">");
				if (bg >= 0)
					bg += tag.Length + 2;
				int slut = xdoc.IndexOf("</", bg);
				svar = xdoc.Substring(bg, slut - bg);
			}
			catch { }
			return svar;
		}

		public string komma2Punktum(string s)
		{
			return s.Replace(",", ".");
		}

		internal string backslash(string s)
		{
			return s.Replace("\\", "' || E'\\\\' || '");
		}

        internal string anf(string s, bool dbklar)
        {
            s = s.Replace("'", "''");
            //s = s.Replace("\"", "\"\"");
            s = backslash(s);
            if (dbklar)
                s = "'" + s + "'";
            return s;
        }

		// til talfelter mv, hvor der ikke skal anførselstegn, men hvor der til gengæld skal stå NOGET
		internal string NullToZero(object s )
		{
			if (s == null || s.ToString() == "")
				s = "0";
			return s.ToString();
		}

        internal string rensSQL(string s)
        {
            string[] tt = s.Replace("\t", " ").Replace("\r", "").Split('\n');
            for (int i =0; i < tt.Length; i++)
            {
                int bg = tt[i].IndexOf("--");
                if (bg >= 0)
                {
                    tt[i] = tt[i].Substring(0, bg);
                }
            }
            string t = string.Join(" ", tt);
            do
            {
                t = t.Replace("  ", " ");
            } while (t.Contains("  "));
            return t;
        }

        public string DatoFuld(string ind, int LægTimeTil)
        {
            try
            {
                DateTime dt = DateTime.Parse(ind);
                if (LægTimeTil != 0)
                {
                    // -1 runder bare ned til hele time
                    if (LægTimeTil != -1) dt = dt.AddHours(LægTimeTil);
                    return dt.ToString("yyyy-MM-dd HH") + ":00:00";
                }
                else
                {
                    // hvis der er sat sekunder, skal der rundes op til helt minut
                    if (dt.Second > 0)
                        dt.AddMinutes(1);
                    return dt.ToString("yyyy-MM-dd HH:mm:00");
                    //					return dt.ToString("yyyy-MM-dd HH:mm:ss");
                }
            }
            catch
            {
                return "";
            }
        }

        // returnerer en kommasepareret liste med værdier til brug i yderligere queries, fx til geoserver
        public string sqlListe(string sql, string felt)
        {
            string svar = "";

            DataBase db = getDB(sql);
            db.Query(true);
            string komma = "";

            if (db.Result != null)
            {
                foreach (DataRow item in db.Result.Rows)
                {
                    svar += komma + item[felt].ToString();
                    komma = ",";
                }
            }
            return svar;
        }


		#endregion


		#region "opslag"

		// navn fra brugerID
		internal string Skjul(string s)
		{
			return db_enkeltopslag("select tekst from o_skjul where id= " + s, "", "tekst", s);
		}

		internal string Status(string s)
		{
			int t = int.Parse(s);
			if (t < 10 || t>=90)
				return db_enkeltopslag("select tekst from o_status where id = " + s, "", "tekst", s);
			else
				return db_enkeltopslag("select navn from brugere.firma where id = " + s, "", "navn", s);
		}


		// navn fra brugerID
		internal string BrugerNavn(string s)
		{
			return db_enkeltopslag("select navn from brugere.brugere where id=" + s,"", "navn", s);
		}

		// firmanavn fra firma_id
		internal string Firma(string s)
		{
			return db_enkeltopslag("select navn from brugere.firma where id=" + s, "", "navn", s);
		}
		// firmanavn fra brugerID
		internal string BrugerFirma(string s)
		{
			return db_enkeltopslag("select b.navn from brugere.brugere a inner join brugere.firma b on a.firmaid=b.id where a.id=" + s, "", "navn", s);
		}

		// opslag i ail_opslag vha gruppe


		#endregion

		public int gemKort(string sql, string tbl, string idfelt)
        {
            int antal = 0;
            string q = "";
            if (sql.Contains("string_agg(") )
            {
                DataBase d = db_query(sql, tbl, "0");
                if (d.Result !=null && d.Result.Rows.Count > 0)
                {
                    antal = int.Parse(d.Result.Rows[0]["antal"].ToString());
                    if (antal == 0)
                        q = "aid = 0";
                    else
                        q = "aid in (" + d.Result.Rows[0]["ids"].ToString() + ")";
                }
                else
                    q = "aid = 0";
            }
            else
            {
                antal = int.Parse(db_enkeltopslag(sql, tbl, "antal", "0"));
                q = "nej";

            }
            //if (d.Result.Rows.Count > 0)
            //{
            //    //antal = d.Result.Rows.Count;
            //    //foreach (DataRow r in d.Result.Rows)
            //    //{
            //    //    q += "," + r[idfelt].ToString();
            //    //}
            //    //q = "aid in (" + q.Substring(1) + ")";
            //}
            //else
            //    q = "aid = 0";
            db_exec("Update log_stat set kort_filter='" + q + "' where cookie ='" + svin_cookie + "'", tbl, "0");

            return antal;
        }

        public int retKort(string sql, string tbl)
        {
            int antal = 0;
            DataBase d = db_query(sql, tbl, "0");
            string q = db_enkeltopslag("select kort_filter from log_stat where cookie ='" + svin_cookie + "'", tbl, "kort_filter","0");
            if (q == "0" || q == "aid = 0")
            {
                q = "";
            }
            else
            { 
                // slet aid in( og )
                q = q.Substring(8);
                q = q.Remove(q.Length - 1);
            }
            if (d.Result.Rows.Count > 0)
            {
                antal = d.Result.Rows.Count;
                foreach (DataRow r in d.Result.Rows)
                {
                    q += "," + r["id"].ToString();
                }
                
                string q2 = "aid in (" + q.Substring(1) + ")";
                db_exec("Update log_stat set kort_filter = case when kort_filter is null or kort_filter ='' or kort_filter ='aid=0' then '" + q2 + "' else replace(kort_filter, ')', '" + q + ")') end where cookie ='" + svin_cookie + "'", tbl, "0");
            }

            return antal;
        }
        
        #region "email"
        public string findBruger(string bruger)
        {
            string svar = db_enkeltopslag("select navn from brugere.brugere where id=" + bruger, "brugere", "navn", bruger);
            return svar;
        }

        public void TilføjUnik(ref List<string> a, string b)
		{
			bool fundet = false;
			foreach (string em in a)
			{
				if (b == em)
				{
					fundet = true;
					break;
				}
			}
			if (fundet == false)
				a.Add(b);
		}

		//public void sendEmailHeader(List<string> modtagere, string besked, string header)
		//{
		//	sendEmailHeader(modtagere, besked, header, "");
		//}

		//public void sendEmailHeader(List<string> modtagere, string besked, string header, string attachment)
		//{
		//	// tilføj resten af beskeden
		//	besked += "\r\n\r\nMed venlig hilsen\r\nDet Centrale BigårdsRegister\r\n\r\nBemærk: Denne mail er automatisk genereret og kan ikke besvares.";

		//	// slå navn og email op på de bruger-ider der ligger i modtagere
		//	string ids = string.Join(",", modtagere.ToArray());
		//	string union = "";
			
		//	string sql = "select navn, email from brugere_aktive where grundid in(" + ids + ")" + union;
		//	DataBase db = db_query(sql, "brugere", "0");
		//	if (db.Result == null || db.Result.Rows.Count == 0)
		//	{
		//		return;
		//	}
		//	// hvis der skal sendes mail til djfmail, dukker de ikke op i databasen, men kan forhåbentlig tilføjes her

		//	switch (System.Configuration.ConfigurationManager.AppSettings["versionstekst"])
		//	{
		//		// slå afsendelsen af mails fra, når programmet kører i IDE 
		//		case "LOKAL": // localhost:3651 - kun debug
		//			//Debug.Print("Modtagere:");
		//			//foreach (DataRow row in db.Result.Rows) 
		//			//{
		//			//  Debug.Print(row["navn"].ToString() + "  -  " + row["email"].ToString());
		//			//}
		//			//Debug.Print("Header: " + header);
		//			//Debug.Print("Besked: " + besked);
		//			//Debug.Print("Attachment: " + attachment);
		//			//Debug.Print("");
					
		//				// skriv alle mails i en fil, så de er lette at kontrollere
		//				StreamWriter wr; 
		//				// Open the file directly using StreamWriter. 
		//				try 
		//				{
		//					wr = new StreamWriter(ConfigurationManager.AppSettings["maillog"], true);
		//				} 
		//				catch //(IOException exc)
		//				{ 
		//						// throw new Exception("Der er sket en fejl i skrivLog:\r\n" + exc.Message);
		//						return;
		//				}
		//				wr.WriteLine(DateTime.Now.ToString() + "\t" + header);
		//				wr.WriteLine("Modtagere:");
		//				foreach (DataRow row in db.Result.Rows)
		//				{
		//					wr.WriteLine("\t" + row["navn"].ToString() + "  -  " + row["email"].ToString());
		//				}

		//				wr.WriteLine("Besked: " + besked);
		//				wr.WriteLine("Attachment: " + attachment);
		//				wr.WriteLine("");
		//				wr.WriteLine("");
		//				wr.WriteLine("");
		//				wr.Close(); 
		//			break;

		//		case "UDVIKLING": // PDCBRUDV - mails til Ghassan og mig
		//			// testopsætning
		//			string modebug = "";
		//			foreach (DataRow row in db.Result.Rows)
		//			{
		//				modebug += "\r\n" + row["navn"].ToString() + "  -  " + row["email"].ToString();
		//			}
		//			Email email = new Email();
		//			email.SendMailHTML("cbr@pdir.dk", "nfr@le34.dk", "Besked fra CBR: " + header, "Kære Nikolaj Frandsen\r\nDenne mail skal i virkeligheden sendes til følgende adresser:\r\n" + modebug + "\r\n\r\n... og have følgende indhold:\r\n" + besked, attachment); //, attachments);
		//			email.SendMailHTML("cbr@pdir.dk", "ghch@pdir.dk", "Besked fra CBR: " + header, "Kære Ghassan Hassib Christensen\r\nDenne mail skal i virkeligheden sendes til følgende adresser:\r\n" + modebug + "\r\n\r\n... og have følgende indhold:\r\n" + besked, attachment); //, attachments);
		//			break;
		//		case "TEST": // testserver - Er der nogen forskel på testserveren og produktionsserveren ??? 
		//		case "": // produktionsserver
		//			foreach (DataRow row in db.Result.Rows)
		//			{
		//				// undertryk mail til udenfor@systemet.dk
		//				if (row["email"].ToString() != "udenfor@systemet.dk" && row["email"].ToString() != "ugyldig@ugyldig.dk" && row["email"].ToString() != null && row["email"].ToString() != "")
		//				{
		//					Email emailprod = new Email();
		//					emailprod.SendMailHTML("cbr@pdir.dk", row["email"].ToString(), System.Configuration.ConfigurationManager.AppSettings["versionstekst"] + "Besked fra CBR: " + header, "Kære " + row["navn"].ToString() + "\r\n" + besked, attachment); //, attachments);
		//				}
		//			}
		//			break;
		//	}
		//}

		//public void sendEmailHeaderEnkelt(string modtager, string besked, string header)
		//{
		//	List<string> svar = new List<string>();
		//	svar.Add(modtager);
		//	sendEmailHeader(svar, besked, header);
  //      }
        #endregion

		#region andre rapporter
		// output Excel
		internal void skrivExcel(ref DataBase db, string filnavn)
		{
			// skriv data i Excel fil vha oledb
			OleDbConnection olecon = new OleDbConnection();
			OleDbCommand olecmd = new OleDbCommand();

			olecon.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filnavn + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;\"";
			olecon.Open();

			olecmd.Connection = olecon;

			// definer tabellen
			string kols = "";
			string komb = "CREATE TABLE Ark1 (";
			string sql = "";
			//string dblkols = "";
			bool skrivkol = false;
			foreach (DataColumn c in db.Result.Columns)
			{
				skrivkol = true;
				//Debug.Print(c.ColumnName + "  -  " + c.DataType.ToString());
				switch (c.DataType.ToString())
				{
					//case "System.Boolean":
					//	kols += komb + c.ColumnName + " bool";
					//	break;
					case "System.Int32":
						kols += komb + c.ColumnName + " int";
						break;
					case "System.Double":
						kols += komb + c.ColumnName + " VARCHAR";
						//						kols += komb + c.ColumnName + " DOUBLE";
						//		dblkols += "," + c.ColumnName + " DOUBLE";
						break;
					case "System.String":
						//case "System.DateTime":
						kols += komb + c.ColumnName + " MEMO"; // VARCHAR";
						break;
					case "System.DateTime":
						kols += komb + c.ColumnName + " DATETIME";
						break;
					default:
						skrivkol = false;
						Debug.Print(c.ColumnName + "  -  " + c.DataType.ToString());
						break;
				}
				if (skrivkol)
					sql += ", " + c.ColumnName;
				komb = ", ";
			}
			olecmd.CommandText = kols + ")";
			Debug.Print("Opret kolonner: " + olecmd.CommandText);
			olecmd.ExecuteNonQuery();

			sql = "INSERT INTO Ark1 (" + sql.Substring(2) + ") values (";



			// skriv data
			string val = "";
			try
			{
				foreach (DataRow r in db.Result.Rows)
				{
					olecmd.Parameters.Clear();
					val = "";
					foreach (DataColumn c in db.Result.Columns)
					{
						string v = r[c.ColumnName].ToString();
						if (v == "")
							val += ", NULL";
						else
						{
							switch (c.DataType.ToString())
							{
								case "System.Int32":
									val += ", " + v;
									break;
								//								case "System.Double":
								//	val += ", " + v;
								//	break;
								case "System.Double":
									val += ", " + anf(v.Replace(".", ","), true);
									break;
								case "System.String":
									val += ", " + anf(v,true) ;
									break;
								case "System.DateTime":
									val += ", @" + c.ColumnName;
									//OleDbParameter p = new OleDbParameter()
									olecmd.Parameters.AddWithValue("@" + c.ColumnName, r.Field<DateTime>(c.ColumnName));
									break;
								default:
									Debug.Print(c.ColumnName + "  -  " + c.DataType.ToString());
									break;
							}
						}
						//val += ", '" + r[c.ColumnName].ToString() + "'";
					}
					olecmd.CommandText = sql + val.Substring(2) + ")";
					Debug.Print(olecmd.CommandText);
					olecmd.ExecuteNonQuery();
				}
				//if (dblkols !="") // ret kommatal fra tekst til tal
				//{

				//}
				olecon.Close();
			}
			catch (Exception e)
			{
				Debug.Print(e.Message);
			}

		}

        public void ExcelRapportLevende(string gridfilter, string filnavn)
        {
            string sql = @"select 
				i.id, 
				c.navn as fv_region,
				b.navn as enhed, 
				i.setx, 
				i.sety, 
				i.set_tid, 
                f.tekst as fundtype, 
                --d.tekst as status, 
                e.tekst as skjul,
                i.fundsted,
                i.an_navn,
                i.setpostnr,
                i.an_vejnavn || ' ' || i.an_husnr || ', ' || i.an_postnr   || ' ' || i.an_by as an_adresse,
                i.setvejnavn || ' ' || i.sethusnr || ', ' || i.setpostnr   || ' ' || i.setby as set_adresse,
                i.an_telefon,
				i.an_email,
                i.billede,
				i.setbeskriv,
				i.antal,
                case when i.reserveret_af_id =0 then 'Nej' else 'Ja' end as noteret,
				i.kommentar as indberetning_kommentar
            from 
                vildsvin.indberetninger i
                --left join brugere.brugere a on i.visitator = a.id
                left join brugere.firma b on i.status = b.id
                left join brugere.firma c on i.fv_region = c.id
                --left join vildsvin.o_status d on i.status = d.id
                left join vildsvin.o_skjul e on i.skjul = e.id
                left join vildsvin.o_fundtype f on i.fundtype = f.id
                " + gridfilter + " order by 1";
            DataBase db = db_query(sql, "indberetninger", "0");

            skrivExcel(ref db, filnavn);
        }
        public void ExcelRapportIndsaml(string gridfilter, string filnavn)
        {
            string sql = @"select f.id, 
				f.postnr, 
    			a.tekst as fundtype, 
		    	c.navn as afd,
	    		d.tekst as status, 
				f.indberetning, 
                f.x,
                f.y,
				f.kommentar as vildsvin_kommentar,
				i.setx, 
				i.sety, 
                e.tekst as skjul,
                i.set_tid,
                i.fundsted,
				f.tid_indsamlet, 
                i.an_navn,
                i.an_vejnavn || ' ' || i.an_husnr || ', ' || i.an_postnr   || ' ' || i.an_by as an_adresse,
                i.setvejnavn || ' ' || i.sethusnr || ', ' || i.setpostnr   || ' ' || i.setby as set_adresse,
                i.an_telefon,
                i.an_email,
				i.kommentar as indberetning_kommentar,
                i.billede,
                b.navn as opsamler     
            from 
                vildsvin f 
                left join vildsvin.indberetninger i on f.indberetning=i.id 
                left join vildsvin.o_fundtype a on i.fundtype = a.id
                left join brugere.firma c on i.afd = c.id
                left join vildsvin.o_status d on f.status = d.id
                left join vildsvin.o_skjul e on i.skjul = e.id
                left join brugere.brugere b on i.reserveret_af_id=b.id 
                " + gridfilter + " order by 1";
            DataBase db = db_query(sql, "vildsvin", "0");

            skrivExcel(ref db, filnavn);
        }

        public void ExcelRapportFind(string value, string felt, string filnavn)
        {
            string filterkrit = " where ";
            switch (felt)
            {
                case "indberetning":
                    filterkrit += "a.id=" + value;
                    break;
                case "vildsvin_id":
                    filterkrit += "b.id=" + value;
                    break;
                case "journalnr_1":
                    filterkrit += "b.journalnr_1 = '" + value + "'";
                    break;
                case "journalnr_2":
                    filterkrit += "b.journalnr_2 = '" + value + "'";
                    break;
            }

            string sql = @"select 
        		a.id as indberetning, 
        		b.id as vildsvin_id, 
                b.journalnr_2 as fvst_journalnr,
                b.journalnr_1 as vet_journalnr
                  from 
                   vildsvin.indberetninger a 
                   left join vildsvin.vildsvin b on b.indberetning=a.id" 
                    + filterkrit + " order by 1,2";
            DataBase db = db_query(sql, "vildsvin", "0");

            skrivExcel(ref db, filnavn);
        }

        public void ExcelRapportBrugere(string gridfilter, string filnavn)
        {
            string sql = @"select b.id, 
        		b.navn, 
        		b.email, 
        		case when b.aktiv = true then 'Ja' else 'Nej' end as aktiv, 
        		b.firmaid, 
        		f.navn as firmanavn, 
        		case when f.aktiv = true then 'Ja' else 'Nej' end as firma_aktiv 
        		from brugere.brugere b, brugere.firma f, brugere.firmatype t where f.firmatype=t.id and t.vildsvin=true and b.firmaid=f.id " + gridfilter;
            DataBase db = db_query(sql, "brugere", "0");

            skrivExcel(ref db, filnavn);
        }


        public void ExcelRapportOff(string aar, string filnavn)
        {
            // denne funktion benytter kortfilterets liste med id'er
            string filter = db_enkeltopslag("select kort_filter from vildsvin.log_stat where cookie ='" + svin_cookie + "'", "", "kort_filter ", "0");
            filter = filter.Substring(8);
            filter = filter.Substring(0, filter.Length - 1);

            string[] ff = filter.Split(',');
            string krit_1 = "";
            string krit_2 = "";
            foreach (string f in ff)
                {
                if (f.StartsWith("-"))
                    krit_2 += "," + f.Substring(1);
                else
                    krit_1 += "," + f;
            }
            string sql = "";
            // vi skal have både levende, spor, ilanddrevne direkte fra indberetninger og døde/nedlagte fra vildsvin --- det kan ikke gøres ordentligt uden union
            sql = @"select 
                a.id, 
                a.indberetning, 
                b.tekst as fundtype, 
                c.navn as afd,
                a.journalnr_1, 
                a.journalnr_2, 
                a.kommune, 
                i.set_tid, 
                a.x, 
                a.y, 
                case when a.status = 5 then 'Test ikke afsluttet' else case when a.pos = 1 then a.resultat_tekst else 'Negativ' end end as resultat, 
                a.pos,
                a.tid_lab, 
                a.tid_slut, 
                i.setvejnavn || ' ' || i.sethusnr || ', ' || i.setpostnr || ' ' || i.setby as set_adresse, 
                i.fundsted, 
                g.tekst as asf,
                h.tekst as csf,
                j.tekst as aujeszkys,
                k.tekst as trikiner,
                i.billede,
				i.kommentar as indberetning_kommentar,
				a.kommentar as vildsvin_kommentar
            from 
	            vildsvin.vildsvin a 
	            left join vildsvin.indberetninger i on a.indberetning=i.id
	            left join vildsvin.o_fundtype b on a.fundtype = b.id
	            left join brugere.firma c on a.status = c.id
	            left join vildsvin.o_resultat g on a.asf= g.id
	            left join vildsvin.o_resultat h on a.csf= h.id
	            left join vildsvin.o_resultat j on a.aujeszkys= j.id
	            left join vildsvin.o_resultat k on a.trikiner= k.id 
 			    where a.id in (" + krit_1.Substring(1) + ")";
            sql += @" union select 
                a.id * -1, 
                a.id, 
                b.tekst as fundtype, 
                c.navn as afd,
                '' as journalnr_1, 
                '' as journalnr_2, 
                '' as kommune, 
                a.set_tid, 
                a.setx, 
                a.sety, 
                '' as resultat, 
                0 as pos,
                null as tid_lab, 
                null as tid_slut, 
                a.setvejnavn || ' ' || a.sethusnr || ', ' || a.setpostnr || ' ' || a.setby as set_adresse, 
                a.fundsted, 
                'Ikke relevant' as asf, 
                'Ikke relevant' as csf, 
                'Ikke relevant' as trikiner, 
                'Ikke relevant' as aujeszkys,
                a.billede,
				a.kommentar as indberetning_kommentar,
                '' as vildsvin_kommentar
            from 
                vildsvin.indberetninger a
	            left join vildsvin.o_fundtype b on a.fundtype = b.id
	            left join brugere.firma c on a.status = c.id
 			    where a.id in (" + krit_2.Substring(1) + ") order by 1";
            DataBase db = db_query(sql, "vildsvin", "0");

            skrivExcel(ref db, filnavn);
        }


        public void ExcelRapportLab(string gridfilter, string filnavn)
        {
            string sqlstr = @"select f.id, 
				f.journalnr_1, 
				f.journalnr_2, 
				f.resultat_tekst, 
				b.tekst as fundtype, 
				d.tekst as status, 
				c.navn as afd, 
				f.indberetning, 
                f.x,
                f.y,
                e.tekst as skjul,
				f.tid_indsamlet,
				f.tid_lab,
				f.tid_lab_1,
				f.tid_lab_2,
				f.tid_slut,
				f.tid_slut_1,
				f.tid_slut_2,
                g.tekst as asf,
                h.tekst as csf,
                j.tekst as aujeszkys,
                k.tekst as trikiner,
				f.kommentar as vildsvin_kommentar
            from 
	            vildsvin.vildsvin f 
	            left join vildsvin.indberetninger i on f.indberetning=i.id
	            left join vildsvin.o_fundtype b on f.fundtype = b.id
	            left join brugere.firma c on f.afd = c.id
	            left join vildsvin.o_status d on f.status = d.id
	            left join vildsvin.o_skjul e on f.skjul = e.id
	            left join vildsvin.o_resultat g on f.asf= g.id
	            left join vildsvin.o_resultat h on f.csf= h.id
	            left join vildsvin.o_resultat j on f.aujeszkys= j.id
	            left join vildsvin.o_resultat k on f.trikiner= k.id" + gridfilter + " order by 1";
            DataBase db = db_query(sqlstr, "vildsvin", "0");

            skrivExcel(ref db, filnavn);
        }

        #endregion
    }

    public class BrugerInfo
    {
        public string brugernavn { get; set; }
        public string brugerid { get; set; }
        public string firma { get; set; }
        public string firmatype { get; set; }
		public string firmanavn { get; set; }
		public bool epidemi { get; set; }
		public BrugerInfo()
        {
            brugernavn ="";
            brugerid ="0";
            firma ="0";
            firmatype = "0";
		    firmanavn = "0";
            epidemi = false;
		}
    }
}
