﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Services;

using System.Data;
using System.Web.Script.Serialization;

namespace Vildsvin
{
    /// <summary>
    /// Summary description for LE34AddressSearchWS
    /// </summary>
    /// 
    [WebService(Namespace = "http://tempuri.org/2")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class LE34AddressSearchWS : System.Web.Services.WebService
    {
        //private string login = System.Configuration.ConfigurationManager.AppSettings["KMSUserName"];
        //private string password = System.Configuration.ConfigurationManager.AppSettings["KMSPassword"];
        //private KMSServiceGeokeys_v4.IGeoKeysFacadeService kmsGeoWS = null;

        public LE34AddressSearchWS()
        {

            //Uncomment the following line if using designed components 
            //InitializeComponent(); 

        }

        public class Address
        {
            public int id { get; set; }
            public string name { get; set; }
            //public string munid { get; set; }
            public string x { get; set; }
            public string y { get; set; }

            public Address() { }

            public Address(int ID, string Name)
            {
                id = ID;
                name = Name;

                //munid = MunID;
            }
        }


        public class DataList<T>
        {
            public List<T> items;
            public int count;
            public DataList()
            {
                items = new List<T>();
            }
        }

        //    [WebMethod]
        //    [System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        //    public DataList<Address> getListOfAddresses(string userinput)
        //    {
        ////        LE34.CultureManager.Save();

        //        DataList<Address> list = new DataList<Address>();

        //        // Preprocess input.
        //        userinput = userinput.ToLower().Trim();

        //        // Extract all parameter keys.
        //        DataBase db = new DataBase("SELECT ogc_fid AS id, address_search AS name, st_x(geom) AS x, st_y(geom) AS y " +
        //                                   //"from postadr_backup WHERE lower(address_search) like '%" + userinput + "%' " +
        //                                   "from postadr_backup WHERE lower(address_search) like :input " +
        //                                   "ORDER BY address_search LIMIT 100",
        //                                   "Server=srv-gis34;Port=5432;Database=stevnskommune;User Id=postgres;Password=Zxcv1234");
        //        //db.Add("input", "%" + userinput + "%", NpgsqlTypes.NpgsqlDbType.Varchar);
        //        db.Add("input", userinput + "%", NpgsqlTypes.NpgsqlDbType.Varchar); //søg med rigtig begyndelse
        //        db.Query(true);

        //        foreach (DataRow item in db.Result.Rows)
        //        {
        //            Address address = new Address();
        //            address.id = (int)item["id"];
        //            address.name = item["name"].ToString();
        //            address.x = item["x"].ToString();
        //            address.y = item["y"].ToString();

        //            list.items.Add(address);

        //        }
        //        list.count = list.items.Count;

        //       // LE34.CultureManager.Restore();

        //        return list;
        //    }

        //[WebMethod]
        //[System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        //public string getKMSList(string userinput)
        //{
        //    //LE34.CultureManager.Save();
        //    WebRequest request = WebRequest.Create("http://visstedet.kms.dk/FindAddress2.aspx?input=" + userinput + "&pretty=true");

        //    request.Credentials = CredentialCache.DefaultCredentials;
        //    request.Method = "GET";
        //    request.ContentType = "application/json";        
        //    //// If the body of the incoming request contains data write it to the stream of the proxy-request
        //    //if (context.Request.ContentLength > 0)
        //    //{
        //    //    byte[] buffer = new byte[context.Request.InputStream.Length];
        //    //    context.Request.InputStream.Read(buffer, 0, buffer.Length);
        //    //    Stream reqstr = request.GetRequestStream();
        //    //    reqstr.Write(buffer, 0, buffer.Length);
        //    //    reqstr.Close();
        //    //}

        //    // Get the response and content-type
        //    WebResponse response = request.GetResponse();

        //    StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream());
        //    StringBuilder result = new StringBuilder();
        //    string str = reader.ReadLine();
        //    while (str != null)
        //    {
        //        result.Append(str);
        //        str = reader.ReadLine();
        //    }

        //    //System.Runtime.Serialization.Json.DataContractJsonSerializer serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(KMSAddressList));
        //    //serializer.ReadObject(request.GetResponse().GetResponseStream());
        //    return result.ToString();
        //}

        //public class KMSAddressList
        //{
        //    public string streetName;
        //    public int buildingNumber;
        //    public string litra;
        //    public bool hasStreetBuildingIdentifier;
        //    public string streetBuildingIdentifier;
        //    public string districtSubdivisionIdentifier;
        //    public string postCodePrefix;
        //    public string postCodeIdentifier;
        //    public bool hasPostCode;
        //    public string districtName;
        //    public string displayName;
        //    public string crs;
        //    public double x;
        //    public double y;
        //    public double x1;
        //    public double y1;
        //    public double x2;
        //    public double y2;
        //    public bool isValidated;
        //    public string municipalityCode;
        //    public string streetCode;
        //}


        [WebMethod]
        //[System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public Vildsvin.AddressService.SearchResponse getLE34List(string query, int epsg, int filterID, int limit)
        //public string getLE34List(string query, int epsg, int filterID)
        {
            Vildsvin.AddressService.SearchResponse result = new Vildsvin.AddressService.SearchResponse();
            //string result = string.Empty;
            using (Vildsvin.AddressService.AddressServiceClient client = new Vildsvin.AddressService.AddressServiceClient())
            {
                Vildsvin.AddressService.SearchResponse response = client.Search(query, epsg, filterID, limit);

                //result = new JavaScriptSerializer().Serialize(response);
                result = response;
            }
            return result;
        }

		[WebMethod]
		//[System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
		public Vildsvin.AddressService.SearchResponse getHouseList(int mun, int street, int epsg, int filterID)
		{
			Vildsvin.AddressService.SearchResponse result = new Vildsvin.AddressService.SearchResponse();
			using (Vildsvin.AddressService.AddressServiceClient client = new Vildsvin.AddressService.AddressServiceClient())
			{
				Vildsvin.AddressService.SearchResponse response = client.SearchHouseNumbers(mun, street, epsg, filterID);
				result = response;
			}
			return result;
		}


        [WebMethod]
        //[System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
        public Vildsvin.AddressService.SearchResponseParsed getLE34ListParsed(string street, string housenumber, int zipcode, string district, int epsg, int filterID, int limit)
        //public string getLE34List(string query, int epsg, int filterID)
        {
            Vildsvin.AddressService.SearchResponseParsed result = new Vildsvin.AddressService.SearchResponseParsed();
            //string result = string.Empty;
            using (Vildsvin.AddressService.AddressServiceClient client = new Vildsvin.AddressService.AddressServiceClient())
            {
                Vildsvin.AddressService.SearchResponseParsed response = client.SearchParsed(street, housenumber, zipcode, district, epsg, filterID, limit);

                //result = new JavaScriptSerializer().Serialize(response);
                result = response;
            }
            return result;
        }

    }
}