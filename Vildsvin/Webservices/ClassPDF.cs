﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
//using System.Linq;
//using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;
//using bier.Webservices;
using System.Data;
using LE34.Data.Access;
using System.Configuration;
using System.Net;


namespace Vildsvin
{
	public class ClassPDF
	{
		svin_util ut;
        //globale variable alle pdf'er
        string FontMappe = "";
        string LogoMappe = "";

        BaseFont A;
		BaseFont AI;
		BaseFont AB;
		BaseFont ABI;
		BaseFont W;

        Font norm = null;
        Font h2 = null;
        Font tit = null;
        //PdfPCell cell;
        Document Doc = null;
        PdfContentByte CB = null;

        public ClassPDF(ref svin_util ut_instance)
		{
			ut = ut_instance;
            //globale variable alle pdf'er
            FontMappe = ut.ProgMappe + "fonts\\";
            LogoMappe = ut.ProgMappe + "images\\Logos\\";

            //globale variable alle pdf'er
            A = BaseFont.CreateFont(FontMappe + "arial.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED);
            AI = BaseFont.CreateFont(FontMappe + "ariali.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED);
            AB = BaseFont.CreateFont(FontMappe + "arialbd.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED);
            ABI = BaseFont.CreateFont(FontMappe + "arialbi.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED);
            W = BaseFont.CreateFont(FontMappe + "WINGDNG2.TTF", BaseFont.WINANSI, BaseFont.EMBEDDED);

            norm = new Font(A, 10,Font.NORMAL);
            h2 = new Font(A, 12, Font.BOLD);
            tit = new Font(A, 24, Font.BOLD);

        }





        public string lavKøreseddel(string ID, DataTable fugle, DataRow rk)
        {
            Document.Compress = true;
            Doc = new Document(PageSize.A4, 72, 57, 0, 18);

            string svar = "Køreseddel_" + ID + ".pdf";
            string FilNavn = ut.PDFMappe + svar; 

            PdfWriter WR = null;
            try
            {
                WR = PdfWriter.GetInstance(Doc, new FileStream(FilNavn, FileMode.Create));
                PageEventHelper pageEventHelper = new PageEventHelper("Indberetning " + ID, A, 10);
                WR.PageEvent = pageEventHelper;
            }
            catch //(Exception ex) 
            {

                return "0";
            }

            //tilføj metadata - NB: skal gøres inden Open
            Doc.AddTitle("Køreseddel " + ID);
            Doc.AddSubject("AI Beredskab");
            Doc.AddCreator("LE34");
            Doc.AddAuthor("Nikolaj Frandsen");
            Doc.AddHeader("Expires", "0");
            Doc.SetMargins(72, 72, 72, 72);
            Doc.Open();
            CB = WR.DirectContent;


            Paragraph p = new Paragraph("Indberetning " + ID, tit);
            p.Alignment = Element.ALIGN_RIGHT;
            Doc.Add(p);
            //CB.BeginText();
            //CB.SetFontAndSize(AB, 24);
            //CB.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, "Indberetning " + ID, 542, 750, 0);
            //CB.EndText();

            //logoer
            Image png = Image.GetInstance(LogoMappe + "mfvm_logo.png");
            png.SetAbsolutePosition(72, 720);
            png.ScaleAbsolute(219, 50);
            Doc.Add(png);


            // resten i tabeller - først tre kolonner til fuglene
            PdfPTable table = new PdfPTable(3);
            table.SpacingBefore = 40;
            //actual width of table in points
            table.TotalWidth = 470;
            //fix the absolute width of the table
            table.LockedWidth = true;
            //table.DefaultCell.Border = 0; // Rectangle.NO_BORDER;


            //relative col widths in proportions - 1/3 and 2/3
            float[] widths = new float[] { 50, 50, 370 };
            table.SetWidths(widths);
            table.HorizontalAlignment = 0;
            //leave a gap before and after the table
            //table.SpacingBefore = 20f;
            //table.SpacingAfter = 30f;

            tilføjRække(ref table, new string[] { "Indberetningens fugle" }, 3); // når der sættes colspan, går vi automatisk til større tekst

            tilføjRække(ref table, new string[] { "Indsaml", "Antal","Art" });

            List<string> fugleliste = new List<string>();
            foreach (DataRow r in fugle.Rows)
            {
                tilføjRække(ref table, new string[] { r["indsaml"].ToString(), r["antal"].ToString(), r["navn"].ToString() });
                if (r["indsaml"].ToString() == "Ja")
                {
                    string[] ids = r["ids"].ToString().Split(',');
                    foreach (string i in ids)
                        fugleliste.Add("Fugl nr " + i + "  -  " + r["navn"].ToString());
                }
            }
            Doc.Add(table);

            table = new PdfPTable(2);
            table.SpacingBefore = 20;
            table.TotalWidth = 470;
            table.LockedWidth = true;
            widths = new float[] { 130, 340};
            table.SetWidths(widths);
            table.HorizontalAlignment = 0;

            tilføjRække(ref table, new string[] { "Oplysninger om anmelder" }, 3); // når der sættes colspan, går vi automatisk til større tekst
            tilføjRække(ref table, new string[] { "Navn", rk["an_navn"].ToString() });
            tilføjRække(ref table, new string[] { "Adresse", rk["an_vejnavn"].ToString() + " " + rk["an_husnr"].ToString() + "\r\n" + rk["an_postnr"].ToString() + " " + rk["an_by"].ToString() });
            tilføjRække(ref table, new string[] { "Telefon", rk["an_telefon"].ToString() });
            tilføjRække(ref table, new string[] { "Email", rk["an_email"].ToString() });


            tilføjRække(ref table, new string[] { "Oplysninger om fundstedet" }, 3); // når der sættes colspan, går vi automatisk til større tekst
            tilføjRække(ref table, new string[] { "Adresse", rk["setvejnavn"].ToString() + " " + rk["sethusnr"].ToString() + "\r\n" + rk["setpostnr"].ToString() + " " + rk["setby"].ToString() });
            tilføjRække(ref table, new string[] { "Beskrivelse af fundet", rk["setbeskriv"].ToString() });
            tilføjRække(ref table, new string[] { "Beskrivelse af fundstedet", rk["fundsted"].ToString() });
            DateTime dt = DateTime.Parse(rk["set_tid"].ToString());
            tilføjRække(ref table, new string[] { "Set dato", dt.ToString("dd-MM-yyyy") });
            dt = DateTime.Parse(rk["visiteret_tid"].ToString());
            tilføjRække(ref table, new string[] { "Visiteret dato", dt.ToString("dd-MM-yyyy") });
            tilføjRække(ref table, new string[] { "Koordinater UTM zone 32", rk["setx"].ToString() + " " + rk["sety"].ToString() });
            if (rk.Field<double>("lon") > 12)
                tilføjRække(ref table, new string[] { "Koordinater UTM zone 33", rk["x_33"].ToString() + " " + rk["y_33"].ToString() });
            tilføjRække(ref table, new string[] { "Koordinater grader", rk["lat"].ToString() + " N " + rk["lon"].ToString() + " E" });
            Doc.Add(table);

            //// er der behov for et sideskift?
            //if (WR.GetVerticalPosition(false) < 400)
            //    Doc.NewPage();
            // sideskift
            Doc.NewPage();

            // kortene - først detalje
            Decimal minX = Decimal.Parse(rk["setx"].ToString()) - 235;
            Decimal maxX = Decimal.Parse(rk["setx"].ToString()) + 235;
            Decimal minY = Decimal.Parse(rk["sety"].ToString()) - 185;
            Decimal maxY = Decimal.Parse(rk["sety"].ToString()) + 185;
            string url = ConfigurationManager.AppSettings["KMSServerUrl"] + "?LAYERS=orto_foraar&SERVICENAME=orto_foraar" + // lag og servicename
                 "&REQUEST=GetMap&SRS=EPSG%3A25832&FORMAT=image%2Fjpeg&SERVICE=WMS&VERSION=1.1.1&EXCEPTIONS=application/vnd.ogc.se_xml&TILED=false&STYLES=" + // standardværdier
                 "&login=" + ConfigurationManager.AppSettings["KMSlogin"] + "&password=" + ConfigurationManager.AppSettings["KMSpassword"] + // login
                 "&BBOX=" + minX.ToString() + "," + minY.ToString() + "," + maxX.ToString() + "," + maxY.ToString() + "&WIDTH=1880&HEIGHT=1480"; // bbox og størrelse


            bool korttegnet = tegnKort(url, false, ref png, "© Kortforsyningen.dk");
            if (!korttegnet) // prøv OSM
            {
                url = "http://ows.terrestris.de/osm/service/?LAYERS=OSM-WMS&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&FORMAT=image%2Fjpeg&SRS=EPSG%3A25832" +
                     "&BBOX=" + minX.ToString() + "," + minY.ToString() + "," + maxX.ToString() + "," + maxY.ToString() + "&WIDTH=1880&HEIGHT=1480";
                korttegnet = tegnKort(url, false, ref png, "© OpenStreetMap.org / terrestris.de");
            }

            // og så oversigtskortet
            minX = Decimal.Parse(rk["setx"].ToString()) - 2350;
            maxX = Decimal.Parse(rk["setx"].ToString()) + 2350;
            minY = Decimal.Parse(rk["sety"].ToString()) - 1850;
            maxY = Decimal.Parse(rk["sety"].ToString()) + 1850;
            //url = ConfigurationManager.AppSettings["KMSServerUrl"] + "?SERVICE=wms&FORMAT=image%2Fjpeg&SRS=EPSG%3A25832&REQUEST=GetMap&VERSION=1.1.1&SERVICENAME=topo_skaermkort&LAYERS=dtk_skaermkort&TILED=false&STYLES=&BBOX=" + minX.ToString() + "," + minY.ToString() + "," + maxX.ToString() + "," + maxY.ToString() +
            //    "&WIDTH=1880&HEIGHT=1480&EXCEPTIONS=application/vnd.ogc.se_xml&login=" + ConfigurationManager.AppSettings["KMSlogin"] + "&password=" + ConfigurationManager.AppSettings["KMSpassword"];
            url = ConfigurationManager.AppSettings["KMSServerUrl"] + "?LAYERS=dtk_skaermkort&SERVICENAME=topo_skaermkort" + // lag og servicename
                "&REQUEST=GetMap&SRS=EPSG%3A25832&FORMAT=image%2Fjpeg&SERVICE=WMS&VERSION=1.1.1&EXCEPTIONS=application/vnd.ogc.se_xml&TILED=false&STYLES=" + // standardværdier
                "&login=" + ConfigurationManager.AppSettings["KMSlogin"] + "&password=" + ConfigurationManager.AppSettings["KMSpassword"] + // login
                "&BBOX=" + minX.ToString() + "," + minY.ToString() + "," + maxX.ToString() + "," + maxY.ToString() + "&WIDTH=1880&HEIGHT=1480"; // bbox og størrelse
            korttegnet = tegnKort(url, true, ref png, "© Kortforsyningen.dk");
            if (!korttegnet) // prøv OSM
            {
                url = "http://ows.terrestris.de/osm/service/?LAYERS=OSM-WMS&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&FORMAT=image%2Fjpeg&SRS=EPSG%3A25832" +
                     "&BBOX=" + minX.ToString() + "," + minY.ToString() + "," + maxX.ToString() + "," + maxY.ToString() + "&WIDTH=1880&HEIGHT=1480";
                korttegnet = tegnKort(url, true, ref png, "© OpenStreetMap.org / terrestris.de");
            }


            // sideskift
            Doc.NewPage();


            // plads til labels
            //PdfContentByte CB = WR.DirectContent;
            for (int i =0; i < fugleliste.Count; i++)
            {
                // DTU skal kun have en pr side
                // og siderne skrives to gange, en til DTU og en kopi til indsamlerne
                for (int j = 0; j < 2; j++)
                {

                    Doc.NewPage();

                    if (j==1)
                    {
                        CB.BeginText();
                        CB.SetFontAndSize(AB, 20);
                        CB.SetColorFill(new BaseColor(System.Drawing.Color.Gray));
                        CB.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "KOPI TIL INDSAMLER", 262, 805, 0);
                        CB.EndText();

                    }

                    CB.BeginText();
                    CB.SetFontAndSize(A, 14);
                    CB.SetColorFill(new BaseColor(System.Drawing.Color.Black));
                    CB.ShowTextAligned(PdfContentByte.ALIGN_LEFT, fugleliste[i], 72, 780, 0);
                    CB.EndText();

                    CB.Rectangle(72, 780 - 15, 380, -250); // x,y,bredde, højde
                    CB.Stroke();

                    // tekst indeni labelpladsen
                    CB.BeginText();
                    CB.SetFontAndSize(AB, 20);
                    CB.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "Indsæt label her", 262, 780 - 50, 0);
                    CB.EndText();

                    CB.BeginText();
                    CB.SetFontAndSize(AI, 10);
                    CB.ShowTextAligned(PdfContentByte.ALIGN_CENTER, "eller angiv grunden til at fuglen ikke opsamles", 262, 780 - 90, 0);
                    CB.EndText();

                    skrivKryds(ref CB, 780 - 130, 90, false);
                    CB.BeginText();
                    CB.SetFontAndSize(A, 12);
                    CB.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Kasseret", 110, 780 - 130, 0);
                    CB.EndText();

                    skrivKryds(ref CB, 780 - 160, 90, false);
                    CB.BeginText();
                    CB.SetFontAndSize(A, 12);
                    CB.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Ikke fundet", 110, 780 - 160, 0);
                    CB.EndText();

                    skrivKryds(ref CB, 780 - 190, 90, false);
                    CB.BeginText();
                    CB.SetFontAndSize(A, 12);
                    CB.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Anden grund ........................................................................", 110, 780 - 190, 0);
                    CB.EndText();



                    // bemærkninger under label-firkanten
                    CB.BeginText();
                    CB.SetFontAndSize(A, 12);
                    CB.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Bemærkninger", 90, 780 - 290, 0);
                    CB.EndText();

                    // DTU skal have oplysninger om fundsted og -dato

                    table = new PdfPTable(2);
                    //table.SpacingBefore = 320;
                    table.TotalWidth = 470;
                    table.LockedWidth = true;
                    widths = new float[] { 130, 340 };
                    table.SetWidths(widths);
                    table.HorizontalAlignment = 0;

                    PdfPCell cell = new PdfPCell(new Phrase("Oplysninger om fundstedet", h2));
                    cell.PaddingTop = 420;
                    cell.PaddingBottom = 5;
                    cell.Colspan = 2;
                    cell.Border = 0;
                    table.AddCell(cell);

                    //tilføjRække(ref table, new string[] { "Oplysninger om fundstedet" }, 3); // når der sættes colspan, går vi automatisk til større tekst
                    tilføjRække(ref table, new string[] { "Adresse", rk["setvejnavn"].ToString() + " " + rk["sethusnr"].ToString() + "\r\n" + rk["setpostnr"].ToString() + " " + rk["setby"].ToString() });
                    tilføjRække(ref table, new string[] { "Beskrivelse af fundet", rk["setbeskriv"].ToString() });
                    tilføjRække(ref table, new string[] { "Beskrivelse af fundstedet", rk["fundsted"].ToString() });
                    dt = DateTime.Parse(rk["set_tid"].ToString());
                    tilføjRække(ref table, new string[] { "Set dato", dt.ToString("dd-MM-yyyy") });
                    dt = DateTime.Parse(rk["visiteret_tid"].ToString());
                    tilføjRække(ref table, new string[] { "Visiteret dato", dt.ToString("dd-MM-yyyy") });
                    tilføjRække(ref table, new string[] { "Koordinater UTM zone 32", rk["setx"].ToString() + " " + rk["sety"].ToString() });
                    if (rk.Field<double>("lon") > 12)
                        tilføjRække(ref table, new string[] { "Koordinater UTM zone 33", rk["x_33"].ToString() + " " + rk["y_33"].ToString() });
                    tilføjRække(ref table, new string[] { "Koordinater grader", rk["lat"].ToString() + " N " + rk["lon"].ToString() + " E" });
                    //table.WriteSelectedRows(0, -1, 200, 50, CB);
                    //Paragraph pg = new Paragraph();
                    //pg.SpacingBefore = 320;
                    //pg.Add(table);
                    //Doc.Add(pg);
                    Doc.Add(table);

                }
            }
            // de særlige felter til BRS
            if (ut._epidemi)
            {
                Doc.NewPage();
                table = new PdfPTable(2);
                table.TotalWidth = 470;
                //fix the absolute width of the table
                table.LockedWidth = true;
                //relative col widths in proportions - 1/3 and 2/3
                widths = new float[] { 230, 240 };
                table.SetWidths(widths);
                table.HorizontalAlignment = 0;
                tilføjRække(ref table, new string[] { "Opsamlingshold" }, 2);
                tilføjStipletRække(ref table, "Køretøj");
                tilføjStipletRække(ref table, "Leder");
                tilføjStipletRække(ref table, "Hjælper");
                tilføjStipletRække(ref table, "Dato");
                tilføjRække(ref table, new string[] { "Tidspunkter" }, 2);
                tilføjStipletRække(ref table, "Afgang fra beredskabscenter / forrige opgave");
                tilføjStipletRække(ref table, "Ankomst til findested");
                tilføjStipletRække(ref table, "Fri. Opgaven afsluttet og klar til næste opgave");
                tilføjStipletRække(ref table, "Klar igen. Hjemme og retableret");
                Doc.Add(table);

                table = new PdfPTable(2);
                table.SpacingBefore = 30;
                table.TotalWidth = 470;
                //fix the absolute width of the table
                table.LockedWidth = true;
                //relative col widths in proportions - 1/3 and 2/3
                widths = new float[] { 170, 300 };
                table.SetWidths(widths);
                table.HorizontalAlignment = 0;
                tilføjRække(ref table, new string[] { "Nyttige telefonnumre" }, 2);
                tilføjRække(ref table, new string[] { "Fødevarestyrelsen", "72 27 69 00" });
                tilføjRække(ref table, new string[] { "Veterinærenhed NORD", "72 27 50 95" });
                tilføjRække(ref table, new string[] { "Veterinærenhed SYD", "72 27 57 70" });
                tilføjRække(ref table, new string[] { "Veterinærenhed ØST", "72 27 62 30" });
                tilføjRække(ref table, new string[] { "DTU Veterinærinstituttet", "35 88 62 50" });
                Doc.Add(table);
            }


            Doc.Close();
            return svar;
        }
        private bool tegnKort(string url, bool oversigt, ref Image png, string copyright )
        {
            try
            {
                using (WebClient webClient = new WebClient())
                {
                    byte[] data = webClient.DownloadData(url);
                    using (MemoryStream mem = new MemoryStream(data))
                    {
                        System.Drawing.Image kort = System.Drawing.Image.FromStream(mem);
                        using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(kort)) // tegn rød prik
                        {
                            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                            if (oversigt)
                            {
                                System.Drawing.Pen pen = new System.Drawing.Pen(System.Drawing.Color.Red, 2);
                                g.DrawRectangle(pen, new System.Drawing.Rectangle(kort.Width / 2 - kort.Width / 20, kort.Height / 2 - kort.Height / 20, kort.Width / 10, kort.Height / 10));
                            }
                            else
                                g.FillEllipse(System.Drawing.Brushes.Red, new System.Drawing.RectangleF(kort.Width / 2 - 16, kort.Height / 2 - 16, 32, 32));
                        }
                        png = Image.GetInstance(kort, System.Drawing.Imaging.ImageFormat.Jpeg);
                        int copytop = 0;
                        if (oversigt)
                        {
                            png.SetAbsolutePosition(72, 462);
                            copytop = 455;
                        }
                        else
                        {
                            png.SetAbsolutePosition(72, 72);
                            copytop = 65;
                        }
                        png.ScaleToFit(470, 370);
                        Doc.Add(png);
                        CB.BeginText();
                        CB.SetFontAndSize(A, 6);
                        CB.ShowTextAligned(PdfContentByte.ALIGN_RIGHT, copyright, 542, copytop, 0);
                        CB.EndText();

                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void tilføjRække(ref PdfPTable table, string[] txt, int colspan =0)
		{
			
            if (colspan > 0)
            {
                PdfPCell cell = new PdfPCell(new Phrase(txt[0],h2)); 
                cell.PaddingTop = 20;
                cell.PaddingBottom = 5;
                cell.Colspan = colspan;
                cell.Border = 0;
                table.AddCell(cell);
            }
            else
            {
                for (int i = 0; i < txt.Length; i++)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(txt[i], norm)); 
                    cell.PaddingBottom = 5;
                    cell.Border = 0;
                    table.AddCell(cell);
                }
            }
		}

		private void tilføjStipletRække(ref PdfPTable table, string vkol)
		{
			iTextSharp.text.pdf.draw.DottedLineSeparator dotlinje = new iTextSharp.text.pdf.draw.DottedLineSeparator();
			dotlinje.LineWidth = 1;
			dotlinje.Gap = 2;
			dotlinje.Percentage = 98;
			dotlinje.Alignment = 0;
			//dotlinje.Offset = 12;

			PdfPCell cell = new PdfPCell(new Phrase(vkol,norm));
            cell.VerticalAlignment = Element.ALIGN_MIDDLE;
            cell.Border = 0;
            //cell.HorizontalAlignment = 0;
            table.AddCell(cell);
			cell = new PdfPCell();
			cell.AddElement(dotlinje);
            cell.Border = 0;
            cell.VerticalAlignment = Element.ALIGN_BOTTOM;
            cell.FixedHeight = 40;
			//cell.PaddingTop = 25;
			//cell.HorizontalAlignment = 1;
			table.AddCell(cell);
		}

	
		private void skrivKryds(ref PdfContentByte CB, int Lod, int venstre, bool kryds)
		{
				CB.BeginText();
				CB.SetFontAndSize(W, 10);
				if (kryds == true)
					CB.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "R", venstre, Lod, 0);
				else
					CB.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "£", venstre, Lod, 0);
				CB.EndText();
		}

		private string maxBred(string txt, int bredde)
		{
			float aktbred = AB.GetWidthPoint(txt,10);
			//int aktbred = AB.GetWidth(txt);
			bool afkortet = aktbred > bredde;
			if (afkortet)
			{
				do
				{
					txt = txt.Substring(0, txt.Length - 1);
					aktbred = aktbred = AB.GetWidthPoint(txt, 10);
				} while (aktbred > bredde);
				txt += " ...";
			}
			return txt;
		}

        string SaveImageFromURL(string url)
        {
            // Perform request.
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);

            // Extract the stream.
            Stream res = null;
            //try
            //{
            res = request.GetResponse().GetResponseStream();

            // save to file
            string filename = Guid.NewGuid().ToString("N").ToUpper() + ".jpg";

            using (var output = new FileStream(filename, FileMode.Create, FileAccess.Write))
            {
                var buffer = new byte[2048]; // read in chunks of 2KB
                int bytesRead;
                while ((bytesRead = res.Read(buffer, 0, buffer.Length)) > 0)
                {
                    output.Write(buffer, 0, bytesRead);
                }
            }

            return filename;
        }

    }

    public class PageEventHelper : PdfPageEventHelper
    {
        PdfContentByte cb;
        PdfTemplate template;
        string text;
        BaseFont bf;
        int size;
        public PageEventHelper(string _text, BaseFont _basefont, int _size)
        {
            text = _text;
            bf = _basefont;
            size = _size;
        }

        public override void OnOpenDocument(PdfWriter writer, Document document)
        {
            cb = writer.DirectContent;
            template = cb.CreateTemplate(50, 50);
        }

        public override void OnEndPage(PdfWriter writer, Document document)
        {
            base.OnEndPage(writer, document);

            int pageN = writer.PageNumber;
            string samlettext = text + " - side " + pageN.ToString() + " af ";
            float len = bf.GetWidthPoint(samlettext, size);

            iTextSharp.text.Rectangle pageSize = document.PageSize;

            cb.SetRGBColorFill(100, 100, 100);

            cb.BeginText();
            cb.SetFontAndSize(bf, size);
            cb.SetTextMatrix(document.LeftMargin, pageSize.GetBottom(document.BottomMargin) - 30);
            cb.ShowText(samlettext);

            cb.EndText();

            cb.AddTemplate(template, document.LeftMargin + len, pageSize.GetBottom(document.BottomMargin) - 30);
        }

        public override void OnCloseDocument(PdfWriter writer, Document document)
        {
            base.OnCloseDocument(writer, document);

            template.BeginText();
            template.SetFontAndSize(bf, size);
            template.SetTextMatrix(0, 0);
            template.ShowText("" + (writer.PageNumber));
            template.EndText();
        }
    }

}
