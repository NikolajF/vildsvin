﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Text;
using System.Net;
using System.IO;

namespace Vildsvin
{
	public class Email
	{

		public void Send(string fromAddress, List<string> toAddresses, string subject, string body)
		{
			SendEmail(fromAddress, toAddresses, subject, body);
		}

		public void Send(string fromAddress, List<string> toAddresses, string subject, string body, List<string> attachments)
		{
			SendEmail(fromAddress, toAddresses, subject, body, attachments);
		}

		public void Send(string fromAddress, string toAddress, string subject, string body)
		{
			List<string> toAddresses = new List<string>();
			toAddresses.Add(toAddress);

			SendEmail(fromAddress, toAddresses, subject, body);
		}

		public void Send(string fromAddress, string toAddress, string subject, string body, string attachment)
		{
			List<string> toAddresses = new List<string>();
			toAddresses.Add(toAddress);
			List<string> attachments = new List<string>();
			attachments.Add(attachment);

			SendEmail(fromAddress, toAddresses, subject, body, attachments);
		}

		// simpel metode, hvor der kun kan være en fra og til adresse - den benyttede
		public void SendMail(string fromAddress, string toAddress, string subject, string body)
		{
			// Build message.
			MailMessage message = new MailMessage();
			message.From = new MailAddress(fromAddress);
			message.To.Add(new MailAddress(toAddress));
			message.Subject = subject;
			message.Body = body;
			message.IsBodyHtml = true;
			message.BodyEncoding = Encoding.UTF8;

            // Create an smtp client and send the email.
            MakeClient(message);
        }

        // simpel metode, hvor der kun kan være en fra og til adresse - den benyttede
        public void SendMailHTML(string fromAddress, string toAddress, string subject, string body)
		{
			SendMailHTML(fromAddress, toAddress, subject, body, "");
		}
		public void SendMailHTML(string fromAddress, string toAddress, string subject, string body, string attachment)
		{
			// Build message.
			MailMessage message = new MailMessage();
			message.From = new MailAddress(fromAddress);
			message.To.Add(new MailAddress(toAddress));
            //message.Bcc.Add("nfr@le34.dk");
            message.Subject = subject;
            message.Body = body; // "<p style='font-family: verdana,arial,tahoma,helvetica; font-size: 10pt;'>" + body.Replace("\r\n", "<br>") + "</p>";
			message.IsBodyHtml = true;
			message.BodyEncoding = Encoding.UTF8;

			if (attachment != "")
			{
				message.Attachments.Add(new Attachment(attachment));
			}

            // Create an smtp client and send the email.
            MakeClient(message);
        }
        public void SendMailHTML(string fromAddress, string toAddress, string subject, string body, AlternateView view)
        {
            // Build message.
            MailMessage message = new MailMessage();
            message.From = new MailAddress(fromAddress);
            message.To.Add(new MailAddress(toAddress));
            message.Bcc.Add("nfr@le34.dk");
            message.Subject = subject;
            message.Body = body; // "<p style='font-family: verdana,arial,tahoma,helvetica; font-size: 10pt;'>" + body.Replace("\r\n", "<br>") + "</p>";
            message.IsBodyHtml = true;
            message.BodyEncoding = Encoding.UTF8;

            message.AlternateViews.Add(view);
            message.AlternateViews.Add(view);

            // Create an smtp client and send the email.
            MakeClient(message);
        }


        private void SendEmail(string fromAddress, List<string> toAddresses, string subject, string body)
		{
			// Build message.
			MailMessage message = new MailMessage();
			message.From = new MailAddress(fromAddress);
			foreach (string toAddress in toAddresses)
			{
				message.To.Add(new MailAddress(toAddress));
			}
			message.Subject = subject;
			message.Body = body;
			message.IsBodyHtml = false;
			message.BodyEncoding = Encoding.UTF8;

            // Create an smtp client and send the email.
            MakeClient(message);
        }

        private void SendEmail(string fromAddress, List<string> toAddresses, string subject, string body, List<string> attachments)
		{
			// Build message.
			MailMessage message = new MailMessage();
			message.From = new MailAddress(fromAddress);
			foreach (string toAddress in toAddresses)
			{
				message.To.Add(new MailAddress(toAddress));
			}
			message.Subject = subject;
			message.Body = body;
			message.IsBodyHtml = false;
			message.BodyEncoding = Encoding.UTF8;

			foreach (string attachment in attachments)
			{
				message.Attachments.Add(new Attachment(attachment));
			}
            // Create an smtp client and send the email.
            MakeClient(message);
        }

        private void MakeClient(MailMessage message)
        {
            if (!System.Diagnostics.Debugger.IsAttached)
            {
                try
                {
                    int port = 25;
                    var client = new SmtpClient("mail.le34.dk", port);
                    client.Credentials = new NetworkCredential("noreply", "Energi34Energi34", "le34.dk");
                    //return client;
                    // Send email.
                    client.Send(message);
                }
                catch (Exception ex)
                {
                    // write error to log file
                    using (StreamWriter w = new StreamWriter(System.Configuration.ConfigurationManager.AppSettings["beskyttet_upload"] + "email.log", true, Encoding.UTF8))
                    {
                        w.WriteLine(DateTime.Now.ToString("dd-MM yyyy h:i:s") + "\tFejl: " + ex.Message);
                        w.WriteLine("Modtagere:\t" + message.To.ToString());
                        w.WriteLine("Titel:\t" + message.Subject.ToString());
                        w.WriteLine();
                        w.WriteLine();
                    }

                    throw new Exception("Kunne ikke sende email: " + ex.Message);

                }
            }
        }
    }
}

