﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Configuration;
using System.Reflection;  // reflection namespace
using System.Data;

//using System.Web.UI;
//using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Text;
using System.Xml;

using LE34.Data.Access;
//using Vildsvin.Webservices;
using System.Drawing;


using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;

namespace Vildsvin
{
	/// <summary>
	/// Summary description for svin_login
	/// </summary>
	[WebService(Namespace = "http://tempuri.org/")]
	[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
	[System.ComponentModel.ToolboxItem(false)]
	// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
	[System.Web.Script.Services.ScriptService]

	public class svin_login : System.Web.Services.WebService
	{
		private svin_util ut;

		#region "Login"

		[WebMethod(EnableSession = true)]
		[System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)] //, XmlSerializeString = false
		public BrugerInfo Login(string email, string kendeord, string nykode)
		{
			ut = new svin_util(true);

			ut.tjekBruger("login", "email: " + email + "\n" + "kendeord: " + kendeord + "\n" + "nykode: " + nykode, email, kendeord);
			bool fejl = false;
			if (nykode != "")
			{
				// så skal det kontrolleres om den er kompleks nok, dvs minds 8 tegn, tal og både store og små bogstaver
				if (nykode.Length < 8)
					fejl = true;
				else
				{
					string talliste = "0123456789";
					string storliste = "ABCDEFGHIJKLMNOPQRSTUVWXYZÆØÅ";
					string småliste = "abcdefghijklmnopqrstuvwxyzæøå";
					bool tal = false;
					bool store = false;
					bool små = false;
					bool mlm = false;
					for (int i = 0; i < nykode.Length; i++)
					{
						string c = nykode.Substring(i, 1);
						if (talliste.Contains(c))
							tal = true;
						else if (storliste.Contains(c))
							store = true;
						else if (småliste.Contains(c))
							små = true;
						else if (c == "")
							mlm = true;

					}
					if (tal == false || store == false || små == false || mlm == true)
						fejl = true;

				}
				if (fejl)
				{
					throw new Exception("Den nye adgangskode opfylder ikke kravene til kompleksitet. Længden skal være mindst 8 tegn og koden skal indeholde både store og små bogstaver og tal, og må ikke indeholde mellemrum.");
				}
				else // gem den nye adgangskode
				{
					ut.db_exec("update brugere.brugere set kendeord= " + ut.anf(nykode, true) + " where id = " + ut.brugerinfo.brugerid, "brugere", ut.brugerinfo.brugerid);
				}

			}

			//lavEUrapport("2009", "2",ref ut, new DateTime(2009,1,1));


			return ut.brugerinfo;
		}
		[WebMethod(EnableSession = true)]
		[System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)] //, XmlSerializeString = false
		public BrugerInfo Logaf()
		{
			ut = new svin_util(true);

			ut.tjekBruger("logaf", "email: \nkendeord: ", "", "");
			return ut.brugerinfo;
		}
		// NB ??? sessionid skal måske rettes til cookie

		//[WebMethod(EnableSession = true)]
		//[System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, XmlSerializeString = false)]
		//public bool Logaf(string brugerID)
		//{
		//    // dette kan ikke gå galt - uanset om der er en bruger at logge ud, skal man på klienten kunne begynde på en ny bruger
		//    ut = new svin_util(true);
		//    ut.tjekBruger(3, "brugerID=" + brugerID);
		//    //??? det her holder ikke i forhold til virk og ad
		//    ut.db_exec("update brugere set sessionid=null, sessionkald=null where nedlagt is null and sessionid='" + ut.svin_cookie + "'", "brugere", "0");
		//    // version 2
		//    //ut.db_exec("update brugere set sessionid=null, sessionkald=null where sessionid='" + ut.sID + "'", "brugere", "0");
		//    return true;
		//}


		[WebMethod(EnableSession = true)]
		[System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json)] //, XmlSerializeString = false
		public void send_password(string email)
		{
			ut = new svin_util(true);
			// slå brugeren op
			DataBase db = ut.db_query("select a.navn, a.kendeord from brugere.brugere a inner join brugere.firma b on a.firmaid=b.id inner join brugere.firmatype c on b.firmatype=c.id where lower(a.email) ='" + email.ToLower() + "' and a.aktiv=true and b.aktiv=true and c.vildsvin=true", "brugere", "0");
			if (db.Result == null || db.Result.Rows.Count == 0)
				throw new Exception("Der findes desværre ikke nogen aktiv bruger med email-adressen " + email);

			// Create an email to the worker.
			StringBuilder body = new StringBuilder();

			body.AppendLine("Hej " + db.Result.Rows[0]["navn"].ToString() + "!");
			body.AppendLine("Din adgangskode til Vildsvin er: " + db.Result.Rows[0]["kendeord"].ToString() + "");
			body.AppendLine("PS: Denne mail kan ikke besvares.");
			//body.AppendLine("<html>");
			//body.AppendLine("<body>");
			//body.AppendLine("Hej " + db.Result.Rows[0]["navn"].ToString() + "!<br><br>");
			//body.AppendLine("Din adgangskode til Vildsvin er: " + db.Result.Rows[0]["kendeord"].ToString() + "<br><br>");
			//body.AppendLine("PS: Denne mail kan ikke besvares.<br>");
			//body.AppendLine("</body>");
			//body.AppendLine("</html>");

			try
			{
				Email mail = new Email();
				mail.Send("Vildsvin <info@fvst.gis34.dk>", email, "Adgangskode til Vildsvin", body.ToString());
			}
			catch (Exception e)
			{
				//System.Diagnostics.Debug.Print(e.Message + "\n" + e.StackTrace);
				throw new Exception(e.Message); // + "\n" + e.StackTrace
			}
		}

		#endregion

		#region "opslagslister"
		public class IdNavn
		{
			public string id { get; set; }
			public string navn { get; set; }
		}

		public class SimpelListe
		{
			public List<IdNavn> List;
			public int Count;

			public SimpelListe()
			{
				List = new List<IdNavn>();
			}
		}

		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public SimpelListe hentListe(string liste)
		{
			ut = new svin_util(true);
			// ??? er det altid uden id-kontrol ?
			ut.tjekBruger("tjek", "liste=" + liste, "", "");

			string sql = "";
			switch (liste)
			{
				case "regioner":
					sql = "select id, replace(navn, 'Veterinærenhed ', '') as navn from brugere.firma where firmatype =2 and navn like 'Veterinærenhed %' order by 2";
					break;
				//            case "pcr":
				//	sql = "select kode as id, dk_tekst as navn from ail_opslag where gruppe='PCR_test_result' order by 2";
				//	break;

				//case "virus_isolation":
				//	sql = "select kode as id, dk_tekst as navn from ail_opslag where gruppe='virus_isolation' order by 2";
				//	break;

				//case "virus_pathotype":
				//	sql = "select kode as id, dk_tekst as navn from ail_opslag where gruppe='Virus_pathotype' order by 2";
				//	break;

				//case "h":
				//	sql = "select kode as id, dk_tekst as navn from ail_opslag where gruppe='Subtype_detected_H_subtype' order by 2";
				//	break;
				//case "n":
				//	sql = "select kode as id, dk_tekst as navn from ail_opslag where gruppe='Subtype_detected_N_subtype' order by 2";
				//	break;
				case "resultat":
					sql = "SELECT id, tekst as navn from o_resultat order by 2";
					break;
				case "skjul":
					sql = "SELECT id, tekst as navn from o_skjul order by 2";
					break;
					//case "indberetninger":
					//    //sql = "SELECT 0 as id, 'Ny' as navn, -1 as sort union SELECT distinct id, case when reserveret_af_id = " + ut.brugerinfo.brugerid + " then id::text || ' *' else id::text end as navn, case when reserveret_af_id = " + ut.brugerinfo.brugerid + " then 0 else 1 end as sort from indberetninger where status =" + ut.brugerinfo.firma + " and skjul=0 and set_tid > '" + ut.nu.AddDays(-7).ToString("yyy-MM-dd") + "' order by 3, 1";
					//    sql = @"SELECT 0 as id, 'Ny indberetning' as navn, -1 as sort
					//    union
					//    SELECT distinct i.id, 
					//    case when i.reserveret_af_id = " + ut.brugerinfo.brugerid + @" then 'Indberetning ' || i.id::text || ' *' else 'Indberetning ' || i.id::text end as navn, 
					//    case when i.reserveret_af_id = " + ut.brugerinfo.brugerid + @" then 0 else 1 end as sort
					//    from indberetninger i inner
					//    join vildsvin v on i.id = v.indberetning
					//    where v.status in (1, 2) and i.set_tid  > '" + ut.nu.AddDays(-7).ToString("yyy-MM-dd") + "' and i.skjul = 0 and v.skjul = 0 order by 3, 1";
					//    break;
			}
			DataBase db = ut.getDB(sql);
			//DataBase db = ut.getDB("SELECT kode as id, tekst as navn, gruppe from opslag where gruppe like 'status_%' order by navn");
			SimpelListe a = new SimpelListe();
			db.Query(true);
			if (db.Result != null && db.Result.Rows.Count > 0)
			{
				foreach (DataRow r in db.Result.Rows)
				{
					IdNavn parameter = new IdNavn();
					parameter.id = r["id"].ToString();
					parameter.navn = r["navn"].ToString();
					a.List.Add(parameter);
				}
				a.Count = a.List.Count;
			}
			return a;
		}


		public class FundType
		{
			public string id { get; set; }
			public string navn { get; set; }
			public string proever { get; set; }
			public string indsamling { get; set; }
		}

		public class FundTyper
		{
			public List<FundType> List;
			public int Count;
			public int TotalCount;

			public FundTyper()
			{
				List = new List<FundType>();
			}
		}

		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public FundTyper hentFundtyper()
		{
			ut = new svin_util(true);
			ut.tjekBruger("tjek", "", "", "");

			FundTyper a = new FundTyper();
			DataBase db = ut.getDB("SELECT * from o_fundtype order by id");
			db.Query(true);
			if (db.Result != null && db.Result.Rows.Count > 0)
			{
				//            FundType parameter = new FundType();
				//            parameter.id = "0";
				//            parameter.navn = "- Ikke på listen -";
				//parameter.proever = "";
				//            parameter.indsamling = "";
				//            a.List.Add(parameter);

				foreach (DataRow r in db.Result.Rows)
				{
					FundType parameter = new FundType();
					parameter.id = r["id"].ToString();
					parameter.navn = r["tekst"].ToString();
					parameter.proever = r["proever"].ToString();
					parameter.indsamling = r["indsamling"].ToString();
					a.List.Add(parameter);
				}
				a.Count = a.List.Count;
			}
			return a;
		}


		public class Firma
		{
			public string id { get; set; }
			public string navn { get; set; }
			//public string firmatype { get; set; }
			public bool aktiv { get; set; }
		}

		public class Firmaliste
		{
			public List<Firma> List;
			public int Count;
			public int TotalCount;

			public Firmaliste()
			{
				List = new List<Firma>();
			}
		}

		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public Firmaliste hentFirmaliste()
		{
			ut = new svin_util(true);
			ut.tjekBruger("tjek", "", "", "");
			Firmaliste a = new Firmaliste();
			DataBase db = ut.getDB("SELECT a.id, a.navn, a.aktiv from brugere.firma a inner join brugere.firmatype b on a.firmatype=b.id where b.vildsvin=true order by upper(a.navn)");
			db.Query(true);
			//Firma parameter = new Firma();
			//parameter.id = "0";
			//parameter.navn = "1a. Afventer visitation"; //"Ikke behandlet";
			//parameter.aktiv = false;
			//a.List.Add(parameter);
			//parameter = new Firma();
			//parameter.id = "90";
			//parameter.navn = "1b. Frasorteret ved visitation"; //"Skal ikke indsamles";
			//parameter.aktiv = true;
			//a.List.Add(parameter);
			if (db.Result != null && db.Result.Rows.Count > 0)
			{
				foreach (DataRow r in db.Result.Rows)
				{
					Firma parameter = new Firma();
					parameter.id = r["id"].ToString();
					parameter.navn = r["navn"].ToString();
					parameter.aktiv = r.Field<bool>("aktiv");
					a.List.Add(parameter);
				}
				a.Count = a.List.Count;
			}
			return a;
		}

		public class Indsamler
		{
			public string id { get; set; }
			public string navn { get; set; }
			public string firmatype { get; set; }
			public bool aktiv { get; set; }
		}

		public class Indsamlerliste
		{
			public List<Indsamler> List;
			public int Count;
			public int TotalCount;

			public Indsamlerliste()
			{
				List = new List<Indsamler>();
			}
		}

		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public Indsamlerliste hentIndsamlerliste()
		{
			ut = new svin_util(true);
			ut.tjekBruger("tjek", "", "", "");
			Indsamlerliste a = new Indsamlerliste();
			// specialregel for hedehusene
			DataBase db = ut.getDB("SELECT id, navn, firmatype, (firmatype = " + (ut._epidemi == true ? "1" : "2") + " and aktiv = true) as aktiv from brugere.firma where firmatype in (1,2,3) order by aktiv desc, upper(navn) asc");
			db.Query(true);
			//Indsamler parameter = new Indsamler();
			//parameter.id = "0";
			//parameter.navn = "1a. Afventer visitation"; //"Ikke behandlet";
			//parameter.firmatype = "0";
			//parameter.aktiv = false;
			//a.List.Add(parameter);
			//parameter = new Indsamler();
			//parameter.id = "90"; 
			//parameter.navn = "1b. Frasorteret ved visitation"; //"Skal ikke indsamles";
			//parameter.firmatype = "0";
			//parameter.aktiv = true;
			//a.List.Add(parameter);

			if (db.Result != null && db.Result.Rows.Count > 0)
			{
				foreach (DataRow r in db.Result.Rows)
				{
					Indsamler parameter = new Indsamler();
					parameter.id = r["id"].ToString();
					parameter.navn = r["navn"].ToString();
					parameter.firmatype = r["firmatype"].ToString();
					if (parameter.id == "31" && ut._epidemi)
					{
						if (ut.brugerinfo.firma == "31" || ut.brugerinfo.firma == "26")
							parameter.aktiv = true;
						else
							parameter.aktiv = false;
					}
					else
						parameter.aktiv = r.Field<bool>("aktiv");
					a.List.Add(parameter);
				}
				a.Count = a.List.Count;
			}
			return a;
		}

		public class Opsamler
		{
			public string id { get; set; }
			public string navn { get; set; }
			public string email { get; set; }
		}

		public class Opsamlerliste
		{
			public List<Opsamler> List;
			public int Count;
			//public int TotalCount;

			public Opsamlerliste()
			{
				List = new List<Opsamler>();
			}
		}


		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public Opsamlerliste hentOpsamlerliste()
		{
			ut = new svin_util(true);
			ut.tjekBruger("tjek", "", "", "");
			Opsamlerliste a = new Opsamlerliste();
			if (ut.brugerinfo.firmatype == (ut.brugerinfo.epidemi ? "1" : "2"))
			{
				DataBase db = ut.getDB("SELECT id, navn, email from brugere.brugere where firmaid = " + ut.brugerinfo.firma + " and aktiv = true order by upper(navn)");
				db.Query(true);

				if (db.Result != null && db.Result.Rows.Count > 0)
				{
					foreach (DataRow r in db.Result.Rows)
					{
						Opsamler parameter = new Opsamler();
						parameter.id = r["id"].ToString();
						parameter.navn = r["navn"].ToString();
						parameter.email = r["email"].ToString();
						a.List.Add(parameter);
					}
					a.Count = a.List.Count;
				}
			}
			return a;
		}

		public class KøreseddelFugl
		{
			public string Antal { get; set; }
			public string Art { get; set; }
			public string Indsaml { get; set; }
		}

		//public class KøreseddelOplysninger
		//{
		//    public string Indberetning { get; set; }
		//    public string FugleID { get; set; } // kommasepareret liste med dem der skal indsamles og dermed laves labelplads til
		//    public string an_navn { get; set; }
		//    public string an_adresse { get; set; }
		//    public string an_telefon { get; set; }
		//    public string an_email { get; set; }


		//    public string fuAdresse { get; set; }
		//    public string fuBeskrivelse { get; set; }
		//    public string fuSetdato { get; set; }
		//    public string fuVisiteretdato { get; set; }
		//    public string fuKoordinaterUTM { get; set; }
		//    public string fuKoordinaterGrader { get; set; }

		//    public List<KøreseddelFugl> List;

		//    public KøreseddelOplysninger()
		//    {
		//        List = new List<KøreseddelFugl>();
		//    }

		//}


		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public void vaelgOpsamler(string opsamler, string indberetning, bool pdf)
		{
			ut = new svin_util(true);
			ut.tjekBruger("tjekLogin", "opsamler=" + opsamler + ";indberetning= " + indberetning, "", "");

			// alle døde vildsvin i indberetningen med status 1 rettes til status 2 og opsamler skrives i det nye felt
			DataBase db = ut.getDB("");
			ut.db_BeginTransaction(db);
			ut.db_exec_t("update vildsvin set status=2 where indberetning=" + indberetning + " and status=1 and skjul =0", "vildsvin", "0", db);
			ut.db_exec_t("update indberetninger set reserveret_af_id= " + opsamler + " where id=" + indberetning + " and skjul =0", "indberetninger", indberetning, db);
			ut.db_CommitTransaction(db);

			//if (pdf == false)
			//{
			//    DataBase dbk = ut.db_query("select f.antal, case when f.status = 90 then 'Nej' else 'Ja' end as indsaml, a.navn, q.ids from indberetninger_fugle f inner join arter a on f.art=a.id inner join (select indberetning_fugl, string_agg(id::text, ',') as ids from afugle group by 1 ) q on q.indberetning_fugl=f.id where indberetning =" + indberetning, "indberetninger_fugle", indberetning);
			//    DataRow rk = ut.db_enkeltrække("select st_x(st_transform(geom,4326)) as lon, st_y(st_transform(geom,4326)) as lat, round(st_x(st_transform(geom,25833))) as x_33, round(st_y(st_transform(geom,25833))) as y_33, * from indberetninger where id=" + indberetning, "indberetninger", indberetning);
			//    new ClassPDF(ref ut).lavKøreseddel(indberetning, dbk.Result, rk);
			//    ut.db_exec("update indberetninger set pdf=true where id=" + indberetning, "indberetninger", indberetning);
			//}

		}



		//      public class Status
		//{
		//	public string id { get; set; }
		//	public string navn { get; set; }
		//	public string gruppe { get; set; }
		//}
		public class Status
		{
			public string id { get; set; }
			public string navn { get; set; }
			public string aktiv { get; set; }
		}

		public class Statusliste
		{
			public List<Status> List;
			public int Count;
			public int TotalCount;

			public Statusliste()
			{
				List = new List<Status>();
			}
		}

		//[WebMethod(EnableSession = true)]
		//[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		//public Statusliste hentStatusliste()
		//{
		//	ut = new svin_util(true);
		//	ut.tjekBruger("tjek", "", "", "");

		//	Statusliste a = new Statusliste();
		//          DataBase db = ut.getDB("SELECT id, tekst as navn, gruppe from o_status order by 2");
		//	db.Query(true);
		//	if (db.Result != null && db.Result.Rows.Count > 0)
		//	{
		//		foreach (DataRow r in db.Result.Rows)
		//		{
		//			Status parameter = new Status();
		//			parameter.id = r["id"].ToString();
		//			parameter.navn = r["navn"].ToString();
		//			parameter.gruppe = r["gruppe"].ToString();
		//			a.List.Add(parameter);
		//		}
		//		a.Count = a.List.Count;
		//	}
		//	return a;
		//}

		// ny variant hvor kun indsamlere skal bruge listen aktivt og det derfor kun er deres aktiv, der behøver sættes
		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public Statusliste hentStatusliste()
		{
			ut = new svin_util(true);
			ut.tjekBruger("tjek", "", "", "");

			Statusliste a = new Statusliste();
			DataBase db = ut.getDB("SELECT id, tekst as navn, gruppe = 'Saml' as aktiv from o_status where id <> 4 order by 1");
			db.Query(true);
			if (db.Result != null && db.Result.Rows.Count > 0)
			{
				foreach (DataRow r in db.Result.Rows)
				{
					Status parameter = new Status();
					parameter.id = r["id"].ToString();
					parameter.navn = r["navn"].ToString();
					parameter.aktiv = r["aktiv"].ToString().ToLower();
					a.List.Add(parameter);
				}
				a.Count = a.List.Count;
			}
			return a;
		}
		// bredder og skjulte kolonner
		public class Kolonne
		{
			public string navn { get; set; }
			public string titel { get; set; }
			public string width { get; set; }
			public string hidden { get; set; }
		}
		public class Grid
		{
			public string navn { get; set; }
			public string titel { get; set; }
			public List<Kolonne> List;
		}

		public class Side
		{
			public string navn { get; set; }
			public string titel { get; set; }
			public List<Grid> List;
		}

		#endregion

		#region "telefon indberetning"

		public class TelefonIndberetning
		{
			/* forstavelsen angiver felttype
					t_ = talfelt, uden anførselstegn
					s_ = tekstfelt, med anførselstegn
					b_ = boolean, uden anførselstegn
					v_ = kun til visning, skal aldrig gemmes
			*/

			public string t_id { get; set; }
			public string t_registrator { get; set; }
			public string t_afd { get; set; }
			public string s_an_navn { get; set; }
			public string s_an_telefon { get; set; }
			public string s_an_email { get; set; }
			public string s_an_vejnavn { get; set; }
			public string s_an_husnr { get; set; }
			public string t_an_postnr { get; set; }
			public string s_an_by { get; set; }
			public string s_an_tid { get; set; }
			public string s_set_tid { get; set; }
			public string t_setx { get; set; }
			public string t_sety { get; set; }
			public string t_setpostnr { get; set; }
			public string s_setbeskriv { get; set; }
			public string t_status { get; set; }
			public string s_setvejnavn { get; set; }
			public string s_sethusnr { get; set; }
			public string s_setby { get; set; }
			public string t_fv_region { get; set; }
			public string s_fundsted { get; set; }
			public string v_brugernavn { get; set; }
			public string v_firmanavn { get; set; }
			public string v_rettet_navn { get; set; }
			public string v_rettet_firma { get; set; }
			public string t_registrator_ret { get; set; }
			public string s_rettet_tid { get; set; }
			public string v_visitator_navn { get; set; }
			public string v_visitator_firma { get; set; }
			public string v_visiteret_tid { get; set; }
			public string l_billede { get; set; }
			public string t_antal { get; set; }
			public string t_fundtype { get; set; }

		}


		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public string gemTelefonIndberetning(TelefonIndberetning t)
		{
			ut = new svin_util(true);
			string logtekst = "";
			PropertyInfo[] propertyInfos;
			propertyInfos = typeof(TelefonIndberetning).GetProperties();
			string ln = "";
			foreach (PropertyInfo prop in propertyInfos)
			{
				//System.Diagnostics.Debug.Print(prop.Name);
				object v = prop.GetValue(t, null);
				if (v == null)
					v = "";
				logtekst += ln + prop.Name + "=" + v.ToString();
				ln = "\r\n";
			}

			//System.Diagnostics.Debug.Print(logtekst);
			string brugerID = ut.tjekBruger("tjekLogin", logtekst, "", "");


			// herefter gælder hvad der skal gemmes
			string isql = "";
			string ival = "";

			string gemtyp = t.t_id == "0" ? "ny" : t.t_id == "-1" ? "lab" : "red";

			string fn = "";
			string val = "";
			string ftyp = "";
			// sæt nogle af værdierne direkte i t-klassen, så de automatisk havner de rette steder
			DataBase db = ut.getDB("");
			string mågemme = "";
			if (int.Parse(t.t_id) > 0)
			{
				// tjek at posten overhovedet må redigeres - det må den ikke hvis
				// den er visiteret
				// den er skjult
				mågemme = ut.db_enkeltopslag("select (skjul || ';' || status) as test from indberetninger where id =" + t.t_id, "indberetninger", "test", t.t_id);
				if (mågemme != "0;0")
				{
					db.Close();
					string[] fejl = mågemme.Split(';');
					mågemme = "Du kan ikke redigere denne indberetning, den er " + (fejl[0] != "0" ? "skjult" : "visiteret");
					throw new Exception(mågemme);

				}
			}

			if (gemtyp != "red")
			{
				t.t_id = db.GetNextval("indberetninger_id_seq").ToString();
				t.t_registrator = brugerID;
				t.s_an_tid = ut.nu.ToString("yyyy-MM-dd hh:mm:ss");
				t.t_afd = ut.brugerinfo.firma;
			}
			t.t_registrator_ret = brugerID;
			t.s_rettet_tid = ut.nuDB;
			t.s_set_tid = t.s_set_tid.Substring(0, t.s_set_tid.IndexOf("T"));
			t.t_status = (gemtyp == "lab" ? ut.brugerinfo.firma : "0");
			string geo = "ST_PointFromText('POINT(" + t.t_setx + " " + t.t_sety + ")',25832)";
			// slå straks op med mindste afstand, så også fundsteder i vandkanten kan komme med
			DataRow rk = ut.db_enkeltrække("select komnavn, fv_region from dagi.dagi_m10_kommune order by st_distance(" + geo + ", geom) limit 1", "dagi_m10_kommune", "0");
			t.t_fv_region = rk["fv_region"].ToString(); //ut.db_enkeltopslag("select fv_region from dagi.dagi_m10_kommune order by st_distance(" + geo + ", geom)  limit 1", "dagi_m10_kommune", "fv_region", "0"); 

			if (int.Parse(t.t_fundtype) < 20)
				t.t_status = ut.db_enkeltopslag("select firmaid from vildsvin.nst_afdelinger order by st_distance(" + geo + ", geom) limit 1", "nst_afdelinger", "firmaid", "0");


			//string vildsvin = "";

			propertyInfos = typeof(TelefonIndberetning).GetProperties();
			foreach (PropertyInfo prop in propertyInfos)
			{
				if (prop.Name == "billede" || prop.Name == "l_billede")
				{
					System.Diagnostics.Debug.Print(prop.Name);
				}
				ftyp = prop.Name.Substring(0, 2);
				fn = prop.Name.Substring(2);
				object v = prop.GetValue(t, null);
				if (v == null)
					val = "";
				else
					val = prop.GetValue(t, null).ToString();
				// visse felter skal aldrig udfyldes
				if (ftyp != "v_" && ftyp != "fu")
				{
					if (ftyp == "s_" || ftyp == "l_")
						val = ut.anf(val, true);

					if (gemtyp != "red")
					{
						isql += ", " + fn;
						ival += ", " + val;
					}
					else
					{
						if (fn != "id")
							isql += ", " + fn + " = " + val;
					}
				}
			}
			// hvis fundtype >= 20 skal der oprettes enkeltposter i vildsvin, ellers skal status vel sættes til noget særligt, så de vises på Levende til Naturstyrelsen - eller skal vi også bare gøre det på fundtypen?

			if (gemtyp != "red")
			{
				isql = "insert into indberetninger(geom" + isql + ") values(" + geo + ival + ")";

			}
			else
			{
				isql = "update indberetninger set geom =" + geo + isql + " where id = " + t.t_id;
			}
			try
			{
				ut.db_BeginTransaction(db);
				ut.db_exec_t(isql, "indberetninger", t.t_id, db);
				//ut.db_exec(isql, "indberetninger", t.t_id);


				// skal der også gemmes vildsvin? Ja, hvis fundtype >= 20
				// 20'erne skal have trikiner, 30'erne kun de andre 3 tests =1 ikke udført
				// 40'erne skal ikke have nogen tests
				// status varierer også - de levende går direkte til 3
				if (int.Parse(t.t_fundtype) >= 20)
				{
					string proever = "";
					if (int.Parse(t.t_fundtype) >= 40)
						proever = ", 1, 0, 0, 0, 0";
					else if (int.Parse(t.t_fundtype) >= 30)
						proever = ", 1, 1, 1, 1, 0";
					else
						proever = ", 1, 1, 1, 1, 1";


					string vsql = "insert into vildsvin(indberetning, afd, x, y, fundtype, status, asf, csf, aujeszkys, trikiner,geom) values";
					int antal = int.Parse(t.t_antal);
					for (int i = 0; i < antal; i++)
					{
						vsql += "(" + t.t_id + ", " + t.t_fv_region + ", " + t.t_setx + ", " + t.t_sety + ", " + t.t_fundtype + proever + "," + geo + ")" + (i == antal - 1 ? ";" : ",");
					}
					if (gemtyp == "red")
						ut.db_exec_t("delete from vildsvin where indberetning=" + t.t_id, "vildsvin", "0", db);
					ut.db_exec_t(vsql, "vildsvin", "0", db);

				}

				ut.db_CommitTransaction(db);
				db.Close();
				return t.t_id;
			}
			catch (Exception e)
			{
				ut.db_RollbackTransaction(db);
				db.Close();
				System.Diagnostics.Debug.Print(e.Message);
				throw new Exception(e.Message);
			}
		}

		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public TelefonIndberetning hentTelefonIndberetning(string id)
		{
			ut = new svin_util(true);
			string brugerid = ut.tjekBruger("tjekLogin", "id: " + id, "", "");
			//ut.gemKort("SELECT id from indberetninger where set_tid >='" + ut.nu.AddDays(-14).ToString("yyy-MM-dd") + "'", "indberetninger", "id");
			TelefonIndberetning t = new TelefonIndberetning();
			if (id == "0") // tom
			{
				t.v_brugernavn = ut.brugerinfo.brugernavn;
				t.v_firmanavn = ut.brugerinfo.firmanavn;
				t.t_registrator = ut.brugerinfo.firma;
				t.t_id = "0";
				t.s_an_tid = DateTime.Now.ToString("dd-MM-yyyy");
				t.s_set_tid = DateTime.Now.ToString("dd-MM-yyyy");
				//				t.titel = "Ny telefonisk indberetning";
			}
			else
			{

				//                i.ancpr as s_ancpr,
				DataBase db = ut.getDB(@"select 
					i.id as t_id,
					i.registrator as t_registrator,
					i.registrator_ret as t_registrator_ret,
					i.afd as t_afd,
					i.an_navn as s_an_navn,
					i.an_telefon as s_an_telefon,
					i.an_vejnavn as s_an_vejnavn,
					i.an_husnr as s_an_husnr,
					i.anp_ostnr as t_an_postnr,
					i.an_by as s_an_by,
					i.an_tid as s_an_tid,
					i.set_tid as s_set_tid,
					i.setx as t_setx,
					i.sety as t_sety,
					i.setpostnr as t_setpostnr,
					i.setbeskriv as s_setbeskriv,
					i.status as t_status,
					i.setvejnavn as s_setvejnavn,
					i.sethusnr as s_sethusnr,
					i.setby as s_setby,
					i.fv_region as t_fv_region,
					i.fundsted as s_fundsted,
					b.navn as v_brugernavn,
					f.navn as v_firmanavn,
					br.navn as v_rettet_navn,
					fr.navn as v_rettet_firma,
					i.rettet_tid as s_rettet_tid,
					bv.navn as v_visitator_navn,
					fv.navn as v_visitator_firma,
					i.visiteret_tid  as v_visiteret_tid, 
					i.billede as l_billede,
                    s.antal, 
                    s.fundtype
					from indberetninger i 
                        left join vildsvin s on a.indberetning = i.id 
                        inner join brugere.brugere b on i.registrator = b.id 
                        inner join brugere.firma f on i.afd = f.id 
					    left join brugere.brugere br on i.registrator_ret = br.id 
                        left join brugere.firma fr on br.firmaid = fr.id 
					    left join brugere.brugere bv on i.visitator = bv.id 
                        left join brugere.firma fv on bv.firmaid = fv.id 
					where i.id=" + id);
				db.Query(true);
				if (db.Result != null && db.Result.Rows.Count == 1)
				{
					// reflect for at gøre det nemmere og hurtigere	
					PropertyInfo[] propertyInfos;
					propertyInfos = typeof(TelefonIndberetning).GetProperties();
					foreach (PropertyInfo prop in propertyInfos)
					{
						string val = db.Result.Rows[0][prop.Name].ToString();
						if (val != "")
						{
							switch (prop.Name)
							{
								case "s_rettet_tid":
								case "v_visiteret_tid":
								//DateTime dd = DateTime.Parse(val);
								//prop.SetValue(t, dd.ToString("dd-MM-yyyy hh:mm"), null);
								//break;
								case "s_an_tid":
								case "s_set_tid":
									DateTime d = DateTime.Parse(val);
									prop.SetValue(t, d.ToString("dd-MM-yyyy"), null);
									break;
								default:
									prop.SetValue(t, db.Result.Rows[0][prop.Name].ToString(), null);
									break;
							}
						}
					}
				}
				//else { }
				// raise error

			}


			return t;
		}

		#endregion

		#region "Levende"

		//klasser til listen
		public class Levende
		{
			public string id { get; set; }
			public string fv_region { get; set; }
			public string afd { get; set; }
			public string status { get; set; }
			public string set_tid { get; set; }
			public string setx { get; set; }
			public string sety { get; set; }
			public string visitator { get; set; }
			public string setbeskriv { get; set; }
			public string skjul { get; set; }
			public string fundsted { get; set; }
			public string pdf { get; set; }
			public string an_navn { get; set; }
			public string an_adresse { get; set; } // sammensat af vej og husnr, postnr og by
			public string an_telefon { get; set; }
			public string an_email { get; set; }
			public string setpostnr { get; set; }
			public string set_adresse { get; set; } // sammensat af vej og husnr, postnr og by
			public string billede { get; set; }
			public string kommentar { get; set; }
			public string antal { get; set; }
			public string fundtype { get; set; }
			public string reserveret_af_id { get; set; }
		}

		public class LevendeListe
		{
			public List<Levende> List;
			public int Count;
			public int TotalCount;
			public string filterkrit;

			public LevendeListe()
			{
				List = new List<Levende>();
			}
		}

		// NB: funktionen kaldes kun et sted fra og addendum er altid false
		private LevendeListe LevendeOutput(string sql, string filterkrit, bool addendum, string sortering_limit)
		{
			LevendeListe t = new LevendeListe();
			t.filterkrit = filterkrit;

			sql = @"select 
				i.id, 
				i.fv_region,
				i.afd, 
				i.setx, 
				i.sety, 
				i.set_tid, 
				i.status, 
				i.visitator, 
				i.skjul,
                i.fundsted,
                i.pdf,
                i.an_navn,
                i.setpostnr,
                i.an_vejnavn || ' ' || i.an_husnr || ', ' || i.an_postnr   || ' ' || i.an_by as an_adresse,
                i.setvejnavn || ' ' || i.sethusnr || ', ' || i.setpostnr   || ' ' || i.setby as set_adresse,
                i.an_telefon,
				i.an_email,
                i.billede,
				i.setbeskriv,
				i.fundtype,
				i.antal,
                i.reserveret_af_id,
				i.kommentar" + sql + filterkrit;

			// rens sql 
			sql = ut.rensSQL(sql);

			//totalen
			if (addendum == false)
			{
				t.TotalCount = ut.gemKort("SELECT  count(*) as antal, string_agg(i.id::text,',') as ids " + sql.Substring(sql.IndexOf(" from ", StringComparison.CurrentCultureIgnoreCase)), "indberetninger", "id");
				sql += sortering_limit;
			}
			else
			{
				t.TotalCount = ut.retKort("SELECT i.id " + sql.Substring(sql.IndexOf(" from ", StringComparison.CurrentCultureIgnoreCase)), "indberetninger");
			}
			DataBase db = ut.getDB(sql);
			db.Query(true);
			int count = 0;
			if (db.Result != null && db.Result.Rows.Count > 0)
			{
				foreach (DataRow r in db.Result.Rows)
				{
					count++;
					Levende rk = new Levende();

					// reflect for at gøre det nemmere og hurtigere	
					PropertyInfo[] props;
					props = typeof(Levende).GetProperties();
					foreach (PropertyInfo prop in props)
					{
						string val = r[prop.Name].ToString();
						if (val != "")
						{
							switch (prop.Name)
							{
								case "pdf": // boolean
									prop.SetValue(rk, val.ToLower(), null);
									break;
								//case "afd":
								//    prop.SetValue(rk, int.Parse(val),null);
								//    break;
								case "set_tid":
									DateTime d = DateTime.Parse(val);
									prop.SetValue(rk, d.ToString("dd-MM-yyyy"), null);
									break;
								//case "setvejnavn":
								//case "sethusnr":
								//case "setpostnr":
								//    break;
								//case "setby":
								//    prop.SetValue("adresse", r["setvejnavn"].ToString() + " " + r["sethusnr"].ToString() + ", " + r["setpostnr"].ToString() + " " + val, null);
								//    break;
								default:
									prop.SetValue(rk, val, null);
									break;
							}
						}
					}
					t.List.Add(rk);
				}
			}
			//t.Count = count;
			return t;
		}

		// funktioner til at hente og gemme Levende
		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public LevendeListe hentLevende(int start, int limit, GridFilter[] filter, string sort, string dir, float x1, float y1, float x2, float y2)
		{
			string filterkrit = " where i.fundtype <20 "; // lås altid til kun at vise levende og spor
			string filterlog = "";
			string filterJoin = "";
			// filtrene foregår pt alle direkte på indberetninger tabellen
			for (int i = 0; i < filter.Length; i++)
			{
				if (filter[i].sql != "")
				{
					// loggen skal have de rå værdier
					filterlog += "Filter " + i.ToString() + filter[i].log;
					switch (filter[i].field)
					{
						case "reserveret_af_id":
							filter[i].field = "i." + filter[i].field;
							filterkrit += " and " + filter[i].field + (filter[i].value == "0" ? " = 0" : (filter[i].value == "1" ? " > 0" : ">=0"));
							break;
						default:
							filter[i].field = "i." + filter[i].field;
							filterkrit += " and " + filter[i].sql;
							break;
					}
				}
			}

			ut = new svin_util(true);
			string sortering = "";
			// sortering skal ud i ekstratabeller
			switch (sort)
			{
				//case "fv_region":
				//    filterJoin += " inner join brugere.firma f on i.fv_region=f.id ";
				//    sortering = "f.navn";
				//    break;
				case "afd":
					filterJoin += " inner join brugere.firma f on i.afd=f.id ";
					sortering = "f.navn";
					break;
				case "status":
					//filterJoin += " inner join firma s on i.status=s.id ";
					filterJoin += " inner join (select 0 as id, '1a. Afventer visitation' as navn union select 90, '1b. Frasorteret ved visitation' union select id, navn from brugere.firma) s on i.status = s.id ";
					sortering = "s.navn";
					break;
				case "skjul":
					filterJoin += " inner join opslag o on i.skjul=o.kode ";
					sortering = "o.tekst";
					filterkrit += " and o.gruppe='skjul' ";
					break;
				default:
					sortering = "i." + sort;
					break;
			}
			sortering += " " + dir;
			// hvis der er sorteret på en værdi som flere poster kan have
			if (sortering != "i.id " + dir)
				sortering += ",i.id desc";
			// login ikke påkrævet
			string brugerID = ut.tjekBruger("", "start=" + start + "\r\nlimit=" + limit + "\r\nfilter=" + ut.anf(filterlog, false) + "sort =" + sort + "\r\ndir =" + dir + "\r\nx1=" + x1 + "\r\ny1=" + y1 + "\r\nx2=" + x2 + "\r\ny2=" + y2, "", "");


			string sql = " from indberetninger i " + filterJoin;

			// og så skal der kunne sættes yderligere filtre på tabellen
			if (x1 != 0)
			{
				filterkrit += " and i.setx >= " + x1.ToString() + " and i.setx <=" + x2.ToString() + " and i.sety >=" + y1.ToString() + " and i.sety <=" + y2.ToString();
			}

			return LevendeOutput(sql, filterkrit, false, " order by " + sortering + " limit " + limit + " offset " + start);
		}

		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public void gemNoteret(string id)
		{
			ut = new svin_util(true);
			string brugerID = ut.tjekBruger("", "id=" + id, "", "");
			ut.db_exec("update indberetninger set reserveret_af_id=" + brugerID + " where id=" + id, "indberetninger", id);
		}


		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public void gemvildsvin_kommentar(string vildsvin, string vildsvin_kommentar)
		{
			ut = new svin_util(true);
			string logtekst = "vildsvin=" + vildsvin + "\r\nvildsvin_kommentar=" + vildsvin_kommentar;
			string brugerID = ut.tjekBruger("tjekLogin", logtekst, "", "");

			ut.db_exec("update vildsvin set kommentar= " + ut.anf(vildsvin_kommentar, true) + ", sidst_rettet ='" + ut.nuDB + "' where id=" + vildsvin, "vildsvin", vildsvin);
		}


		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public void gemindberetning_kommentar(string indberetning, string kommentar)
		{
			ut = new svin_util(true);
			string logtekst = "indberetning=" + indberetning + "\r\nkommentar=" + kommentar;
			string brugerID = ut.tjekBruger("tjekLogin", logtekst, "", "");

			ut.db_exec("update indberetninger set kommentar= " + ut.anf(kommentar, true) + " where id=" + indberetning, "indberetninger", indberetning);
		}



		//     [WebMethod(EnableSession = true)]
		//     [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		//     public IndberetningSvar gemLevende(IndberetningGem g)
		//     {
		//         ut = new svin_util(true);
		//         string logtekst = "";
		//         PropertyInfo[] propertyInfos;
		//         propertyInfos = typeof(IndberetningGem).GetProperties();
		//         string ln = "";
		//         foreach (PropertyInfo prop in propertyInfos)
		//         {
		//             System.Diagnostics.Debug.Print(prop.Name);
		//             if (prop.Name != "List")
		//             {
		//                 object v = prop.GetValue(g, null);
		//                 if (v == null)
		//                     v = "";
		//                 logtekst += ln + prop.Name + "=" + v.ToString();
		//                 ln = "\r\n";
		//             }
		//         }
		//         //System.Diagnostics.Debug.Print(logtekst);
		//         string brugerID = ut.tjekBruger("tjekLogin", logtekst, "", "");

		//         string mågemme = ut.db_enkeltopslag("select (skjul || ';' || status) as test from indberetninger where id =" + g.indberetning, "indberetninger", "test", g.indberetning);
		//         if (mågemme != "0;0")
		//         {
		//             string[] fejl = mågemme.Split(';');
		//             mågemme = "Du kan ikke visitere denne indberetning, den er " + (fejl[0] != "0" ? "skjult" : "visiteret");
		//             throw new Exception(mågemme);

		//         }

		//         // og så det egentlige
		//         // skal indberetningen skjules
		//         if (g.skjul != "0")
		//         {
		//             // ??? skal sidst rettet + dato ikke også sættes
		//             ut.db_exec("update indberetninger set skjul = " + g.skjul + " where id = " + g.indberetning, "indberetninger", g.indberetning); 
		//         }
		//         else
		//         {
		//             int antalfugle = 0;
		//             // transaction
		//             DataBase t = ut.getDB("");
		//             ut.db_BeginTransaction(t);
		//             try
		//             {
		//                 // selve indberetningen skal have den nye status + hvem der har visiteret
		//                 ut.db_exec_t("update indberetninger set status=" + g.afd + ", pdf=" + (g.afd != "90") + ", visitator=" + brugerID + ", visiteret_tid='" + ut.nuDB + "', kommentar= " + ut.anf(g.kommentar,true) + " where id=" + g.indberetning, "indberetninger", g.indberetning, t);
		//                 ut.db_CommitTransaction(t);
		//                 // send email til indsamler 
		//                 if (g.afd != "90") // kun hvis fuglene faktisk skal indsamles
		//                 {
		//                     string email = "";
		//                     if (ut.db_enkeltopslag("select kode from fugle.opslag where gruppe = 'mail_indsamler'", "opslag", "kode", "0") == "1") // ja
		//                         email = ut.db_enkeltopslag("select email from brugere.firma where id = " + g.afd, "firma", "email", g.afd);

		//                     string indsamler = ut.db_enkeltopslag("select navn from brugere.firma where id = " + g.afd, "firma", "navn", g.afd);
		//                     // Create an email 
		//                     StringBuilder body = new StringBuilder();

		//                     body.AppendLine("Hej");
		//                     body.AppendLine("Der er visiteret " + antalfugle + " fugle, som skal indsamles af " + indsamler);
		//                     body.AppendLine("PS: Denne mail kan ikke besvares.");
		//                     Email mail = new Email();

		//                     if (email != "")
		//                     {
		//                         if (email.Contains(";"))
		//                         {
		//                             string[] emails = email.Split(';');
		//                             List<string> toAddresses = new List<string>();
		//                             foreach (string em in emails)
		//                             {
		//                                 toAddresses.Add(em);
		//                             }
		//                             mail.Send("Vildsvin <info@fvst.gis34.dk>", toAddresses, "Besked fra Vildsvin", body.ToString());
		//                         }
		//                         else
		//                             mail.Send("Vildsvin <info@fvst.gis34.dk>", email, "Besked fra Vildsvin", body.ToString());
		//                     }

		//                     DataBase dbk = ut.db_query("select f.antal, case when f.status = 90 then 'Nej' else 'Ja' end as indsaml, a.navn, q.ids from indberetninger_fugle f inner join arter a on f.art=a.id inner join (select indberetning_fugl, string_agg(id::text, ',') as ids from afugle group by 1 ) q on q.indberetning_fugl=f.id where indberetning =" + g.indberetning, "indberetninger_fugle", g.indberetning);
		//                     DataRow rk = ut.db_enkeltrække("select st_x(st_transform(geom,4326)) as lon, st_y(st_transform(geom,4326)) as lat, round(st_x(st_transform(geom,25833))) as x_33, round(st_y(st_transform(geom,25833))) as y_33, * from indberetninger where id=" + g.indberetning, "indberetninger", g.indberetning);
		//                     new ClassPDF(ref ut).lavKøreseddel(g.indberetning, dbk.Result, rk);
		//                 }
		//             }
		//             catch (Exception e)
		//             {
		//                 System.Diagnostics.Debug.Print(e.Message);
		//                 ut.db_RollbackTransaction(t);
		//             }
		//             finally
		//             {
		//                 t.Close();
		//             }

		//         }
		//         IndberetningSvar svar = new IndberetningSvar();
		//         svar.skjul = g.skjul;
		//         svar.afd = g.afd;
		//         svar.visitator = brugerID;
		//svar.kommentar = g.kommentar;

		//return svar;
		//     }

		//[WebMethod(EnableSession = true)]
		//[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		//public bool lavKoereseddel(string indberetning)
		//{
		//    ut = new svin_util(true);

		//    DataBase t = ut.getDB("");
		//    ut.db_BeginTransaction(t);
		//    try
		//    {
		//        DataBase dbk = ut.db_query("select f.antal, case when f.status = 90 then 'Nej' else 'Ja' end as indsaml, a.navn, q.ids from indberetninger_fugle f inner join arter a on f.art=a.id inner join (select indberetning_fugl, string_agg(id::text, ',') as ids from afugle group by 1 ) q on q.indberetning_fugl=f.id where indberetning =" + indberetning, "indberetninger_fugle", indberetning);
		//        DataRow rk = ut.db_enkeltrække("select st_x(st_transform(geom,4326)) as lon, st_y(st_transform(geom,4326)) as lat, round(st_x(st_transform(geom,25833))) as x_33, round(st_y(st_transform(geom,25833))) as y_33, * from indberetninger where id=" + indberetning, "indberetninger", indberetning);
		//        new ClassPDF(ref ut).lavKøreseddel(indberetning, dbk.Result, rk);
		//        return true;
		//    }
		//    catch //(Exception e)
		//    {
		//        return false;
		//    }
		//}


		#endregion

		#region "indsamling"

		// klasser til listen
		public class Indsamling
		{
			// redigerbare felter
			public string id { get; set; }
			public string afd { get; set; }
			public string status { get; set; }
			public string skjul { get; set; }
			public string x { get; set; }
			public string y { get; set; }
			public string vildsvin_kommentar { get; set; }
			public string afd_ny { get; set; }
			public string pdf { get; set; }
			public string zone33 { get; set; }
			public string opsamler { get; set; }

			//ekstra oplysninger 
			public string indberetning { get; set; }
			public string postnr { get; set; } // vises kun i tabel, skal måske fjernes
			public string tid_indsamlet { get; set; }
			public string fundtype { get; set; }

			// ekstra oplysninger (fra lab prøver)
			public string an_navn { get; set; }
			public string set_tid { get; set; }
			public string an_vejnavn { get; set; }
			public string an_husnr { get; set; }
			public string an_postnr { get; set; }
			public string an_by { get; set; }
			public string an_telefon { get; set; }
			public string an_email { get; set; }
			public string billede { get; set; }
			public string setbeskriv { get; set; }
			public string setvejnavn { get; set; }
			public string sethusnr { get; set; }
			public string setpostnr { get; set; }
			public string setby { get; set; }
			public string setx { get; set; }
			public string sety { get; set; }
			public string fundsted { get; set; }
			public string indberetning_kommentar { get; set; }
			public string antal { get; set; }
			//public string an_tid { get; set; }
		}

		public class Indsamlinger
		{
			public List<Indsamling> List;
			public int Count;
			public int TotalCount;
			public string filterkrit;

			public Indsamlinger()
			{
				List = new List<Indsamling>();
			}
		}

		// funktioner til at hente og gemme Levende
		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public Indsamlinger hentIndsamlinger(int start, int limit, GridFilter[] filter, string sort, string dir, float x1, float y1, float x2, float y2)
		{
			string filterkrit = "";
			string filterlog = "";
			string join = "";
			for (int i = 0; i < filter.Length; i++)
			{
				if (filter[i].sql != "")
				{
					// loggen skal have de rå værdier
					filterlog += "Filter " + i.ToString() + filter[i].log;
					switch (filter[i].field)
					{
						//case "afd":
						//	filter[i].field = "fi.navn";
						//	filterkrit += " and " + filter[i].sql;
						//	join = " inner join brugere.firma fi on f.afd = fi.id ";
						//	break;
						//case "fundtype":
						//	filter[i].field = "a.tekst";
						//	filterkrit += " and " + filter[i].sql;
						//	join += " inner join o_fundtype a on f.fundtype = a.id ";
						//	break;
						//case "status":
						//	filter[i].field = "o.tekst";
						//	filterkrit += " and " + filter[i].sql;
						//	join = " inner join o_status o on f.status = o.id ";
						//	break;
						//case "skjul":
						//	filter[i].field = "s.tekst";
						//	filterkrit += " and " + filter[i].sql;
						//	join = " inner join o_skjul s on f.skjul = s.id";
						//	break;
						//case "opsamler":
						//	filter[i].field = "b.navn";
						//	filterkrit += " and " + filter[i].sql;
						//	break;
						case "set_tid":
							filter[i].field = "i.set_tid";
							filterkrit += " and " + filter[i].sql;
							break;
						default:
							filterkrit += " and f." + filter[i].sql;
							break;

					}
				}
			}
			ut = new svin_util(true);
			string sortering = "";
			switch (sort)
			{
				//case "id":
				//case "postnr":
				//case "indberetning":
				//	sortering = "f." + sort;
				//	break;

				case "set_tid":
					sortering = "i.set_tid";
					break;
				case "afd":
					join = " inner join brugere.firma fi on f.afd = fi.id ";
					sortering = "fi.navn";
					break;
				case "fundtype":
					join += " inner join o_fundtype a on f.fundtype = a.id ";
					sortering = "a.tekst";
					break;
				case "status":
						join = " inner join o_status o on f.status = o.id ";
					sortering = "o.tekst";
					break;
				case "skjul":
					join = " inner join o_skjul s on f.skjul = s.id";
					sortering = "s.tekst";
					break;
				case "opsamler":
					sortering = "b.navn";
					break;
				default:
					sortering = "f." + sort;
					break;
			}
			sortering += " " + dir;
			// hvis der er sorteret på en værdi som flere poster kan have
			if (sortering != "f.id " + dir)
				sortering += ",f.id desc";

			// og så skal der kunne sættes yderligere filtre på tabellen
			if (x1 != 0)
			{
				filterkrit += " and i.setx >= " + x1.ToString() + " and i.setx <=" + x2.ToString() + " and i.sety >=" + y1.ToString() + " and i.sety <=" + y2.ToString();
			}
			if (filterkrit != "")
				filterkrit = " where " + filterkrit.Substring(5);
			// login ikke påkrævet
			//string brugerID = ut.tjekBruger("", "sortering=" + sort + " " + dir + "\r\naar=" + aar.ToString());
			string brugerID = ut.tjekBruger("", "start=" + start + "\r\nlimit=" + limit + "\r\nfilter=" + ut.anf(filterlog, false) + "sort =" + sort + "\r\ndir =" + dir + "\r\nx1=" + x1 + "\r\ny1=" + y1 + "\r\nx2=" + x2 + "\r\ny2=" + y2, "", "");



			Indsamlinger t = new Indsamlinger();
			t.filterkrit = filterkrit;
		string sql = @"select f.id, 
				f.postnr, 
				f.fundtype, 
				f.status, 
				f.afd,
                '0' as afd_ny, 
				f.indberetning, 
                f.x,
                f.y,
				f.kommentar as vildsvin_kommentar,
				i.setx, 
				i.sety, 
                f.skjul,
                i.set_tid,
                i.fundsted,
                i.pdf,
				f.tid_indsamlet, 
                i.an_navn,
                i.an_vejnavn,
				i.an_husnr,
				i.an_postnr,
				i.an_by,
                i.setvejnavn,
				i.sethusnr,
				i.setpostnr,
				i.setby,
                i.an_telefon,
                i.an_email,
				i.kommentar as indberetning_kommentar,
				i.setbeskriv,
                i.billede,
                '' as zone33,
                b.navn as opsamler,
				i.antal
            from 
				vildsvin f 
				left join indberetninger i on f.indberetning=i.id 
				left join brugere.brugere b on i.reserveret_af_id=b.id " + join + filterkrit;
				


			//totalen
			t.TotalCount = ut.gemKort("SELECT  count(*) as antal, string_agg(f.id::text,',') as ids " + sql.Substring(sql.IndexOf(" from ", StringComparison.CurrentCultureIgnoreCase)), "vildsvin", "id");

			sql += " order by " + sortering;
			sql += " limit " + limit + " offset " + start;
			DataBase db = ut.getDB(sql);
			db.Query(true);
			int count = 0;
			if (db.Result != null && db.Result.Rows.Count > 0)
			{
				foreach (DataRow r in db.Result.Rows)
				{
					count++;
					Indsamling rk = new Indsamling();

					// reflect for at gøre det nemmere og hurtigere	
					PropertyInfo[] props;
					props = typeof(Indsamling).GetProperties();
					foreach (PropertyInfo prop in props)
					{
						string val = r[prop.Name].ToString();
						if (val != "")
						{
							//if (prop.Name == "tid_fvst" || prop.Name == "set_tid")
							//{
							//    DateTime dt = DateTime.Parse(val);
							//    val = dt.ToString("dd-MM yyyy");
							//}
							//else 
							if (prop.Name == "pdf")
								val = val.ToLower();

							prop.SetValue(rk, val, null);
						}
					}
					t.List.Add(rk);
				}
			}
			t.Count = count;
			return t;
		}

		// klasser til redigering af den enkelte indberetning
		public class IndsamletFugl
		{
			public int Count;
			public int TotalCount;

		}

		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public Indsamling gemIndsamling(Indsamling g)
		{
			ut = new svin_util(true);
			string logtekst = "";
			PropertyInfo[] propertyInfos;
			propertyInfos = typeof(Indsamling).GetProperties();
			string ln = "";
			foreach (PropertyInfo prop in propertyInfos)
			{
				System.Diagnostics.Debug.Print(prop.Name);
				object v = prop.GetValue(g, null);
				if (v == null)
					v = "";
				logtekst += ln + prop.Name + "=" + v.ToString();
				ln = "\r\n";
			}
			//System.Diagnostics.Debug.Print(logtekst);
			string brugerID = ut.tjekBruger("tjekLogin", logtekst, "", "");

			// og så det egentlige
			// skal vildsvinet skjules
			if (g.skjul != "0")
			{
				// ??? skal sidst rettet + dato ikke også sættes
				ut.db_exec("update vildsvin set skjul = " + g.skjul + ", tid_indsamlet='" + ut.nuDB + "'  where id = " + g.id, "vildsvin", g.id);
			}
			else if (int.Parse(g.status) >= 90)
			{
				// ikke indsamlet af forskellige årsager
				// ??? skal sidst rettet + dato ikke også sættes
				ut.db_exec("update vildsvin set status = " + g.status + ", tid_indsamlet='" + ut.nuDB + "'  where id = " + g.id, "vildsvin", g.id);
			}
			else if (g.afd_ny != g.afd && g.afd != "0")
			{
				// flyt til anden indsamler
				ut.db_exec("update vildsvin set afd = " + g.afd_ny + " where id = " + g.id, "vildsvin", g.id);
				g.afd = g.afd_ny;
				g.afd_ny = "";
			}
			else // så skal alle felter udfyldes
			{
				// det forekommer vist kun ved nye vildsvin
				if (g.afd == "0" && g.afd_ny != "0")
					g.afd = g.afd_ny;
				string koord = "";
				if (g.zone33 == "1")
				{
					koord = ", x_33=" + g.x + ", y_33= " + g.y; // + ", x=st_x(st_transform(" + geo + ",25832)), y = st_y(st_transform(" + geo + ",25832))";
																// slå de konverterede koordinater op
					DataRow rkk = ut.db_enkeltrække("select st_x(st_transform(ST_PointFromText('POINT(" + g.x + " " + g.y + ")',25833),25832)) as x, st_y(st_transform(ST_PointFromText('POINT(" + g.x + " " + g.y + ")',25833),25832)) as y", "koordinater", "0");
					g.x = rkk["x"].ToString();
					g.y = rkk["y"].ToString();
				}

				// transaction
				DataBase t = ut.getDB("");
				ut.db_BeginTransaction(t);
				try
				{
					string kommune = ut.db_enkeltopslag("select komnavn from dagi.dagi_m10_kommune order by st_distance(ST_PointFromText('POINT(" + g.x + " " + g.y + ")', 25832), geom) limit 1", "dagi_m10_kommune", "komnavn", "0");
					// hvis det er en ny fugl, skal den oprettes
					if (g.id == "0")
					{
						// skal der oprettes en ny indberetning eller knyttes til en eksisterende
						if (g.indberetning == "0")
						{
							g.indberetning = t.GetNextval("indberetninger_id_seq").ToString();
							// ??? burde det være brugerens firma snarere end vildsvinets afd?
							ut.db_exec_t("insert into indberetninger(id, registrator, afd, status, visitator) values(" + g.indberetning + ", " + ut.brugerinfo.brugerid + ", " + g.afd + ", " + g.afd + ",-1)", "indberetninger", g.indberetning, t);

						}
						g.id = t.GetNextval("vildsvin_id_seq").ToString();
						ut.db_exec_t("insert into vildsvin(id, indberetning) values(" + g.id + "," + g.indberetning + ")", "vildsvin", g.id, t);
					}
					// så er det udelukkende update
					ut.db_exec_t(@"update vildsvin set status=" + g.status + ", afd =" + g.afd + ", tid_indsamlet ='" + ut.nuDB + "', fundtype = " + g.fundtype + ", kommentar =" + ut.anf(g.vildsvin_kommentar, true)
							+ ", x = " + g.x + ", y = " + g.y + koord + ", geom = ST_PointFromText('POINT(" + g.x + " " + g.y + ")',25832)" + ", kommune='" + kommune + "' where id=" + g.id, "vildsvin", g.id, t);
					ut.db_CommitTransaction(t);
				}
				catch (Exception e)
				{
					System.Diagnostics.Debug.Print(e.Message);
					ut.db_RollbackTransaction(t);
				}
				finally
				{
					t.Close();
				}

			}
			return g;
		}


		#endregion

		#region "lab"

		// klasser til listen
		public class LabProeve
		{
			public string id { get; set; }
			public string afd { get; set; }
			public string journalnr_1 { get; set; }
			public string journalnr_2 { get; set; }
			public string status { get; set; }
			//public string status_1 { get; set; }
			//public string status_2 { get; set; }
			public string fundtype { get; set; }
			public string resultat_tekst { get; set; }
			public string indberetning { get; set; }
			public string skjul { get; set; }
			public string x { get; set; }
			public string y { get; set; }
			public string tid_indsamlet { get; set; }
			public string tid_lab { get; set; }
			public string tid_slut { get; set; }
			public string tid_lab_1 { get; set; }
			public string tid_slut_1 { get; set; }
			public string tid_lab_2 { get; set; }
			public string tid_slut_2 { get; set; }
			public string asf { get; set; }
			public string csf { get; set; }
			public string aujeszkys { get; set; }
			public string trikiner { get; set; }
			public string vildsvin_kommentar { get; set; }
			public string an_navn { get; set; }
			public string set_tid { get; set; }
			public string an_vejnavn { get; set; }
			public string an_husnr { get; set; }
			public string an_postnr { get; set; }
			public string an_by { get; set; }
			public string an_telefon { get; set; }
			public string an_email { get; set; }
			public string billede { get; set; }
			public string setbeskriv { get; set; }
			public string setvejnavn { get; set; }
			public string sethusnr { get; set; }
			public string setpostnr { get; set; }
			public string setby { get; set; }
			public string setx { get; set; }
			public string sety { get; set; }
			public string fundsted { get; set; }
			public string antal { get; set; }
			public string indberetning_kommentar { get; set; }
		}

		public class LabProever
		{
			public List<LabProeve> List;
			public int Count;
			public int TotalCount;
			public string filterkrit;

			public LabProever()
			{
				List = new List<LabProeve>();
			}
		}
		private LabProever hentLabProever(string sql, bool addendum, string sortering_limit, string filterkrit = "")
		{
			LabProever t = new LabProever();
			t.filterkrit = filterkrit;
			//select + felter skrives kun en gang, nemlig her, men joins, sort osv 
			sql = @"select f.id, 
				f.journalnr_1, 
				f.journalnr_2, 
				f.resultat_tekst, 
				f.fundtype, 
				f.status, 
				f.status_1, 
				f.status_2, 
				f.afd, 
				f.indberetning, 
                f.x,
                f.y,
                f.skjul,
				f.tid_indsamlet,
				f.tid_lab,
				f.tid_lab_1,
				f.tid_lab_2,
				f.tid_slut,
				f.tid_slut_1,
				f.tid_slut_2,
                f.asf,
                f.csf,
                f.aujeszkys,
                f.trikiner,
				f.kommentar as vildsvin_kommentar,
				i.an_navn,
				i.set_tid,
				i.an_vejnavn,
				i.an_husnr,
				i.an_postnr,
				i.an_by,
				i.an_telefon,
				i.an_email,
				i.billede,
				i.setbeskriv,
				i.setvejnavn,
				i.sethusnr,
				i.setpostnr,
				i.setby,
				i.setx,
				i.sety,
				i.fundsted,
				i.antal,
				i.kommentar as indberetning_kommentar" + sql;
			// rens sql 
			sql = ut.rensSQL(sql);
			if (addendum == false)
			{
				t.TotalCount = ut.gemKort("SELECT  count(*) as antal, string_agg(f.id::text,',') as ids " + sql.Substring(sql.IndexOf(" from ", StringComparison.CurrentCultureIgnoreCase)), "vildsvin", "id");

				sql += sortering_limit;
			}
			else
			{
				// tilføj de nye rækker til kortets sql
				t.TotalCount = ut.retKort("SELECT f.id " + sql.Substring(sql.IndexOf(" from ", StringComparison.CurrentCultureIgnoreCase)), "vildsvin");
			}
			DataBase db = ut.getDB(sql);
			db.Query(true);
			int count = 0;
			if (db.Result != null && db.Result.Rows.Count > 0)
			{
				foreach (DataRow r in db.Result.Rows)
				{
					count++;
					LabProeve rk = new LabProeve();

					// reflect for at gøre det nemmere og hurtigere	
					PropertyInfo[] props;
					props = typeof(LabProeve).GetProperties();
					foreach (PropertyInfo prop in props)
					{
						string val = r[prop.Name].ToString();
						if (val != "")
						{
							prop.SetValue(rk, val, null);
						}
					}
					t.List.Add(rk);
				}
			}
			t.Count = count;
			return t;

		}
		// funktioner til at hente og gemme lab analyser
		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public LabProever hentLabListe(int start, int limit, GridFilter[] filter, string sort, string dir, float x1, float y1, float x2, float y2)
		{
			string filterkrit = " where f.fundtype in (20,21,30) ";
			string filterlog = "";
			string sortjoin = "";
			// filtrene foregår pt alle direkte på indberetninger tabellen
			for (int i = 0; i < filter.Length; i++)
			{
				if (filter[i].sql != "")
				{
					// loggen skal have de rå værdier
					filterlog += "Filter " + i.ToString() + filter[i].log;
					//					filterkrit += " and f." + filter[i].sql;
//					switch (filter[i].field)
	//				{
		//				default:
							filter[i].field = "f." + filter[i].field;
			//				break;
				//	}
					filterkrit += " and " + filter[i].sql;
				}
			}
			ut = new svin_util(true);
			string sortering = "";
			// sortering skal ud i ekstratabeller
			switch (sort)
			{
				case "id":
				case "resultat_tekst":
				case "journalnr_1":
				case "journalnr_2":
				case "indberetning":
				case "tid_lab":
					sortering = "f." + sort;
					break;

				case "afd": // indsamler
					sortjoin = " left join brugere.firma fi on f.afd = fi.id ";
					sortering = "fi.navn";
					break;
				case "fundtype":
					if (sortjoin.Contains(" inner join o_fundtype a on f.fundtype = a.id ") == false)
						sortjoin = " inner join o_fundtype a on f.fundtype = a.id ";
					sortering = "a.tekst";
					break;

				case "status":
					sortjoin = " inner join o_status o on f.status = o.id";
					sortering = "o.tekst";
					break;
				case "skjul":
					sortjoin = " inner join o_skjul s on f.skjul = s.id";
					sortering = "s.tekst";
					break;
			}
			sortering += " " + dir;
			// hvis der er sorteret på en værdi som flere poster kan have
			if (sortering != "f.id " + dir)
				sortering += ",f.id desc";

			// og så skal der kunne sættes yderligere filtre på tabellen
			if (x1 != 0)
				filterkrit += " and f.x >= " + x1.ToString() + " and f.x <=" + x2.ToString() + " and f.y >=" + y1.ToString() + " and f.y <=" + y2.ToString();

			//        if (filterkrit != "")
			//filterkrit = " where " + filterkrit.Substring(5);
			// login ikke påkrævet
			//string brugerID = ut.tjekBruger("", "sortering=" + sort + " " + dir + "\r\naar=" + aar.ToString());
			string brugerID = ut.tjekBruger("", "start=" + start + "\r\nlimit=" + limit + "\r\nfilter=" + ut.anf(filterlog, false) + "sort =" + sort + "\r\ndir =" + dir + "\r\nx1=" + x1 + "\r\ny1=" + y1 + "\r\nx2=" + x2 + "\r\ny2=" + y2, "", "");


			string sortering_limit = " order by " + sortering + " limit " + limit + " offset " + start;
			string sql = " from vildsvin f " + sortjoin + " left join indberetninger i on f.indberetning=i.id" + filterkrit;
			return hentLabProever(sql, false, sortering_limit, filterkrit);
		}

		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public LabProeve gemLabProeve(LabProeve g)
		{
			ut = new svin_util(true);
			string logtekst = "";
			PropertyInfo[] propertyInfos;
			propertyInfos = typeof(LabProeve).GetProperties();
			string ln = "";
			foreach (PropertyInfo prop in propertyInfos)
			{
				//System.Diagnostics.Debug.Print(prop.Name);
				object v = prop.GetValue(g, null);
				if (v == null)
					v = "";
				logtekst += ln + prop.Name + "=" + v.ToString();
				ln = "\r\n";
			}
			//System.Diagnostics.Debug.Print(logtekst);
			string brugerID = ut.tjekBruger("tjekLogin", logtekst, "", "");
			string sql = "update vildsvin set ";
			string komb = "";
			// her begynder det egentlige tjek af de indsendte data
			if (ut.brugerinfo.firmatype == "9" || ut.brugerinfo.firmatype == "4" || ut.brugerinfo.firmatype == "0") // FVST Ringsted
			{
				// er journalnummer udfyldt, skal det være unikt, også når der skal kasseres
				if (g.tid_lab_2 != "")
				{
					string j = ut.db_enkeltopslag("select journalnr_2 from vildsvin where skjul =0 and journalnr_2 = '" + g.journalnr_2 + "' and id <> " + g.id, "vildsvin", "journalnr_2", "0");
					if (j != "" && j != "0")
						throw new Exception("Journalnummeret \"" + g.journalnr_2 + "\" hos FVST Ringsted er allerede i brug.");
					//jour_sql = ", journalnr_2='" + g.journalnr_2 + "'";
					sql += komb + "journalnr_2='" + g.journalnr_2 + "'";
					komb = ", ";

					sql += komb + "tid_lab_2='" + g.tid_lab_2.Substring(0, 10) + "'";
					if (g.tid_slut_2 != "")
						sql += komb + "tid_slut_2='" + g.tid_slut_2.Substring(0, 10) + "'";
					sql += komb + "trikiner=" + g.trikiner;
				}
			}

			if (ut.brugerinfo.firmatype == "3" || ut.brugerinfo.firmatype == "4" || ut.brugerinfo.firmatype == "0") // FVST Ringsted
			{
				if (g.tid_lab_1 != "")
				{
					string j = ut.db_enkeltopslag("select journalnr_1 from vildsvin where skjul =0 and journalnr_1 = '" + g.journalnr_1 + "' and id <> " + g.id, "vildsvin", "journalnr_1", "0");
					if (j != "" && j != "0")
						throw new Exception("Journalnummeret \"" + g.journalnr_1 + "\" hos DTU Veterinærinstituttet er allerede i brug.");
					//                  jour_sql = ", journalnr_1='" + g.journalnr_1 + "'";
					sql += komb + "journalnr_1='" + g.journalnr_1 + "'";
					komb = ", ";
					sql += komb + "tid_lab_1='" + g.tid_lab_1.Substring(0, 10) + "'";
					if (g.tid_slut_1 != "")
						sql += komb + "tid_slut_1='" + g.tid_slut_1.Substring(0, 10) + "'";
					sql += komb + "asf=" + g.asf;
					sql += komb + "csf=" + g.csf;
					sql += komb + "aujeszkys=" + g.aujeszkys;
				}
			}
			// tjek datoer mod hinanden
			if (g.tid_lab_1 != "" || g.tid_lab_2 != "")
			{
				g.status = "5";
				if (g.tid_lab_1 == "")
					g.tid_lab = g.tid_lab_2;
				else if (g.tid_lab_2 == "")
					g.tid_lab = g.tid_lab_1;
				else if (String.Compare(g.tid_lab_1, g.tid_lab_2, ignoreCase: true) > 0) // første størst
					g.tid_lab = g.tid_lab_2;
				else
					g.tid_lab = g.tid_lab_1;
			}


			// sluttid skal kun sættes, hvis den er sat i begge afdelinger eller den kun er relevant for den ene = fundet død
			if (g.tid_slut_1 != "" && g.tid_slut_2 != "")
			{
				g.status = "6";
				// sæt fælles slut-dato til den seneste
				if (String.Compare(g.tid_slut_1, g.tid_slut_2, ignoreCase: true) > 0)
					g.tid_lab = g.tid_lab_1;
				else
					g.tid_slut = g.tid_slut_2;
			}
			else if (g.tid_slut_1 != "" && g.fundtype == "30")
			{
				g.status = "6";
				g.tid_slut = g.tid_slut_1;
			}

			if ((g.trikiner == "0" || g.trikiner == "4") && g.asf == "4" && g.csf == "4" && g.aujeszkys == "4")
			{
				g.status = "92"; // kasseret
				g.resultat_tekst = "Test ikke mulig";
				// og så skal der sættes slut-dato, hvis ikke det allerede er gjort
				if (g.tid_slut == "")
					g.tid_slut = ut.nuDB;
			}
			else if (g.trikiner == "2" || g.asf == "2" || g.csf == "2" || g.aujeszkys == "2")
			{
				sql += komb + "pos=1";
				string res = "";
				if (g.asf == "2")
					res += ", Afrikansk svinepest";
				if (g.csf == "2")
					res += ", Klassisk svinepest";
				if (g.aujeszkys == "2")
					res += ", Aujeszky’s sygdom";
				if (g.trikiner == "2")
					res += ", Trikiner";
				g.resultat_tekst = res.Substring(2);
			}
			else if (g.trikiner == "1" || g.asf == "1" || g.csf == "1" || g.aujeszkys == "1")
			{
				g.resultat_tekst = "Negativ";
			}
			sql += komb + "resultat_tekst=" + ut.anf(g.resultat_tekst, true);
			sql += komb + "kommentar=" + ut.anf(g.vildsvin_kommentar, true);

			if (g.tid_lab != "")
				sql += komb + "tid_lab='" + g.tid_lab.Substring(0, 10) + "'";
			if (g.tid_slut != "")
				sql += komb + "tid_slut='" + g.tid_slut.Substring(0, 10) + "'";
			sql += komb + "status=" + g.status;
			// resultat_tekst og pos skal sættes
			ut.db_exec(sql + " where id = " + g.id, "vildsvin", g.id);
			return g;
		}


		#endregion

		#region "Offentlig_side"
		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public LabAnStart LabAnalyserAar()
		{
			//string sql = "select distinct date_part('year', tid_lab) as aar from vildsvin where tid_lab is not null and status in(5,6) and skjul = 0 order by 1";
			string sql = "select distinct date_part('year', a.set_tid) as aar from indberetninger a left join vildsvin b on a.id=b.indberetning where a.fundtype in(1,10,40) or b.status in(5,6) and a.skjul = 0 order by 1";
			ut = new svin_util(true);
			DataBase db = ut.db_query(sql, "", "0");
			LabAnStart list = new LabAnStart();
			string res = "";
			if (db.Result != null)
			{
				foreach (DataRow item in db.Result.Rows)
				{
					res += ";" + item["aar"].ToString();
					list.valgtaar = item["aar"].ToString();
				}
				if (res != "") res = res.Substring(1);
				list.aar = res;
				if (ut.nu.Month < 4 && list.valgtaar == ut.nu.Year.ToString())
					list.valgtaar = ut.nu.AddYears(-1).Year.ToString() + "," + list.valgtaar;

			}
			// og så den sidste indtastningsdato
			sql = "select max(sidst_rettet) as b from vildsvin" +
				" union select max(rettet_tid) from indberetninger" +
				" order by 1 desc";
			db = ut.db_query(sql, "", "0");
			DateTime d = DateTime.Parse(db.Result.Rows[0]["b"].ToString());
			list.SidstRettet = d.ToString("d. MMMM yyyy", new System.Globalization.CultureInfo("da-DK"));


			list.aktaar = ut.nu.Year.ToString();
			db = ut.db_query("select count(*) as antal from vildsvin where date_part('year', tid_lab) =" + list.aktaar, "vildsvin", "0");
			if (db.Result.Rows.Count == 0)
				list.aktaar_antal = "0";
			else
				list.aktaar_antal = db.Result.Rows[0]["antal"].ToString();

			return list;
		}

		// ny definition til brug med udvidet query
		//public class KvartalStatistik
		//{
		//    public string navn { get; set; }
		//    public string aar { get; set; }
		//    public string kvartal { get; set; }
		//    public string doede { get; set; }
		//    public string doede_pos { get; set; }
		//    public string nedlagt { get; set; }
		//    public string nedlagt_pos { get; set; }
		//    public string levende { get; set; }
		//    public string levende_pos { get; set; }
		//    public string ialt { get; set; }
		//    public string pos { get; set; }

		//    public string h5 { get; set; }
		//    public string h7 { get; set; }

		//    public string l5 { get; set; }
		//    public string l7 { get; set; }
		//    public string la { get; set; }
		//    public KvartalStatistik()
		//    {
		//    }
		//}




		//public class KvartalStatistikListe
		//{
		//    public List<KvartalStatistik> List;
		//    public int Count;
		//    //public int TotalCount;

		//    public KvartalStatistikListe()
		//    {
		//        List = new List<KvartalStatistik>();
		//    }
		//}

		//// denne metode returnerer standard liste til brug i grid - med mulighed for at vælge flere år
		//[WebMethod(EnableSession = true)]
		//[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		//public KvartalStatistikListe LabAnalyserKvartaler(string aar)
		//{
		//    ut = new svin_util(true);
		//    string sql = "select * from v_kvartalsanalyser where aar in(" + aar + ")";
		//    DataBase db = ut.db_query(sql, "afugle", "0");
		//    KvartalStatistikListe svar = new KvartalStatistikListe();

		//    if (db.Result != null)
		//    {
		//        foreach (DataRow r in db.Result.Rows)
		//        {
		//            KvartalStatistik ks = new KvartalStatistik();
		//            PropertyInfo[] props;
		//            props = typeof(KvartalStatistik).GetProperties();
		//            foreach (PropertyInfo prop in props)
		//            {
		//                string val = r[prop.Name].ToString();
		//                if (val == "0")
		//                    val = "";
		//                prop.SetValue(ks, val, null);
		//            }
		//            svar.List.Add(ks);
		//        }
		//    }
		//    svar.Count = svar.List.Count;

		//    return svar;
		//}





		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public LabAnList visLabAnalyser(string aar, string fund, int start, int limit, GridFilter[] filter, string sort, string dir, float x1, float y1, float x2, float y2)
		{
			string join1 = "";
			//string join2 = "";
			string filterkrit = "";
			string filterkrit2 = "";
			string filterlog = "";
			//string filterkomb = "";
			for (int i = 0; i < filter.Length; i++)
			{
				if (filter[i].sql != "")
				{
					// loggen skal have de rå værdier
					filterlog += "Filter " + i.ToString() + filter[i].log;
					//filterkrit += filterkomb;
					//filterkrit2 += filterkomb;
					switch (filter[i].field)
					{
						case "fundtype":
							filter[i].field = "a." + filter[i].field;
							filterkrit += " and " + filter[i].sql;
							filterkrit2 += " and " + filter[i].sql;
							break;
							// de følgende findes ikke i levende og skal derfor blokere for levende rækker
						case "resultat":
						case "journalnr_1":
						case "journalnr_2":
						case "kommune":
							if (filter[i].field == "resultat")
								filter[i].field = "a." + filter[i].field + "_tekst";
							else
								filter[i].field = "a." + filter[i].field;
							filterkrit += " and " + filter[i].sql;
							filterkrit2 += " and a.id=0";
							break;

						case "id":
							filter[i].field = "a.id";
							filterkrit += " and " + filter[i].sql;

							filter[i].field = "a.id*-1";
							filterkrit2 += " and " + filter[i].sql;
							break;
						case "indberetning":
							filter[i].field = "a.indberetning";
							filterkrit += " and " + filter[i].sql;
							filter[i].field = "a.id" ;
							filterkrit2 += " and " + filter[i].sql;
							break;
						case "set_tid":
							string flt = filter[i].field;
							filter[i].field = "i." + flt;
							filterkrit += " and " + filter[i].sql;
							filter[i].field = "a." + flt;
							filterkrit2 += " and " + filter[i].sql;
							break;
					}
				}
				//filterkomb = " and ";
			//}
			}

			ut = new svin_util(true);
			string sortering = sort;
			string sortfelt = "";
			// vi skal have rettet feltnavnet til det rette i forhold til querien
			// NB: når det er en union forespørgsel, skal der vist ikke tabel foran
			switch (sort)
			{
				case "id":
				case "indberetning":
				case "set_tid":
				case "resultat":
				case "journalnr_1":
				case "journalnr_2":
				case "kommune":
					break;

				case "fundtype":
					join1 = " left join o_fundtype f on a.fundtype = f.id ";
					//join2 = " left join o_fundtype f on a.fundtype = f.id ";
					sortering = "tekst";
					sortfelt = ", f.tekst";
					break;

			}
			sortering += " " + dir;
			// hvis der er sorteret på en værdi som flere poster kan have
			if (sortering != "id " + dir)
				sortering += ",id desc";


			// login ikke påkrævet
			string brugerID = ut.tjekBruger("", "start=" + start + "\r\nlimit=" + limit + "\r\nfilter=" + ut.anf(filterlog, false) + "sortering=" + sort + " " + dir + "\r\naar=" + aar, "", "");



			LabAnList list = new LabAnList();
			if (x1 != 0)
			{
				filterkrit += " and a.x >= " + x1.ToString() + " and a.x <=" + x2.ToString() + " and a.y >=" + y1.ToString() + " and a.y <=" + y2.ToString();
				filterkrit2 += " and setx >= " + x1.ToString() + " and setx <=" + x2.ToString() + " and sety >=" + y1.ToString() + " and sety <=" + y2.ToString();
			}
			//string logdb = "brugere";
			string sql = "";
			// vi skal have både levende, spor, ilanddrevne direkte fra indberetninger og døde/nedlagte fra vildsvin --- det kan ikke gøres ordentligt uden union
			if (fund != "l")
			{
				sql = @"select 
                a.id, 
                a.indberetning, 
                a.fundtype, 
                a.journalnr_1, 
                a.journalnr_2, 
                a.kommune, 
                i.set_tid, 
                a.x, 
                a.y, 
                case when a.status = 5 then 'Test ikke afsluttet' else case when a.pos = 1 then a.resultat_tekst else 'Negativ' end end as resultat, 
                a.pos,
                a.tid_lab, 
                a.tid_slut, 
                i.setvejnavn || ' ' || i.sethusnr || ', ' || i.setpostnr || ' ' || i.setby as set_adresse, 
                i.fundsted, 
                a.asf, 
                a.csf, 
                a.trikiner, 
                a.aujeszkys,
                i.billede,
				i.kommentar as indberetning_kommentar,
				a.kommentar as vildsvin_kommentar " + sortfelt  
              + @" from vildsvin a 
                left join indberetninger i on a.indberetning = i.id " + join1 
              + " where i.skjul =0 and a.skjul = 0 and a.status in (5,6) and i.fundtype in(20,21,30) and date_part('year', i.set_tid) in(" + aar.ToString() + ") " + filterkrit;
			}
			string sql2 = "";
			if (fund == "ld")
				sql2 = " union ";
			if (fund != "d")
			{
				sql2 += @"select 
                a.id *-1 as id, 
                a.id as indberetning, 
                a.fundtype, 
                '' as journalnr_1, 
                '' as journalnr_2, 
                '' as kommune, 
                a.set_tid, 
                a.setx as x, 
                a.sety as y, 
                '' as resultat, 
                0 as pos,
                null as tid_lab, 
                null as tid_slut, 
                a.setvejnavn || ' ' || a.sethusnr || ', ' || a.setpostnr || ' ' || a.setby as set_adresse, 
                a.fundsted, 
                0 as asf, 
                0 as csf, 
                0 as trikiner, 
                0 as aujeszkys,
                a.billede,
				a.kommentar as indberetning_kommentar,
                '' as vildsvin_kommentar " + sortfelt
			  + @" from indberetninger a" + join1
			  + " where skjul =0 and fundtype in(1,10,40) and date_part('year', set_tid) in(" + aar.ToString() + ") " + filterkrit2;
			}

			string[] aars = aar.Split(',');
			// og så skal der kunne sættes yderligere filtre på tabellen

			// nb: total-antallet fejler ved union, det skal laves på hver af delforespørgslerne
			string sqltal = "";
			if (fund != "l")
				sqltal = "SELECT a.id " + sql.Substring(sql.IndexOf(" from ", StringComparison.CurrentCultureIgnoreCase));
			if (fund == "ld")
				sqltal += " union ";
			if (fund != "d")
				sqltal += " SELECT -id " + sql2.Substring(sql2.IndexOf(" from ", StringComparison.CurrentCultureIgnoreCase));
			list.TotalCount = ut.gemKort("select count(*) as antal, string_agg(q.id::text,',') as ids from (" + sqltal + ") q", "lab_analyser", "id");
			// tilføj sortering og begrænsning
			sql += sql2;
			sql += " order by " + sortering;
			sql += " limit " + limit + " offset " + start;

			DataBase db = ut.db_query(sql, "indberetninger", "0");

			if (db.Result != null)
			{
				foreach (DataRow item in db.Result.Rows)
				{
					LabAn parameter = new LabAn();
					parameter.id = item["id"].ToString();
					parameter.pos = item["pos"].ToString() == "0" ? "0" : "1";
					parameter.fundtype = item["fundtype"].ToString();
					parameter.journalnr_1 = item["journalnr_1"].ToString();
					parameter.journalnr_2 = item["journalnr_2"].ToString();
					parameter.resultat = item["resultat"].ToString();
					parameter.kommune = item["kommune"].ToString();
					DateTime dt = DateTime.Parse(item["set_tid"].ToString());
					parameter.set_tid = dt.ToString("dd-MM-yyyy");
					parameter.x = item["x"].ToString();
					parameter.y = item["y"].ToString();
					if (item["tid_lab"].ToString() != "")
					{
						dt = DateTime.Parse(item["tid_lab"].ToString());
						parameter.tid_lab = dt.ToString("dd-MM-yyyy");
					}
					if (item["tid_slut"].ToString() != "")
					{
						dt = DateTime.Parse(item["tid_slut"].ToString());
						parameter.tid_slut = dt.ToString("dd-MM-yyyy");
					}
					if (brugerID != "0")
					{
						parameter.set_adresse = item["set_adresse"].ToString();
						parameter.fundsted = item["fundsted"].ToString();
						parameter.billede = item["billede"].ToString();
					}
					parameter.indberetning = item["indberetning"].ToString();
					parameter.asf = item["asf"].ToString();
					parameter.aujeszkys = item["aujeszkys"].ToString();
					parameter.csf = item["csf"].ToString();
					parameter.trikiner = item["trikiner"].ToString();
					parameter.indberetning_kommentar = item["indberetning_kommentar"].ToString();
					parameter.vildsvin_kommentar = item["vildsvin_kommentar"].ToString();
					list.List.Add(parameter);
				}
			}
			list.Count = list.List.Count;
			list.filterkrit = filterkrit;
			list.filterkrit2 = filterkrit2;
			return list;
		}

		public class LabAn
		{
			public string id { get; set; }
			public string indberetning { get; set; }
			public string pos { get; set; }
			public string fundtype { get; set; }
			public string journalnr_1 { get; set; }
			public string journalnr_2 { get; set; }
			public string resultat { get; set; }
			//public string indsamler { get; set; }
			public string kommune { get; set; }
			public string x { get; set; }
			public string y { get; set; }
			public string set_tid { get; set; }
			public string tid_lab { get; set; }
			public string tid_slut { get; set; }
			public string set_adresse { get; set; }
			public string fundsted { get; set; }
			public string billede { get; set; }
			public string trikiner { get; set; }
			public string asf { get; set; }
			public string csf { get; set; }
			public string aujeszkys { get; set; }
			public string indberetning_kommentar { get; set; }
			public string vildsvin_kommentar { get; set; }
		}
		public class LabAnList
		{
			public List<LabAn> List;
			public int Count;
			public int TotalCount;
			public string filterkrit { get; set; }
			public string filterkrit2 { get; set; }
			public LabAnList()
			{
				List = new List<LabAn>();
			}
		}
		public class LabAnStart
		{
			public string SidstRettet { get; set; }
			public string aar { get; set; }
			public string valgtaar { get; set; }
			public string aktaar { get; set; }
			public string aktaar_antal { get; set; }
		}

		//        public class LabAar
		//        {
		//            public string id { get; set; }
		////			public string text{ get; set; }
		//        }

		//        public class LabAarList
		//        {
		//            public List<LabAar> List;
		//            public int Count;
		//            //public int TotalCount;
		//            public string SidstRettet { get; set; }
		//            public LabAarList()
		//            {
		//                List = new List<LabAar>();
		//            }
		//        }
		#endregion


		#region "FindVildsvin"

		public class Vildsvin
		{
			public string indberetning { get; set; }
			public string indberetning_status { get; set; }
			public string indberetning_skjul { get; set; }
			public string noteret { get; set; }
			public string an_navn { get; set; }
			public string set_tid { get; set; }
			public string an_vejnavn { get; set; }
			public string an_husnr { get; set; }
			public string an_postnr { get; set; }
			public string an_by { get; set; }
			public string an_telefon { get; set; }
			public string an_email { get; set; }
			public string billede { get; set; }
			public string antal { get; set; }
			public string fundtype { get; set; }
			public string setbeskriv { get; set; }
			public string setvejnavn { get; set; }
			public string sethusnr { get; set; }
			public string setpostnr { get; set; }
			public string setby { get; set; }
			public string setx { get; set; }
			public string sety { get; set; }
			public string fundsted { get; set; }
			public string indberetning_kommentar { get; set; }
			public string an_tid { get; set; }
			public string registrator { get; set; }
			public string registrator_firma { get; set; }
			public string vildsvin_id { get; set; }
			public string vildsvin_status { get; set; }
			public string resultat_tekst { get; set; }
			public string vildsvin_skjul { get; set; }
			public string tid_indsamlet { get; set; }
			public string opsamler { get; set; }
			public string afd { get; set; }
			public string x { get; set; }
			public string y { get; set; }
			public string tid_lab { get; set; }
			public string tid_slut { get; set; }
			public string vildsvin_kommentar { get; set; }
			public string journalnr_1 { get; set; }
			public string tid_lab_1 { get; set; }
			public string tid_slut_1 { get; set; }
			public string asf { get; set; }
			public string csf { get; set; }
			public string aujeszkys { get; set; }
			public string journalnr_2 { get; set; }
			public string trikiner { get; set; }
			public string tid_lab_2 { get; set; }
			public string tid_slut_2 { get; set; }

			//public string indberetning { get; set; }
			//public string an_navn { get; set; }
			//public string an_telefon { get; set; }
			//         public string an_email { get; set; }
			//         public string an_vejnavn { get; set; }
			//public string an_husnr { get; set; }
			//public string an_postnr { get; set; }
			//public string an_by { get; set; }
			//public string an_tid { get; set; }
			//public string set_tid { get; set; }
			//public string setx { get; set; }
			//public string sety { get; set; }
			//public string setpostnr { get; set; }
			//public string setbeskriv { get; set; }
			//public string indberetning_status { get; set; } // opslag
			//public string setvejnavn { get; set; }
			//public string sethusnr { get; set; }
			//public string setby { get; set; }
			//public string fv_region { get; set; }// opslag
			//public string fundsted { get; set; }
			//public string registrator { get; set; }// opslag
			//public string registrator_firma { get; set; }
			//public string registrator_ret { get; set; }// opslag
			//public string registrator_ret_firma { get; set; }
			//public string rettet_tid { get; set; }
			//public string visitator { get; set; }// opslag
			//public string visitator_firma { get; set; }
			//public string visiteret_tid { get; set; }
			//public string reserveret_af_id { get; set; }// opslag
			//public string billede { get; set; }
			//public string indberetning_skjul { get; set; }// opslag

			//public string fugleid { get; set; }
			//public string fugl_afd { get; set; }// opslag
			//public string label { get; set; }
			//public string journalnr { get; set; }
			//public string fugl_status { get; set; }// opslag
			//public string art { get; set; }// opslag
			//public string art_praecis { get; set; }// opslag
			//public string resultat_tekst { get; set; }
			//public string fugl_skjul { get; set; }// opslag
			//public string x { get; set; }
			//public string y { get; set; }
			//public string art_bem { get; set; }
			//public string pcr_test_resultat { get; set; }// opslag
			//public string virus_isolation { get; set; }// opslag
			//public string virus_pathotype { get; set; }// opslag
			//public string h { get; set; }// opslag
			//public string n { get; set; }// opslag
			//public string tid_indsamlet { get; set; }
			//public string tid_lab { get; set; }
			//public string tid_slut { get; set; }
			//public string ring { get; set; }

		}
		public class VildsvinListe
		{
			public List<Vildsvin> List;
			public int Count;
			public int TotalCount;

			public VildsvinListe()
			{
				List = new List<Vildsvin>();
			}
		}


		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public VildsvinListe FindVildsvin(string value, string felt)
		{
			VildsvinListe svar = new VildsvinListe();
			ut = new svin_util(true);
			ut.tjekBruger("tjek", "value=" + value + "\nfelt=" + felt, "", "");

			// skal alle opslag laves her eller kan de klares i webfelterne?
			string filterkrit = "";
			switch (felt)
			{
				case "indberetning":
					filterkrit = "a.id=" + value;
					break;
				case "vildsvin_id":
					filterkrit = "v.id=" + value;
					break;
				case "journalnr_1":
					filterkrit = "v.journalnr_1 = '" + value + "'";
					break;
				case "journalnr_2":
					filterkrit = "v.journalnr_2 = '" + value + "'";
					break;
			}

			string sql = @"select 
				a.id as indberetning,
				d.navn as indberetning_status, -- a.status 
				e.tekst as indberetning_skjul,-- a.skjul 
				case when a.reserveret_af_id = 0 then 'Nej' else 'Ja' end as noteret,
				a.an_navn,
				a.set_tid,
				a.an_vejnavn,
				a.an_husnr,
				a.an_postnr,
				a.an_by,
				a.an_telefon,
				a.an_email,
				a.billede,
				a.antal,
				f.tekst as fundtype,-- a.fundtype
				a.setbeskriv,
				a.setvejnavn,
				a.sethusnr,
				a.setpostnr,
				a.setby,
				a.setx,
				a.sety,
				a.fundsted,
				a.kommentar as indberetning_kommentar,
				a.an_tid,
				b.navn as registrator,
				c.navn as registrator_firma,
				v.id as vildsvin_id,
				g.tekst as vildsvin_status,-- v.status 
				v.resultat_tekst,
				h.tekst as vildsvin_skjul, --v.skjul 
				v.tid_indsamlet,
				j.navn as opsamler, -- a.reserveret_af_id
				k.navn as afd, -- v.afd
				v.x,
				v.y,
				v.tid_lab,
				v.tid_slut,
				v.kommentar as vildsvin_kommentar,
				v.journalnr_1,
				v.tid_lab_1,
				v.tid_slut_1,
				l.tekst as asf, -- v.asf
				m.tekst as csf, -- v.csf
				n.tekst as aujeszkys, -- v.aujeszkys
				v.journalnr_2,
				o.tekst as trikiner, -- v.trikiner
				v.tid_lab_2,
				v.tid_slut_2
			from 
				vildsvin.indberetninger a 
				left join vildsvin.vildsvin v on a.id=v.indberetning
				left join brugere.brugere b on a.registrator = b.id 
				left join brugere.firma c on b.firmaid = c.id 
				left join brugere.firma d on a.status = d.id 
				left join vildsvin.o_skjul e on a.skjul = e.id 
				left join vildsvin.o_fundtype f on a.fundtype= f.id 
				left join vildsvin.o_status g on v.status = g.id 
				left join vildsvin.o_skjul h on v.skjul = h.id 
				left join brugere.brugere j on a.reserveret_af_id = j.id 
				left join brugere.firma k on v.afd = k.id 
				left join vildsvin.o_resultat l on v.asf = l.id 
				left join vildsvin.o_resultat m on v.csf = m.id 
				left join vildsvin.o_resultat n on v.aujeszkys = n.id 
				left join vildsvin.o_resultat o on v.trikiner = o.id 
			where " + filterkrit + " order by b.id, a.id";

			DataBase db = ut.db_query(sql, "vildsvin", "0");
			//DataBase db = ut.getDB(sql);
			//db.Query(true);

			int count = 0;
			if (db.Result != null && db.Result.Rows.Count > 0)
			{

				//PropertyInfo[] props;
				//props = typeof(Fugl).GetProperties();
				foreach (DataRow r in db.Result.Rows)
				{
					count++;
					Vildsvin rk = new Vildsvin();
					//// reflect for at gøre det nemmere og hurtigere	
					//foreach (PropertyInfo prop in props)
					//{
					//	string val = r[prop.Name].ToString();
					//	if (val != "")
					//	{
					//		prop.SetValue(rk, val, null);
					//	}
					//}

					// On all tables' columns
					foreach (DataColumn dc in db.Result.Columns)
					{
						string s = r[dc].ToString();
						switch (dc.ColumnName)
						{
							//// dato og klokkeslæt
							//case "an_tid":
							//case "rettet_tid":
							//case "visiteret_tid":
							//case "tid_indsamlet":
							//case "tid_slut":
							//// kun dato
							//case "set_tid":
							//case "tid_lab":

							case "vildsvin_id":
								if (s == "") // null, dvs ingen post i vildvsin
									rk.vildsvin_id = "-" + rk.indberetning;
								else
									rk.vildsvin_id = s;
								break;
							default:
								var propertyInfo = rk.GetType().GetProperty(dc.ColumnName);
								if (propertyInfo != null)  //this probably works. Yes it is
								{
									propertyInfo.SetValue(rk, s, null);
								}
								break;
						}

					}
					svar.List.Add(rk);
				}
			}
			svar.Count = count;
			svar.TotalCount = count;

			return svar;
		}
		#endregion


		#region "Kort"
		//public class NaermesteAdresse
		//{
		//    public string vejnavn { get; set; }
		//    public string husnummer{ get; set; }
		//    public string postnummer { get; set; }
		//    public string postdistrikt { get; set; }
		//    public string afstand { get; set; }
		//}

		//[WebMethod(EnableSession = true)]
		//[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		//public NaermesteAdresse findNaermesteAdresse(string x, string y)
		//{
		//    ut = new svin_util(true);
		//    ut.tjekBruger("tjek", "x=" + x + "\r\ny=" +y, "", "");

		//    try
		//    {
		//        string url = "http://kortforsyningen.kms.dk/?servicename=RestGeokeys_v2&method=nadresse&geop=" + x + "," + y + "&hits=1&geometry=true&login=" + ConfigurationManager.AppSettings["KMSlogin"] + "&password=" + ConfigurationManager.AppSettings["KMSpassword"];
		//        HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
		//        // Get response  
		//        string svar="";
		//        using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)  
		//        {  
		//            // Get the response stream  
		//            StreamReader reader = new StreamReader(response.GetResponseStream());  

		//            // Console application output  
		//            svar = reader.ReadToEnd();  
		//        }  
		//    }
		//    catch (Exception ex)
		//    {
		//        throw new Exception("Det lykkedes ikke at hente fundstedets adresse hos Kortforsyningen.\nServeren giver følgende besked om fejlen:\n" + ex.Message);
		//    }
		//    NaermesteAdresse a = new NaermesteAdresse();
		//    a.afstand = nearestAddresses[0].addressDistance.distance.ToString();
		//    a.vejnavn = nearestAddresses[0].address.streetName;
		//    a.husnummer = nearestAddresses[0].address.streetBuildingIdentifier;
		//    a.postnummer = nearestAddresses[0].address.postCodeIdentifier;
		//    a.postdistrikt = nearestAddresses[0].address.districtName;
		//    //a.stednavn = nearestAddresses[0].address.districtSubDivisionIdentifier);
		//    return a;
		//}
		/*
			{
			 "type": "FeatureCollection",
			 "crs": {
			  "type": "name",
			  "properties": {"name": "EPSG:25832"}
			 },
			 "features": [{
			  "type": "Feature",
			  "properties": {
			   "husnr": "69",
			   "kommune_kode": "0746",
			   "kommune_navn": "Skanderborg Kommune",
			   "kommune_href": "http:\/\/kortforsyningen.kms.dk\/?servicename=RestGeokeys_v2&method=kommune&komkode=0746",
			   "vej_kode": "0793",
			   "vej_navn": "Ravnsøvej",
			   "vej_href": "http:\/\/kortforsyningen.kms.dk\/?servicename=RestGeokeys_v2&method=vej&vejkode=0793",
			   "postdistrikt_kode": "8670",
			   "postdistrikt_navn": "Låsby",
			   "postdistrikt_href": "http:\/\/kortforsyningen.kms.dk\/?servicename=RestGeokeys_v2&method=postdistrikt&postnr=8670",
			   "afstand_enhed": "meter",
			   "afstand_afstand": "90"
			  },
			  "geometry": {
			   "type": "POINT",
			   "coordinates": [
				550085.8,
				6221029.94
			   ]
			  }
			 }]
			}		
		*/
		#endregion
		#region "generelle funktioner"

		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public bool skjulPost(string skjul, string panel, string id)
		{
			ut = new svin_util(true);
			string logtekst = "skjul=" + skjul + "\npanel=" + panel + "\nid=" + id;
			string brugerID = ut.tjekBruger("tjekLogin", logtekst, "", "");
			// det er kun LE34 og FVST der må skjule
			if (ut.brugerinfo.firmatype != "0" && ut.brugerinfo.firmatype != "4")
			{
				throw new Exception("Du har ikke de nødvendige rettigheder til at skjule en fugl eller indsamling.");
			}

			// herefter gælder hvad der skal gemmes
			string tabel = "";
			switch (panel)
			{
				case "LabListe_panel":
				case "indsamlinger_panel":
					tabel = "vildsvin";
					break;
				case "Levende_panel": // indberetninger
					tabel = "indberetninger";
					break;
				default:
					throw new Exception("Forkert kald til skjulPost.");
			}
			ut.db_exec("update " + tabel + " set skjul=" + skjul + " where id=" + id, tabel, id);
			return true;
		}

		public class xy
		{
			public string x { get; set; }
			public string y { get; set; }
		}
		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public xy latlonUTM(string lat, string lon)
		{
			xy svar = new xy();
			ut = new svin_util(true);
			DataRow rk = ut.db_enkeltrække("select round(st_x(b.pkt)) as x, round(st_y(b.pkt)) as y  from (select st_transform(st_setsrid(ST_PointFromText('POINT(" + lon + " " + lat + ")'), 4326), 25832) as pkt) b", "geotrans", "0");
			svar.x = rk["x"].ToString();
			svar.y = rk["y"].ToString();
			return svar;
		}
		public class GridFilter
		{
			public int id;

			public string type { get; set; }
			public string value { get; set; }
			public string field { get; set; }
			public string comparison { get; set; }
			public string sql
			{
				get
				{
					string sql = "";
					if (value != "")
					{
						string comp = "";
						switch (comparison)
						{
							case "gt":
								comp = ">";
								break;
							case "lt":
								comp = "<";
								break;
							default:
								comp = "=";
								break;
						}
						switch (type)
						{
							case "string":
								sql = field + " ilike '%" + value + "%'";
								break;
							case "boolean":
								sql = field + "=" + value;
								//								sql = string.Format("(it.{0} = {1})", field, (value == "true") ? 1 : 0);
								break;
							case "numeric":
								sql = field + comp + value;
								break;
							case "date":
								sql = field + comp + "'" + value + "'";
								break;
							case "list":
								// value kan både være tal og tekst - hvordan skelner man? 

								sql = field + " in(" + value + ")";

								break;
						}
					}
					return sql;
				}
			}
			public string log
			{
				get
				{
					string filterlog = "";
					if (sql != "")
					{
						// loggen skal have de rå værdier
						filterlog = "field: " + field + "; value: " + value + "; type: " + type + "; comparison: " + comparison + "\r\n";
					}
					return filterlog;
				}
			}

		}
		#endregion

		#region "brugere"

		// klasser til listen
		public class Bruger
		{
			public string id { get; set; }
			public string navn { get; set; }
			public string email { get; set; }
			public string aktiv { get; set; }
			public string firmaid { get; set; }
			public string firmanavn { get; set; }
			public string firmaaktiv { get; set; }
		}
		public class Brugere
		{
			public List<Bruger> List;
			public int Count;
			public int TotalCount;
			public string filterkrit { get; set; }

			public Brugere()
			{
				List = new List<Bruger>();
			}
		}
		// funktioner til at hente og gemme lab analyser
		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public Brugere hentBrugere(int start, int limit, GridFilter[] filter, string sort, string dir)
		{
			string filterkrit = "";
			string filterlog = "";
			// filtrene foregår pt alle direkte på indberetninger tabellen
			for (int i = 0; i < filter.Length; i++)
			{
				if (filter[i].sql != "")
				{
					// loggen skal have de rå værdier
					filterlog += "Filter " + i.ToString() + filter[i].log;
					switch (filter[i].field)
					{
						case "id":
						case "navn":
						case "email":
						case "aktiv":
						case "firmaid":
							filter[i].field = "b." + filter[i].field;
							break;

						case "firmanavn":
							filter[i].field = "f.navn";
							break;
						case "firmaaktiv":
							filter[i].field = "f.aktiv";
							break;
					}
					filterkrit += " and " + filter[i].sql;
				}
			}
			ut = new svin_util(true);
			string sortering = "";
			// sortering skal ud i ekstratabeller
			switch (sort)
			{
				case "id":
				case "navn":
				case "email":
				case "aktiv":
				case "firmaid":
					sortering = "b." + sort;
					break;

				case "firmanavn":
					sortering = "f.navn";
					break;
				case "firmaaktiv":
					sortering = "f.aktiv";
					break;
			}
			sortering += " " + dir;
			// hvis der er sorteret på en værdi som flere poster kan have
			if (sortering != "b.id " + dir)
				sortering += ",b.id desc";

			string brugerID = ut.tjekBruger("tjekLogin", "start=" + start + "\r\nlimit=" + limit + "\r\nfilter=" + ut.anf(filterlog, false) + "sort =" + sort + "\r\ndir =" + dir, "", "");
			// det er kun LE34 og FVST der må dette
			if (ut.brugerinfo.firmatype != "0" && ut.brugerinfo.firmatype != "4")
			{
				throw new Exception("Du har ikke de nødvendige rettigheder til at redigere brugere.");
			}

			Brugere t = new Brugere();
			t.filterkrit = filterkrit;

			string sql = @"select b.id, 
				b.navn, 
				b.email, 
				b.aktiv, 
				b.firmaid, 
				f.navn as firmanavn, 
				f.aktiv as firmaaktiv
				from brugere.brugere b, brugere.firma f, brugere.firmatype t where f.firmatype=t.id and t.vildsvin=true and b.firmaid=f.id " + filterkrit;


			// og så skal der kunne sættes yderligere filtre på tabellen
			// rens sql 
			sql = ut.rensSQL(sql);

			//totalen
			//string sqltal = "SELECT count(*) as totalantal" + sql.Substring(sql.IndexOf(" from ", StringComparison.CurrentCultureIgnoreCase));
			//t.TotalCount = int.Parse(ut.db_enkeltopslag(sqltal, "indberetninger", "totalantal", "0"));

			t.TotalCount = int.Parse(ut.db_enkeltopslag("SELECT count(*) as antal " + sql.Substring(sql.IndexOf(" from ", StringComparison.CurrentCultureIgnoreCase)), "brugere", "antal", "0"));

			sql += " order by " + sortering;
			sql += " limit " + limit + " offset " + start;
			DataBase db = ut.getDB(sql);
			db.Query(true);
			int count = 0;
			if (db.Result != null && db.Result.Rows.Count > 0)
			{
				foreach (DataRow r in db.Result.Rows)
				{
					count++;
					Bruger rk = new Bruger();

					// reflect for at gøre det nemmere og hurtigere	
					PropertyInfo[] props;
					props = typeof(Bruger).GetProperties();
					foreach (PropertyInfo prop in props)
					{
						string val = r[prop.Name].ToString();
						if (val != "")
						{
							if (prop.Name.IndexOf("aktiv") > -1)
								val = val.ToLower();
							prop.SetValue(rk, val, null);
						}
					}
					t.List.Add(rk);
				}
			}
			t.Count = count;
			return t;
		}


		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public Bruger gemBruger(Bruger g)
		{
			ut = new svin_util(true);
			string logtekst = "";
			PropertyInfo[] propertyInfos;
			propertyInfos = typeof(Bruger).GetProperties();
			string ln = "";
			foreach (PropertyInfo prop in propertyInfos)
			{
				System.Diagnostics.Debug.Print(prop.Name);
				object v = prop.GetValue(g, null);
				if (v == null)
					v = "";
				logtekst += ln + prop.Name + "=" + v.ToString();
				ln = "\r\n";
			}
			string brugerID = ut.tjekBruger("tjekLogin", logtekst, "", "");
			// det er kun LE34 og FVST der må dette
			if (ut.brugerinfo.firmatype != "0" && ut.brugerinfo.firmatype != "4")
			{
				throw new Exception("Du har ikke de nødvendige rettigheder til at redigere brugere.");
			}

			// der må ikke være flere aktive brugere med denne email-adresse
			string ebrugt = ut.db_enkeltopslag("select count(*) as antal from brugere.brugere where aktiv =true and email ='" + g.email + "' and id <> " + g.id, "brugere", "antal", g.id);
			if (ebrugt != "" && ebrugt != "0")
				throw new Exception("Email-adressen " + g.email + " er allerede benyttet af en aktiv bruger.");

			if (g.id == "0") // ny bruger
			{
				// ??? tjek evt om brugeren findes i forvejen
				string pw = System.Web.Security.Membership.GeneratePassword(8, 0);
				pw = pw.Replace('&', 'q');
				ut.db_exec("insert into brugere.brugere (navn, email, kendeord, firmaid) values('" + g.navn + "', '" + g.email + "', '" + pw + "'," + g.firmaid + ")", "brugere", "0");
				g.id = ut.db_enkeltopslag("select id from brugere.brugere where navn ='" + g.navn + "' and email = '" + g.email + "' and firmaid = " + g.firmaid, "brugere", "id", "0");
				// send straks mail til den nye bruger
				StringBuilder body = new StringBuilder();

				body.AppendLine("Hej " + g.navn + "!");
				body.AppendLine("Du er nu oprettet som bruger af Vildsvin og har fået adgangskoden: " + System.Net.WebUtility.HtmlEncode(pw));
				body.AppendLine("PS: Denne mail kan ikke besvares.");

				if (!System.Diagnostics.Debugger.IsAttached)
				{
					try
					{
						Email mail = new Email();
						mail.Send("Vildsvin <info@fvst.gis34.dk>", g.email, "Velkommen til Vildsvin", body.ToString());
					}
					catch (Exception e)
					{
						//System.Diagnostics.Debug.Print(e.Message + "\n" + e.StackTrace);
						throw new Exception(e.Message);
					}
				}

			}
			else
			{
				// her kan det kun være nedlagt, navn og email, der er ændret
				ut.db_exec("update brugere.brugere set aktiv =" + g.aktiv + ", navn='" + g.navn + "', email= '" + g.email + "' where id=" + g.id, "brugere", g.id);
			}
			return g;
		}
		#endregion
		#region "labels"
		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public string hentLabels(int antal)
		{
			ut = new svin_util(true);
			string brugerID = ut.tjekBruger("tjek", "antal=" + antal, "", ""); // login ikke krævet
																			   // slået fra fordi der næsten altid er brug for preview
																			   //<script type='text/javascript'>window.onload =function(){window.print();}</script>
			string svar = "<html><head><style>img {width:800; page-break-before:always}</style></head><body>";
			long nr = 0;
			string filnavn = "";
			int bred = 1485;
			int høj = 1050;
			int tekstbred = 1185;
			int teksthøj = 200;
			int vkant = 210;
			string mappe = AppDomain.CurrentDomain.BaseDirectory + "labels\\";
			StringFormat c = new StringFormat();
			c.Alignment = StringAlignment.Center;
			StringFormat v = new StringFormat();
			v.Alignment = StringAlignment.Near;

			Bitmap bmp = new Bitmap(2970, 2970);
			//Bitmap bmp = new Bitmap(3054, 2028);

			Graphics g = Graphics.FromImage(bmp);
			DataBase db = ut.db_query("select nextval('label_id_seq') from generate_series(1," + antal + ")", "label_id_seq", "0");
			//DataBase db = ut.getDB("");
			//db.SetSQLStatement("select nextval('label_id_seq') from generate_series(1," + antal + ")");
			//db.Query(true);
			string pgbreak = " style='page-break-before:avoid'";

			foreach (DataRow rk in db.Result.Rows)
			//for (int i = 0; i < antal; i++)
			{
				g.Clear(Color.White);
				//nr = int.Parse(db.GetNextval("label_id_seq").ToString());
				nr = rk.Field<long>("nextval");//  int.Parse(db.GetNextval("label_id_seq").ToString());
				filnavn = "label_" + nr + ".png";
				g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
				//g.InterpolationMode = InterpolationMode.HighQualityBicubic;
				//g.PixelOffsetMode = PixelOffsetMode.HighQuality;
				//g.DrawLine(new Pen(Brushes.Black), new Point(0, høj), new Point(2 * bred, høj));
				//g.DrawLine(new Pen(Brushes.Black), new Point(bred, 0), new Point(bred, 2 * høj));
				for (int x = 0; x < 2; x++)
				{
					for (int y = 0; y < 2; y++)
					{
						RectangleF rectf = new RectangleF(bred * x + vkant, høj * y + 100, tekstbred, teksthøj);
						g.DrawString("FA-" + nr.ToString("00000"), new Font("Arial", 80, FontStyle.Bold), Brushes.Black, rectf, c);
						rectf = new RectangleF(bred * x + vkant, høj * y + 310, tekstbred, teksthøj);
						g.DrawString("Postnummer: __  __  __  __", new Font("Arial", 55, FontStyle.Regular), Brushes.Black, rectf, v);
						rectf = new RectangleF(bred * x + vkant, høj * y + 485, tekstbred, teksthøj);
						g.DrawString("Øst:   __  __  __  __  __  __", new Font("Arial", 55, FontStyle.Regular), Brushes.Black, rectf, v);
						rectf = new RectangleF(bred * x + vkant, høj * y + 660, tekstbred, teksthøj);
						g.DrawString("Nord:  __  __  __  __  __  __  __", new Font("Arial", 55, FontStyle.Regular), Brushes.Black, rectf, v);
						rectf = new RectangleF(bred * x + vkant, høj * y + 860, tekstbred, teksthøj);
						g.DrawString("LE34", new Font("Arial", 55, FontStyle.Bold), Brushes.Black, rectf, c);
					}
				}
				g.Flush();
				bmp.RotateFlip(RotateFlipType.Rotate270FlipNone);
				Bitmap bmp2 = new Bitmap(2100, 2970);
				using (Graphics g2 = Graphics.FromImage(bmp2))
				{
					g2.DrawImage(bmp, new Rectangle(0, 0, bmp2.Width, bmp2.Height),
									 new Rectangle(0, 0, bmp2.Width, bmp2.Height),
									 GraphicsUnit.Pixel);
				}
				bmp2.Save(mappe + filnavn);
				//Bitmap bmp2 = new Bitmap(2028, 3054);
				//bmp2 = bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
				//bmp2.Save(mappe + filnavn);
				//                svar += "<img src='" + filnavn + "'" + (i == 0 ? " style='page-break-before:avoid'" : "") + ">";
				svar += "<img src='" + filnavn + "'" + pgbreak + ">";
				pgbreak = "";
			}
			// skriv manuel log
			//db.SetSQLStatement("insert into log_sql(")
			//db.Close();
			svar += "</body></html>";
			//return svar;
			StreamWriter w = new StreamWriter(mappe + "labels_" + nr + ".html", false);
			w.Write(svar);
			w.Close();
			return "/labels/labels_" + nr + ".html";
		}


		#endregion



		#region "kolonnebredder"
		// kolonnebredder i to tabeller
		//public class SideGrid
		//{
		//    public string side { get; set; }
		//    public string sidetitel { get; set; }
		//    public string grid { get; set; }
		//    public string gridtitel { get; set; }
		//}


		public class KolonneBredde
		{
			public string side { get; set; }// nr på siden
			public string titel { get; set; }// kolonnens
											 //public string raekkefoelge { get; set; }
											 //public string id { get; set; }
											 //public string bredde { get; set; }
											 //public string skjul { get; set; }
			public int raekkefoelge { get; set; }
			public long id { get; set; }
			public int bredde { get; set; }
			public bool vis { get; set; }
			public int def_bredde { get; set; }
			public bool def_vis { get; set; }
		}
		public class KolonneBredder
		{
			public List<KolonneBredde> List;
			public int Count;
			public int TotalCount;

			public KolonneBredder()
			{
				List = new List<KolonneBredde>();
			}
		}

		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public KolonneBredder hentKolonner()
		{
			ut = new svin_util(true);
			string brugerID = ut.tjekBruger("tjek", "", "", "");

			string sqlstr = @"SELECT
                c.side, 
                c.titel, 
                c.bredde as def_bredde, 
                c.hidden = false as def_vis,
                c.id,
                c.raekkefoelge,
                d.bredde, 
                d.hidden  = false as vis
            FROM
              vildsvin.indstillinger_kolonner c
              left join vildsvin.indstillinger_brugere d on c.id = d.kolonne and d.bruger=" + brugerID
			+ @" order by
              --b.raekkefoelge, 
              c.raekkefoelge";

			DataBase db = ut.db_query(sqlstr, "indstillinger_kolonner", "0");
			KolonneBredder t = new KolonneBredder();
			foreach (DataRow r in db.Result.Rows)
			{
				KolonneBredde rk = new KolonneBredde();
				rk.side = "a" + r["side"].ToString();
				rk.titel = r.Field<string>("titel");
				rk.raekkefoelge = r.Field<int>("raekkefoelge");
				rk.id = r.Field<long>("id");
				rk.def_bredde = r.Field<int>("def_bredde");
				rk.def_vis = r.Field<bool>("def_vis");
				if (r["bredde"].ToString() == "")
				{
					rk.bredde = r.Field<int>("def_bredde");
					rk.vis = r.Field<bool>("def_vis");
				}
				else
				{
					rk.bredde = r.Field<int>("bredde");
					rk.vis = r.Field<bool>("vis");
				}
				t.List.Add(rk);
			}
			return t;
		}

		public class GemmeKolonne
		{
			public long id { get; set; }
			public long side { get; set; }
			public int bredde { get; set; }
			public bool vis { get; set; }
		}
		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public void gemKolonner(GemmeKolonne[] kols)
		{
			// ren version af dette: side er altid tallet, så man ved hvad der skal slettes
			// selve sidens bredde er kolonne = -id

			ut = new svin_util(true);
			string brugerID = ut.tjekBruger("tjek", "", "", "");
			DataBase db = ut.getDB("");
			try
			{
				string sql = "";
				foreach (GemmeKolonne k in kols)
				{
					if (sql == "")
						sql = "insert into vildsvin.indstillinger_brugere(bruger, side, kolonne, bredde, hidden) values(" + brugerID + ", " + k.side + ", " + k.id + ", " + k.bredde + ", " + !k.vis + ")";
					else
						sql += ", (" + brugerID + ", " + k.side + ", " + k.id + ", " + k.bredde + ", " + !k.vis + ")";
				}

				db.BeginTransaction();
				db.SetSQLStatement("delete from vildsvin.indstillinger_brugere where bruger =" + brugerID + " and side = " + kols[0].side);
				db.Execute();
				db.SetSQLStatement(sql);
				db.Execute();
				db.CommitTransaction();
				db.Close();
			}
			catch (Exception e)
			{
				db.RollbackTransaction();
				db.Close();
				throw new Exception(e.Message);
			}

		}


		public class GridTrae
		{
			public string text { get; set; } // titel, det der vises i træet
			public string id { get; set; }
			public int niveau { get; set; }
			public string navn { get; set; }
			public string leaf { get; set; }
			public bool kort { get; set; }
			public int bredde { get; set; }
			public int def_bredde { get; set; }
			public string kommando { get; set; }
			public List<GridTrae> d { get; set; }
		}

		[WebMethod(EnableSession = true)]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
		public List<GridTrae> hentGrid()
		{
			ut = new svin_util(true);
			string brugerID = ut.tjekBruger("tjek", "", "", "");
			string firmakrit = "";
			// 0 og 4 skal se alt
			// i fredstid skal 2 også se 1 og 1 slet intet
			// ellers kun eget tal + 0 
			if (brugerID == "0")
				firmakrit = " and a.firma in (0)";
			else
			{
				switch (ut.brugerinfo.firmatype)
				{
					case "1":

						if (ut.brugerinfo.epidemi)
							firmakrit = " and a.firma in (-1, 0, 1)";
						else
							firmakrit = " and a.firma in (-1, 0)";
						break;
					case "2":
						if (ut.brugerinfo.epidemi)
							firmakrit = " and a.firma in (-1, 0, 2)";
						else
							firmakrit = " and a.firma in (-1, 0, 1, 2)";
						break;
					case "3":
						firmakrit = " and a.firma in (-1, 0, 3)";
						break;
				}
			}

			List<GridTrae> a = new List<GridTrae>();
			try
			{
				string sql = @"SELECT 
                        a.id,
                        a.titel,
                        a.bredde as def_bredde,
                        a.kort,
                        a.kommando, 
                        b.bredde
                    FROM 
        	            indstillinger_sider a
                        left join indstillinger_brugere b on a.id = -b.kolonne and b.bruger= " + brugerID
								+ @"
                    WHERE 
        	            a.udelad =false " + firmakrit
						+ @" ORDER BY 
        	            a.raekkefoelge";
				DataBase db = ut.db_query(sql, "indstillinger_sider", "0");
				DataTable ks = db.Result;

				//        sql = @"SELECT 
				//                 side,
				//                 id,
				//                 grid,
				//                 titel
				//FROM 
				//	indstillinger_grids
				//ORDER BY 
				//	side,raekkefoelge";
				//        db = ut.db_query(sql, "indstillinger_grids", "0");
				//        DataTable kg = db.Result;


				GridTrae t = null;
				//GridTrae tt = null;
				foreach (DataRow r in ks.Rows)
				{
					t = new GridTrae();
					t.id = "a" + r["id"].ToString();
					t.navn = "";
					t.text = r.Field<string>("titel");
					t.leaf = "false";
					t.niveau = 1;
					t.kort = r.Field<bool>("kort");
					t.def_bredde = r.Field<int>("def_bredde");
					if (r["bredde"].ToString() != "")
						t.bredde = r.Field<int>("bredde");
					else
						t.bredde = r.Field<int>("def_bredde");
					t.kommando = r.Field<string>("kommando");
					t.d = new List<GridTrae>();
					//DataRow[] rk2 = kg.Select("side=" + r["id"].ToString());
					//if (rk2.Length > 0)
					//{
					//    foreach (DataRow r2 in rk2)
					//    {
					//        tt = new GridTrae();
					//        tt.id = "b" + r2["id"].ToString();
					//        tt.navn = r2["grid"].ToString();
					//        tt.text = r2.Field<string>("titel");
					//        tt.leaf = "true";
					//        tt.niveau = 2;
					//        t.d.Add(tt);
					//    }
					//}
					a.Add(t);
				}
			}
			catch (Exception e)
			{
				throw new Exception(e.Message);
			}
			return a;
		}

		//[WebMethod(EnableSession = true)]
		//[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		//public bool hentKolonner()
		//{
		//    ut = new svin_util(true);
		//    string brugerID = ut.tjekBruger("tjekLogin", "", "", "");

		//    SELECT
		//  b.side,
		//  b.id as gridnr,
		//  b.titel as grid,
		//  c.dataindex,
		//  c.titel,
		//  c.bredde,
		//  c.hidden
		//FROM


		//  fugle.indstillinger_sider a


		//  left join fugle.indstillinger_grids b on b.side = a.id


		//  left join fugle.indstillinger_kolonner c on c.grid = b.id
		//WHERE
		//  a.udelad = false
		//  --AND a.firma

		//order by
		//  a.raekkefoelge,
		//  b.raekkefoelge,
		//  c.raekkefoelge
		//        }
		#endregion


		#region "Ikke benyttet"


		//public class ParameterListSider
		//{
		//    public List<Dictionary<string, string>> List;
		//    public int Count;
		//    public int TotalCount;
		//    public ParameterListSider()
		//    {
		//        List = new List<Dictionary<string, string>>();
		//    }
		//}
		#endregion
	}
}
