﻿function visForside() {
    toemFormular();

    var forside_panel = Ext.create('Ext.panel.Panel', {
        title: 'Overvågning af vildsvin i Danmark',
        id: 'forside_panel',
        layout: 'anchor',
        anchor: '-20',
        bodyPadding: '10 10 10 10',
        autoWidth: true,
        autoHeight: false,
        autoScroll: true,
        renderTo: 'vestdiv',
        items: [{
            xtype: 'displayfield',
            fieldLabel: '',
            autoHeight: true,
            hideLabel: true,
            fieldCls: 'forsidetekst',
            //value: 'Fødevarestyrelsen overvåger og undersøger i samarbejde med DTU Veterinærinstituttet og andre samarbejdspartnere forekomsten af vildsvin i Danmark. '
            value: 'Fødevarestyrelsen overvåger forekomsten af vildsvin i Danmark i samarbejde med Naturstyrelsen og DTU Veterinærinstituttet. Vildsvin, der er nedlagt ved jagt eller fundet døde, undersøges for en række sygdomme bl.a. afrikansk svinepest. Resultatet af undersøgelserne kan følges på kortsiden Undersøgte vildsvin. Her kan man også kan se, hvor der er fundet levende vildsvin eller spor efter disse.'
        //}, {
        //    xtype: 'displayfield',
        //    autoHeight: true,
        //    hideLabel: true,
        //    fieldCls: 'forsidetekst',
        //    style: 'margin-top: 12px; ',
        //    value: 'Vildsvin der er nedlagt ved jagt eller fundet døde undersøges for en række sygdomme. Resultaterne af undersøgelserne kan følges på kortsiden <b>Undersøgte vildsvin</b>, hvor man dels kan se hvor der er fundet vildsvin og dels om de er blevet testet positive for nogen af sygdommene.'
        }]
    });
}