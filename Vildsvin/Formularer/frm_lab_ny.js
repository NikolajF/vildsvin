﻿function redJournr() {
    Ext.apply(Ext.form.VTypes, {
        'journrText': 'Ikke et gyldigt journalnummer. Skal være i formatet xx-xxxxx-xx.',
        'journrMask': /[\-\0-9]/,
        'journrRe': /^[0-9]{2}[-]{1}[0-9]{5}[-]{1}[0-9]{2}$/,
        'journr': function (v) {
            return this.journrRe.test(v);
        }
    });

	// Function to format a journal number
	Ext.apply(Ext.util.Format, {
		journr: function (value) {
			var journr = value.replace(/-/g, '').replace(/[^0-9]/g, '');


			switch (journr.length) {
				case 0:
					return '';
					
				case 1:
				    return journr;
                case 2:
				    return journr.substr(0, 2) + '-';
                case 3:
                    return journr.substr(0, 2) + '-' + journr.substr(2, 1);
                case 4:
                    return journr.substr(0, 2) + '-' + journr.substr(2, 2);
				case 5:
				    return journr.substr(0, 2) + '-' + journr.substr(2, 3);
				case 6:
				    return journr.substr(0, 2) + '-' + journr.substr(2, 4);
				case 7:
				    return journr.substr(0, 2) + '-' + journr.substr(2, 5) + '-' ;
                case 8:
                    return journr.substr(0, 2) + '-' + journr.substr(2, 5) + '-' + journr.substr(7, 1);
                case 9:
			    default:
			        return journr.substr(0, 2) + '-' + journr.substr(2, 5) + '-' + journr.substr(7);
					
			}
		}
	});

	Ext.namespace('Ext.ux.plugin');

	// Plugin to format a journal number on value change
	Ext.ux.plugin.Formatjournr = Ext.extend(Ext.form.TextField, {
		init: function (c) {
			c.on('change', this.onChange, this);
		},
		onChange: function (c) {
			c.setValue(Ext.util.Format.journr(c.getValue()));
		}
	});

}

function visLabListe() {
    showMap();

    toemFormular('LabListe_panel', 'visLabListe');
	redJournr();

	Ext.define('mLabListe', {
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'id', type: 'string', defaultValue: '' },
			{ name: 'afd', type: 'string', defaultValue: '0' },
			{ name: 'indberetning', type: 'string', defaultValue: '0' },
            { name: 'tid_lab', type: 'string', defaultValue: '' },
            { name: 'tid_slut', type: 'string', defaultValue: '' }, 
			{ name: 'status', type: 'string', defaultValue: '0' },
			{ name: 'resultat_tekst', type: 'string', defaultValue: '0' },
            { name: 'fundtype', type: 'string', defaultValue: '0' },
			{ name: 'vildsvin_kommentar', type: 'string', defaultValue: '0' },
			{ name: 'skjul', type: 'string', defaultValue: '0' },
			{ name: 'x', type: 'string', defaultValue: '0' },
			{ name: 'y', type: 'string', defaultValue: '0' },
            { name: 'journalnr_1', type: 'string', defaultValue: '' },
            { name: 'tid_lab_1', type: 'string', defaultValue: '' },
            { name: 'tid_slut_1', type: 'string', defaultValue: '' },
            { name: 'journalnr_2', type: 'string', defaultValue: '' },
            { name: 'tid_lab_2', type: 'string', defaultValue: '' },
            { name: 'tid_slut_2', type: 'string', defaultValue: '' },
            { name: 'tid_indsamlet', type: 'string', defaultValue: '' },
            { name: 'trikiner', type: 'string', defaultValue: '0' },
            { name: 'asf', type: 'string', defaultValue: '0' },
            { name: 'csf', type: 'string', defaultValue: '0' },
			{ name: 'aujeszkys', type: 'string', defaultValue: '0' },
			// ekstra oplysninger fra indberetningen
			'antal',
			'an_navn',
			'set_tid',
			'an_vejnavn',
			'an_husnr',
			'an_postnr',
			'an_by',
			'an_telefon',
			'an_email',
			'billede',
			'setbeskriv',
			'setvejnavn',
			'sethusnr',
			'setpostnr',
			'setby',
			'setx',
			'sety',
			'fundsted',
			'indberetning_kommentar'
		],
		idProperty: 'id'
	});

	storeLabListe = Ext.create('Ext.data.JsonStore', {
		storeId: 'st_LabListe',
		pageSize: 10,
		model: 'mLabListe',
		remoteSort: true,
		proxy: {
			type: 'gridmap',
			url: 'Webservices/svin_login.asmx/hentLabListe'
		},
		sorters: [{
			property: 'id',
			direction: 'DESC'
		}],
		listeners: {
			load: function (store) {
			    retLaastPost = false;
			    mapPanel.redrawLayers('vildsvin_proevesvar');
			    aktRec = undefined;
				aiz = [0, 0, 0, 0];
				var xmin, xmax, ymin, ymax;

				// løb de nye poster igennem
				if (store.getCount() === 0) {
					//Ext.getCmp('selectonmap').disable();
					Ext.getCmp('zoomliste').disable();
					Ext.getCmp('zoommarkeret').disable();
				} else {
					//Ext.getCmp('selectonmap').enable();
					Ext.getCmp('zoomliste').enable();
					Ext.getCmp('zoommarkeret').enable();
					//  nulstilling
					xmin = 0;
					xmax = xmin;
					ymin = 0;
					ymax = ymin;
					store.each(function (record) {
						if (record.data.x !== null && record.data.x !== '0') {
							if (xmin === 0) {
								xmin = Number(record.data.x);
								xmax = xmin;
								ymin = Number(record.data.y);
								ymax = ymin;
							} else {
								if (Number(record.data.x) < xmin) xmin = Number(record.data.x);
								if (Number(record.data.x) > xmax) xmax = Number(record.data.x);
								if (Number(record.data.y) < ymin) ymin = Number(record.data.y);
								if (Number(record.data.y) > ymax) ymax = Number(record.data.y);
							} // slut if 0
						}
					}); // slut each
					if ((xmax - xmin) < 200) {
						xmidt = (Number(xmax) + Number(xmin)) / 2;
						xmax = Number(xmidt) + 100;
						xmin = Number(xmidt - 100);
					}
					if ((ymax - ymin) < 200) {
						ymidt = (Number(ymax) + Number(ymin)) / 2;
						ymax = Number(ymidt) + 100;
						ymin = Number(ymidt - 100);
					}

					zmListe = [xmin, ymin, xmax, ymax];
					if (Ext.getCmp('zoomliste').pressed === true)
					    mapPanel.zoomTo(zmListe);

					visLabAkt();

				} // slut count
			    // sæt de aktuelle filtre på Excel-udtræk
				Ext.get('btn_excel').down('a').dom.href = 'VisFil.ashx?type=xl_lab&id=' + brugerid + '&gridfilter=' + store.proxy.reader.rawData.d.filterkrit;
			}

		}
	});
	storeAkt = storeLabListe;
    mapPanel.config.layers.data.vildsvin_proevesvar.setVisibility(true);

	var filters = Ext.create('Ext.ux.grid.FiltersFeature', {
		// encode and local configuration options defined previously for easier reuse
		encode: true, // json encode the filter query
		local: false   // defaults to false (remote filtering)
	});
	var LabListe_grid = Ext.create('Ext.grid.Panel', {
		layout: 'fit',
		id: 'LabListe_grid',
		autoHeight: true,
		autoWidth: true,
		title: '',
		store: storeLabListe,
		disableSelection: false,
		loadMask: true,
		features: [filters],
		forceFit: true,
		viewConfig: {
			id: 'vg',
			//trackOver: false,
			//stripeRows: true
			getRowClass: function (record, rowIndex, rowParams, store) {
//                return ((record.get('status') == '4' || record.get('status') == '5') && record.get('skjul') == '0' ? '' : 'readonly');
			    return ((record.get('status') == '3' || record.get('status') == '5') && record.get('skjul') == '0' ? '' : 'readonly');
			}
		},
		// grid columns
		// specify any defaults for each column

		columns: {
		    defaults: { renderer: renderTooltip },
		    items: [{
		        //id: 'id',
		        text: "ID",
		        tooltip: "ID",
		        dataIndex: 'id',
		        width: frm_lab.LabListe_grid.id.width,
		        hidden: frm_lab.LabListe_grid.id.hidden,
		        //flex: 1,
		        filterable: true,
		        sortable: true,
		        filter: { type: 'numeric' }
		    }, {
		        text: "Indsamler",
		        tooltip: "Indsamler",
		        dataIndex: 'afd',
		        width: frm_lab.LabListe_grid.afd.width,
		        hidden: frm_lab.LabListe_grid.afd.hidden,
		        filterable: true,
		        sortable: true,
		        flex: 1,
		        renderer: renderIndsamler,
		        filter: {
		            type: 'list',
		            store: storeIndsamlere,
		            idField: 'id',
		            labelField: 'navn',
		            active: false
		        }
		    }, {
		        text: 'Indberetning',
		        tooltip: 'Indberetning',
		        dataIndex: 'indberetning',
		        width: frm_lab.LabListe_grid.indberetning.width,
		        hidden: frm_lab.LabListe_grid.indberetning.hidden,
		        filterable: true,
		        //flex: 1,
		        sortable: true,
		        filter: { type: 'numeric' }
		    }, {
		        text: 'Modtaget dato',
		        tooltip: 'Modtaget dato',
		        dataIndex: 'tid_lab',
		        width: frm_lab.LabListe_grid.tid_lab.width,
		        hidden: frm_lab.LabListe_grid.tid_lab.hidden,
		        filterable: true,
		        flex: 1,
		        sortable: true,
		        renderer: renderDato,
		        //format: 'd-m-Y',
		        //altFormats: 'd/m/Y H:i:s',
		        filter: { type: 'date' }
		    }, {
		        text: "Fundtype",
                tooltip: "Fundtype",
                dataIndex: 'fundtype',
                width: frm_lab.LabListe_grid.fundtype.width,
                hidden: frm_lab.LabListe_grid.fundtype.hidden,
		        filterable: true,
		        flex: 1,
		        sortable: true,
		        renderer: renderFundtyper,
		        filter: {
                        type: 'list',
                    store: storeFundtyper,
                        idField: 'id',
                        labelField: 'navn'//,
                        //value: ['3', '4', '5'],
                        //active: true
                    }
		    }, {
		        text: "Lab journalnr.",
                tooltip: "Lab journalnr.",
		        dataIndex: 'journalnr_1',
		        width: frm_lab.LabListe_grid.journalnr_1.width,
		        hidden: frm_lab.LabListe_grid.journalnr_1.hidden,
		        filterable: true,
		        sortable: true,
		        flex: 1,
		        filter: { type: 'string' }
            }, {
                text: "FVST journalnr.",
                tooltip: "Lab journalnr.",
                dataIndex: 'journalnr_2',
                width: frm_lab.LabListe_grid.journalnr_2.width,
                hidden: frm_lab.LabListe_grid.journalnr_2.hidden,
                filterable: true,
                sortable: true,
                flex: 1,
                filter: { type: 'string' }
		    }, {
		        text: "Status",
		        tooltip: "Status",
		        dataIndex: 'status',
		        width: frm_lab.LabListe_grid.status.width,
		        hidden: frm_lab.LabListe_grid.status.hidden,
		        filterable: true,
		        sortable: true,
		        flex: 1,
		        renderer: renderStatus,
		        filter: {
		            type: 'list',
		            store: storeStatus,
		            idField: 'id',
		            labelField: 'navn',
		            value: ['3', '5'],
		            active: true
		        }
		    }, {
		        text: "Resultat",
		        tooltip: "Resultat",
		        dataIndex: 'resultat_tekst',
		        width: frm_lab.LabListe_grid.resultat_tekst.width,
		        hidden: frm_lab.LabListe_grid.resultat_tekst.hidden,
		        filterable: true,
		        sortable: true,
		        flex: 1,
		        filter: { type: 'string' }
		    }, {
		        text: "Vis / Skjul",
		        tooltip: "Vis / Skjul",
		        dataIndex: 'skjul',
		        width: frm_lab.LabListe_grid.skjul.width,
		        hidden: frm_lab.LabListe_grid.skjul.hidden,
		        filterable: true,
		        //flex: 1,
		        sortable: true,
		        renderer: renderVis,
		        filter: {
		            type: 'list',
		            store: storeSkjul,
		            idField: 'id',
		            labelField: 'navn',
		            value: '0',
		            active: true
		        }
		    }]
		},
	    // gridkol_slut
		selModel: Ext.create('Ext.selection.RowModel', {
			listeners: {
				select: {
					fn: function (selModel, rec, index) {
					    retLaastPost = false;
					    aktRec = rec;
						aktRk = index;
						aiz = [0, 0, 0, 0];
						aiz[0] = Number(rec.data.x - 100);
						aiz[1] = Number(rec.data.y - 100);
						aiz[2] = Number(Number(rec.data.x) + 100);
						aiz[3] = Number(Number(rec.data.y) + 100);

						if (Ext.getCmp('zoommarkeret').pressed === true && rec.data.x !== null && rec.data.x != '0')
							mapPanel.zoomTo(aiz);

						// ??? sæt knapper aktive og inaktive

						mapPanel.clearLayers();
						mapPanel.addSelectedPoint(rec.data.x, rec.data.y);
						visLabAkt(rec.data.id);
					}
				}
			}
		}),
		// paging bar on the bottom
		tbar: Ext.create('Ext.PagingToolbar', {
			store: storeLabListe,
			displayInfo: true
		}),
		bbar: createZoombar(true)
   });


	LabListe_grid.child('pagingtoolbar').add(['->', {
		text: 'Nulstil alle filtre',
		handler: function () {
			LabListe_grid.filters.clearFilters();
			x1 = 0;
			y1 = 0;
			x2 = 0;
			y2 = 0;

		}
	}]);

	var bx = vestPanel.getBox();

	var LabListe_panel = Ext.create('Ext.panel.Panel', {
		title: 'Analyse af indsamlede vildsvin',
		id: 'LabListe_panel',
		layout: 'anchor',
		anchor: '-20',
		autoWidth: true,
		autoHeight: false,
		height: bx.height,
        autoScroll: true,
		bodyPadding: '10 10 10 10',
		renderTo: 'vestdiv',
		items: [
			LabListe_grid,
            {
            //    xtype: 'fieldset', // her kan intet redigeres
            //    title: 'Samlet status', 
            //    id: 'lab_samlet',
            //    bodyPadding: 10,
            //    autoWidth: true,
            //    autoHeight: true,
            //    style: 'margin-top: 10px',
            //    flex: 1,
            //    items: [{
            //        xtype: 'fccolumn_ro',
            //        items: [{
            //            xtype: 'acombo',
            //            hideLabel: false,
            //            fieldLabel: 'Status',
            //            id: 'status',
            //            //store: storeStatusLAB,
            //            store: storeStatus,
            //            valueField: 'id',
            //            displayField: 'navn',
            //            margin: '0 3 0 0', // venstre
            //            columnWidth: 0.4
            //        }, {
            //    //        fieldLabel: 'Modtaget dato',
            //    //        xtype: 'datefield',
            //    //        id: 'tid_lab',
            //    //        //margin: '0 0 0 3', // højre
            //    //        columnWidth: 0.3,
            //    //        format: 'd-m-Y',
            //    //        altFormats: 'Y-m-d H:i:s|D M Y H:i:s|d-m-Y|d-m-y|Y-m-d|j-n-y|j-n-Y|j-m-y|j-m-Y|d-n-y|d-n-Y|d/m/Y H:i:s|Y-m-dTH:i:s|'
            //    //    }, {
            //    //        fieldLabel: 'Afsluttet dato',
            //    //        xtype: 'datefield',
            //    //        id: 'tid_slut',
            //    //        margin: '0 0 0 3', // højre
            //    //        columnWidth: 0.3,
            //    //        format: 'd-m-Y',
            //    //        altFormats: 'Y-m-d H:i:s|D M Y H:i:s|d-m-Y|d-m-y|Y-m-d|j-n-y|j-n-Y|j-m-y|j-m-Y|d-n-y|d-n-Y|d/m/Y H:i:s|Y-m-dTH:i:s|'
            //    //    }]
            //    //}, {
            //    //    xtype: 'fccolumn_ro',
            //    //    items: [{
            //            xtype: 'textfield',
            //            hideLabel: false,
            //            fieldLabel: 'Resultat',
            //            id: 'resultat_tekst',
            //            columnWidth: 0.6,
            //            margin: '0 3 0 0' // venstre
            //        //}, {
            //        //    xtype: 'fcombo',
            //        //    hideLabel: false,
            //        //    fieldLabel: 'Fundtype',
            //        //    id: 'fundtype',
            //        //    store: storeFundtyper,
            //        //    valueField: 'id',
            //        //    displayField: 'navn',
            //        //    columnWidth: 0.3
            //        }]
            //    }]
            //}, {
				xtype: 'fieldset', // her er kun de redigerbare felter
				title: 'LAB', // titlen skal måske afhænge af opslag / config fil
                readOnly: (firmatype != 3 && firmatype != 4 && firmatype != 0),
                id: 'lab_vildsvin',
				bodyPadding: 10,
				autoWidth: true,
				autoHeight: true,
				style: 'margin-top: 10px',
				flex: 1,
				items: [{
					xtype: 'fccolumn',
					items: [{
						fieldLabel: 'Modtaget dato',
						xtype: 'datefield',
						id: 'tid_lab_1',
						//margin: '0 0 0 3', // højre
						columnWidth: 0.5,
						//allowBlank: false,
						format: 'd-m-Y',
						altFormats: 'Y-m-d H:i:s|D M Y H:i:s|d-m-Y|d-m-y|Y-m-d|j-n-y|j-n-Y|j-m-y|j-m-Y|d-n-y|d-n-Y|d/m/Y H:i:s|Y-m-dTH:i:s|'
					}, {
						fieldLabel: 'Afsluttet dato',
						xtype: 'datefield',
						id: 'tid_slut_1',
						margin: '0 0 0 3', // højre
						columnWidth: 0.5,
						//allowBlank: true,
						//readOnly: true,
						format: 'd-m-Y',
                        altFormats: 'Y-m-d H:i:s|D M Y H:i:s|d-m-Y|d-m-y|Y-m-d|j-n-y|j-n-Y|j-m-y|j-m-Y|d-n-y|d-n-Y|d/m/Y H:i:s|Y-m-dTH:i:s|'
					}]
				}, {
                    xtype: 'fccolumn',
                    items: [{
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'Lab journalnr.',
                        id: 'journalnr_1',
                        columnWidth: 0.5,
                        margin: '0 3 0 0', // venstre
                        //allowBlank: false,
                        vtype: 'journr',
                        plugins: new Ext.ux.plugin.Formatjournr()
                }, {
                        xtype: 'acombo',
                        hideLabel: false,
                        fieldLabel: 'Aujeszky’s sygdom',
                        id: 'aujeszkys',
                        store: storeResultat_aujeszkys,
                        valueField: 'id',
                        displayField: 'navn',
                        columnWidth: 0.5
                    }]
                }, {
					xtype: 'fccolumn',
					items: [{
						xtype: 'acombo',
						hideLabel: false,
                        fieldLabel: 'Afrikansk svinepest',
						id: 'asf',
                        store: storeResultat_asf,
						valueField: 'id',
						displayField: 'navn',
						columnWidth: 0.5
					}, {
						xtype: 'acombo',
						hideLabel: false,
                        fieldLabel: 'Klassisk svinepest',
						id: 'csf',
                        store: storeResultat_csf,
						valueField: 'id',
						displayField: 'navn',
						columnWidth: 0.5
					}]
				}]
            }, {
                xtype: 'fieldset', // her er kun de redigerbare felter
                title: 'FVST Ringsted', // titlen skal måske afhænge af opslag / config fil
                id: 'lab_ringsted',
                readOnly: (firmatype != 9 && firmatype != 4 && firmatype != 0),
                bodyPadding: 10,
                autoWidth: true,
                autoHeight: true,
                style: 'margin-top: 10px',
                flex: 1,
                items: [{
                    xtype: 'fccolumn',
                    items: [{
                        fieldLabel: 'Modtaget dato',
                        xtype: 'datefield',
                        id: 'tid_lab_2',
                        //margin: '0 0 0 3', // højre
                        columnWidth: 0.5,
                        //allowBlank: false,
                        format: 'd-m-Y',
                        altFormats: 'Y-m-d H:i:s|D M Y H:i:s|d-m-Y|d-m-y|Y-m-d|j-n-y|j-n-Y|j-m-y|j-m-Y|d-n-y|d-n-Y|d/m/Y H:i:s|Y-m-dTH:i:s|'
                    }, {
                        fieldLabel: 'Afsluttet dato',
                        xtype: 'datefield',
                        id: 'tid_slut_2',
                        margin: '0 0 0 3', // højre
                        columnWidth: 0.5,
                        //allowBlank: true,
                        format: 'd-m-Y',
                        altFormats: 'Y-m-d H:i:s|D M Y H:i:s|d-m-Y|d-m-y|Y-m-d|j-n-y|j-n-Y|j-m-y|j-m-Y|d-n-y|d-n-Y|d/m/Y H:i:s|Y-m-dTH:i:s|'
                    }]
                }, {
                    xtype: 'fccolumn',
                    items: [{
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'FVST journalnr.',
                        id: 'journalnr_2',
                        columnWidth: 0.5,
                        margin: '0 3 0 0', // venstre
                        //allowBlank: false,
                        vtype: 'journr',
                        plugins: new Ext.ux.plugin.Formatjournr()
                    }, {
                        xtype: 'acombo',
                        hideLabel: false,
                        fieldLabel: 'Trikiner',
                        id: 'trikiner',
                        store: storeResultat_trikiner,
                        valueField: 'id',
                        displayField: 'navn',
                        columnWidth: 0.5
                    }]
                }]
            }, {
                xtype: 'fieldset',
                title: 'Kommentar til dette vildsvin, kan redigeres selvom posten er låst',
                id: 'fs_kommentar',
                bodyPadding: 10,
                autoWidth: true,
                autoHeight: true,
                style: 'margin-top: 10px',
                flex: 1,
                items: [{
                    xtype: 'fccolumn',
                    items: [{
                        xtype: 'textarea',
                        hideLabel: true,
                        id: 'vildsvin_kommentar',
                        margin: '0',
                        columnWidth: 1,
                        grow: true,
                        growMin: 30,
                        growMax: 300
                    }]
                }]
			}, tegn_fs_anmelder(), tegn_fs_fund(true), tegn_fs_fotografi() 
		],
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'bottom',
			//ui: 'footer',
			//defaults: { minWidth: minButtonWidth },
			items: [{
			xtype: 'button',
		text: 'Ret låst post',
		id: 'btn_laast',
		iconCls: 'icon-edit',
		style: 'margin-right: 15px',
		hidden: true,
		handler: function ()
		{
			//enableCtl(Ext.getCmp('akt_fugl'), true);
			retLaastPost = true;
		}
	}, {
			xtype: 'button',
			text: 'Skjul',
			id: 'skjulmenu_0',
			iconCls: 'icon-remove',
			style: 'margin-right: 15px',
			menu: skjulMenu,
			hidden: (firmatype == '0' || firmatype == '4' ? false : true)
		}, '->', {
			xtype: 'button',
			text: 'Gem',
			id: 'btn_gem',
			iconCls: 'icon-ok',
				handler: function ()
				{
					if (aktRec == undefined)
					{
						fejlbox('Der er ingen post at gemme ændringer i.');
						return;
					}

					// opret et objekt med gemme-værdierne
					var g = {};
					aktRec.fields.each(function (field) 
					{
						g[field.name] = aktRec.get(field.name);
						try
						{
							g[field.name] = Ext.getCmp(field.name).getValue();
						} catch (e)
						{

						}
						if (g[field.name] == null)
							g[field.name] = '';

					});

					// hvad må man gemme - ud fra hvem man er og hvad status er?
					// skal knappen låses, når der ikke er nogen post?
					// hvis posten er låst, kan man kun gemme kommentaren - men det kan man så også bare gøre
					if (aktRec.data.status >= 6) // låst, gem kun kommentar
					{

						// send til serveren
						var param = {
							vildsvin: aktRec.data.id,
							vildsvin_kommentar: Ext.getCmp('vildsvin_kommentar').getValue()
						};
						var params = Ext.JSON.encode(param);
						Ext.Ajax.request({
							url: 'Webservices/svin_login.asmx/gemvildsvin_kommentar',
							params: params,
							headers: { 'Content-Type': 'application/json;charset=utf-8' },
							success: function (response, opts)
							{
								var opdatvar = new Array();
								opdatvar.push('vildsvin_kommentar');
								opdatvar.push(Ext.getCmp('vildsvin_kommentar').getValue());
								opdaterRaekke(opdatvar);
							},
							failure: ajaxFailed
						});
					}
					else // gemmeproceduren kan vel godt være den samme for alle relevante gemmere, de kan jo kun ændre hver deres del
					{
						// er udfyldningen korrekt?
						var fejltekst = '';


						// først ringsted
						if ((aktRec.data.fundtype == '20' || aktRec.data.fundtype == '21') && (firmatype == 9 || firmatype == 4 || firmatype == 0)) // trikiner, kun aktiv ved nedlagte
						{
							// så skal det tjekkes og gemmes
							// er der overhovedet udfyldt noget i denne blok?
							if (g.tid_lab_2 != '' || g.journalnr_2 != '' || Number(g.trikiner) >= 2 || g.tid_slut_2 != '')
							{
								// modtaget lab skal udfyldes ved alle gyldige svar
								if (g.tid_lab_2 == '')
									fejltekst += '\nModtaget dato hos FVST Ringsted skal udfyldes';
								else if (Ext.getCmp('tid_lab_2').isValid() === false)
									fejltekst += '\nModtaget dato hos FVST Ringsted er ikke udfyldt korrekt ';
								// journalnummer skal udfyldes ved alle gyldige svar
								if (g.journalnr_2 == '')
									fejltekst += '\nJournalnummer hos FVST Ringsted skal udfyldes med et nummer i formatet XX-XXXXX-XX';
								else if (Ext.getCmp('journalnr_2').isValid() === false)
									fejltekst += '\nJournalnummer hos FVST Ringsted skal udfyldes med et nummer i formatet XX-XXXXX-XX';
								// afsluttet, så skal der være resultat
								if (g.tid_slut_2 != '' && Ext.getCmp('tid_slut_2').isValid() === true)
								{
									if (Number(g.trikiner) < 2)
										fejltekst += '\nTrikiner skal udfyldes, når afsluttet dato er udfyldt';
								}
								if (g.tid_slut_2 != '' && Ext.getCmp('tid_slut_2').isValid() === false)
									fejltekst += '\nAfsluttet dato hos FVST Ringsted er ikke udfyldt korrekt ';

							}
						}
						if (firmatype == 3 || firmatype == 4 || firmatype == 0) // øvrige tests
						{
							// så skal det tjekkes og gemmes
							// er der overhovedet udfyldt noget i denne blok?
							if (g.tid_lab_1 != '' || g.journalnr_1 != '' || g.tid_slut_1 != '' || Number(g.asf) >= 2 || Number(g.csf) >= 2 || Number(g.aujeszkys) >= 2)
							{
								// modtaget lab skal udfyldes ved alle gyldige svar
								if (g.tid_lab_1 == '')
									fejltekst += '\nModtaget dato hos LAB skal udfyldes';
								else if (Ext.getCmp('tid_lab_1').isValid() === false)
									fejltekst += '\nModtaget dato hos LAB er ikke udfyldt korrekt ';
								// journalnummer skal udfyldes ved alle gyldige svar
								if (g.journalnr_1 == '')
									fejltekst += '\nJournalnummer hos LAB skal udfyldes med et nummer i formatet XX-XXXXX-XX';
								else if (Ext.getCmp('journalnr_1').isValid() === false)
									fejltekst += '\nJournalnummer hos LAB skal udfyldes med et nummer i formatet XX-XXXXX-XX';

								// afsluttet, så skal alle resultat-felter være udfyldt
								if (g.tid_slut_1 != '' && Ext.getCmp('tid_slut_1').isValid() === true)
								{
									if (Number(g.asf) < 2)
										fejltekst += '\nAfrikansk svinepest skal udfyldes, når afsluttet dato er udfyldt';
									if (Number(g.csf) < 2)
										fejltekst += '\nKlassisk svinepest skal udfyldes, når afsluttet dato er udfyldt';
									if (Number(g.aujeszkys) < 2)
										fejltekst += '\nAujeszky’s sygdom skal udfyldes, når afsluttet dato er udfyldt';
								}
								if (g.tid_slut_1 != '' && Ext.getCmp('tid_slut_1').isValid() === false)
									fejltekst += '\nAfsluttet dato hos LAB er ikke udfyldt korrekt ';
							}
						}

						if (fejltekst !== '')
						{
							fejlbox(fejltekst);
							return;
						}


						// send til serveren
						var param = { g: g };
						var params = Ext.JSON.encode(param);


						Ext.Ajax.request({
							url: 'Webservices/svin_login.asmx/gemLabProeve',
							params: params,
							headers: { 'Content-Type': 'application/json;charset=utf-8' },
							success: function (response, opts)
							{
								if (retLaastPost)
								{
									storeLabListe.reload();
								}
								else
								{
									// ret kun den aktuelle række
									var ret = Ext.JSON.decode(response.responseText).d;
									var opdatvar = new Array();
									opdatvar.push('vildsvin_kommentar');
									opdatvar.push(ret.vildsvin_kommentar);
									opdatvar.push('status');
									opdatvar.push(ret.status);
									opdatvar.push('tid_lab');
									opdatvar.push(ret.tid_lab);
									opdatvar.push('tid_slut');
									opdatvar.push(ret.tid_slut);

									opdatvar.push('journalnr_1');
									opdatvar.push(ret.journalnr_1);
									opdatvar.push('tid_lab_1');
									opdatvar.push(ret.tid_lab_1);
									opdatvar.push('tid_slut_1');
									opdatvar.push(ret.tid_slut_1);

									opdatvar.push('journalnr_2');
									opdatvar.push(ret.journalnr_2);
									opdatvar.push('tid_lab_2');
									opdatvar.push(ret.tid_lab_2);
									opdatvar.push('tid_slut_2');
									opdatvar.push(ret.tid_slut_2);

									opdatvar.push('asf');
									opdatvar.push(ret.asf);
									opdatvar.push('csf');
									opdatvar.push(ret.csf);
									opdatvar.push('aujeszkys');
									opdatvar.push(ret.aujeszkys);
									opdatvar.push('trikiner');
									opdatvar.push(ret.trikiner);

									opdaterRaekke(opdatvar);
								}
								visLabAkt();
							},
							failure: ajaxFailed
						});
					}
				}
			}]
		}],
		listeners: {
		    afterrender: function()
		    {
		//        LabListe_grid.down('[dataIndex=skjul]').setVisible(false);
		    }
		}
	});
	aktGrid = 'LabListe_grid';
	// vi skal sikre at de indledende filtre sættes
	//filters.createFilters();

	retKolonner(LabListe_grid, 6);

	storeLabListe.load();
	//LabListe_panel .doLayout();
	//LabListe_grid.down('[dataIndex=skjul]').setVisible(false);

}



function visLabAkt(id) {
    var redfugl = false;
	Ext.getCmp('btn_laast').hide();
	visFelter(storeLabListe, id);




        //readonly er sat på grundlag af firmatype - nu kan vi sætte disabled på grundlag af fundtype
		//if (id != undefined)
			Ext.getCmp('lab_ringsted').setDisabled(id != undefined && aktRec.data.fundtype != '20' && aktRec.data.fundtype != '21' ); // trikiner, kun aktiv ved nedlagte 
        //Ext.getCmp('lab_vildsvin').setDisabled(aktRec.data.fundtype == '20' || aktRec.data.fundtype == '21'); // trikiner, kun aktiv ved nedlagte 
	//}
}


