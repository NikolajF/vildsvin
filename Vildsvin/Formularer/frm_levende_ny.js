﻿function visLevende() {
	showMap();
	toemFormular('Levende_panel', 'visLevende');


	Ext.define('mLevende', {
		extend: 'Ext.data.Model',
		fields: [
			'id',
			'fv_region',
			'afd',
			'setx',
			'sety',
			'set_tid',
			'status',
			{ name: 'fundsted', type: 'string', defaultValue: '' },
			{ name: 'pdf', type: 'boolean', defaultValue: false },
			{ name: 'an_navn', type: 'string', defaultValue: '' },
			{ name: 'an_adresse', type: 'string', defaultValue: '' },
			{ name: 'an_telefon', type: 'string', defaultValue: '' },
			{ name: 'an_email', type: 'string', defaultValue: '' },
			{ name: 'set_adresse', type: 'string', defaultValue: '' },
            'setpostnr',
            'setbeskriv',
            'fundtype',
            'antal',
            'kommentar',
			'skjul',
			'reserveret_af_id',
			'billede'
		],
		idProperty: 'id'
	});

	var storeLevende = Ext.create('Ext.data.JsonStore', {
		storeId: 'st_Levende',
		pageSize: 10,
		model: 'mLevende',
		remoteSort: true,
		proxy: {
			type: 'gridmap',
			url: 'Webservices/svin_login.asmx/hentLevende'
		},
		sorters: [{
			property: 'set_tid',
			direction: 'DESC'
		}],
		listeners: {
			load: function (store) {
			    retLaastPost = false;
			    mapPanel.redrawLayers('indberetninger');
			    aiz = [0, 0, 0, 0];
				var xmin, xmax, ymin, ymax;
				// nulstil felterne 
				Ext.getCmp('akt_indberetning').setTitle('Indberetning - ingen valgt');
				Ext.getCmp('check_fund').setValue(false);
			    //Ext.getCmp('tx_indsamler').setOriginal('0');
				Ext.getCmp('tx_an_navn').setValue('');
				Ext.getCmp('tx_an_adresse').setValue('');
				Ext.getCmp('tx_an_telefon').setValue('');
				Ext.getCmp('tx_an_email').setValue('');
				Ext.getCmp('tx_set_adresse').setValue('');
				Ext.getCmp('tx_fundsted').setValue('');
				Ext.getCmp('setbeskriv').setValue('');
				Ext.getCmp('tx_fundtype').setValue('');
				Ext.getCmp('tx_antal').setValue('');
//				Ext.getCmp('vis_pdf').hide();

				// løb de nye poster igennem
	//			Ext.getCmp('ret_indberetning').disable();
		//		Ext.getCmp('btn_laast').hide();
				if (store.getCount() == 0) {
					//Ext.getCmp('selectonmap').disable();
					Ext.getCmp('zoomliste').disable();
					Ext.getCmp('zoommarkeret').disable();
				} else {
					//Ext.getCmp('selectonmap').enable();
					Ext.getCmp('zoomliste').enable();
					Ext.getCmp('zoommarkeret').enable();
					//  nulstilling
					xmin = 0;
					xmax = xmin;
					ymin = 0;
					ymax = ymin;
					store.each(function (record) {
						if (record.data.setx != null && record.data.setx != '0') {
							if (xmin == 0) {
								xmin = Number(record.data.setx);
								xmax = xmin;
								ymin = Number(record.data.sety);
								ymax = ymin;
							} else {
								if (Number(record.data.setx) < xmin) xmin = Number(record.data.setx);
								if (Number(record.data.setx) > xmax) xmax = Number(record.data.setx);
								if (Number(record.data.sety) < ymin) ymin = Number(record.data.sety);
								if (Number(record.data.sety) > ymax) ymax = Number(record.data.sety);
							} // slut if 0
						}
					}); // slut each
					if ((xmax - xmin) < 200) {
						xmidt = (Number(xmax) + Number(xmin)) / 2;
						xmax = Number(xmidt) + 100;
						xmin = Number(xmidt - 100);
					}
					if ((ymax - ymin) < 200) {
						ymidt = (Number(ymax) + Number(ymin)) / 2;
						ymax = Number(ymidt) + 100;
						ymin = Number(ymidt - 100);
					}

					zmListe = [xmin, ymin, xmax, ymax];
					if (Ext.getCmp('zoomliste').pressed == true) mapPanel.zoomTo(zmListe);

				} // slut count
				// sæt de aktuelle filtre på Excel-udtræk
				Ext.get('btn_excel').down('a').dom.href = 'VisFil.ashx?type=xl_levende&id=' + brugerid + '&gridfilter=' + store.proxy.reader.rawData.d.filterkrit;
			}

		}
	});
	storeAkt = storeLevende;
	aktGrid = 'visit_grid';
	mapPanel.config.layers.data.indberetninger.setVisibility(true);


	var filters = Ext.create('Ext.ux.grid.FiltersFeature', {
		// encode and local configuration options defined previously for easier reuse
		encode: true, // json encode the filter query
		local: false   // defaults to false (remote filtering)
	});
	var LevendeGrid = Ext.create('Ext.grid.Panel', {
		layout: 'fit',
		id: 'visit_grid',
		autoHeight: true,
		autoWidth: true,
		title: '',
		store: storeLevende,
		disableSelection: false,
		loadMask: true,
		features: [filters],
		forceFit: true,
		viewConfig: {
			id: 'vg',
			//trackOver: false,
			//stripeRows: true
			getRowClass: function (record, rowIndex, rowParams, store) {
				return (record.get('reserveret_af_id') == '0' && Number(record.get('fundtype')) < 20 ? '' : 'readonly');
			}
		},
		// grid columns
		// specify any defaults for each column

		columns: {
		    defaults: { renderer: renderTooltip },
		    items: [{
//		        id: 'id',
		        text: "Indberetning",
		        tooltip: "Indberetning",
		        dataIndex: 'id',
		        width: frm_levende.LevendeGrid.id.width,
		        hidden: frm_levende.LevendeGrid.id.hidden,
		        //flex: 1,
		        filter: { type: 'numeric' }
		    }, {
		        text: "Antal",
		        tooltip: "Antal",
		        dataIndex: 'antal',
		        width: frm_levende.LevendeGrid.antal.width,
		        hidden: frm_levende.LevendeGrid.antal.hidden,
		        filterable: true,
		        sortable: true
		    }, {
		        text: "Fundtype",
		        tooltip: "Fundtype",
		        dataIndex: 'fundtype',
		        width: frm_levende.LevendeGrid.fundtype.width,
		        hidden: frm_levende.LevendeGrid.fundtype.hidden,
		        filterable: true,
		        sortable: true,
		        renderer: renderFundtyper,
		        filter: {
		            type: 'list',
		            store: storeFundtyperLev,
		            idField: 'id',
		            labelField: 'navn'
                }
		    }, {
		        text: "Registrator",
		        tooltip: "Registrator",
		        dataIndex: 'afd',
		        width: frm_levende.LevendeGrid.afd.width,
		        hidden: frm_levende.LevendeGrid.afd.hidden,
		        filterable: true,
		        sortable: true,
		        flex: 1,
		        renderer: renderFirma,
		        filter: {
		            type: 'list',
		            store: storeFirma,
		            idField: 'id',
		            labelField: 'navn'//,
		            //value: (firmatype == '2' ? firma : undefined),
		            //active: (firmatype == '2' ? true : false)
		        }
		    }, {
		        text: "Enhed",
		        tooltip: "Lokal enhed i Naturstyrelsen",
		        dataIndex: 'status',
		        width: frm_levende.LevendeGrid.status.width,
		        hidden: frm_levende.LevendeGrid.status.hidden,
		        filterable: true,
		        sortable: true,
		        flex: 1,
		        renderer: renderFirma,
		        filter: {
		            type: 'list',
		            store: storeFirmaVisit,
		            idField: 'id',
		            labelField: 'navn'//,
		            //value: '0',
		            //active: true
		        }
		    }, {
		        text: "Noteret",
		        tooltip: "Noteret",
		        dataIndex: 'reserveret_af_id',
		        width: frm_levende.LevendeGrid.reserveret_af_id.width,
		        hidden: frm_levende.LevendeGrid.reserveret_af_id.hidden,
		        filterable: true,
		        sortable: true,
		        renderer: renderNoteret,
		        filter: {
		            type: 'list',
		            store: storeNoteret,
		            idField: 'id',
		            labelField: 'navn',
		            value: '0',
		            active: true
		        }
		    }, {
		        text: "Postnr",
		        tooltip: "Postnr",
		        dataIndex: 'setpostnr',
		        width: frm_levende.LevendeGrid.setpostnr.width,
		        hidden: frm_levende.LevendeGrid.setpostnr.hidden,
		        filterable: true,
		        //flex: 1,
		        sortable: true
		    }, {
		        text: "Fund dato",
		        tooltip: "Fund dato",
		        dataIndex: 'set_tid',
		        width: frm_levende.LevendeGrid.set_tid.width,
		        hidden: frm_levende.LevendeGrid.set_tid.hidden,
		        filterable: true,
		        //flex: 1,
		        sortable: true,
                renderer: renderDato,
		        filter: {
		            type: 'date'
		        }
		    }, {
		        text: "Vis / Skjul",
		        tooltip: "Vis / Skjul",
		        dataIndex: 'skjul',
		        width: frm_levende.LevendeGrid.skjul.width,
		        hidden: frm_levende.LevendeGrid.skjul.hidden,
		        filterable: true,
		        //flex: 1,
		        sortable: true,
		        renderer: renderVis,
		        filter: {
		            type: 'list',
		            store: storeSkjul,
		            idField: 'id',
		            labelField: 'navn',
		            value: '0',
		            active: true
		        }
		    }]
		},
	    // gridkol_slut
		selModel: Ext.create('Ext.selection.RowModel', {
			listeners: {
				select: {
					fn: function (selModel, rec, index) {
					    retLaastPost = false;
					    aktRec = rec;
						aktRk = index;
						aiz = [0, 0, 0, 0];
						aiz[0] = Number(rec.data.setx - 100);
						aiz[1] = Number(rec.data.sety - 100);
						aiz[2] = Number(Number(rec.data.setx) + 100);
						aiz[3] = Number(Number(rec.data.sety) + 100);

						if (Ext.getCmp('zoommarkeret').pressed == true)
							mapPanel.zoomTo(aiz);

						// ??? sæt knapper aktive og inaktive


						mapPanel.clearLayers();
						mapPanel.addSelectedPoint(rec.data.setx, rec.data.sety);
						Ext.getCmp('akt_indberetning').setTitle('Indberetning ' + rec.data.id);
						Ext.getCmp('check_fund').setValue(rec.data.reserveret_af_id > 0);
						Ext.getCmp('check_fund').setDisabled(rec.data.reserveret_af_id > 0); // allerede noteret
						Ext.getCmp('tx_kommentar').setValue(rec.data.kommentar);

						Ext.getCmp('tx_an_navn').setValue(aktRec.data.an_navn);
						Ext.getCmp('tx_an_adresse').setValue(aktRec.data.an_adresse);
						Ext.getCmp('tx_an_telefon').setValue(aktRec.data.an_telefon);
						Ext.getCmp('tx_an_email').setValue(aktRec.data.an_email);
						Ext.getCmp('tx_set_adresse').setValue(aktRec.data.set_adresse);
						Ext.getCmp('tx_fundsted').setValue(aktRec.data.fundsted);
						Ext.getCmp('setbeskriv').setValue(rec.data.setbeskriv);
						Ext.getCmp('tx_fundtype').setValue(rec.data.fundtype);
						Ext.getCmp('tx_antal').setValue(rec.data.antal);
					    // billedet
						Ext.getCmp('billede').setSrc(rec.data.billede);
						var visbil = false;
						if (rec.data.billede != null && rec.data.billede != '') 
							visbil = true;
						Ext.getCmp('f_billede').setVisible(visbil);

			//			Ext.getCmp('vis_pdf').hide();
				//		Ext.getCmp('btn_laast').hide();

						// sæt fieldset disabled eller aktiv
						if (rec.data.skjul != '0') {
							Ext.getCmp('akt_indberetning').setTitle('Vis skjult indberetning (' + storeSkjul.getById(rec.data.skjul).data.navn + ')');
							Ext.getCmp('akt_indberetning').disable();
					//		Ext.getCmp('tx_indsamler').hide();
							//Ext.getCmp('ret_indberetning').enable();
							//Ext.getCmp('ret_indberetning').text = 'Vis indberetning';
						}
						//else if (rec.data.visitator == '0') {
						//	Ext.getCmp('akt_indberetning').enable();
						//	//Ext.getCmp('tx_indsamler').show();
						//	//Ext.getCmp('tx_indsamler').setOriginal(rec.data.status);
						//	//Ext.getCmp('ret_indberetning').enable();
						//	//Ext.getCmp('ret_indberetning').text = 'Ret indberetning';


						//}
						else {
						    Ext.getCmp('akt_indberetning').enable();

						    //Ext.getCmp('tx_indsamler').hide();
							//Ext.getCmp('ret_indberetning').setText('Vis indberetning');
							//Ext.getCmp('ret_indberetning').enable();
							//if (rec.data.pdf)
							//{
							//    Ext.getCmp('vis_pdf').show();
							//    Ext.getCmp('vis_pdf').href = Ext.getCmp('vis_pdf').htmp + rec.data.id;
							//    Ext.getDom('vis_pdf' + '-btnEl').href = Ext.getCmp('vis_pdf').href;
							//    Ext.getCmp('vis_pdf').enable();
							//}
                        }
					    // er der noget at låse op for dem der kan?
						if (Ext.getCmp('akt_indberetning').disabled && (firmatype == '0' || firmatype == '4')) {
						    Ext.getCmp('btn_laast').show();
						    Ext.getCmp('btn_laast').setDisabled(false);
						}

					}
				}
			}
		}),
		// paging bar on the bottom
		tbar: Ext.create('Ext.PagingToolbar', {
			store: storeLevende,
			displayInfo: true
		}),
		bbar: createZoombar(true)

	});

	
	LevendeGrid.child('pagingtoolbar').add(['->', {
			text: 'Nulstil alle filtre',
			handler: function () {
				LevendeGrid.filters.clearFilters();
				x1 = 0;
				y1 = 0;
				x2 = 0;
				y2 = 0;

			}
		}]);


	var Levende = Ext.create('Ext.panel.Panel', {
	    title: 'Levende vildsvin og spor efter vildsvin',
		id: 'Levende_panel',
		layout: 'anchor',
	    anchor: '-20',
		bodyPadding: '10 10 10 10',
		autoWidth: true,
		autoHeight: false,
		renderTo: 'vestdiv',
        autoScroll: true,
		buttonAlign: 'right',
		items: [
			LevendeGrid,
			{
				xtype: 'fieldset',
				title: 'Indberetning ',
				id: 'akt_indberetning',
				bodyPadding: 10,
				autoWidth: true,
				autoHeight: true,
				style: 'margin-top: 10px',
				flex: 1,
				items: [{
				xtype: 'fccolumn',
				items: [{
				    xtype: 'checkbox',
				    boxLabel: 'Fundet er noteret',
				    id: 'check_fund',
				    //style: 'margin-right: 5px',
				    margin: '0 3 0 0', // venstre
				    checked: false,
                    disabled: true,
				    columnWidth: 1,
				    handler: function (field, value) {
				        scope: this,
                        this.checkValue = field.getValue();
				        console.log(this.checkValue);
				        if (this.checkValue == true) {
				            //this.store.filter('Cold', 'Yes');
				            // send til serveren
				            var param = { id: aktRec.data.id };
				            var params = Ext.JSON.encode(param);

				            Ext.Ajax.request({
				                url: 'Webservices/svin_login.asmx/gemNoteret',
				                params: params,
				                headers: { 'Content-Type': 'application/json;charset=utf-8' },
				                success: function (response, opts) {
				                    var opdatvar = new Array();
				                    opdatvar.push('reserveret_af_id');
				                    opdatvar.push(brugerid);
				                    opdaterRaekke(opdatvar);
				                },
				                failure: ajaxFailed
				            });
				        }
				    }
				}]
			}, {
				xtype: 'toolbar',
				style: 'margin-top: 6px',
				hidden: (firmatype == '0' || firmatype == '4' ? false : true),
				items: ['->', {
				//	xtype: 'button',
				//	text: 'Ret indberetning',
				//	id: 'ret_indberetning',
				//	style: 'margin-right: 15px',
				//	handler: function () {
				//		indberetningsformular(aktRec.data.id);
				//	}
				//}, {
				//    xtype: 'button',
				//    //text: 'Ret indberetning',
				//    id: 'vis_pdf',
                //    tooltip: 'Vis PDF køreseddel',
                //    iconCls: 'icon-pdf',
                //    hidden: true,
                //    href: 'VisFil.ashx?type=pdf&id=0',
                //    htmp: 'VisFil.ashx?type=pdf&id=',
                //    target: '_blank',
                //    menu: {
                //        xtype: 'menu',
                //        items: [{
                //            xtype: 'menuitem',
                //            id: 'menuPDF',
                //            text: 'Opret køreseddel',
                //            handler: function()
                //            {
                //                Ext.Ajax.request({
                //                    url: 'Webservices/svin_login.asmx/lavKoereseddel',
                //                    params: JSON.stringify({
                //                        indberetning: aktRec.data.id
                //                    }),
                //                    headers: { 'Content-Type': 'application/json;charset=utf-8' },
                //                    success: function (response, opts) {
                //                        okbesked('Der er nu lavet en ny køreseddel');
                //                    },
                //                    failure: ajaxFailed
                //                });


                //            }
                //        }]
                //    }

			    //}, '->', {
                    xtype: 'button',
                    text: 'Ret låst post',
                    id: 'btn_laast',
                    iconCls: 'icon-edit',
                    style: 'margin-right: 15px',
                    hidden: true,
                    handler: function () {
                        enableCtl(Ext.getCmp('akt_indberetning'), true);
                        Ext.getCmp('tx_indsamler').show();
                        retLaastPost = true;
//                        okbesked('Når du er færdig med at redigere den post, du lige har låst op, er det en god ide at trykke på knappen Opfrisk - ellers er det ikke sikkert at du kan se resultatet af din ændring.');
                    }
				}, {					
				    xtype: 'button',
					text: 'Skjul',
					id: 'skjulmenu_0',
					iconCls: 'icon-remove',
					style: 'margin-right: 15px',
					menu: skjulMenu,
					hidden: (firmatype == '0' || firmatype == '4' ? false : true)
				//}, {
				//	xtype: 'button',
				//	text: 'Udfør',
				//	id: 'visit_ok',
				//	iconCls: 'icon-ok',
				//	handler: function () {
				//		// opret et objekt med gemme-værdierne
				//		var g = {
				//			indberetning: aktRec.data.id,
				//			setx: aktRec.data.setx,
				//			sety: aktRec.data.sety,
				//			setpostnr: aktRec.data.setpostnr,
				//			skjul: aktRec.data.skjul,
				//			afd: Ext.getCmp('tx_indsamler').getValue(),
				//			kommentar: Ext.getCmp('tx_kommentar').getValue()
				//		};
				//		// nu skal KUN de ændrede rækker tilføjes til aInd
				//		var nr = 0;

				//		// er udfyldningen korrekt?
				//		// der skal være valgt noget i afd
				//		if (g.afd == '0' ) {
				//			fejlbox('Du skal foretage et valg i feltet Indsamler.');
				//			return;
				//		}


				//		// send til serveren
				//		var param = { g: g };
				//		var params = Ext.JSON.encode(param);


				//		Ext.Ajax.request({
				//			url: 'Webservices/svin_login.asmx/gemLevende',
				//			params: params,
				//			headers: { 'Content-Type': 'application/json;charset=utf-8' },
				//			success: function (response, opts) {
				//			    if (retLaastPost) {
				//			        storeLevende.reload();
				//		        }
				//		        else {
				//			        // ret kun den aktuelle række
				//				    var ret = Ext.JSON.decode(response.responseText).d;
				//				    var opdatvar = new Array();
				//				    opdatvar.push('status');
				//				    opdatvar.push(ret.afd);
				//				    //opdatvar.push('skjul');
				//				    //opdatvar.push(ret.skjul);
				//				    opdatvar.push('visitator');
				//				    opdatvar.push(ret.visitator);
				//				    opdatvar.push('kommentar');
				//				    opdatvar.push(ret.kommentar);
				//				    opdaterRaekke(opdatvar);
				//				    // og så skal vi have genvalgt rækken i griddet
				//				    //aktGrid.getSelectionModel().fireEvent('select', aktGrid.getSelectionModel(), aktRec, aktRk);
				//			    }
				//			},
				//			failure: ajaxFailed
				//		});

				//	}
				}]
			}]
            }, {
                xtype: 'fieldset',
                title: 'Kommentar, kan redigeres selvom indberetningen er låst',
                id: 'fs_kommentar',
                bodyPadding: 10,
                autoWidth: true,
                autoHeight: true,
                style: 'margin-top: 10px',
                flex: 1,
                items: [{
                    xtype: 'fccolumn',
                    items: [{
                        xtype: 'textarea',
                        hideLabel: true,
                        id: 'tx_kommentar',
                        margin: '0',
                        columnWidth: 0.8,
                        grow: true,
                        growMin: 30,
                        growMax: 300
                    }, {
                        xtype: 'displayfield',
                            columnWidth: 0.02,
                            hideLabel: true,
                            value: ''

                    }, {
                        xtype: 'button',
                        text: 'Gem',
                        tooltip: 'Gem kommentar',
                        id: 'gem_kommentar',
                        iconCls: 'icon-ok',
                        columnWidth: 0.18,
                        handler: function () {
                            // kun hvis der er en post
                            if (aktRec != undefined) {
                                // send til serveren
                                var param = {
                                    indberetning: aktRec.data.id,
                                    kommentar: Ext.getCmp('tx_kommentar').getValue()
                                };
                                var params = Ext.JSON.encode(param);
                                Ext.Ajax.request({
                                    url: 'Webservices/svin_login.asmx/gemindberetning_kommentar',
                                    params: params,
                                    headers: { 'Content-Type': 'application/json;charset=utf-8' },
                                    success: function (response, opts) {
                                        var opdatvar = new Array();
                                        opdatvar.push('kommentar');
                                        opdatvar.push(Ext.getCmp('tx_kommentar').getValue());
                                        opdaterRaekke(opdatvar);
                                    },
                                    failure: ajaxFailed
                                });
                            }
                        }
                    }]
                }]
			}, {
				xtype: 'fieldset',
				title: 'Ekstra oplysninger',
				id: 'ekstra_indberetning',
				bodyPadding: 10,
				autoWidth: true,
				autoHeight: true,
				style: 'margin-top: 10px',
				flex: 1,
				items: [{
				    xtype: 'fccolumn_ro',
				    items: [{
				        xtype: 'textfield',
				        hideLabel: false,
				        fieldLabel: 'Anmelders navn',
				        id: 'tx_an_navn',
				        margin: '0 3 0 0', // venstre
				        columnWidth: 1
				    }]
				}, {
				    xtype: 'fccolumn_ro',
				    items: [{
				        xtype: 'textfield',
				        hideLabel: false,
				        fieldLabel: 'Anmelders adresse',
				        id: 'tx_an_adresse',
				        margin: '0 3 0 0', // venstre
				        columnWidth: 1
				    }]
				}, {
				    xtype: 'fccolumn_ro',
				    items: [{
				        xtype: 'textfield',
				        hideLabel: false,
				        fieldLabel: 'Anmelders telefon',
				        id: 'tx_an_telefon',
				        margin: '0 3 0 0', // venstre
				        columnWidth: 0.5
				    }, {
				        xtype: 'textfield',
				        hideLabel: false,
				        fieldLabel: 'Email',
				        id: 'tx_an_email',
				        margin: '0 3 0 0', // venstre
				        columnWidth: 0.5
				    }]
				}, {
				    xtype: 'fccolumn_ro',
				    items: [{
				        xtype: 'textfield',
				        hideLabel: false,
				        fieldLabel: 'Fundets adresse',
				        id: 'tx_set_adresse',
				        margin: '0 3 0 0', // venstre
				        columnWidth: 1
				    }]
				}, {
				    xtype: 'fccolumn_ro',
				    items: [{
				        xtype: 'textfield',
				        hideLabel: false,
				        fieldLabel: 'Fundsted',
				        id: 'tx_fundsted',
				        margin: '0 3 0 0', // venstre
				        columnWidth: 1
				    }]
				}, {
				    xtype: 'fccolumn_ro',
				    items: [{
				        xtype: 'textfield',
				        hideLabel: false,
				        fieldLabel: 'Antal',
				        id: 'tx_antal',
				        margin: '0 3 0 0', // venstre
				        columnWidth: 0.2
				    }, {
				        xtype: 'combo',
				        hideLabel: false,
				        fieldLabel: 'Fundtype',
				        id: 'tx_fundtype',
				        margin: '0 3 0 0', // venstre
				        store: storeFundtyper,
				        valueField: 'id',
				        displayField: 'navn',
				        columnWidth: 0.8
				    }]
				}, {
				    xtype: 'fccolumn_ro',
					items: [{
						xtype: 'textarea',
						fieldLabel: 'Beskrivelse af fundet',
						id: 'setbeskriv',
						margin: '0',
						columnWidth: 1,
						grow: true,
						growMin: 30,
						growMax: 300
					}]
				}]
			}, {
				xtype: 'fieldset',
				title: 'Fotografi',
				id: 'f_billede',
				bodyPadding: 10,
				autoWidth: true,
				autoHeight: true,
				hidden: true,
				style: 'margin-top: 10px',
				flex: 1,
				items: [{
					xtype: 'image',
					id: 'billede',
					src: '',
					hidden: false,
					shrinkWrap:true, 
					width: '100%'
				}]
			}]
	});
	// vi skal sikre at de indledende filtre sættes
	filters.createFilters();
	storeLevende.load();

	retKolonner(LevendeGrid, 8);

}


