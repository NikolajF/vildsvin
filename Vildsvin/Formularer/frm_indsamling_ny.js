﻿function visIndsamlinger()
{
	showMap();
	toemFormular('Indsamlinger_panel', 'visIndsamlinger');


	Ext.define('mIndsamlinger', {
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'id', type: 'string', defaultValue: '0' },
			{ name: 'afd', type: 'string', defaultValue: '0' },
			{ name: 'afd_ny', type: 'string', defaultValue: '0' },
			{ name: 'indberetning', type: 'string', defaultValue: '0' },
			{ name: 'postnr', type: 'string', defaultValue: '' },
			{ name: 'status', type: 'string', defaultValue: '0' },
			//{ name: 'setx', type: 'string', defaultValue: '0' },
			//{ name: 'sety', type: 'string', defaultValue: '0' },
			{ name: 'vildsvin_kommentar', type: 'string', defaultValue: '' },
			{ name: 'skjul', type: 'string', defaultValue: '0' },
			{ name: 'x', type: 'string', defaultValue: '0' },
			{ name: 'y', type: 'string', defaultValue: '0' },
			//{ name: 'set_tid', type: 'string', defaultValue: '' },
			{ name: 'tid_indsamlet', type: 'string', defaultValue: '' },
			//{ name: 'fundsted', type: 'string', defaultValue: '' },
			{ name: 'pdf', type: 'boolean', defaultValue: false },
			//{ name: 'an_navn', type: 'string', defaultValue: '' },
			//{ name: 'an_adresse', type: 'string', defaultValue: '' },
			//{ name: 'an_telefon', type: 'string', defaultValue: '' },
			//{ name: 'an_email', type: 'string', defaultValue: '' },
			//{ name: 'set_adresse', type: 'string', defaultValue: '' },
			//{ name: 'kommentar', type: 'string', defaultValue: '' },
			//{ name: 'billede', type: 'string', defaultValue: '' },
			{ name: 'opsamler', type: 'string', defaultValue: '' },
			// ekstra oplysninger fra indberetningen
			{ name: 'fundtype', type: 'string', defaultValue: '0' },
			'antal',
			'an_navn',
			'set_tid',
			'an_vejnavn',
			'an_husnr',
			'an_postnr',
			'an_by',
			'an_telefon',
			'an_email',
			'billede',
			'setbeskriv',
			'setvejnavn',
			'sethusnr',
			'setpostnr',
			'setby',
			'setx',
			'sety',
			'fundsted',
			'indberetning_kommentar'
		],
		idProperty: 'id'
	});

	storeIndsamlinger = Ext.create('Ext.data.JsonStore', {
		storeId: 'st_Indsamlinger',
		pageSize: 10,
		model: 'mIndsamlinger',
		remoteSort: true,
		autoLoad: false,
		proxy: {
			type: 'gridmap',
			url: 'Webservices/svin_login.asmx/hentIndsamlinger'
		},
		sorters: [{
			property: 'id',
			direction: 'DESC'
		}],
		listeners: {
			load: function (store)
			{
				retLaastPost = false;
				mapPanel.redrawLayers('vildsvin_doede')
				aktRec = undefined;
				aiz = [0, 0, 0, 0];
				var xmin, xmax, ymin, ymax;

				// løb de nye poster igennem
				if (store.getCount() === 0)
				{
					//	Ext.getCmp('selectonmap').disable();
					Ext.getCmp('zoomliste').disable();
					Ext.getCmp('zoommarkeret').disable();
				} else
				{
					//	Ext.getCmp('selectonmap').enable();
					Ext.getCmp('zoomliste').enable();
					Ext.getCmp('zoommarkeret').enable();
					//nulstilling
					xmin = 0;
					xmax = xmin;
					ymin = 0;
					ymax = ymin;
					store.each(function (record)
					{
						if (record.data.setx != null && record.data.setx != '0')
						{
							if (xmin == 0)
							{
								xmin = Number(record.data.setx);
								xmax = xmin;
								ymin = Number(record.data.sety);
								ymax = ymin;
							} else
							{
								if (Number(record.data.setx) < xmin) xmin = Number(record.data.setx);
								if (Number(record.data.setx) > xmax) xmax = Number(record.data.setx);
								if (Number(record.data.sety) < ymin) ymin = Number(record.data.sety);
								if (Number(record.data.sety) > ymax) ymax = Number(record.data.sety);
							} // slut if 0
						}
					}); // slut each
					if ((xmax - xmin) < 200)
					{
						xmidt = (Number(xmax) + Number(xmin)) / 2;
						xmax = Number(xmidt) + 100;
						xmin = Number(xmidt - 100);
					}
					if ((ymax - ymin) < 200)
					{
						ymidt = (Number(ymax) + Number(ymin)) / 2;
						ymax = Number(ymidt) + 100;
						ymin = Number(ymidt - 100);
					}

					zmListe = [xmin, ymin, xmax, ymax];
					if (Ext.getCmp('zoomliste').pressed == true)
						mapPanel.zoomTo(zmListe);
					visIndsamletVildsvin();

				} // slut count
				// sæt de aktuelle filtre på Excel-udtræk
				Ext.get('btn_excel').down('a').dom.href = 'VisFil.ashx?type=xl_indsaml&id=' + brugerid + '&gridfilter=' + store.proxy.reader.rawData.d.filterkrit;
			}

		}
	});
	storeAkt = storeIndsamlinger;
	mapPanel.config.layers.data.vildsvin_doede.setVisibility(true);



	filters = Ext.create('Ext.ux.grid.FiltersFeature', {
		// encode and local configuration options defined previously for easier reuse
		encode: true, // json encode the filter query
		local: false // defaults to false (remote filtering)
	});
	var Indsamlinger_grid = Ext.create('Ext.grid.Panel', {
		layout: 'fit',
		id: 'Indsamlinger_grid',
		autoHeight: true,
		autoWidth: true,
		title: '',
		store: storeIndsamlinger,
		disableSelection: false,
		loadMask: true,
		features: [filters],
		forceFit: true,
		viewConfig: {
			id: 'vg',
			//trackOver: false,
			//stripeRows: true
			getRowClass: function (record, rowIndex, rowParams, store)
			{
				//return ((record.get('status') == '1' || record.get('status') == '2' || record.get('status') == '3') && record.get('skjul') == '0' ? '' : 'readonly');
				return ((record.get('status') == '1' || record.get('status') == '2') && record.get('skjul') == '0' ? '' : 'readonly');
			}
		},
		// grid columns
		// specify any defaults for each column

		columns: {
			defaults: { renderer: renderTooltip },
			items: [{
				//id: 'id',
				text: "ID",
				tooltip: "ID",
				dataIndex: 'id',
				width: frm_indsamling.Indsamlinger_grid.id.width,
				hidden: frm_indsamling.Indsamlinger_grid.id.hidden,
				//flex: 1,
				filterable: true,
				sortable: true,
				filter: { type: 'numeric' }
			}, {
				text: "Afdeling",
				tooltip: "Afdeling",
				dataIndex: 'afd',
				width: frm_indsamling.Indsamlinger_grid.afd.width,
				hidden: frm_indsamling.Indsamlinger_grid.afd.hidden,
				filterable: true,
				sortable: true,
				flex: 1,
				renderer: renderIndsamler,
				filter: {
					type: 'list',
					store: storeIndsamlere,
					idField: 'id',
					labelField: 'navn',
					value: ((firmatype == indsamlertype) ? firma : undefined),
					active: ((firmatype == indsamlertype) ? true : false)
				}
			}, {
				text: "Opsamler",
				tooltip: "Opsamler",
				dataIndex: 'opsamler',
				width: frm_indsamling.Indsamlinger_grid.opsamler.width,
				hidden: frm_indsamling.Indsamlinger_grid.opsamler.hidden,
				filterable: true,
				//flex: 1,
				sortable: true,
				filter: { type: 'string' }
			}, {
				text: "Postnr",
				tooltip: "Postnr",
				dataIndex: 'postnr',
				width: frm_indsamling.Indsamlinger_grid.postnr.width,
				hidden: frm_indsamling.Indsamlinger_grid.postnr.hidden,
				filterable: true,
				sortable: true,
				flex: 1,
				filter: { type: 'numeric' }
			}, {
				text: "Indberetning",
				tooltip: "Indberetning",
				dataIndex: 'indberetning',
				width: frm_indsamling.Indsamlinger_grid.indberetning.width,
				hidden: frm_indsamling.Indsamlinger_grid.indberetning.hidden,
				filterable: true,
				//flex: 1,
				sortable: true,
				filter: { type: 'numeric' }
			}, {
				text: "Fundtype",
				tooltip: "Fundtype",
				dataIndex: 'fundtype',
				width: frm_indsamling.Indsamlinger_grid.fundtype.width,
				hidden: frm_indsamling.Indsamlinger_grid.fundtype.hidden,
				filterable: true,
				flex: 1,
				sortable: true,
				renderer: renderFundtyper,
				filter: {
					type: 'list',
					store: storeFundtyperDoed,
					idField: 'id',
					labelField: 'navn'
				}
			}, {
				text: "Set dato",
				tooltip: "Set dato",
				dataIndex: 'set_tid',
				width: frm_indsamling.Indsamlinger_grid.set_tid.width,
				hidden: frm_indsamling.Indsamlinger_grid.set_tid.hidden,
				filterable: true,
				//flex: 1,
				sortable: true,
				renderer: renderDato,
				filter: { type: 'date' }
			}, {
				text: "Status",
				tooltip: "Status",
				dataIndex: 'status',
				width: frm_indsamling.Indsamlinger_grid.status.width,
				hidden: frm_indsamling.Indsamlinger_grid.status.hidden,
				filterable: true,
				sortable: true,
				flex: 1,
				renderer: renderStatus,
				filter: {
					type: 'list',
					store: storeStatus,
					idField: 'id',
					labelField: 'navn',
					value: ['1', '2'],
					active: true
				}
			}, {
				text: "Vis / Skjul",
				tooltip: "Vis / Skjul",
				dataIndex: 'skjul',
				width: frm_indsamling.Indsamlinger_grid.skjul.width,
				hidden: frm_indsamling.Indsamlinger_grid.skjul.hidden,
				filterable: true,
				//flex: 1,
				sortable: true,
				renderer: renderVis,
				filter: {
					type: 'list',
					store: storeSkjul,
					idField: 'id',
					labelField: 'navn',
					value: '0',
					active: true
				}
			}]
		},
		// gridkol_slut
		selModel: Ext.create('Ext.selection.RowModel', {
			listeners: {
				select: {
					fn: function (selModel, rec, index)
					{
						retLaastPost = false;
						aktRec = rec;
						aktRk = index;
						aiz = [0, 0, 0, 0];
						aiz[0] = Number(rec.data.setx - 100);
						aiz[1] = Number(rec.data.sety - 100);
						aiz[2] = Number(Number(rec.data.setx) + 100);
						aiz[3] = Number(Number(rec.data.sety) + 100);

						if (Ext.getCmp('zoommarkeret').pressed == true && rec.data.setx != null && rec.data.setx != '0')
							mapPanel.zoomTo(aiz);


						// ??? sæt knapper aktive og inaktive

						mapPanel.clearLayers();
						mapPanel.addSelectedPoint(rec.data.setx, rec.data.sety);
						visIndsamletVildsvin(rec.data.id); //???
					}
				}
			}
		}),
		// paging bar on the bottom
		tbar: Ext.create('Ext.PagingToolbar', {
			store: storeIndsamlinger,
			displayInfo: true
		}),
		bbar: createZoombar(true)
	});

	// hent liste med indberetninger - resten udføres når listen er hentet og dernæst når der vælges i listen
	//var storeIndberetningIndsamling = Ext.create('Ext.data.JsonStore', {
	//	storeId: 'storeIndberetningIndsamling',
	//	model: 'id_navn',
	//	remoteSort: true,
	//	autoLoad: true,
	//	proxy: {
	//		type: 'simpel',
	//		extraParams: { liste: 'indberetninger' },
	//		url: 'Webservices/svin_login.asmx/hentListe'
	//	},
	//	//sorters: [{
	//	//    property: 'navn',
	//	//    direction: 'ASC'
	//	//}],
	//	listeners: {
	//		load: function (store) {
	//			var indbMenu = Ext.create('Ext.menu.Menu');
	//			store.each(function (record) {
	//				indbMenu.add({ xtype: 'menuitem', text: record.data.navn, id: 'indbMenu_' + record.data.id, handler: function () { tilfoejIndsamlerVildsvin(this.id) } });
	//			}, this);
	//			var btn = Ext.create('Ext.Button', {
	//				text: 'Tilføj vildsvin',
	//                   id: 'tilfoej_vildsvin',
	//				iconCls: 'icon-add',
	//                   tooltip: 'Vælg en indberetning at knytte det nye vildsvin til. Indberetninger med * bagefter er reserveret af dig.',
	//				menu: indbMenu
	//			});

	//			Indsamlinger_grid.dockedItems.items[2].add(btn)

	//		}
	//	}
	//});




	//	Indsamlinger_grid.bbar.add(['->', {
	//		text: 'Tilføj fugl',
	//		id: 'tilfoej_fugl',
	//		iconCls: 'icon-add',
	//		handler: function () { }
	//	}]);


	aktGrid = 'Indsamlinger_grid';

	Indsamlinger_grid.child('pagingtoolbar').add(['->', {
		text: 'Nulstil alle filtre',
		handler: function ()
		{
			Indsamlinger_grid.filters.clearFilters();
			x1 = 0;
			y1 = 0;
			x2 = 0;
			y2 = 0;

		}
	}]);

	Ext.apply(Ext.form.field.VTypes, {
		nord: function (val, field)
		{
			var nordName = /^6[0-9]{6}$/;
			return nordName.test(val);
		},
		nordText: 'Nord skal udfyldes med et 7-cifret tal.',
		nordMask: /[0-9]/i
	});
	Ext.apply(Ext.form.field.VTypes, {
		ost: function (val, field)
		{
			var ostName = /^[4-9]{1}[0-9]{5}$/;
			return ostName.test(val);
		},
		ostText: 'Øst skal udfyldes med et 6-cifret tal.',
		ostMask: /[0-9]/i
	});


	var bx = vestPanel.getBox();
	var Indsamlinger_panel = Ext.create('Ext.panel.Panel', {
		title: 'Døde og skudte vildsvin',
		id: 'Indsamlinger_panel',
		layout: 'anchor',
		anchor: '-20',
		bodyPadding: '10 10 10 10',
		autoWidth: true,
		autoHeight: false,
		height: bx.height,
		autoScroll: true,
		renderTo: 'vestdiv',
		items: [
			Indsamlinger_grid,
			{
				xtype: 'fieldset', // her er kun de redigerbare felter
				title: 'Redigér indsamlet vildsvin',
				id: 'akt_indsamling',
				bodyPadding: 10,
				autoWidth: true,
				autoHeight: true,
				style: 'margin-top: 10px',
				flex: 1,
				items: [{
					xtype: 'fccolumn',
					items: [{
						xtype: 'acombo',
						hideLabel: false,
						fieldLabel: 'Status',
						id: 'status',
						store: storeStatus,
						valueField: 'id',
						displayField: 'navn',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.3
					}, {
						xtype: 'acombo',
						hideLabel: false,
						fieldLabel: 'Indsamlet af afdeling',
						id: 'afdeling',
						store: storeIndsamlereA,
						valueField: 'id',
						displayField: 'navn',
						columnWidth: 0.4
						//	}]
						//}, {
						//	xtype: 'fccolumn_ro',
						//	items: [{
						//		xtype: 'textfield',
						//		hideLabel: false,
						//		fieldLabel: 'Indberetning',
						//		id: 'indberetning',
						//		columnWidth: 0.2
					}, {
						xtype: 'datefield',
						hideLabel: false,
						fieldLabel: 'Indsamlet dato',
						emptyText: 'Udfyldes automatisk',
						id: 'tid_indsamlet',
						margin: '0 0 0 3', // højre
						format: 'd-m-Y',
						altFormats: 'Y-m-d H:i:s|D M Y H:i:s|d-m-Y|d-m-y|Y-m-d|j-n-y|j-n-Y|j-m-y|j-m-Y|d-n-y|d-n-Y|d/m/Y H:i:s|Y-m-dTH:i:s|',
						readOnly: true,
						columnWidth: 0.3
					}]
				}, {
					xtype: 'fccolumn',
					items: [{
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Øst',
						id: 'x',
						columnWidth: 0.5,
						allowBlank: false,
						vtype: 'ost',
						listeners: {
							blur: tegnFundsted
						}
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Nord',
						id: 'y',
						margin: '0 0 0 3', // højre
						columnWidth: 0.5,
						allowBlank: false,
						vtype: 'nord',
						listeners: {
							blur: tegnFundsted
						}
					}]
				}, {
					xtype: 'fccolumn_ro',
					id: 'ro_gps',
					hidden: true,
					style: 'margin-top: 6px; ',
					items: [{
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Øst',
						id: 'gx',
						columnWidth: 0.3
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Nord',
						id: 'gy',
						margin: '0 0 0 3', // højre
						columnWidth: 0.3
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Usikkerhed (m)',
						id: 'gz',
						margin: '0 0 0 3', // højre
						columnWidth: 0.2
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Fejl',
						id: 'fejl',
						margin: '0 0 0 3', // højre
						columnWidth: 0.2
					}]
				}, {
					xtype: 'toolbar',
					id: 'ro_gps2',
					hidden: true,
					style: 'margin-top: 6px',
					items: [{
						xtype: 'button',
						text: 'Ny GPS måling',
						id: 'btn_maal',
						handler: gps_maal
					}, '->', {
						xtype: 'button',
						text: 'Udfør',
						tooltip: 'Benyt GPS-målingen',
						id: 'btn_ok',
						handler: function ()
						{
							Ext.getCmp('x').setValue(Ext.getCmp('gx').getValue());
							Ext.getCmp('y').setValue(Ext.getCmp('gy').getValue());
							Ext.getCmp('btn_gps').toggle();
						}
					}]
				}]
			}, {
				xtype: 'fieldset',
				title: 'Kommentar til dette vildsvin, kan redigeres selvom posten er låst',
				id: 'fs_kommentar',
				bodyPadding: 10,
				autoWidth: true,
				autoHeight: true,
				style: 'margin-top: 10px',
				flex: 1,
				items: [{
					xtype: 'fccolumn',
					items: [{
						xtype: 'textarea',
						hideLabel: true,
						id: 'vildsvin_kommentar',
						margin: '0',
						columnWidth: 1,
						grow: true,
						growMin: 30,
						growMax: 300
					}]
				}]
			}, tegn_fs_anmelder(), tegn_fs_fund(true), tegn_fs_fotografi()
		],
		dockedItems: [{
			xtype: 'toolbar',
			dock: 'bottom',
			items: [{
				xtype: 'button',
				text: 'Send til opsamlingshold',
				id: 'opsamlerMenu_0',
				style: 'margin-right: 15px',
				menu: opsamlerMenu,
				hidden: true
			}, {
				id: 'vis_pdf',
				tooltip: 'Vis PDF køreseddel',
				iconCls: 'icon-pdf',
				hidden: true,
				href: 'VisFil.ashx?type=pdf&id=0',
				htmp: 'VisFil.ashx?type=pdf&id=',
				target: '_blank',
				menu: {
					xtype: 'menu',
					items: [{
						xtype: 'menuitem',
						id: 'menuPDF',
						text: 'Opret køreseddel',
						handler: function ()
						{
							Ext.Ajax.request({
								url: 'Webservices/svin_login.asmx/lavKoereseddel',
								params: JSON.stringify({
									indberetning: aktRec.data.indberetning
								}),
								headers: { 'Content-Type': 'application/json;charset=utf-8' },
								success: function (response, opts)
								{
									okbesked('Der er nu lavet en ny køreseddel');
								},
								failure: ajaxFailed
							});


						}
					}]
				}
			}, '-', {
				//xtype: 'button',
				text: '',
				toggleGroup: 'geopunkt',
				enableToggle: true,
				pressed: true,
				tooltip: 'Skriv koordinater',
				id: 'btn_skriv',
				iconCls: 'icon-edit',
				handler: function ()
				{
					//Ext.getCmp('x').setValue(x);
					//Ext.getCmp('y').setValue(y);
				}
			}, {
				//xtype: 'button',
				toggleGroup: 'geopunkt',
				enableToggle: true,
				tooltip: 'Brug GPS',
				id: 'btn_gps',
				iconCls: 'icon-gps',
				listeners: {
					toggle: function (ctl, pressed)
					{
						if (pressed == true)
						{
							// kun hvis knappen er trykket ned
							ctl.btnInnerEl.setStyle({ color: '#600000', fontWeight: 'bold' });
							Ext.getCmp('ro_gps').show();
							Ext.getCmp('ro_gps2').show();
							gps_maal();

						}
						else
						{
							ctl.btnInnerEl.setStyle({ color: '#333333', fontWeight: 'normal' });
							Ext.getCmp('ro_gps').hide();
							Ext.getCmp('ro_gps2').hide();
						}
					}
				}
			}, {
				//xtype: 'button',
				toggleGroup: 'geopunkt',
				enableToggle: true,
				tooltip: 'Marker fundsted på kortet',
				id: 'btn_mark',
				iconCls: 'icon-koordinatfanger',
				listeners: {
					toggle: function (ctl, pressed)
					{
						if (pressed == true)
						{
							// kun hvis knappen er trykket ned
							ctl.btnInnerEl.setStyle({ color: '#600000', fontWeight: 'bold' });
							DrawPoint = new OpenLayers.Control.DrawFeature(mapPanel.config.layers.selections.coordinateLayer, OpenLayers.Handler.Point);  //, { 'displayClass': 'olControlDrawBigaard' }
							DrawPoint.events.register('featureadded', this, function ()
							{
								var x = arguments[0].feature.geometry.x;
								var y = arguments[0].feature.geometry.y;
								mapPanel.clearLayers();
								mapPanel.showPoint(x, y);
								Ext.getCmp('x').setValue(Math.round(x));
								Ext.getCmp('y').setValue(Math.round(y));
							});
							mapPanel.map.addControl(DrawPoint);
							DrawPoint.activate();
						}
						else
						{
							ctl.btnInnerEl.setStyle({ color: '#333333', fontWeight: 'normal' });
							DrawPoint.deactivate();
						}
					}
				}
			}, '->', {
				xtype: 'checkbox',
				boxLabel: 'Zone 33',
				id: 'menu_zone',
				//style: 'margin-right: 5px',
				style: 'margin-right: 35px; font-size: 11px !important; ',
				checked: false
			}, {
				//xtype: 'button',
				text: 'Ret låst post',
				id: 'btn_laast',
				iconCls: 'icon-edit',
				style: 'margin-right: 15px',
				hidden: true,
				handler: function ()
				{
					enableCtl(Ext.getCmp('akt_indsamling'), true);
					retLaastPost = true;
					//					        okbesked('Når du er færdig med at redigere den post, du lige har låst op, er det en god ide at trykke på knappen Opfrisk - ellers er det ikke sikkert at du kan se resultatet af din ændring.');
				}
			}, {
				//xtype: 'button',
				text: 'Skjul',
				id: 'skjulmenu_0',
				iconCls: 'icon-remove',
				style: 'margin-right: 15px',
				menu: skjulMenu,
				hidden: (firmatype == '0' || firmatype == '4' ? false : true)
			}, {
				//xtype: 'button',
				text: 'Gem',
				id: 'gem_indsamling',
				iconCls: 'icon-ok',
				handler: function ()
				{
					gemIndsamling(false);
				}
			}]
		}]
	});
	// vi skal sikre at de indledende filtre sættes
	//Indsamlinger_panel.doLayout();
	filters.createFilters();
	storeIndsamlinger.load();
	retKolonner(Indsamlinger_grid, 5);

}


var redIndsamling = false;
function visIndsamletVildsvin(id)
{
	redIndsamling = false;
	// tøm først alle felter
	Ext.getCmp('btn_laast').hide();

	visFelter(storeIndsamlinger, id);


	//Ext.getCmp('indberetning').setValue('');
	//Ext.getCmp('postnr').setValue('');
	//Ext.getCmp('afdeling').setOriginal('');
	//Ext.getCmp('status').setOriginal('');
	//Ext.getCmp('fundtype').setValue('');
	//Ext.getCmp('fundtype').readOnly = true;
	//Ext.getCmp('x').setValue('');
	//Ext.getCmp('y').setValue('');
	//Ext.getCmp('setx').setValue('');
	//Ext.getCmp('sety').setValue('');
	//Ext.getCmp('set_tid').setValue('');
	////	Ext.getCmp('tid_fvst').setValue('');

	//Ext.getCmp('an_navn').setValue('');
	//Ext.getCmp('an_adresse').setValue('');
	//Ext.getCmp('an_telefon').setValue('');
	//Ext.getCmp('an_email').setValue('');
	//Ext.getCmp('set_adresse').setValue('');
	//Ext.getCmp('fundsted').setValue('');
	//Ext.getCmp('f_billede').setVisible(false);
	//Ext.getCmp('opsamlerMenu_0').setVisible(false);
	//Ext.getCmp('menu_zone').setValue(false);
	//Ext.getCmp('vis_pdf').hide();
	//Ext.getCmp('btn_skriv').hide();
	//Ext.getCmp('btn_gps').hide();
	//Ext.getCmp('btn_mark').hide();
	//Ext.getCmp('kommentar').setValue('');
	//Ext.getCmp('vildsvin_kommentar').setValue('');
	//Ext.getCmp('tilfoej_vildsvin').setDisabled(true);



	if (aktRec == undefined)
	{
		Ext.getCmp('akt_indsamling').setTitle('Intet aktuelt vildsvin');
	}
	else
	{
		// opdater listen med statusværdier til at have de rette aktive, baseret på aktuel status og fundtype
		///*
		//    afventer indsamling false
		//    reserveret false

		//    indsamling udført true døde og ilanddrevne, false nedlagte - eller allerede valgt
		//    sendt til lab true døde og nedlagte, false ilanddrevne
		//    bortskaffet true ilanddrevne, ellers false
		//    Ikke fundet true døde og ilanddrevne, false nedlagte
		//    Kasseret true døde, ellers false
		//*/
		//var a = storeStatusBRS.getById('3'); //indsamling udført 
		//a.set('aktiv', aktRec.data.fundtype >= 30 && aktRec.data.status != 3);
		//a = storeStatusBRS.getById('4'); //sendt til lab
		//a.set('aktiv', aktRec.data.fundtype < 40 && aktRec.data.status < 4);
		//a = storeStatusBRS.getById('7'); // bortskaffet 
		//a.set('aktiv', aktRec.data.fundtype == 40 && aktRec.data.status < 4);
		//a = storeStatusBRS.getById('91'); //Ikke fundet 
		//a.set('aktiv', aktRec.data.fundtype >= 30 && aktRec.data.status < 4);
		//a = storeStatusBRS.getById('92'); //Kasseret 
		//a.set('aktiv', aktRec.data.fundtype == 30 && aktRec.data.status < 4);

		/*
				afventer indsamling false
				reserveret false
									  
				indsamling udført true døde og nedlagte - eller allerede valgt
				sendt til lab - bruges ikke længere, altid false 
				bortskaffet true ilanddrevne, ellers false
				Ikke fundet true døde og ilanddrevne, false nedlagte
				Kasseret true døde, ellers false
		*/
		var a = storeStatus.getById('2'); // reserveret af - kun aktiv hvis man er fra indsamler-firma
		a.set('aktiv', firmatype == indsamlertype && aktRec.data.status == 1);
		a = storeStatus.getById('3'); //indsamling udført 
		a.set('aktiv', aktRec.data.fundtype < 40 && aktRec.data.status < 3);
		a = storeStatus.getById('7'); // bortskaffet 
		a.set('aktiv', aktRec.data.fundtype == 40 && aktRec.data.status < 3);
		a = storeStatus.getById('91'); //Ikke fundet 
		a.set('aktiv', aktRec.data.fundtype >= 30 && aktRec.data.status < 3);
		a = storeStatus.getById('92'); //Kasseret 
		a.set('aktiv', aktRec.data.fundtype == 30 && aktRec.data.status < 3);

		//storeStatus.sort();

		if (aktRec.data.skjul != '0')
		{
			Ext.getCmp('akt_indsamling').setTitle('Vis skjult indsamling (' + storeSkjul.getById(aktRec.data.skjul).data.navn + ')');
		}
		else
		{
			if (id == '0')
			{
				// ny fugl 
				// dette skal kun være aktivt for indsamlere, det klares med knappen
				redIndsamling = true;
				Ext.getCmp('akt_indsamling').setTitle('Nyt indsamlet vildsvin');
				Ext.getCmp('afdeling').setOriginal(aktRec.data.afd);
			}
			//            else if ((aktRec.data.status == '1' || aktRec.data.status == '2' || aktRec.data.status == '3') && (firmatype == indsamlertype || firmatype == '0' || firmatype == '4'))
			else if ((aktRec.data.status == '1' || aktRec.data.status == '2') && (firmatype == indsamlertype || firmatype == '0' || firmatype == '4'))
			{
				// eksisterende fugl, kan redigeres
				redIndsamling = true;
				// status = sendt til indsamling
				Ext.getCmp('akt_indsamling').setTitle('Redigér indsamling af vildsvin');
				Ext.getCmp('afdeling').setOriginal(aktRec.data.afd);
				Ext.getCmp('opsamlerMenu_0').setVisible(aktRec.data.status == '1' && firmatype == indsamlertype);
				Ext.getCmp('btn_skriv').show();
				Ext.getCmp('btn_gps').show();
				Ext.getCmp('btn_mark').show();
				Ext.getCmp('tilfoej_vildsvin').setDisabled(false);
			}
			else
			{
				// status != sendt til indsamling
				Ext.getCmp('akt_indsamling').setTitle('Vis aktuelt vildsvin');
				//				Ext.getCmp('afdeling').setOriginal(aktRec.data.afd);
			}
			//Ext.getCmp('indberetning').setValue(aktRec.data.indberetning);
			//Ext.getCmp('postnr').setValue(aktRec.data.postnr);
			//Ext.getCmp('status').setOriginal(aktRec.data.status);
			//Ext.getCmp('fundtype').setValue(aktRec.data.fundtype);
			//Ext.getCmp('fundtype').readOnly = (aktRec.data.id != '0');
			//Ext.getCmp('x').setValue(aktRec.data.x);
			//Ext.getCmp('y').setValue(aktRec.data.y);
			//Ext.getCmp('setx').setValue(aktRec.data.setx);
			//Ext.getCmp('sety').setValue(aktRec.data.sety);
			//Ext.getCmp('set_tid').setValue(aktRec.data.set_tid);
			////Ext.getCmp('tid_fvst').setValue(aktRec.data.tid_fvst);
			//Ext.getCmp('an_navn').setValue(aktRec.data.an_navn);
			//Ext.getCmp('an_adresse').setValue(aktRec.data.an_adresse);
			//Ext.getCmp('an_telefon').setValue(aktRec.data.an_telefon);
			//Ext.getCmp('an_email').setValue(aktRec.data.an_email);
			//Ext.getCmp('set_adresse').setValue(aktRec.data.set_adresse);
			//Ext.getCmp('fundsted').setValue(aktRec.data.fundsted);
			//Ext.getCmp('kommentar').setValue(aktRec.data.kommentar);
			//Ext.getCmp('vildsvin_kommentar').setValue(aktRec.data.vildsvin_kommentar);
			////	Ext.getCmp('f_billede').setValue(aktRec.data.billede);
			//Ext.getCmp('billede').setSrc(aktRec.data.billede);
			//if (aktRec.data.billede != null && aktRec.data.billede != '')
			//	Ext.getCmp('f_billede').setVisible(true);
			if (aktRec.data.pdf)
			{
				Ext.getCmp('vis_pdf').show();
				Ext.getCmp('vis_pdf').href = Ext.getCmp('vis_pdf').htmp + aktRec.data.indberetning;
				Ext.getDom('vis_pdf' + '-btnEl').href = Ext.getCmp('vis_pdf').href;
				Ext.getCmp('vis_pdf').enable();
			}


			// sæt aktiv eller ej
			enableCtl(Ext.getCmp('akt_indsamling'), redIndsamling);
			// er der noget at låse op for dem der kan?
			if (redIndsamling == false && (firmatype == '0' || firmatype == '4'))
			{
				Ext.getCmp('btn_laast').show();
				Ext.getCmp('btn_laast').setDisabled(false);
			}


		}
	}
}

function tegnFundsted()
{
	var cx = Ext.getCmp('x');
	var cy = Ext.getCmp('y');

	if (cx.isValid() == true && cy.isValid() == true)
	{
		mapPanel.showPoint(cx.getValue(), cy.getValue());

	}
}


function vaelgOpsamler(menu_nr)
{
	var opsamler = menu_nr.substring(menu_nr.indexOf('_') + 1);
	Ext.Ajax.request({
		url: 'Webservices/svin_login.asmx/vaelgOpsamler',
		method: "POST",
		params: JSON.stringify({
			opsamler: opsamler,
			indberetning: aktRec.data.indberetning,
			pdf: aktRec.data.pdf
		}),
		success: function ()
		{
			// genindlæs , der kan være rettet mange poster
			storeIndsamlinger.load();
		},
		failure: function (response, opts)
		{
			//console.log("failed");
			ajaxFailed(response, opts);
		},
		headers: { 'Content-Type': 'application/json' }
	});
}

//function tilfoejIndsamlerVildsvin(menu_nr) {
//	var indberetning = menu_nr.substring(menu_nr.indexOf('_') + 1);

//	var nr = storeAkt.getCount();
//	var ny = Ext.create('mIndsamlinger', {
//		id: '0',
//		afd: firma,
//        indberetning: indberetning,
//        status: 3
//	});
//	storeAkt.add(ny); // modellen, med standardværdier
//	// sæt fokus på den nye række
//	Ext.getCmp("Indsamlinger_grid").getView().select(nr);
//}
// ny variant, knytter til markerede indberetning
function tilfoejIndsamlerVildsvin()
{
	//var indberetning = menu_nr.substring(menu_nr.indexOf('_') + 1);

	var nr = storeAkt.getCount();
	var ny = Ext.create('mIndsamlinger', {
		id: '0',
		afd: aktRec.data.afd,
		indberetning: aktRec.data.indberetning,
		fundtype: aktRec.data.fundtype,
		status: 3
	});
	storeAkt.add(ny); // modellen, med standardværdier
	// sæt fokus på den nye række
	Ext.getCmp("Indsamlinger_grid").getView().select(nr);
}


function gemIndsamlingSpoerg(svar)
{
	if (svar == 'yes')
	{
		gemIndsamling(true);
	}
}

function gemIndsamling(harSpurgt)
{
	// kun hvis der er en post
	if (aktRec == undefined)
	{
		fejlbox('Du kan ikke gemme, når der ikke er valgt nogen post.');
		return;
	}

	if (harSpurgt == undefined) harSpurgt = false;
	// hvis posten er låst, kan man kun gemme vildsvin_kommentaren
	if (redIndsamling == false)
	{
		// send til serveren
		var param = {
			vildsvin: aktRec.data.id,
			vildsvin_kommentar: Ext.getCmp('vildsvin_kommentar').getValue()
		};
		var params = Ext.JSON.encode(param);
		Ext.Ajax.request({
			url: 'Webservices/svin_login.asmx/gemvildsvin_kommentar',
			params: params,
			headers: { 'Content-Type': 'application/json;charset=utf-8' },
			success: function (response, opts)
			{
				var opdatvar = new Array();
				opdatvar.push('vildsvin_kommentar');
				opdatvar.push(Ext.getCmp('vildsvin_kommentar').getValue());
				opdaterRaekke(opdatvar);
			},
			failure: ajaxFailed
		});
		return;
	}

	// opret et objekt med gemme-værdierne
	var g = {
		id: aktRec.data.id,
		indberetning: aktRec.data.indberetning,
		status: Ext.getCmp('status').getValue(),
		afd: aktRec.data.afd,
		afd_ny: Ext.getCmp('afdeling').getValue(),
		vildsvin_kommentar: Ext.getCmp('vildsvin_kommentar').getValue(),
		skjul: aktRec.data.skjul,
		fundtype: Ext.getCmp('fundtype').getValue(),
		x: Ext.getCmp('x').getValue(),
		y: Ext.getCmp('y').getValue(),
		zone33: (Ext.getCmp('menu_zone').getValue() == true ? '1' : '')
	};
	var fejltekst = '';
	var advarsel = '';

	// hvis dyret sendes til en anden indsamler, må intet andet være ændret - og det må ikke være et nyt dyret
	if (g.afd_ny != g.afd && g.afd != '0')
	{
		if (g.indberetning == '0')
		{
			fejlbox('Du kan ikke sende et vildsvin, du lige har tilføjet, til en anden indsamler.');
			return;
		}
		if (harSpurgt == false && (g.status != aktRec.data.status || g.fundtype != aktRec.data.fundtype || g.x != aktRec.data.x || g.y != aktRec.data.y))
		{
			spoerg('Når du sender et vildsvin til en anden indsamler, kan du ikke gemme andre oplysninger om det, dvs ikke angive fx koordinater. Vil du fortsætte?', gemIndsamlingSpoerg);
			return;
		}
	}
	// hvis status er ikke indsamlet, må intet andet ændres
	else if (Number(g.status) > 90)
	{
		if (g.indberetning == '0')
		{
			fejlbox('Der er ingen grund til at tilføje et vildsvin, medmindre du har samlet det ind.');
			return;
		}
		if (harSpurgt == false && (g.fundtype != aktRec.data.fundtype || g.x != aktRec.data.x || g.y != aktRec.data.y))
		{
			spoerg('Når et vildsvin ikke samles ind, kan du ikke gemme andre oplysninger om den, dvs ikke angive fx koordinater. Vil du fortsætte?', gemIndsamlingSpoerg);
			return;
		}
	}
	else if (harSpurgt == false)
	{
		// OK, nu er skjul og afdeling uændret - nu skal alle relevante felter være udfyldt
		// fundtype skal være udfyldt
		if (g.fundtype == '0')
			fejltekst += '\nFundtype skal udfyldes';
		if (g.status == '0')
			fejltekst += '\nStatus skal udfyldes';
		var afstand = 0.0;
		try
		{
			var x = Number(g.x);
			var y = Number(g.y);
			if ((x < 437000 || x > 902000 || y < 6043000 || y > 6405000) && g.zone33 == '' || (x < 0 || x > 515000 || y < 6040000 || y > 6410000) && g.zone33 == '1')// tjek at de ligger indenfor Danmarks boundingbox
				fejltekst += '\nKoordinater er ikke korrekt udfyldt - punktet ligger ikke i Danmark.';
			else if (g.id != '0' && g.zone33 == '')
			{
				afstand = Math.sqrt(Math.pow(x - Number(aktRec.data.setx), 2) + Math.pow(y - Number(aktRec.data.sety), 2));
				if (afstand > 1000)
					advarsel = 'Punktet ligger ' + afstand + ' meter fra det sted, der blev angivet i indberetningen.\nTryk OK, hvis du vil fortsætte.\nTryk Annullér, hvis du vil rette de indtastede koordinater.';
			}
		}
		catch (ex)
		{
			fejltekst += '\nKoordinater skal udfyldes - Øst skal være et 6-cifret tal og Nord skal være et 7-cifret tal.';
		}
	}
	if (fejltekst != '')
	{
		fejlbox(fejltekst);
		return;
	}

	if (advarsel != '')
	{
		if (confirm(advarsel) == false)
			return;
	}

	// send til serveren
	var param = { g: g };
	var params = Ext.JSON.encode(param);


	Ext.Ajax.request({
		url: 'Webservices/svin_login.asmx/gemIndsamling',
		params: params,
		headers: { 'Content-Type': 'application/json;charset=utf-8' },
		success: function (response, opts)
		{
			if (retLaastPost)
			{
				storeIndsamlinger.reload();
			}
			else
			{
				// ret kun den aktuelle række
				var ret = Ext.JSON.decode(response.responseText).d;
				var opdatvar = new Array();
				opdatvar.push('id');
				opdatvar.push(ret.id);
				opdatvar.push('fundtype');
				opdatvar.push(ret.fundtype);
				opdatvar.push('status');
				opdatvar.push(ret.status);
				//opdatvar.push('skjul');
				//opdatvar.push(ret.skjul);
				opdatvar.push('afd_ny');
				opdatvar.push(ret.afd_ny);
				opdatvar.push('afd');
				opdatvar.push(ret.afd);
				opdatvar.push('x');
				opdatvar.push(ret.x);
				opdatvar.push('y');
				opdatvar.push(ret.y);
				opdatvar.push('indberetning_kommentar');
				opdatvar.push(ret.indberetning_kommentar);
				opdatvar.push('vildsvin_kommentar');
				opdatvar.push(ret.vildsvin_kommentar);
				opdaterRaekke(opdatvar);
			}
		},
		failure: ajaxFailed
	});

}

function gps_error(err)
{
	// afslut trykket, skriv fejlmeddelelse
	Ext.getCmp('btn_maal').setText('Ny GPS måling');
	Ext.getCmp('fejl').setValue(err);

}
function gps_success(position)
{
	Ext.getCmp('fejl').setValue('');
	Ext.getCmp('btn_maal').setText('Ny GPS måling');
	// spørg på serveren om transformation
	Ext.Ajax.request({
		url: 'Webservices/svin_login.asmx/latlonUTM',
		method: "POST",
		params: JSON.stringify({
			lat: position.coords.latitude,
			lon: position.coords.longitude
		}),
		success: function (response, opts)
		{
			var ret = Ext.JSON.decode(response.responseText).d;
			mapPanel.clearLayers();
			mapPanel.showPoint(ret.x, ret.y);
			Ext.getCmp('gx').setValue(ret.x);
			Ext.getCmp('gy').setValue(ret.y);
			Ext.getCmp('gz').setValue(position.coords.accuracy);
			var gps_punkt = [0, 0, 0, 0];
			gps_punkt[0] = Number(ret.x - 100);
			gps_punkt[1] = Number(ret.y - 100);
			gps_punkt[2] = Number(Number(ret.x) + 100);
			gps_punkt[3] = Number(Number(ret.y) + 100);
			mapPanel.zoomTo(gps_punkt);
		},
		failure: function (response, opts)
		{
			ajaxFailed(response, opts);
		},
		headers: { 'Content-Type': 'application/json' }
	});
}
function gps_maal()
{
	navigator.geolocation.getCurrentPosition(gps_success, gps_error);
	Ext.getCmp('btn_maal').setText('Arbejder på at få en GPS måling ...');
}

