﻿function visBredder()
{

    visBredder.redfelter = [];

    visBredder.tree = Ext.create('Ext.tree.Panel', {
        title: 'Sider med tabeller',
        collapsible: true,
        rootVisible: false,
        useArrows: true,
        frame: false,
        autoWidth: true,
        autoHeight: true,
        loadMask: true,
        store: grid_treestore,
        listeners: {
            select: {
                fn: function (tree, record, index, eOpts)
                {
                    //if (record.data.leaf == true)
                    //{
                        // find først de overordnede værdier
                    visBredder.parent = record; //.parentNode;

                        visBredder.redfelter = [];
                        visBredder.fs_tabel.removeAll();
                        visBredder.fs_tabel.setTitle(record.data.text);
                        var ct = undefined;
                        ct = Ext.create('fccolumn', {
                            defaults: { fieldCls: 'header_fed' },
                            items: [{
                                xtype: 'displayfield',
                                hideLabel: true,
                                fieldLabel: '', //overskrift,
                                margin: '0 3 0 0', // venstre
                                columnWidth: 0.7,
                                value: 'Kolonne'
                            }, {
                                xtype: 'displayfield',
                                hideLabel: true,
                                fieldLabel: '',
                                margin: '0 3 0 0', // venstre
                                //feltnr: rec.data.id,
                                columnWidth: 0.15,
                                value: 'Bredde'
                            }, {
                                xtype: 'displayfield',
                                hideLabel: true,
                                fieldLabel: '',
                                margin: '0 3 0 0', // venstre
                                //feltnr: rec.data.id,
                                columnWidth: 0.03,
                                value: ''
                            }, {
                                xtype: 'displayfield',
                                hideLabel: true,
                                fieldLabel: '',
                                margin: '0 3 0 0', // venstre
                                //feltnr: rec.data.id,
                                columnWidth: 0.07,
                                value: 'Synlig'
                            }]
                        });
                        visBredder.redfelter.push(ct);
                        //console.log("ja");
                        storeKolonner.clearFilter();
                        storeKolonner.filterBy(function (rec) { return rec.get('side') == record.data.id; });
                        var i = 0;
                        var bred = 0;
                        storeKolonner.each(function (rec) {
                            ct = Ext.create('fccolumn', {
                                items: [{
                                    xtype: 'displayfield',
                                    hideLabel: true,
                                    fieldLabel: '', //overskrift,
                                    margin: '0 3 0 0', // venstre
                                    columnWidth: 0.7,
                                    value: rec.data.titel
                                },{
                                    xtype: 'numberfield',
                                    hideLabel: true,
                                    fieldLabel: '',
                                    margin: '0 3 0 0', // venstre
                                    id: 'kol_tx_' + rec.data.id + '_bredde', // nummeret til at gemme med
                                    columnWidth: 0.15,
                                    hideTrigger: true,
                                    allowDecimals: false,
                                    value: rec.data.bredde,
                                    listeners: {
                                        change: {
                                            fn: function (ctl, newValue, oldValue, eOpts) {
                                                if (newValue != undefined && newValue != '')
                                                var id = ctl.id.split('_')[2];
                                                storeKolonner.clearFilter();
                                                storeKolonner.filterBy(function (rec) { return rec.get('id') == id; });
                                                rec.set('bredde', newValue);
                                                rec.commit();
                                                retTotalbredde();
                                            }
                                        }

                                    }
                                },{
                                    xtype: 'displayfield',
                                    hideLabel: true,
                                    fieldLabel: '', //overskrift,
                                    margin: '0 3 0 0', // venstre
                                    columnWidth: 0.03,
                                    value: ''
                                }, {
                                    xtype: 'checkbox',
                                    hideLabel: true,
                                    fieldLabel: '',
                                    id: 'kol_tx_' + rec.data.id + '_vis',
                                    margin: '0 3 0 0', // venstre
                                    columnWidth: 0.07,
                                    hideTrigger: true,
                                    checked: rec.data.vis,
                                    listeners: {
                                        change: {
                                            fn: function (ctl, newValue, oldValue, eOpts) {
                                                var id = ctl.id.split('_')[2];
                                                storeKolonner.clearFilter();
                                                storeKolonner.filterBy(function (rec) { return rec.get('id') == id; });
                                                rec.set('vis', newValue);
                                                rec.commit();
                                                retTotalbredde();
                                            }
                                        }

                                    }

                                }]
                            });
                            visBredder.redfelter.push(ct);
                            if (rec.data.vis == true)
                                bred += rec.data.bredde;

                        });
                        ct = Ext.create('fccolumn', {
                            items: [{
                                xtype: 'displayfield',
                                hideLabel: true,
                                fieldLabel: '', //overskrift,
                                id: 'kol_tx_samlet', 
                                margin: '0 3 0 0', // venstre
                                columnWidth: 1,
                                value: 'Samlet bredde af kolonnerne: ' + bred 
                            }]
                        });
                        visBredder.redfelter.push(ct);
                        //if (visBredder.parent.childNodes.length > 1)
                        //{
                        //    for (var i =0; i < visBredder.parent.childNodes.length; i++)
                        //    {
                        //        if (visBredder.parent.childNodes[i].data.text != record.data.text)
                        //        {
                        //            bred = findTotalbredde(visBredder.parent.childNodes[i].data.navn);
                        //            ct = Ext.create('fccolumn', {
                        //                items: [{
                        //                    xtype: 'displayfield',
                        //                    hideLabel: true,
                        //                    fieldLabel: '', //overskrift,
                        //                    margin: '0 3 0 0', // venstre
                        //                    columnWidth: 1,
                        //                    value: 'Samlet bredde af tabellen ' + visBredder.parent.childNodes[i].data.text + ': ' + bred
                        //                }]
                        //            });
                        //            visBredder.redfelter.push(ct);

                        //        }
                        //    }
                        //}
                        ct = Ext.create('fccolumn', {
                            margin: '12 0 0 0', //top
                            items: [{
                                xtype: 'displayfield',
                                fieldCls: 'header_fed',
                                hideLabel: true,
                                fieldLabel: '', //overskrift,
                                margin: '0 3 0 0', // venstre
                                columnWidth: 0.7,
                                value: 'Formularens bredde'
                            }, {
                                xtype: 'numberfield',
                                hideLabel: true,
                                fieldLabel: '',
                                margin: '0 3 0 0', // venstre
                                id: 'form_tx_' + record.data.id + '_bredde', // nummeret til at gemme med
                                columnWidth: 0.15,
                                hideTrigger: true,
                                allowDecimals: false,
                                    value: record.data.bredde,
                                listeners: {
                                    change: {
                                        fn: function (ctl, newValue, oldValue, eOpts) {
                                            if (newValue != undefined && newValue != '')
                                            {
                                                visBredder.parent.set('bredde', newValue);
                                                visBredder.parent.commit();
                                            }
                                        }
                                    }

                                }
                            }]
                        });
                        visBredder.redfelter.push(ct);

                        visBredder.fs_tabel.add(visBredder.redfelter);
                    //}
                }
            }
        }
    });

    visBredder.fs_tabel = Ext.create('Ext.form.FieldSet', {
        title: 'Ingen aktuel tabel',
        id: 'akt_tabel',
        bodyPadding: 10,
        autoWidth: true,
        autoHeight: true,
        style: 'margin-top: 10px; margin-bottom: 0px; margin-right:20px;',
        flex: 1,
        items: []
    });

    visBredder.knappanel = Ext.create('Ext.toolbar.Toolbar', {
        style: 'margin-top: 0px; ',
        items: [{
            text: 'Nulstil',
            handler: function () {
                // gemmer ikke på serveren
                storeKolonner.clearFilter();
                storeKolonner.filterBy(function (rec) { return rec.get('side') == visBredder.parent.data.id });
                var bred = 0;
                storeKolonner.each(function (rec) {
                    rec.set('bredde', rec.data.def_bredde);
                    rec.set('vis', rec.data.def_vis);
                    rec.commit();
                    Ext.getCmp('kol_tx_' + rec.data.id + '_bredde').setValue(rec.data.bredde);
                    Ext.getCmp('kol_tx_' + rec.data.id + '_vis').setValue(rec.data.vis);
                    if (rec.data.vis == true)
                        bred += rec.data.bredde;

                });
                Ext.getCmp('kol_tx_samlet').value = 'Samlet bredde af kolonnerne: ' + bred

                // ??? skal nulstillingen gemmes i databasen straks?
            }
        }, {
            text: 'Vis',
            handler: function () {
                window[visBredder.parent.data.kommando]();
            }
        }, '->', {
            text: 'Gem ændringer',
            hidden: (brugerid == '0'),
            handler: function () {
                // løb kolonnerne igennem og tilføj dem hvor bredde eller synlig er forskellig fra default til array
                // nej forskellig fra forrig indstilling
                // sidens bredde skal også gemmes
                // send array
                // på serveren: 
                    //slet alle brugerens bredder
                    // tilføj alle brugerens nye bredder, der adskilller sig fra standard
                    // gem sidens bredde

                // strukturen er id, bredde, vis
                var kol = [];
                var side = visBredder.parent.data.id.substring(1);
                // først formularens bredde
                if (visBredder.parent.data.bredde != visBredder.parent.data.def_bredde || visBredder.parent.data.bredde != visBredder.parent.raw.bredde )
                {
                    var flt = {};
                    flt.id = side * -1;
                    flt.bredde = visBredder.parent.data.bredde;
                    flt.side = side;
                    flt.vis = true;
                    kol.push(flt);
                }
                // og så kolonnerne
                storeKolonner.clearFilter();
                storeKolonner.filterBy(function (rec) { return rec.get('side') == visBredder.parent.data.id});
                storeKolonner.each(function (rec) {
                    if (rec.data.bredde != rec.data.def_bredde || rec.data.vis != rec.data.def_vis || rec.data.bredde != rec.raw.bredde || rec.data.vis != rec.raw.vis )
                	{
                        var flt = {};
                        flt.id = rec.data.id;
                        flt.bredde = rec.data.bredde;
                        flt.vis = rec.data.vis;
                        flt.side = side;
                        kol.push(flt);
                    }
                });
                if (kol.length == 0)
                {
                    fejlbox('Du har ikke foretaget nogen ændringer, så der er ikke noget at gemme.')
                    return;
                }
                Ext.Ajax.request({
                    url: 'Webservices/svin_login.asmx/gemKolonner',
                    headers: { 'Content-Type': 'application/json;charset=utf-8' },
                    //timeout: 600000,
                    method: 'post',
                    jsonData : { kols: kol},
                    success: function (response, opts) {
                    //    storeAD.load();
                    },
                    failure: ajaxFailed
                });
            }
        }]
    });


    var x = viewport.width - 610;
    var y = centerPanel.el.dom.offsetTop + Ext.getCmp('logoheader').getHeight() +10;

    // åbn vindue
    visBredder.vindue = new Ext.Window({
        width: 600,
        height: centerPanel.el.dom.offsetHeight -20,// - 300, //650,
        x: x,
        y: y, //200,
        closeAction: 'destroy',
        constrainHeader: true,
        modal: false,
        initCenter: false,
        autoHeight: false,
        autoScroll: true,
        anchor: '-20',
        title: 'Definer kolonnebredder',
        buttonAlign: 'right',
        items: [visBredder.tree, visBredder.fs_tabel, visBredder.knappanel],
        floating: true//,
//        buttons: [{
//            text: 'Gem ændringer',
////            iconCls: 'x-tbar-loading',
//            handler: function () {
//                //Ext.Ajax.request({
//                //    url: '/vandloeb_ws.asmx/opdater_AD',
//                //    headers: { 'Content-Type': 'application/json;charset=utf-8' },
//                //    timeout: 600000,
//                //    method: 'post',
//                //    //params: params,
//                //    success: function (response, opts) {
//                //        storeAD.load();
//                //    },
//                //    failure: ajaxFailed
//                //});
//            }
//        }, '->', {
//            text: 'Luk',
//            iconCls: 'icon-cancel',
//            handler: function () {
//                visBredder.vindue.close();
//            }
//        }]
    });
    visBredder.tree.expandAll();
    visBredder.vindue.show();


}

function retTotalbredde() {
    var bsum = 0;
    storeKolonner.clearFilter();
    storeKolonner.filterBy(function (rec) { return rec.get('side') == visBredder.parent.data.id && rec.get('vis') == true; });
    bsum = storeKolonner.sum('bredde');
    Ext.getCmp('kol_tx_samlet').setValue('Samlet bredde af kolonnerne: ' + bsum);

}
