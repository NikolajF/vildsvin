﻿function visBrugere() {
    toemFormular('brugere_panel', 'visBrugere');

    Ext.define('mAktiv', {
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'id', type: 'string'},
			{ name: 'navn', type: 'string'}
		],
		idProperty: 'id'
	});
    storeAktiv = Ext.create('Ext.data.ArrayStore', {
        model: Ext.define('mAktiv', {
			extend: 'Ext.data.Model',
			fields: ['id', 'navn']
		}),
        data: [['true', 'Aktiv'], [ 'false', 'Nedlagt']],
		sorters: [{
			property: 'navn',
			direction: 'ASC'
		}
							]
	});

	Ext.define('mBrugere', {
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'id', type: 'string', defaultValue: '0' },
			{ name: 'navn', type: 'string', defaultValue: '' },
			{ name: 'email', type: 'string', defaultValue: '' },
			{ name: 'aktiv', type: 'string', defaultValue: 'true' },
			{ name: 'firmaid', type: 'string', defaultValue: '0' },
			{ name: 'firmanavn', type: 'string', defaultValue: '' },
			{ name: 'firmaaktiv', type: 'string', defaultValue: 'true' }
		],
		idProperty: 'id'
	});

	var storeBrugere = Ext.create('Ext.data.JsonStore', {
		storeId: 'st_brugere',
		pageSize: 10,
		model: 'mBrugere',
		remoteSort: true,
		proxy: {
			type: 'gridmap',
			url: 'Webservices/svin_login.asmx/hentBrugere'
		},
		sorters: [{
			property: 'navn',
			direction: 'ASC'
		}],
		listeners: {
			load: function (store) {
				aktRec = undefined;
				Ext.get('btn_excel').down('a').dom.href = 'VisFil.ashx?type=xl_brugere&id=' + brugerid + '&gridfilter=' + store.proxy.reader.rawData.d.filterkrit;
			}

		}
	});
	storeAkt = storeBrugere;

	var filters = Ext.create('Ext.ux.grid.FiltersFeature', {
		// encode and local configuration options defined previously for easier reuse
		encode: true, // json encode the filter query
		local: false   // defaults to false (remote filtering)
	});
	var Brugere_grid = Ext.create('Ext.grid.Panel', {
		layout: 'fit',
		id: 'Brugere_grid',
		autoHeight: true,
		autoWidth: true,
		title: '',
		store: storeBrugere,
		disableSelection: false,
		loadMask: true,
		features: [filters],
		forceFit: true,
		viewConfig: {
			id: 'vg',
			//trackOver: false,
			//stripeRows: true
			getRowClass: function (record, rowIndex, rowParams, store) {
                return (record.get('firmaaktiv') == 'false' ? 'lysegraa_tekst' : record.get('aktiv') == 'false' ? 'readonly' : '');
			}
		},
		// grid columns
		// specify any defaults for each column

		columns: {
		    defaults: { renderer: renderTooltip },
		    items: [{
		        id: 'id',
		        text: "ID",
		        tooltip: "ID",
		        dataIndex: 'id',
		        width: frm_brugere.Brugere_grid.id.width,
		        hidden: frm_brugere.Brugere_grid.id.hidden,
		        //flex: 1,
		        filterable: true,
		        sortable: true,
		        filter: { type: 'numeric' }
		    }, {
		        text: "Navn",
		        tooltip: "Navn",
		        dataIndex: 'navn',
		        width: frm_brugere.Brugere_grid.navn.width,
		        hidden: frm_brugere.Brugere_grid.navn.hidden,
		        filterable: true,
		        sortable: true,
		        flex: 1
		    }, {
		        text: "Email",
		        tooltip: "Email",
		        dataIndex: 'email',
		        width: frm_brugere.Brugere_grid.email.width,
		        hidden: frm_brugere.Brugere_grid.email.hidden,
		        filterable: true,
		        flex: 1,
		        sortable: true
		    }, {
		        text: "Status",
		        tooltip: "Status",
		        dataIndex: 'aktiv',
		        width: frm_brugere.Brugere_grid.aktiv.width,
		        hidden: frm_brugere.Brugere_grid.aktiv.hidden,
		        filterable: true,
		        //flex: 1,
		        sortable: true,
		        renderer: function (value, metaData, record, row, col, store, gridView) {
		            if (value == 'true' || value == 'True')
                        value = 'Aktiv';
		            else
                        value = 'Nedlagt';
		            metaData.tdAttr = 'data-qtip="' + value + '"';
		            return value;

		        },
		        filter: {
		            type: 'list',
                    store: storeAktiv,
		            idField: 'id',
		            labelField: 'navn',
		            value: 'true',
		            active: true
		        }
		    }, {
		        text: "Firmanavn",
		        tooltip: "Firmanavn",
		        dataIndex: 'firmanavn',
		        width: frm_brugere.Brugere_grid.firmanavn.width,
		        hidden: frm_brugere.Brugere_grid.firmanavn.hidden,
		        filterable: true,
		        flex: 1,
		        sortable: true,
		        filter: { type: 'string' }
		    }, {
		        text: "Firma status",
		        tooltip: "Firma status",
		        dataIndex: 'firmaaktiv',
		        width: frm_brugere.Brugere_grid.firmaaktiv.width,
		        hidden: frm_brugere.Brugere_grid.firmaaktiv.hidden,
		        filterable: true,
		        sortable: true,
		        flex: 1,
		        renderer: function (value, metaData, record, row, col, store, gridView) {
		            if (value == 'true' || value == 'True')
                        value = 'Aktiv';
		            else
                        value = 'Nedlagt';
		            metaData.tdAttr = 'data-qtip="' + value + '"';
		            return value;

		        },
		        filter: {
		            type: 'list',
                    store: storeAktiv,
		            idField: 'id',
		            labelField: 'navn',
		            value: 'true',
		            active: true
		        }
		    }]
		},
	    // gridkol_slut
		selModel: Ext.create('Ext.selection.RowModel', {
			listeners: {
				select: {
					fn: function (selModel, rec, index) {
						aktRec = rec;
						aktRk = index;
						visAktBruger(rec.data.id);
					}
				}
			}
		}),
		// paging bar on the bottom
		tbar: Ext.create('Ext.PagingToolbar', {
			store: storeBrugere,
			displayInfo: true
		}),
		bbar: [{
			xtype: 'button',
			text: 'Tilføj ny bruger',
			iconCls: 'icon-add',
			id: 'tilfoej_bruger',
			handler: function () {
                var rc = storeAkt.add({ id: '0', navn: '', email: '', aktiv: 'true', firmaid: '0', firmanavn: '', firmaaktiv: 'true' });
				//marker den sidste (nye) række
				Brugere_grid.getSelectionModel().select(rc);
			}
		}, '->', ExcelButtonCode()
	]
});


Brugere_grid.child('pagingtoolbar').add(['->', {
	text: 'Nulstil alle filtre',
	handler: function () {
		Brugere_grid.filters.clearFilters();
	}
}]);



var Brugere_panel = Ext.create('Ext.panel.Panel', {
	title: 'Brugere',
	id: 'Brugere_panel',
	layout: 'anchor',
	bodyPadding: '10 10 10 10',
	autoWidth: true,
	autoHeight: false,
	renderTo: 'vestdiv',
	items: [
			Brugere_grid,
			{
				xtype: 'fieldset', // her er kun de redigerbare felter
				title: 'Redigér aktuel bruger',
				id: 'akt_bruger',
				bodyPadding: 10,
				autoWidth: true,
				autoHeight: true,
				style: 'margin-top: 10px',
				flex: 1,
				items: [{
					xtype: 'fccolumn',
					items: [{
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Navn',
						id: 'tx_navn',
						columnWidth: 0.6,
						allowBlank: true
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Email',
						id: 'tx_email',
						columnWidth: 0.4,
						allowBlank: true
					}]
				}, {
					xtype: 'fccolumn',
					items: [{
						xtype: 'acombo',
						hideLabel: false,
						fieldLabel: 'Firma',
						id: 'tx_firma',
						store: storeFirma,
						valueField: 'id',
						displayField: 'navn',
						allowBlank: false,
						listEmptyText: 'Feltet skal udfyldes',
						columnWidth: 0.6
					}, {
						xtype: 'fcombo',
						hideLabel: false,
						fieldLabel: 'Status',
						id: 'tx_aktiv',
						store: storeAktiv,
						valueField: 'id',
						displayField: 'navn',
						allowBlank: false,
						listEmptyText: 'Feltet skal udfyldes',
						columnWidth: 0.4
					}]
				}, {
					xtype: 'toolbar',
					style: 'margin-top: 6px',
					items: ['->', {
						xtype: 'button',
						text: 'Udfør',
						id: 'gem_bruger',
						iconCls: 'icon-ok',
						handler: function () {
							// opret et objekt med gemme-værdierne
							var g = {
								id: aktRec.data.id,
								navn: Ext.getCmp('tx_navn').getValue(),
								email: Ext.getCmp('tx_email').getValue(),
								firmaid: Ext.getCmp('tx_firma').getValue(),
                                aktiv: Ext.getCmp('tx_aktiv').getValue(),
                                firmaaktiv: aktRec.data.firmaaktiv
							};
							g.firmanavn = Ext.getCmp('tx_firma').getStore().findRecord('id', g.firmaid).get('navn');

							// er udfyldningen korrekt?
							// hvis status er afsluttet, skal der være resultat
							var fejltekst = '';

							if (g.navn === '')
								fejltekst += '\nNavn skal udfyldes';
							if (g.email === '')
								fejltekst += '\nEmail skal udfyldes';
							if (g.firmaid === '')
								fejltekst += '\nFirma skal udfyldes';
                            if (Ext.getCmp('tx_aktiv').isValid() === false)
                                fejltekst += '\nAktiv skal udfyldes';


							if (fejltekst !== '') {
								fejlbox(fejltekst);
								return;
							}


							// send til serveren
							var param = { g: g };
							var params = Ext.JSON.encode(param);


							Ext.Ajax.request({
								url: 'Webservices/svin_login.asmx/gemBruger',
								params: params,
								headers: { 'Content-Type': 'application/json;charset=utf-8' },
								success: function (response, opts) {
									// ret kun den aktuelle række
									var ret = Ext.JSON.decode(response.responseText).d;
									var opdatvar = new Array();
									opdatvar.push('id');
									opdatvar.push(ret.id);
									opdatvar.push('navn');
									opdatvar.push(ret.navn);
									opdatvar.push('email');
									opdatvar.push(ret.email);
									opdatvar.push('firmaid');
									opdatvar.push(ret.firmaid);
									opdatvar.push('firmanavn');
									opdatvar.push(ret.firmanavn);
                                    opdatvar.push('aktiv');
                                    opdatvar.push(ret.aktiv);
                                    opdatvar.push('firmaaktiv');
                                    opdatvar.push(ret.firmaaktiv);
									opdaterRaekke(opdatvar);
								},
								failure: ajaxFailed
							});
						}

					}]
				}]
			}]
});
	aktGrid = 'Brugere_grid';
	// vi skal sikre at de indledende filtre sættes
	filters.createFilters();
	retKolonner(Brugere_grid, 2);
	storeBrugere.load();
}




function visAktBruger(id) {
	if (aktRec === undefined) {
		Ext.getCmp('akt_bruger').setTitle('Ingen aktuel bruger');
		Ext.getCmp('tx_navn').setValue('');
		Ext.getCmp('tx_email').setValue('');
		Ext.getCmp('tx_firma').setOriginal('');
        Ext.getCmp('tx_aktiv').setValue('');
	} else if (aktRec.data.id == '0') {
		Ext.getCmp('akt_bruger').setTitle('Opret ny bruger');
		Ext.getCmp('tx_navn').setValue('');
		Ext.getCmp('tx_email').setValue('');
		Ext.getCmp('tx_firma').setOriginal('');
        Ext.getCmp('tx_aktiv').setValue('true');
		enableCtl(Ext.getCmp('tx_firma'), true);
        enableCtl(Ext.getCmp('tx_aktiv'), false);
	} else {
		// eksisterende bruger, kan redigeres
		Ext.getCmp('akt_bruger').setTitle('Redigér eksisterende bruger');
		enableCtl(Ext.getCmp('tx_firma'), false);
        enableCtl(Ext.getCmp('tx_aktiv'), true);
		Ext.getCmp('tx_navn').setValue(aktRec.data.navn);
		Ext.getCmp('tx_email').setValue(aktRec.data.email);
		Ext.getCmp('tx_firma').setOriginal(aktRec.data.firmaid);
        Ext.getCmp('tx_aktiv').setValue(aktRec.data.aktiv);
	}
}


