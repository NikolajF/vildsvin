﻿

function mapMoveEnd() {
	var obj = mapPanel.map.getExtent();
	x1 = obj.left;
	x2 = obj.right;
	y1 = obj.bottom;
	y2 = obj.top;
	storeAkt.currentPage = 1;
	storeAkt.load();
}

function ExcelButtonCode(navn)
{
	if (navn == undefined)
		navn = 'btn_excel';
	var ctl = Ext.create('Ext.Button', {
		id: navn,
		text: 'Excel rapport',
		iconCls: 'icon-excel',
		href: 'VisFil.ashx?type=xl&id=' + brugerid, // værdien sættes først rigtigt på selve siden
		target: '_blank'
	});
	return ctl;
}
function createZoombar(ExcelKnap)
{
    //var ctl = Ext.define('zoombar', {
	//    extend: 'Ext.toolbar.Toolbar',
	//    alias: 'widget.zoombar',
    var ctl = Ext.create('Ext.toolbar.Toolbar', {
        items: [{
		    text: 'Begræns til kortudsnit',
		    //cls: 'ux-filtered-column',
		    //iconCls: 'icon-select-area',
		    id: 'selectonmap',
		    toggleGroup: 'zoomgroup',
		    enableToggle: true,
		    listeners: {
			    toggle: function (ctl, pressed) {
				    if (pressed == true) {
					    // kun hvis knappen er trykket ned
					    ctl.btnInnerEl.setStyle({ color: '#600000', fontWeight: 'bold' });

					    mapPanel.map.events.register("moveend", this, mapMoveEnd);
					    mapMoveEnd();
				    }
				    else {
					    ctl.btnInnerEl.setStyle({ color: '#333333', fontWeight: 'normal' });
					    mapPanel.map.events.unregister('moveend', this, mapMoveEnd);
					    x1 = 0;
					    y1 = 0;
					    x2 = 0;
					    y2 = 0;
					    storeAkt.currentPage = 1;
					    storeAkt.load();
				    }
			    }
		    }
	    }, '', {
		    xtype: 'button',
		    text: 'Zoom til liste',
		    id: 'zoomliste',
		    toggleGroup: 'zoomgroup',
		    enableToggle: true,
		    listeners: {
			    toggle: function (ctl, pressed) {
				    if (pressed == true) {
					    mapPanel.zoomTo(zmListe);
				    }
			    }
		    }
	    }, '', {
		    xtype: 'button',
		    id: 'zoommarkeret',
		    text: 'Zoom til markeret',
		    style: 'padding-left: 10px; padding-right: 5px',
		    toggleGroup: 'zoomgroup',
		    enableToggle: true,
		    listeners: {
			    toggle: function (ctl, pressed) {
				    if (pressed == true) {
                        // kun hvis der en aktuel post,dvs hvis aiz er forskellig fra 0,0,0,0
                        if (aiz[0] != 0)
				            mapPanel.zoomTo(aiz);
				    }
			    }
		    }
	    }, '->' ] // slut toolbars items
    });

    if (ExcelKnap && brugerid > 0){
    	ctl.add(ExcelButtonCode());
    }
    //switch (aktPanel) {
    //	case 'Levende_panel':
    //	case 'Indsamlinger_panel':
    //	case 'LabListe_panel': //lab
    //	case 'FindVildsvin_panel':
    //		ctl.add(ExcelButtonCode());
    //		ctl.add({
    //			xtype: 'button',
    //			id: 'btn_excel',
    //			text: 'Excel rapport',
    //			iconCls: 'icon-excel',
    //			href: 'VisFil.ashx?type=xl&id=' + brugerid, // værdien sættes først rigtigt på selve siden
    //			//id: 'btn_lab_csv',
    //			//text: 'Excel rapport',
    //			//iconCls: 'icon-excel',
    //			//href: 'VisFil.ashx?type=lab&id=' + brugerid,
    //			target: '_blank'
    //		});
    //	break;
    //}

    switch (aktPanel) {
    	case 'Indsamlinger_panel':
    	    ctl.add({
    	        xtype: 'button',
    	        text: 'Tilføj vildsvin',
    	        id: 'tilfoej_vildsvin',
    	        iconCls: 'icon-add',
    	        handler: tilfoejIndsamlerVildsvin
    	    });
    	    break;
    }

    if (aktPanel == 'LabListe_panel') // || aktPanel == 'Levende_panel' ) //lab
    {
        ctl.add({
            xtype: 'button',
            text: 'Ny indberetning', //(aktPanel == 'Levende_panel' ? 'Tilføj fugle' : 'Ny indberetning'),
            id: 'tilfoej_vildsvin',
            iconCls: 'icon-add',
            handler: function () {
                //if (aktPanel == 'LabListe_panel') //lab
                //{
                //    // ??? husk listens filter-kriterier
                indberetningsformular(-1);
                //}
                //else  //Levende
                //{
                //    indberetningsformular(0);
                //}
            }
        });

    }
    return ctl;

}
//var zoombar = Ext.define('zoombar', {
//	extend: 'Ext.toolbar.Toolbar',
//	alias: 'widget.zoombar',
//	items: [{
//		text: 'Begræns til kortudsnit',
//		//cls: 'ux-filtered-column',
//		//iconCls: 'icon-select-area',
//		id: 'selectonmap',
//		toggleGroup: 'zoomgroup',
//		enableToggle: true,
//		listeners: {
//			toggle: function (ctl, pressed) {
//				if (pressed == true) {
//					// kun hvis knappen er trykket ned
//					ctl.btnInnerEl.setStyle({ color: '#600000', fontWeight: 'bold' });

//					mapPanel.map.events.register("moveend", this, mapMoveEnd);
//					mapMoveEnd();
//				}
//				else {
//					ctl.btnInnerEl.setStyle({ color: '#333333', fontWeight: 'normal' });
//					mapPanel.map.events.unregister('moveend', this, mapMoveEnd);
//					x1 = 0;
//					y1 = 0;
//					x2 = 0;
//					y2 = 0;
//					//storeAkt.currentPage = 1;
//					//storeAkt.load();
//				}
//			}
//		}
//	}, '', {
//		xtype: 'button',
//		text: 'Zoom til liste',
//		id: 'zoomliste',
//		toggleGroup: 'zoomgroup',
//		enableToggle: true,
//		listeners: {
//			toggle: function (ctl, pressed) {
//				if (pressed == true) {
//					mapPanel.zoomTo(zmListe);
//				}
//			}
//		}
//	}, '', {
//		xtype: 'button',
//		id: 'zoommarkeret',
//		text: 'Zoom til markeret',
//		style: 'padding-left: 10px; padding-right: 5px',
//		toggleGroup: 'zoomgroup',
//		enableToggle: true,
//		listeners: {
//			toggle: function (ctl, pressed) {
//				if (pressed == true) {
//					mapPanel.zoomTo(aiz);
//				}
//			}
//		}
//	}, '->', {
//		xtype: 'button',
//		text: 'Tilføj fugl',
//		id: 'tilfoej_fugl',
//		iconCls: 'icon-add',
//		hidden: true,
//		handler: function () {
//		    storeFuglearterVisit.clearFilter();
//		    // skal bruges af indsamlere og lab
//			//if (aktPanel == 'Indsamlinger_panel') {
//			//    var nr = storeAkt.getCount();
//			//    var ny = Ext.create('mIndsamlinger', {
//            //        id: '0',
//			//        afd: firma
//			//    });
//			//    storeAkt.add(ny); // modellen, med standardværdier
//			//    // sæt fokus på den nye række
//			//    Ext.getCmp("Indsamlinger_grid").getView().select(nr);

//			//}
//			//else
//			if (aktPanel == 'LabListe_panel') //lab
//			{
//				// ??? husk listens filter-kriterier
//				indberetningsformular(-1);
//			}
//			else  //Levende
//			{
//				indberetningsformular(0);
//			}


//		}
//	}] // slut toolbars items
//});


var forceCombo = Ext.define('forceCombo', {
	extend: 'Ext.form.ComboBox',
	alias: 'widget.fcombo',
	hideLabel: false,
	typeAhead: true,
	minChars: 1,
	typeAheadDelay: 50,
	forceSelection: true,
	autoSelect: true,
	queryMode: 'local'
});

var comboTemplate = Ext.create('Ext.XTemplate',
    '<tpl for=".">',
        '<div class="x-boundlist-item {[this.getClass(values)]}">{navn}</div>',
    '</tpl>',
    {
        getClass: function (rec) {
            return rec.aktiv == true ? 'comboItemActive' : 'comboItemDisabled';
        }
    }
);

var forceComboActive = Ext.define('forceComboActive', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.acombo',
    hideLabel: false,
    typeAhead: true,
    minChars: 1,
    typeAheadDelay: 50,
    forceSelection: true,
    autoSelect: true,
    queryMode: 'local',
    tpl: comboTemplate,
    original: '',
    feltnr: 0,
    matchFieldWidth: false,
    setOriginal: function (value) {
        // her går vi ud fra at værdien altid er en streng, ikke et array
        if (value === null && typeof value === "object")
            this.original = '';
        else
            this.original = value + '';
        if (this.original == null)
            console.log(this.fieldLabel + '  null fra  |' + value + '| ');
//        var v = eval('[' + this.original + ']');
        //        this.setValue(v);
        this.setValue(this.original);
    },
    getVaerdi: function () {
        // returnerer streng, ikke array
        var svar = this.getValue();
        if (svar == null)
            svar = '';
        else
            svar = svar.toString();
        return svar;
    },
    aendret: function () {
        var ny = this.getValue();
        if (ny == null)
            ny = '';
        else
            ny = ny.toString();
        var svar = (ny != this.original);
        return svar;
    },
    listeners: {
        beforeselect: function (combo, record, index) {
            // nedlagte værdier må ikke vælges - kun hvis template er slået til
            if (combo.tpl != '' && record.data.aktiv == false && record.data.id != combo.original) {
                fejlbox('Denne værdi kan ikke benyttes.');
                combo.setValue(combo.original);
                return false;
            }
        }
    }

});

var fnumber = Ext.define('fnumber', {
    extend: 'Ext.form.field.Number',
    alias: 'widget.fnumber',
    hideLabel: false,
    original: '',
    feltnr: 0,
    allowDecimals: false,
    decimalPrecision: 5,
    keyNavEnabled: false,
    mouseWheelEnabled: false,
    setOriginal: function (value) {
        if (value === null && typeof value === "object")
            this.original = '';
        else
            this.original = value;
        if (this.original == null)
            console.log(this.fieldLabel + '  null fra  |' + value + '| ');
        this.setValue(this.original);
    },
    getVaerdi: function () {
        // returnerer streng, ikke array
        var svar = this.getValue();
        if (svar == null)
            svar = '';
        else
            svar = svar.toString();
        return svar;
    },
    aendret: function () {
        var ny = this.getValue();
        if (ny == null)
            ny = '';
        return (ny != this.original);
    }

});

var fccolumn = Ext.define('fccolumn', {
	extend: 'Ext.form.FieldContainer',
	alias: 'widget.fccolumn',
	hideLabel: true,
	autoWidth: true,
	autoHeight: true,
	//resizable: true,
	layout: 'column',
	alignment: 'stretch',
	flex: 1,
	defaults: {
		labelSeparator: '',
		labelAlign: 'top',
		margin: '0 3 0 3',
		//margin: '0',
		flex: 1,
		autoWidth: true,
		autoHeight: true,
		xtype: 'textfield'
	}
});

var fccolumn_ro = Ext.define('fccolumn_ro', {
	extend: 'Ext.form.FieldContainer',
	alias: 'widget.fccolumn_ro',
	hideLabel: true,
	autoWidth: true,
	autoHeight: true,
	//resizable: true,
	layout: 'column',
	alignment: 'stretch',
	flex: 1,
	defaults: {
		labelSeparator: '',
		labelAlign: 'top',
		margin: '0 3 0 3',
		//margin: '0',
		flex: 1,
		autoWidth: true,
		autoHeight: true,
		xtype: 'textfield',
		readOnly: true,
		fieldCls: 'readonly'
	}
});






/************* PROXY   ********************/

// den simple
Ext.define('Vildsvin.proxy.ProxySimpel', {
	alias: 'proxy.simpel',
	extend: 'Ext.data.proxy.Ajax',
	headers: { 'Content-Type': "application/json" },
	url: '',
	actionMethods: { create: 'POST', read: 'POST', update: 'POST', destroy: 'POST' },
	params: {},
	encode: true,
	pageParam: undefined,
	limitParam: undefined,
	startParam: undefined,
	sortParam: undefined,
	directionParam: undefined,

	constructor: function () {
		this.reader = {
			type: 'json',
			root: 'd.List',
			totalProperty: 'd.TotalCount',
			encode: true
		};
		this.writer = {
			type: 'json',
			encode: true
		};
		this.callParent(arguments);
	},
	doRequest: function (operation, callback, scope) {
		var writer = this.getWriter(),
			request = this.buildRequest(operation, callback, scope);

		if (operation.allowWrite()) {
			request = writer.write(request);
		}

		var pr = Ext.JSON.encode(operation.request.params);
		operation.request.params = pr;

		Ext.apply(request, {
			headers: this.headers,
			timeout: this.timeout,
			scope: this,
			callback: this.createRequestCallback(request, operation, callback, scope),
			method: this.getMethod(request)//,
		});
		Ext.Ajax.request(request);
		return request;
	},
	// sends single sort as multi parameter
	simpleSortMode: true
});

// den komplicerede med mange parametre
Ext.define('Vildsvin.proxy.ProxyGridMap', {
	alias: 'proxy.gridmap', 
	extend: 'Ext.data.proxy.Ajax',
	headers: { 'Content-Type': "application/json" },
	url: '',
	actionMethods: { create: 'POST', read: 'POST', update: 'POST', destroy: 'POST' },
	//extraParams: {},
	encode: true,
	constructor: function() { 
		this.reader = { 
			type: 'json',
				root: 'd.List',
				totalProperty: 'd.TotalCount',
				encode: true
		}; 
		this.writer= { 
			type: 'json',
			encode: true
		}; 
		this.callParent(arguments); 
	}, 
	doRequest: function (operation, callback, scope) {
		var writer = this.getWriter(),
			request = this.buildRequest(operation, callback, scope);

		if (operation.allowWrite()) {
			request = writer.write(request);
		}
		operation.request.params.x1 = x1;
		operation.request.params.x2 = x2;
		operation.request.params.y1 = y1;
		operation.request.params.y2 = y2;

		var xxx = operation.params['filter'];
		if (xxx === undefined) {
			operation.request.params.filter = proxyDummy;
			operation.request.params = Ext.JSON.encode(operation.request.params);
		}
		else {
			var fl = eval(operation.request.params.filter);
			delete operation.request.params.filter;
			var pr = Ext.JSON.encode(operation.request.params);
			//var pr = JSON.stringify(operation.request.params);
			for (i = 0; i < fl.length; i++) {
				if (fl[i].type == 'list') {
					var nv = '';
					for (j = 0; j < fl[i].value.length; j++) {
						nv += ',' + fl[i].value[j];
					}
					fl[i].value = nv.substr(1);
				}
			}
			fl = Ext.JSON.encode(fl);
			operation.request.params = pr.substring(0, pr.length - 1) + ', "filter":' + fl + '}';
		}

		Ext.apply(request, {
			headers: this.headers,
			timeout: this.timeout,
			scope: this,
			callback: this.createRequestCallback(request, operation, callback, scope),
			method: this.getMethod(request)//,
			//				disableCaching: false // explicitly set it to false, ServerProxy handles caching
		});
		Ext.Ajax.request(request);
		return request;
	},
	// sends single sort as multi parameter
	simpleSortMode: true
});

Ext.define('id_navn', {
	extend: 'Ext.data.Model',
	fields: [
			'id',
			'navn'
		],
	idProperty: 'id'
});

Ext.define('id_navn_aktiv', {
    extend: 'Ext.data.Model',
    fields: [
			'id',
			'navn',
            {name: 'aktiv', type: 'boolean'}
    ],
    idProperty: 'id'
});


//var svin_Proxy = Ext.define('svin_Proxy', {
//	extend: 'Ext.data.proxy.Proxy',
//	alias: 'widget.aiProxy',
//	type: 'ajax',
//	headers: { 'Content-Type': "application/json" },
//	url: 'Webservices/svin_login.asmx/hentIndberetFugle',
//	actionMethods: { create: 'POST', read: 'POST', update: 'POST', destroy: 'POST' },
//	encode: true,
//	reader: {
//		type: 'json',
//		root: 'd.List',
//		totalProperty: 'd.TotalCount',
//		encode: true
//	},
//	writer: {
//		type: 'json',
//		encode: true
//	},
//	doRequest: function (operation, callback, scope) {
//		var writer = this.getWriter(),
//		request = this.buildRequest(operation, callback, scope);

//		if (operation.allowWrite()) {
//			request = writer.write(request);
//		}

//		var pr = Ext.JSON.encode(operation.request.params);
//		operation.request.params = pr;

//		Ext.apply(request, {
//			headers: this.headers,
//			timeout: this.timeout,
//			scope: this,
//			callback: this.createRequestCallback(request, operation, callback, scope),
//			method: this.getMethod(request)//,
//			//disableCaching: false // explicitly set it to false, ServerProxy handles caching
//		});
//		Ext.Ajax.request(request);
//		return request;
//	},
//	// sends single sort as multi parameter
//	simpleSortMode: true
//});

//// selve funktionen
//function proxyFilter(store, options) {
//	// vi skal samle alle parametre til en samlet forespørgsel til serveren
//	// men hvor kan vi aflevere det?
//	this.proxy.request.params = JSON.stringify(this.proxy.extraParams);

//	//var nyfilter = { 'sort': this.sorters.items[0].property, 'dir': this.sorters.items[0].direction  };
//	// nyfilter.add(this.proxy.extraParams);
//	//	proxy.conn.jsonData = params;
//	//	proxy.conn.jsonData.limit = proxy.conn.sidestr;
//	//	var fil;
//	//	if (params.filter === undefined) {
//	//		proxy.conn.jsonData.filter = proxyDummy;
//	//	}
//	//	else {
//	//		fil = eval(params.filter);
//	//		for (i = 0; i < fil.length; i++) {
//	//			if (fil[i].type == 'list') {
//	//				var nv = '';
//	//				for (j = 0; j < fil[i].value.length; j++) {
//	//					nv += ',' + fil[i].value[j];
//	//				}
//	//				fil[i].value = nv.substr(1);
//	//			}
//	//		}
//	//		proxy.conn.jsonData.filter = fil; //eval(params.filter);
//	//	}
//	// 
//	//	// hvis params er ændret, skal sidetallet nulstilles
//	//	if (proxy.conn.jsonData.start === undefined) {
//	//		proxy.conn.jsonData.start = 0;
//	//	}
//	//	// hvis filter eller sortering er ændret, skal sidetallet nulstilles
//	//	else if (fil != undefined && proxy.conn.gemmeParam != undefined) {
//	//		var pf = Ext.encode(fil);
//	//		var gf = Ext.encode(proxy.conn.gemmeParam.filter);
//	//		var filterens = (pf == gf);
//	//		if (filterens == false && params.filter === undefined && proxy.conn.gemmeParam.filter.length == 1 && proxy.conn.gemmeParam.filter[0].field == 'dummy') filterens = true;

//	//		if (filterens == false || params.sort != proxy.conn.gemmeParam.sort || params.dir.toLowerCase() != proxy.conn.gemmeParam.dir.toLowerCase()) {
//	//			proxy.conn.jsonData.start = 0;
//	//		}
//	//	}
//	//	// her gemmer vi de samlede parametre
//	//	proxy.conn.gemmeParam = proxy.conn.jsonData;
//}



// kolonnebredder i to tabeller - til efterregulering af grids
function hentKolonner()
{

    Ext.define('mGrid', {
        extend: 'Ext.data.Model',
        fields: [
                { name: 'text', type: 'string' },
                { name: 'id', type: 'string' },
                { name: 'leaf', type: 'boolean' },
                { name: 'niveau', type: 'numeric' },
                { name: 'navn', type: 'string' },
                { name: 'kort', type: 'boolean' },
                { name: 'bredde', type: 'numeric' },
                { name: 'def_bredde', type: 'numeric' },
                { name: 'kommando', type: 'string' }
            ]
    });
    grid_treestore = Ext.create('Ext.data.TreeStore', {
        model: 'mGrid',
        autoLoad: true,
        clearOnLoad: true,
        proxy: {
            type: 'ajax',
            url: '/Webservices/svin_login.asmx/hentGrid',
            //actionMethods : {create: 'POST', read: 'POST', update: 'POST', destroy: 'POST'},
            headers: { 'Content-Type': 'application/json;charset=utf-8' },
            reader: {
                type: 'json',
                //method: 'post',
                root: 'd'
            }
        },
        root: {
            text: 'root',
            id: '´0',
            leaf: false,
            niveau: 0,
            navn: '',
            expanded: true
        },
        folderSort: false
    });

    Ext.define('mKol', {
        extend: 'Ext.data.Model',
        fields: [
            { name: 'side', type: 'string' },
            { name: 'titel', type: 'string' },
            { name: 'raekkefoelge', type: 'numeric' },
            { name: 'id', type: 'numeric' },
            { name: 'bredde', type: 'numeric' },
            { name: 'vis', type: 'boolean' },
            { name: 'def_bredde', type: 'numeric' },
            { name: 'def_vis', type: 'boolean' }
],
        idProperty: 'id'
    });

    storeKolonner= Ext.create('Ext.data.JsonStore', {
        storeId: 'st_kolonner',
        //pageSize: 10,
        model: 'mKol',
        remoteSort: true,
        autoLoad: true,
        proxy: {
            type: 'simpel',
            url: 'Webservices/svin_login.asmx/hentKolonner'
        },
        sorters: [{
            property: 'raekkefoelge',
            direction: 'ASC'
        }]//,
        //listeners: {
        //    load : function (store, rec, opts) {
        //        var i = 0;
        //        store.each(function (record) {
        //            i++;
        //        }, this);
        //        //console.log(i + ' records');
        //    }
        //}
    });
    //storeKolonner.load({
    //    callback: function (records, operation, success) {
    //        /* perform operations on the records*/
    //        //storeGrids.load();
    //        visLabAnalyser();
    //    }
    //});
}

function retKolonner(grid, sidenr)
{
    storeKolonner.clearFilter();
    storeKolonner.filterBy(function (rec) { return rec.get('side') == 'a' + sidenr; });
    storeKolonner.each(function (rec) {
        var ct = grid.query('gridcolumn')[rec.data.raekkefoelge - 1];
        ct.setWidth(rec.data.bredde);
        ct.setVisible(rec.data.vis);

    });
}

function tegn_fs_anmelder()
{
	var fstemp = Ext.create('Ext.form.FieldSet', {
		title: 'Oplysninger om anmelder',
		margins: 6,
		autoWidth: true,
		autoHeight: true,
		flex: 1,
		items: [{
			xtype: 'fccolumn_ro',
			items: [{
				fieldLabel: 'Navn',
				id: 'an_navn',
				//margin: '0 3 0 0', // venstre
				columnWidth: 0.75
			}, {
				fieldLabel: 'Observeret dato',
				xtype: 'datefield',
				format: 'd-m-Y',
				altFormats: 'd/m/Y H:i:s',
				id: 'set_tid',
				flex: 1,
				//margin: '0 0 0 0', // højre
				columnWidth: 0.25
			}]
		}, {
			xtype: 'fccolumn_ro',
			items: [{
				fieldLabel: 'Vejnavn',
				//margin: '0 3 0 0', // venstre
				id: 'an_vejnavn',
				columnWidth: 0.35
			}, {
				fieldLabel: 'Husnr',
				id: 'an_husnr',
				//margin: '0 3 0 0', // højre
				columnWidth: 0.15
			}, {
				fieldLabel: 'Postnr',
				id: 'an_postnr',
				//margin: '0 3 0 0', // venstre
				columnWidth: 0.15
			}, {
				fieldLabel: 'Postdistrikt',
				id: 'an_by',
				flex: 1,
				//margin: '0 0 0 0', // højre
				columnWidth: 0.35
			}]
		}, {
			xtype: 'fccolumn_ro',
			items: [{
				fieldLabel: 'Telefon',
				id: 'an_telefon',
				//margin: '0 3 0 0', // venstre
				columnWidth: 0.5
			}, {
				fieldLabel: 'Email',
				id: 'an_email',
				flex: 1,
				//margin: '0 0 0 0', // højre
				columnWidth: 0.5
			}]
		}]
	});
	return fstemp;
}

function tegn_fs_fund(combo)
{
	var ctl;
	if (combo == true)
	{
		ctl = {
			xtype: 'combo',
			fieldLabel: 'Fundtype',
			id: 'fundtype',
			//margin: '0 3 0 0', // højre
			store: storeFundtyper,
			valueField: 'id',
			displayField: 'navn',
			columnWidth: 0.3
		};
	}
	else
	{
		ctl = {
			xtype: 'textfield',
			fieldLabel: 'Fundtype',
			id: 'fundtype',
			//margin: '0 3 0 0', // højre
			columnWidth: 0.3
		};
	}

	var fstemp = Ext.create('Ext.form.FieldSet', {
		title: 'Oplysninger om fund og fundsted',
		autoWidth: true,
		autoHeight: true,
		flex: 1,
		items: [{
			xtype: 'fccolumn_ro',
			items: [{
				xtype: 'textfield',
				hideLabel: false,
				fieldLabel: 'Antal',
				id: 'antal',
				margin: '0 3 0 0', // venstre
				columnWidth: 0.2
			}, ctl, {
				xtype: 'textfield',
				fieldLabel: 'Øst',
				id: 'setx',
				margin: '0 3 0 0', // venstre
				columnWidth: 0.25
			}, {
				xtype: 'textfield',
				fieldLabel: 'Nord',
				id: 'sety',
				margin: '0 0 0 3', // højre
				columnWidth: 0.25
			}]
		}, {
			xtype: 'fccolumn_ro',
			items: [{
				xtype: 'textarea',
				fieldLabel: 'Beskrivelse af fundet',
				id: 'setbeskriv',
				margin: '0', // højre
				columnWidth: 1,
				grow: true,
				growMin: 30,
				growMax: 300
			}]
		}, {
			xtype: 'fccolumn_ro',
			items: [{
				fieldLabel: 'Vejnavn',
				id: 'setvejnavn',
				margin: '0 3 0 0', // venstre
				columnWidth: 0.35
			}, {
				fieldLabel: 'Husnr',
				id: 'sethusnr',
				//margin: '0 3 0 0', // højre
				columnWidth: 0.15
			}, {
				fieldLabel: 'Postnr',
				id: 'setpostnr',
				//margin: '0 3 0 0', // venstre
				columnWidth: 0.15
			}, {
				fieldLabel: 'Postdistrikt',
				id: 'setby',
				margin: '0 0 0 3', // højre
				columnWidth: 0.35
			}]
		}, {
			xtype: 'fccolumn_ro',
			items: [{
				xtype: 'textarea',
				fieldLabel: 'Beskrivelse af fundstedet - hvis det ikke blot er en adresse',
				id: 'fundsted',
				margin: '0', // højre
				columnWidth: 1,
				grow: true,
				growMin: 30,
				growMax: 300
			}]
		}, {
			xtype: 'fccolumn_ro',
			items: [{
				xtype: 'textarea',
				fieldLabel: 'Kommentar til indberetningen',
				id: 'indberetning_kommentar',
				margin: '0',
				columnWidth: 1,
				//height: 20,
				grow: true,
				growMin: 30,
				growMax: 300
			}]
		}]
	});
	return fstemp;
}

function tegn_fs_fotografi() {
	var fstemp = Ext.create('Ext.form.FieldSet', {
		title: 'Fotografi',
		//id: 'lb_billede',
		bodyPadding: 10,
		autoWidth: true,
		autoHeight: true,
		style: 'margin-top: 10px',
		flex: 1,
		items: [{
			xtype: 'image',
			id: 'billede',
			src: '',
			hidden: true,
			shrinkWrap: true,
			width: '100%'
		}]
	});
	return fstemp;
}
