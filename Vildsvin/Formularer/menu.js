﻿/*
// ekstra funktion
function cloneObject(source){
for (i in source){
if (typeof source[i] == 'source'){
this[i] = new cloneObject(source[i]);
}
else{
this[i] = source[i];
}
}
}
 */

// globale variable
var brugernavn = '';
var brugerid = '0';
var firma = '0';
var firmatype = '0';
var firmanavn = '';

var vestPanel;
var centerPanel;
var aktPanel = '';
var aktGrid = '';
var aktRec;
var aktRk;
var aktID = -1;
var cssReadonly = 'color: #1a5d4b;';
var cssFejl = 'color: #ff0000;';
var graanet_raekke = 'color: #666666;';
var brd = 500;

//var fed_valgt = 'font-weight: bold; color: black';
//var sundhedsfarver = new Array('#cccccc', '#00ff00', '#ffff00', '#ff0000');
var sundhedsfarver = ['#cccccc', '#00ff00', '#ffff00', '#ff0000'];
var header_luft = 'font-family: tahoma,arial,helvetica,sans-serif; font-size: 11px; margin-top: 6px;';
var header = 'font-family: tahoma,arial,helvetica,sans-serif; font-size: 11px; ';
//var header_fed = 'font-family: tahoma,arial,helvetica,sans-serif; font-size: 11px; font-weight: bold;';
var helpwin;
var mapPanel;
var legendbred = 230;
var legendhoej = 260;
var sidestr20 = 20;
var sidestr15 = 15;
var sidestr10 = 10;
var dummy = [{
		'type' : 'string',
		'value' : '',
		'field' : 'dummy'
	}];
var hjaelpkrit = "";
var begivMenu;
//var kmsticket = '';
var udbrudID = '0';
var storeFundtyper;
var storeFirma;
var storeFirmaVisit;
var storeIndsamlere;
var storeIndsamlereA; // til brug ved afugle, dvs uden frasorteret ved Levende
var storeOpsamlere;
var opsamlerMenu;

var storeFVRegion;
var storeStatus;
//var storeStatusBRS;
//var storeStatusLAB;
var storeSkjul;
var skjulMenu;
var epidemi = false;
var indsamlertype = '2';
var kanGemme = false;

var store_pcr_test;
var store_virus_isolation;
var store_virus_pathotype;
var store_h;
var store_n;

var centerDiv;
//var aktuelIndberetning;
var aiz = [0, 0, 0, 0];
var zmListe = [0, 0, 0, 0];
var x1 = 0;
var y1 = 0;
var x2 = 0;
var y2 = 0;
var storeAkt;
var header_height = 90;
var toolbar_height = 30;
var popwin;
var ibWin;
var retLaastPost = false;

// global variabel
var BackKeyCalled = false;
var isIE = (navigator.userAgent.indexOf("MSIE") != -1);
if (isIE == false)
	isIE = (navigator.userAgent.indexOf("rv:11") != -1);

// sådan her kan IE 11 svare
//Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; .NET CLR 2.0.50727; .NET CLR 3.0.30729; .NET CLR 3.5.30729; InfoPath.3; rv:11.0) like Gecko
window.onbeforeunload = function () {
	if (BackKeyCalled || isIE)
	{
		BackKeyCalled = false;
		return "Er du sikker på at du ønsker at forlade siden?"
		//	return "You have not saved your document yet.  If you continue, your work will not be saved."
	}
}
//console.log(navigator.userAgent);

// sørg for at indlæse de relevante klasser fra Ext
Ext.Loader.setConfig({
	enabled : true
});
Ext.Loader.setPath('Ext.ux', '../ux/');
Ext.require([
	'Ext.grid.*',
	'Ext.data.*',
	'Ext.util.*',
	'Ext.ux.grid.FiltersFeature',
	'Ext.toolbar.Paging',
	'Ext.ux.PreviewPlugin',
	'Ext.ModelManager',
	'Ext.tip.QuickTipManager',
	'Ext.container.Viewport',
	'Ext.layout.container.Border',
	'GeoExt.panel.Map',
	'GeoExt.container.WmsLegend',
	'GeoExt.container.UrlLegend',
	'GeoExt.container.VectorLegend',
	'GeoExt.panel.Legend'
]);

//Ext.Loader.setConfig({ enabled: true });
//Ext.Loader.setPath('Ext.ux', '../ux');
//Ext.require([
//	'Ext.grid.*',
//	'Ext.data.*',
//	'Ext.toolbar.Paging',
//	'Ext.ux.ajax.JsonSimlet',
//	'Ext.ux.ajax.SimManager'
//]);

// tegning af menu og skærm
Ext.onReady(function () {

	Ext.EventManager.on(document, 'keydown', function (e) {
		if (e.getKey() == 8) {
			BackKeyCalled = true;
		}
	});

	Ext.EventManager.on(document, 'keyup', function (e) {
		if (e.getKey() == 8) {
			BackKeyCalled = false;
		}
	});

	Ext.define('Ext.overrides.form.field.Text', {
	    override: 'Ext.form.field.Base',
	    /**
         * Ensure all text fields have spell check disabled
         */
	    inputAttrTpl: [
            'spellcheck=false'
	    ],
	    constructor: function () {

	        this.callParent(arguments);

	    }
	});


    // Override for adding tooltips to form fields
	Ext.override(Ext.form.Field, {
	    afterRender: function () {
	        this.callParent(arguments);
	        try {
	            if (this.qtipText) {
	                create_tooltip_fields(this, this.qtipText);
	            }
	        } catch (e) { }
	    }
	});

	function create_tooltip_fields(field, tooltipText) {
	    Ext.QuickTips.register({
	        target: field.getEl(),
	        title: '',
	        text: '<span style="">' + tooltipText + '</span>',
	        enabled: true,
	        trackMouse: true
	    });
	    var label = findLabel(field);
	    if (label) {
	        Ext.QuickTips.register({
	            target: label,
	            title: '',
	            text: '<span style="">' + tooltipText + '</span>',
	            enabled: true,
	            trackMouse: true
	        });
	    }
	}

	var findLabel = function (field) {

	    var wrapDiv = null;
	    var label = null

	    // find form-element and label
	    wrapDiv = field.getEl().up('div.x-form-element');
	    if (wrapDiv)
	        label = wrapDiv.child('label');

	    if (label)
	        return label;

	    // find form-item and label
	    wrapDiv = field.getEl().up('div.x-form-item');
	    if (wrapDiv)
	        label = wrapDiv.child('label');

	    if (label)
	        return label;
	}

	/*
	// ret fejl i IE9 - menuer for små
	Ext.override(Ext.menu.Menu,{
	autoWidth: function() {
	//var ua = navigator.userAgent.toLowerCase();
	//var isIE8 = !Ext.isOpera && ua.indexOf("msie 8") > -1;
	var el = this.el, ul = this.ul;
	if (!el) {
	return;
	}
	if (Ext.isIE9) {
	el.setWidth(this.minWidth);
	var t = el.dom.offsetWidth; // force recalc
	el.setWidth(ul.getWidth() + el.getFrameWidth("lr"));
	}
	else {
	var w = this.width;
	if (w) {
	el.setWidth(w);
	}
	}
	}
	});

	Ext.override(Ext.form.Checkbox, {
	onClick: function(e, o) {
	if (this.readOnly === true) {
	e.preventDefault();
	} else {
	if (this.el.dom.checked != this.checked) {
	this.setValue(this.el.dom.checked);
	}
	}
	}
	});

	*/

	Ext.tip.QuickTipManager.init();
	Ext.form.Field.prototype.msgTarget = 'title'; //qtip


	//Ext.override('Ext.form.field.Radio', {
	//    boxLabelCls: 'radio_nopadding x-form-cb-label x-form-cb-label-after'
	//});


	var toolbaritems = [{
			xtype : 'button',
			toggleGroup: 'menuer',
			enableToggle: true,
			pressed: true,
			id: 'menuForside',
			text : 'Forside',
			style : 'margin-right: 15',
			handler : visForside
		}, {
			xtype : 'button',
			toggleGroup: 'menuer',
			enableToggle: true,
			id: 'menuLogin',
			text : 'Login',
			style : 'margin-right: 15',
			handler: visLogin
		}, {
			xtype: 'button',
			toggleGroup: 'menuer',
			enableToggle: true,
			id: 'menuLogAf',
			text: 'Log af',
			style: 'margin-right: 15',
			handler: logaf
		}, {
			xtype : 'button',
			toggleGroup: 'menuer',
			enableToggle: true,
			id: 'menuBrugere',
			text: 'Administration af brugere',
			style: 'margin-right: 15',
			handler: visBrugere
		}, {
			xtype: 'button',
			toggleGroup: 'menuer',
			enableToggle: true,
			id : 'menuTelIndNy',
			text : 'Ny telefonisk indberetning',
			style : 'margin-right: 15',
			handler : function () {
				indberetningsformular(0);
			}
		}, {
		    xtype: 'button',
		    toggleGroup: 'menuer',
		    enableToggle: true,
		    id: 'menuVisit',
			text: 'Levende og spor', // - send A-fugle til indsamling
			style: 'margin-right: 15',
			handler: visLevende //function () { }
		}, {
			xtype : 'button',
			toggleGroup: 'menuer',
			enableToggle: true,
			id : 'menuFundIndsaml',
			text : 'Døde og skudte',
			style : 'margin-right: 15',
			handler : visIndsamlinger
		}, {
			xtype : 'button',
			toggleGroup: 'menuer',
			enableToggle: true,
			id : 'menuFundLab',
			text : 'Prøvesvar',
			style : 'margin-right: 15',
			handler: visLabListe
		}, {
			xtype: 'button',
			toggleGroup: 'menuer',
			enableToggle: true,
			id: 'menuFind',
			text: 'Find vildsvin',
			style: 'margin-right: 15',
			handler: visFindVildsvin
		}, {
			xtype : 'button',
			toggleGroup: 'menuer',
			enableToggle: true,
			id: 'menuOff',
			text : 'Undersøgte vildsvin',
			style : 'margin-right: 15',
			handler : visLabAnalyser
	    }, '->', {
	        xtype : 'button',
	        id : 'menuBred',
	        text : 'Indstilling af kolonnebredder',
	        style : 'margin-right: 15',
	        handler : visBredder
	    }
    ]; // slut på menuer


	viewport = new Ext.Viewport({
			layout : 'border',
			id : 'movieview',
			items : [{
				region : 'north',
				id : 'logoheader',
				html: '<div style = "font-size:2.0em; font-weight: bold; color:#1a5d4b; top:0px; height: 55px; padding-top: 12px; width: 100%; text-align: center">Vildsvin</div>' +
				'<div style = "font-size:1.4em; font-weight: bold; color:#1a5d4b; width: 100%; text-align: center" id = "loginnavn">Du er ikke logget ind</div>' +
				'<a href = "http://www.foedevarestyrelsen.dk/Fugleinfluenza/Overvaagning/Fund_af_doede_eller_syge_fugle/forside.htm" style = "position: absolute; left: 25px; top: 10px" target = "_blank"><img src = "/images/logos/mfvm_logo.png" alt = "Link til fødevarestyrelsens side om fund af døde eller syge fugle"></a>',
//				html : '<table border = "0" style = "top:0px; width:100%; vertical-align: top">'
//				+ '<tr>' // style = "vertical-align: middle">'
//				+ '<td><a href = "http://www.foedevarestyrelsen.dk/Fugleinfluenza/Overvaagning/Fund_af_doede_eller_syge_fugle/forside.htm" style = "margin-left: 25px; " target = "_blank"><img src = "/images/logos/toplogo.gif" alt = "Link til fødevarestyrelsens side om fund af døde eller syge fugle"></a></td>'
//				+ '<td style = "text-align:center; padding-left: 2px; padding-right: 98px; font-weight:bold; "><div style = "font-size:2.0em; color:#1a5d4b; top:0px; height: 70px; padding-top: 12px; ">Fugleinfluenza beredskab</div><div id = "loginnavn">Du er ikke logget ind</div></td>'
//				//				+ '<td style = "text-align:right; padding-right: 20px; vertical-align: middle; width:292px">&nbsp;</td>'
//				+ '<td style = "text-align:right; padding-right: 20px; vertical-align: middle"><a href = "http://www.vet.dtu.dk/Default.aspx?ID = 10834" target = "_blank"><img src = "/images/logos/Vi_Fi_logo.gif" alt = "Link til Veterinærinstituttets forside"></a></td>'
//				+ '</tr></table>',
				height : 90,
				border : false
			}, {
				region : 'center',
				layout : 'border',
				id : 'indhold',
				items : [
				    new Ext.Toolbar({
					region : 'north',
					height : toolbar_height,
					items : toolbaritems
				}), {
					id : 'vest',
					region : 'west',
					xtype : 'container',
					layout : 'fit',
					autoScroll : true,
					viewConfig : {
						forceFit : true
					},
					width : 500,
					split: true,
					autoWidth : true,
					autoHeight : true,
					//resizable : true,
					html : '<div id = "vestdiv"></div>',
					listeners : {
						resize : renderVest
					}
				}, {
					region : 'center',
					id : 'centerP',
					xtype : 'container',
//					split: true,
					resizable: true,
					border: false,
					//autoScroll: true,
					layout : 'fit',
					html : '<div id = "centerDiv"></div>',
					listeners : {
						resize : renderCenter
					}
				}]
			}]
		});
	vestPanel = Ext.getCmp('vest');
	centerPanel = Ext.getCmp('centerP');
	centerDiv = document.getElementById('centerDiv');
	setMenu();

	viewport.doLayout();



	hentKolonner();
    indlaesFundtype();

	indlaesStatus();
	indlaesSkjul();
    //visLabAnalyser();
	visForside();

});

function setMenu() {
	toemFormular('');
	/*
	// luk ikke modale vinduer
	try{
	Ext.getCmp('LE34AddressSearch').close();
	}
	catch (ex) { }

	try{
	if (winNyBigaard && !winNyBigaard.isDestroyed){
	winNyBigaard.close();
	}
	}
	catch (ex) {}
	*/
	//firmatype = '0';

    // forsiden og den offentlige og bredder er altid synlige
	//Ext.getCmp('menuForside').setVisible(true);
	//Ext.getCmp('menuOff').setVisible(true);
	//Ext.getCmp('menuBred').setVisible(true);

	Ext.getCmp('menuLogin').setVisible(brugerid==0);
	Ext.getCmp('menuLogAf').setVisible(brugerid>0);
	Ext.getCmp('menuBrugere').setVisible(brugerid>0 && (firmatype == '0' || firmatype == '4'));
	Ext.getCmp('menuTelIndNy').setVisible(brugerid>0 && (firmatype == '0' || firmatype == '2' || firmatype == '4'));
	Ext.getCmp('menuVisit').setVisible(brugerid > 0 && (firmatype == '0' || firmatype == '2' || firmatype == '4'));
	Ext.getCmp('menuFundIndsaml').setVisible(brugerid>0 && (firmatype == '0' || firmatype == '2' || firmatype == '4'));
	Ext.getCmp('menuFundLab').setVisible(brugerid>0 && (firmatype == '0' || firmatype == '3' || firmatype == '4'));
	Ext.getCmp('menuFind').setVisible(brugerid > 0);
	
	var loginnavn = document.getElementById('loginnavn');
	//slå så den aktuelle brugers menuer til
	if (brugernavn != '') 
		loginnavn.innerHTML = 'Du er logget ind som ' + brugernavn + ", " + firmanavn;
	 else // ikke logget ind, vis login-formen
		loginnavn.innerHTML = 'Du er ikke logget ind';

}

function skjulKort() {
    mapPanel.hide();
    // skjul også alle kontroller, der kan have åbnet et vindue
    mapPanel.config.tools.legend.legendWindow.hide();
}

function toemFormular(nynavn, kalder) {
    if (kanGemme) {
		if (confirm('Du har lavet ændringer, der ikke er gemt. Vil du gemme nu?') == true)
			return;
	}
	if (mapPanel != undefined) {
		mapPanel.clearLayers();

		// fjern datastrukturer og kortlag
		// skjul først alle lag

        // den forrige formular 
		switch (aktPanel) {
		    case 'telefon_indberetning':
		        //mapPanel.config.layers.data.indberetninger.setVisibility(false);
		        // afmeld klik-event
		        mapPanel.map.events.unregister('featureadded');
		        if (DrawPoint != undefined)
		            DrawPoint.deactivate();
		        break;
		    case 'Levende_panel':
		        mapPanel.config.layers.data.indberetninger.setVisibility(false);
		        break;
		    case 'Indsamlinger_panel':
			    mapPanel.config.layers.data.vildsvin_doede.setVisibility(false);
			    mapPanel.map.events.unregister('featureadded');
			    if (DrawPoint != undefined)
			    	DrawPoint.deactivate();
			    break;
            case 'LabListe_panel':
                mapPanel.config.layers.data.vildsvin_proevesvar.setVisibility(false);
				break;
			case 'lab_analyser_panel':
			    mapPanel.config.layers.data.vildsvin_off.setVisibility(false);
				break;
            default:
				break;
		}
	}

    // brug navnet på kaldende funktion til at finde oplysninger om hvordan siden skal vises
	brd = 600;
	var kort = false;
	if (nynavn != '') {
	    //var kalder = toemFormular.caller.name;
	    var fundet = false;
	    if (kalder != undefined && kalder != '')
	    {
	        try {
	            for (var i = 0; i < grid_treestore.tree.root.childNodes.length; i++) {
	                if (grid_treestore.tree.root.childNodes[i].data.kommando == kalder) {
	                    brd = grid_treestore.tree.root.childNodes[i].data.bredde;
	                    kort = grid_treestore.tree.root.childNodes[i].data.kort;
	                    if (kort)
	                    {
	                        if (brd > viewport.width - 600)
	                            brd = viewport.width - 600;
	                    }
	                    else
	                    {
	                        if (brd > viewport.width )
	                            brd = viewport.width ;
                        }
	                    fundet = true;
	                    break;
	                }
	            }
	        }
	        catch (e) { }
	        if (fundet == false)
	        {
	            console.log(nynavn + ' - kort ikke defineret');
	            kort = (nynavn == 'telefon_indberetning' || nynavn == 'lab_analyser_panel' || nynavn == 'LabListe_panel' || nynavn == 'Levende_panel' || nynavn == 'Indsamlinger_panel');
	        }
	    }
	}


    try
    {
        if (kort) {
            mapPanel.show();
            switch (nynavn) {
                //case 'telefon_indberetning':
                //    break;
                case 'Levende_panel':
                    legendbred = 260;
                    legendhoej = 165;
                    break;
                case 'Indsamlinger_panel':
                    legendbred = 330;
                    legendhoej = 400;
                    break;
                case 'LabListe_panel':
                    legendbred = 260;
                    legendhoej = 320;
                    break;
                case 'lab_analyser_panel':
                    legendbred = 230;
                    legendhoej = 260;
                    break;
                default:
                    legendbred = 100;
                    legendhoej = 100;
                    break;
            }
            if (legendbred == 100)
            {
                // skjul hvis den er vist
                if (Ext.getCmp('toolbar_item_Legend').pressed)
                    Ext.getCmp('toolbar_item_Legend').toggle();
            }
            else 
            {
                if (Ext.getCmp('toolbar_item_Legend').pressed == false)
                    Ext.getCmp('toolbar_item_Legend').toggle();
                Ext.getCmp('toolbar_legend_window').setSize(legendbred, legendhoej);
                Ext.getCmp('toolbar_legend_window').alignTo(centerPanel, 'tr-tr', [0, 34]);
            }
        }
        else
        {
            //mapPanel.hide();
            skjulKort();
        }
    }
	catch (e) { }

	document.getElementById('vestdiv').innerHTML = '';
	aktPanel = nynavn;

	retLaastPost = false;

	vestPanel.setWidth(brd);
	centerPanel.setPosition(brd + 5, centerPanel.top);
	viewport.doLayout();
}

// generel beforeload-listener til alle grids med filter
// globale variable
var proxyDummy = [{
		'type' : 'string',
		'value' : '',
		'field' : 'dummy'
	}
];

function hentData() {
	Ext.Ajax.request({
		url : 'Webservices/svin_login.asmx/visLabAnalyser',
		method : "POST",
		params : JSON.stringify({
			rows : 10,
			role : "Admin",
			index : 'txt',
			question : 'hvordan'
		}),
		success : function () {
			console.log("ok");
		},
		failure : function (response, opts) {
			console.log("failed");
		},
		headers : {
			'Content-Type' : 'application/json'
		}
	});
}

function renderDato(value, metaData, record, row, col, store, gridView) {
    if (value != '')
    {
        value = value + '';
        var mlm = value.indexOf(' ');
	    if (mlm > 0)
	        value = value.substring(0, mlm);
	    var dt = Ext.Date.parse(value, 'd/m/Y');
        if (dt != undefined)
	        value = Ext.Date.format(dt, 'd-m-Y');
    }
    metaData.tdAttr = 'data-qtip="' + value + '"';
    return value;
}

//function renderCheckbox(value, metaData, record, row, col, store, gridView) {
//    metaData.tdAttr = 'data-qtip="' + value + '"';
//    return value;
//}
function renderTooltip(value, metaData, record, row, col, store, gridView) {
    metaData.tdAttr = 'data-qtip="' + value + '"';
    return value;
}

function renderVis(value, metaData, record, row, col, store, gridView) {
    value = storeSkjul.getById(value).data.navn;
    metaData.tdAttr = 'data-qtip="' + value + '"';
    return value;
}

function renderIndsamler(value, metaData, record, row, col, store, gridView) {
    if (value == '' || value == null || value == '0')
        value  = '- Ikke på listen -';
    else {
        try {
            value = storeIndsamlere.getById(value).data.navn;
        }
        catch (ex) {
            value = '- Ikke på listen -';
        }
    }
    metaData.tdAttr = 'data-qtip="' + value + '"';
    return value;
}

function renderStatus(value, metaData, record, row, col, store, gridView) {
    try {
        value = storeStatus.getById(value).data.navn;
    }
    catch (ex) {
        value = '- Ikke på listen -';
    }
    metaData.tdAttr = 'data-qtip="' + value + '"';
    return value;
}

function renderFirma(value, metaData, record, row, col, store, gridView) {
    if (value == '' || value == null)
        value = '- Ikke på listen -';
    else {
        // slå op i firmalisten
        try {
            value = storeFirmaVisit.getById(value).data.navn;
        }
        catch (err) {
            value = value + ' fejl: ' + err;
        }
    }
    metaData.tdAttr = 'data-qtip="' + value + '"';
    return value;
}

function renderFundtyper(value, metaData, record, row, col, store, gridView) {
    try {
        value = storeFundtyper.getById(value).data.navn;
    }
    catch (ex) {
        value = '- Ikke på listen -';
    }
    metaData.tdAttr = 'data-qtip="' + value + '"';
    return value;
}


function renderBruger(value, metaData, record, row, col, store, gridView) {
	//try {
	//	if (record.data.art_praecis != '0')
	//		value = storeFuglearter.getById(record.data.art_praecis).data.navn;
	//	else
	//		value = storeFuglearter.getById(value).data.navn;
	//}
	//catch (ex) {
	//	value = '- Ikke på listen -';
	//}
	//metaData.tdAttr = 'data-qtip="' + value + '"';
	return value;
}
function renderBrugerFirma(value, metaData, record, row, col, store, gridView) {
	//try {
	//	if (record.data.art_praecis != '0')
	//		value = storeFuglearter.getById(record.data.art_praecis).data.navn;
	//	else
	//		value = storeFuglearter.getById(value).data.navn;
	//}
	//catch (ex) {
	//	value = '- Ikke på listen -';
	//}
	//metaData.tdAttr = 'data-qtip="' + value + '"';
	return value;
}

function renderFVRegion(value, metaData, record, row, col, store, gridView) {
	if (value == '' || value == null || value == '0')
		value = '- Ikke på listen -';
	else {
		// slå op i listen med FødevareRegioner = firmaliste, hvor firmatypen er 2
		try {
			value = storeFVRegion.getById(value).data.navn;
		}
		catch (err) {
			value = value + ' fejl: ' + err;
		}
	}
	metaData.tdAttr = 'data-qtip="' + value + '"';
	return value;
}

function renderNoteret(value, metaData, record, row, col, store, gridView) {
    if (value == '' || value == null || value == '0')
        value = 'Nej';
    else  if (Number(value) >0) {
        value = 'Ja';
    }
    metaData.tdAttr = 'data-qtip="' + value + '"';
    return value;
}

function renderPositiv(value, metaData, record, row, col, store, gridView) {
    try {
        if (value < 0)
            value = '' ;
    }
    catch (ex) {
        value = '';
    }
    metaData.tdAttr = 'data-qtip="' + value + '"';
    return value;
}


function ajaxFailed(response, opts) {
	var responseObject = Ext.JSON.decode(response.responseText);
	if (responseObject == undefined) {
		fejlbox('Serveren svarede ikke i tide. ');
		return;
	}
	fejlbox(responseObject.Message);
}

function fejlbox(tekst) {
    tekst = tekst.replace(/\n/g,'<br/>')
    Ext.Msg.show({
		msg: tekst,
		buttons: Ext.Msg.OK,
		icon: Ext.Msg.ERROR
	});
}

function okbesked(tekst, callbackfunktion) {
	Ext.Msg.show({
			msg : tekst,
			buttons: Ext.Msg.OK,
			icon: Ext.Msg.INFO,
			fn : callbackfunktion
		}); 
}

function spoerg(tekst, callbackfunktion)
{
    Ext.Msg.show({
        title: 'Bekræft',
        msg: tekst,
        //buttons: Ext.Msg.YESNO,
        buttonText: { yes: "Ja", no: "Nej"},
        icon: Ext.Msg.QUESTION,
        modal: true,
        fn: callbackfunktion
    });
}

function tomFunktion() {
	return;
}

function laesBrugerrettigheder(response, logind) {
	var responseObject = Ext.JSON.decode(response.responseText).d;
	brugernavn = responseObject.brugernavn;
	brugerid = responseObject.brugerid;
	firma = responseObject.firma;
	firmatype = responseObject.firmatype;
	firmanavn = responseObject.firmanavn;
	epidemi = responseObject.epidemi;
	indsamlertype = (epidemi == true ? '1' : '2');
	indlaesFirma();
	indlaesIndsamlere();
	grid_treestore.reload();
	storeKolonner.reload();
	setMenu();
}

function renderVest() {
	if (aktPanel!== '') {
		var g = Ext.getCmp(aktPanel);
		if (g != undefined)
		{
		    var bx = this.getBox();
		    brd = bx.width;
		    g.autoWidth = false;
		    g.width = bx.width;
		    g.autoHeight = false;
		    g.height = bx.height;
		    g.autoScroll = true;
		    g.doLayout();
		}
	}
}

function renderCenter() {
	if (mapPanel!= undefined) {
		mapPanel.width = centerPanel.el.dom.offsetWidth;
		mapPanel.height = centerPanel.el.dom.offsetHeight;
		mapPanel.doLayout();
	    try {
	        Ext.getCmp('toolbar_legend_window').alignTo(centerPanel, 'tr-tr', [0, 34]);
	    }
	    catch (ex) { }
	}
}


function opdaterRaekke(opdatvar) {
	if (opdatvar != undefined) {
		for (ia = 0; ia < opdatvar.length; ia += 2) {
			aktRec.set(opdatvar[ia], opdatvar[ia + 1]);
		}
	}
	aktRec.commit();
	var grid = Ext.getCmp(aktGrid);
	grid.getView().refresh();
	var sm=grid.getSelectionModel();
	sm.fireEvent('select', sm, aktRec, aktRk);
}

function indlaesFundtype() {
	if (storeFundtyper!= undefined)
		return;

	//Ext.define('Fundtyper', {
	//	extend : 'Ext.data.Model',
	//	fields : [
	//		'id',
	//		'navn',
	//		'visit',
	//		'euring_kode'
	//	],
	//	idProperty : 'id'
	//});

	// create the Data Store
	storeFundtyper = Ext.create('Ext.data.JsonStore', {
	    storeId: 'storeFundtyper',
	    model: 'id_navn',
			remoteSort : true,
			autoLoad : true,
			proxy : {
				type : 'simpel',
				url : 'Webservices/svin_login.asmx/hentFundtyper'
			},
			sorters : [{
				property : 'navn',
				direction : 'ASC'
			}],
			listeners: {
			    load: function (store, records, successful, eOpts) {
			        var tmpA = [];
			        var tmpB = [];
			        store.each(function (record) {
			            if (record.data.id < 20)
			                tmpA.push([record.data.id, record.data.navn]);
                        else
			                tmpB.push([record.data.id, record.data.navn]);
			        });

			        storeFundtyperLev = Ext.create('Ext.data.ArrayStore', {
			            model: 'id_navn',
			            data: tmpA,
			            sorters: [{
			                property: 'navn',
			                direction: 'ASC'
			            }]
			        });
			        storeFundtyperDoed = Ext.create('Ext.data.ArrayStore', {
			            model: 'id_navn',
			            data: tmpB,
			            sorters: [{
			                property: 'navn',
			                direction: 'ASC'
			            }]
			        });

			    }
			}
	});

	storeFVRegion = Ext.create('Ext.data.JsonStore', {
	    storeId: 'storeFVRegion',
	    model: 'id_navn',
	    remoteSort: true,
	    autoLoad: true,
	    proxy: {
	        type: 'simpel',
	        extraParams: { liste: 'regioner' },
	        url: 'Webservices/svin_login.asmx/hentListe'
	    },
	    sorters: [{
	        property: 'navn',
	        direction: 'ASC'
	    }]
	});

	storeResultat = Ext.create('Ext.data.JsonStore', {
	    storeId: 'storeResultat',
	    model: 'id_navn',
	    remoteSort: true,
	    autoLoad: true,
	    proxy: {
	        type: 'simpel',
	        extraParams: { liste: 'resultat' },
	        url: 'Webservices/svin_login.asmx/hentListe'
	    },
	    sorters: [{
	        property: 'navn',
	        direction: 'ASC'
	    }],
	    listeners: {
	        load: function (store, records, successful, eOpts) {
	            var tmpA = [];
	            store.each(function (record) {
	                tmpA.push([record.data.id, record.data.navn, (Number(record.data.id) > 0)]);
	            });

	            storeResultat_asf = Ext.create('Ext.data.ArrayStore', {
	                model: 'id_navn_aktiv',
	                data: tmpA,
	                sorters: [{
	                    property: 'id',
	                    direction: 'ASC'
	                }]
	            });
	            storeResultat_csf = Ext.create('Ext.data.ArrayStore', {
	                model: 'id_navn_aktiv',
	                data: tmpA,
	                sorters: [{
	                    property: 'id',
	                    direction: 'ASC'
	                }]
	            });
	            storeResultat_aujeszkys = Ext.create('Ext.data.ArrayStore', {
	                model: 'id_navn_aktiv',
	                data: tmpA,
	                sorters: [{
	                    property: 'id',
	                    direction: 'ASC'
	                }]
	            });
	            storeResultat_trikiner = Ext.create('Ext.data.ArrayStore', {
	                model: 'id_navn_aktiv',
	                data: tmpA,
	                sorters: [{
	                    property: 'id',
	                    direction: 'ASC'
	                }]
	            });

	        }
	    }
	});



	storeNoteret= new Ext.data.ArrayStore({
	fields: [{ name: 'id', type: 'string' }, { name: 'navn', type: 'string' }],
	    data: [
            ['1', 'Ja'],
            ['0', 'Nej']
	    ]
	});

}

function indlaesFirma() {
	if (storeFirma!= undefined)
		return;


	// create the Data Store
	storeFirmaVisit = Ext.create('Ext.data.JsonStore', {
	    storeId: 'storeFirmaVisit',
		model: 'id_navn_aktiv',
		remoteSort : true,
		autoLoad : true,
		proxy : {
			type : 'simpel',
			url : 'Webservices/svin_login.asmx/hentFirmaListe'
		},
		sorters : [{
			property : 'navn',
			direction : 'ASC'
		}],
		listeners: {
			load: function(store, records, successful, eOpts )
			{
			    storeFirmaVisit.filter([{ filterFn: function (item) { return item.get("id") != 0 && item.get("id") != 90; } }
			    ]);

			    storeFirma = Ext.create('Ext.data.ArrayStore', {
			        storeId: 'storeFirma',
			        model: 'id_navn_aktiv',
			        remoteSort: true,
			        sortOnLoad: true,
			        sorters: [{
			            property: 'navn',
			            direction: 'ASC'
			        }],
			        idProperty: 'id',
			        data: storeFirmaVisit.getRange()
			    });
			    storeFirmaVisit.clearFilter();

			    ////storeFirma = new Ext.data.JsonStore();
			    //storeFirma = Ext.create('Ext.data.JsonStore', {
			    //    storeId: 'storeFirma',
			    //    model: 'id_navn_aktiv',
			    //    remoteSort: true,
			    //    autoLoad: false,
			    //    sorters: [{
			    //        property: 'navn',
			    //        direction: 'ASC'
			    //    }]
			    //});

				//storeFirmaVisit.each(function (record) {
				//	if (record.data.id != 0 && record.data.id != 90)
				//		storeFirma.add(record.copy());
				//});
			}
		}
	});
}

function indlaesIndsamlere() {

    Ext.define('Indsamlere', {
    	extend : 'Ext.data.Model',
    	fields : [
    		'id',
    		'navn',
    		'firmatype', {
    			name : 'aktiv',
    			type : 'boolean'
    		}
    	],
    	idProperty : 'id'
    });

    //create the Data Store
    storeIndsamlere = Ext.create('Ext.data.JsonStore', {
        storeId: 'storeIndsamler',
        model: 'Indsamlere',
        remoteSort: true,
        autoLoad: true,
        proxy: {
            type: 'simpel',
            url: 'Webservices/svin_login.asmx/hentIndsamlerListe'
        },
        sorters: [
            {
                property: 'aktiv',
                direction: 'DESC'
            },
            {
            property: 'navn',
            direction: 'ASC'
        }],
        listeners: {
        load: function (store, rec, opts) {
            if (store.getCount() > 0) {
                var tmpA=[];
                store.each(function (record) {
                    if (record.data.id != '90')
                        tmpA.push([record.data.id, record.data.navn, record.data.firmatype, record.data.aktiv]);
                });

                storeIndsamlereA = Ext.create('Ext.data.ArrayStore', {
                    model: 'Indsamlere',
                    data: tmpA,
                    sorters: [{
                        property: 'navn',
                        direction: 'ASC'
                    }]
                });

            }
        }
    }
});

    Ext.define('Opsamlere', {
        extend: 'Ext.data.Model',
        fields: [
    		'id',
    		'navn',
    		'email'
        ],
        idProperty: 'id'
    });

    //create the Data Store
    storeOpsamlere = Ext.create('Ext.data.JsonStore', {
        storeId: 'storeOpsamlere',
        model: 'Opsamlere',
        remoteSort: true,
        autoLoad: true,
        proxy: {
            type: 'simpel',
            url: 'Webservices/svin_login.asmx/hentOpsamlerListe'
        },
        sorters: [{
            property: 'navn',
            direction: 'ASC'
        }],
        listeners: {
            load: function (store, rec, opts) {
                opsamlerMenu = Ext.create('Ext.menu.Menu');
                //if (store.getCount > 1) {
                // løb alle posterne i store igennem og skriv menuen
                store.each(function (record) {
                    opsamlerMenu.add({ xtype: 'menuitem', text: record.data.navn, id: 'opsamlerMenu_' + record.data.id, handler: function () { vaelgOpsamler(this.id) } });
                }, this);
                //}
            }
        }

    });

}

// ??? vi mangler også regionerne

function indlaesStatus() {

	//Ext.define('Status', {
	//	extend : 'Ext.data.Model',
	//	fields : [
	//		{ name: 'id', type: 'numeric' },
	//		'navn',
	//		'gruppe'
	//	],
	//	idProperty : 'id'
	//});

	// create the Data Store
	storeStatus = Ext.create('Ext.data.JsonStore', {
	    storeId: 'storeStatus',
	    //model: 'Status',
	    model: 'id_navn_aktiv',
	    remoteSort: false,//true,true,
	    autoLoad: false,//true,
	    proxy: {
	        type: 'simpel',
	        url: 'Webservices/svin_login.asmx/hentStatusListe'
	    },
	    sorters: [{
	        property: 'id',
	        direction: 'ASC'
	    }]//,
	    //listeners: {
        //    load: function (store, rec, opts) {
	    //        if (store.getCount() > 0) {
	    //            //var tmpBRS =[];
	    //            //var tmpLAB = [];
	    //            //store.each(function (record) {
	    //            //    tmpBRS.push([record.data.id, record.data.navn, (record.data.gruppe == 'status_brs')]);
	    //            //    tmpLAB.push([record.data.id, record.data.navn, (record.data.gruppe == 'status_lab')]);
	    //            //});

	    //            //storeStatusBRS = Ext.create('Ext.data.ArrayStore', {
	    //            //    model: 'id_navn_aktiv',
	    //            //    data: tmpBRS,
	    //            //    sorters: [
        //            //        {
        //            //            property: 'aktiv',
        //            //            direction: 'DESC'
        //            //        }, {
        //            //            property: 'id',
        //            //            direction: 'ASC'
        //            //        }]
	    //            //});
	    //            //storeStatusLAB = Ext.create('Ext.data.ArrayStore', {
	    //            //    model: 'id_navn_aktiv',
	    //            //    data: tmpLAB,
	    //            //    sorters: [
        //            //        {
        //            //            property: 'aktiv',
        //            //            direction: 'DESC'
        //            //        }, {
	    //            //        property: 'navn',
	    //            //        direction: 'ASC'
	    //            //    }]
	    //            //});

	    //        }
	    //    }
	    //}
	});
	storeStatus.load();


}



function indlaesSkjul() { 
	if (storeSkjul!= undefined)
		return;

	storeSkjul = Ext.create('Ext.data.JsonStore', {
		storeId: 'storeSkjul',
		model: 'id_navn',
		remoteSort: true,
		autoLoad: true,
		proxy: {
			type: 'simpel',
			extraParams: { liste: 'skjul' },
			url: 'Webservices/svin_login.asmx/hentListe'
		},
		sorters: [{
			property: 'navn',
			direction: 'ASC'
		}],
		listeners: {
			load: function (store, rec, opts) {
				// løb alle posterne i store igennem og skriv menuen
				//skjulMenu = new Ext.menu.Menu({ text: 'Skjul' });
				skjulMenu = Ext.create('Ext.menu.Menu');
				//skjulMenu.text = 'Skjul';
				store.each(function (record) {
					if (record.data.id != '0')
						skjulMenu.add({ xtype: 'menuitem', text: record.data.navn, id: 'skjulmenu_' + record.data.id, handler: function () { skjulPost(this.id) } });
				}, this);
			}
		}
	});
}


function skjulPost(menu_nr) {
	// vi gemmer med vilje kun lige netop at posten bliver skjult
	var aarsag = menu_nr.substring(menu_nr.indexOf('_')+1);
	Ext.Ajax.request({
		url: 'Webservices/svin_login.asmx/skjulPost',
		method: "POST",
		params: JSON.stringify({
			skjul: aarsag,
			panel: aktPanel,
			id: aktRec.data.id
		}),
		success: function () {
		    if (retLaastPost)
		        storeAkt.reload();
		    else {
		        var opdatvar = new Array();
			    opdatvar.push('skjul');
			    opdatvar.push(aarsag);
			    opdaterRaekke(opdatvar);
		    }
		},
		failure: function (response, opts) {
			//console.log("failed");
			ajaxFailed(response, opts);
		},
		headers: { 'Content-Type': 'application/json' }
	});
}


function enableCtl(ctl, vaerdi) {
	if (vaerdi)
		{ctl.enable();}
	else
		{ctl.disable();}
}
function showCtl(ctl, vaerdi) {
	if (vaerdi)
		{ctl.show();}
	else
		{ctl.hide();}
}
// bruges ikke 
//function findTop(maxhoejde) {
//	var wintop = 50;
//	if (document.body.offsetHeight < maxhoejde) { //|| Ext.isIE
//		wintop = Ext.getCmp('logoheader').getHeight();
//	}
//	//	else if (Ext.getCmp('logoheader').getHeight() > 0 && document.body.offsetHeight > = maxhoejde + header_height + toolbar_height) { //|| Ext.isIE
//	//	wintop = header_height + toolbar_height;
//	//	}
//	return wintop;
//}
// dette er nu bygget ind i selve indberet-formularen
//function popPanel(titel, indhold, hjaelp, okhandler, callback) {
//	// dette panel tegnes altid som fuld højde 

//	/* parametre
//	titel
//	indhold
//	knapper
//	-- vinduets navn, hvis ikke de alle kan hedde det samme, vi prøver først med samme navn
//	callback, hvis der skal gøres noget ekstra efter vinduet er vist

//	*/
//	var bx = vestPanel.getBox();   

//	// åbn vindue
//	popwin = new Ext.Window({
//			width : 500,
//			id : 'popwindow',
//			//closeable: true,
//			//closeAction: 'destroy',
//			modal : false,
//			initCenter : false,
//			x : 0,
//			y : bx.y,
//			height : bx.height,
//			autoScroll: true,
//			anchor: '0',
//			title : titel,
//			buttonAlign : 'left',
//			items : indhold,
//			floating : true,
//			buttons : [{
//					text : 'Hjælp',
//					iconCls : 'icon-help',
//					hidden : hjaelp == '' ? true : false,
//					handler : function () {
//						visHjaelp(hjaelp);
//					}
//				}, '->', {
//					text : 'Annullér',
//					iconCls : 'icon-cancel',
//					handler : function () {
//						popwin.close();
//					}
//				}, {
//					text : 'OK',
//					iconCls : 'icon-ok',
//					handler : okhandler
//				}
//			]

//		});
//		popwin.on("close", function () {
//			popwin = undefined;
//		});
//	popwin.setPosition(0, bx.y);
//	popwin.show();
//	popwin.doLayout();
//}

// bruges ikke
function popVindue(maxhoejde, titel, indhold, hjaelp, okhandler, cb) {
	/* parametre
	max højde
	titel
	indhold
	knapper
	-- vinduets navn, hvis ikke de alle kan hedde det samme, vi prøver først med samme navn
	callback, hvis der skal gøres noget ekstra efter vinduet er vist

	*/
	var wintop = toolbar_height + header_height;
	var ah = true; // wintop > 0;
	if (document.body.offsetHeight < maxhoejde - wintop) {
		//wintop = Ext.getCmp('logoheader').getHeight();
		ah = false;
	}
	else
		wintop = 50;

	// åbn vindue
	popwin = new Ext.Window({
		width: 500,
		id: 'popwindow',
		closeAction: 'close',
		modal: true,
		initCenter: false,
		x: 50,
		y: wintop,
		autoHeight: ah,
		height: ah == true ? undefined : (document.body.offsetHeight - wintop),
		autoScroll: !ah,
		anchor: (ah == false ? '-20' : '0'),
		title: titel,
		buttonAlign: 'left',
		items: indhold,
		floating: true,
		buttons: [{
			text: 'Hjælp',
			iconCls: 'icon-help',
			hidden: hjaelp == '' ? true : false,
			handler: function () {
				visHjaelp(hjaelp);
			}
		}, '->', {
			text: 'Annullér',
			iconCls: 'icon-cancel',
			handler: function () {
				popwin.close();
			}
		}, {
			text: 'OK',
			iconCls: 'icon-ok',
			handler: okhandler
		}
			]

	});
	popwin.setPosition(50, wintop);
	popwin.show();
}

function virkerIkke() {
	alert('Denne funktion virker ikke endnu.');
}

// ren javascript uden Ext - til at ændre indholdet af css-klasser
function HideShowColumns(newValue) {
	var theRules = new Array();
	if (document.styleSheets[1].cssRules) {
		theRules = document.styleSheets[1].cssRules;
	}
	else if (document.styleSheets[1].rules) {
		theRules = document.styleSheets[1].rules;
	}
	var setting = 'expand';
	if (newValue == false) 
		setting = 'collapse';
	// det er første punkt i filen
	theRules[0].style.visibility = setting;
	/*for (n in theRules) {
		if (theRules[n].selectorText == selectorText) {
			theRules[n].style.color = 'blue';
		}
	}*/
}

function logaf() {
	var param = {};
	var logindata = Ext.JSON.encode(param);


	Ext.Ajax.request(
	{
		url: 'Webservices/svin_login.asmx/Logaf',
		params: logindata,
		headers:
	{
		'Content-Type': 'application/json;charset=utf-8'
	},
		success: function (response, opts) {
			toemFormular('');
			Ext.getCmp('menuLogin').show();
			// vi skal også have skjult kortet
			if (mapPanel != undefined) {
				//mapPanel.width = 0;
				//mapPanel.doLayout();
				mapPanel.hide();
			}
			laesBrugerrettigheder(response);
		},
		failure: ajaxFailed
	});
}


/*
xtype: 'acombo',
xtype: 'adrsearch',
xtype: 'box',
xtype: 'button',
xtype: 'checkbox',
xtype: 'combo',
xtype: 'component',
xtype: 'datefield',
xtype: 'displayfield',
xtype: 'fccolumn',
xtype: 'fccolumn_ro',
xtype: 'fcombo',
xtype: 'fieldset',
xtype: 'form',
xtype: 'image',
xtype: 'menucheckitem'
xtype: 'menuitem',
xtype: 'numberfield',
xtype: 'panel',
xtype: 'radiogroup',
xtype: 'textarea',
xtype: 'textfield',
xtype: 'toolbar',

*/
// fælles funktion til at løbe alle felter igennem med each på aktrec eller model
function visFelter(aktStore, id)
{
	var fi = aktStore.model.getFields();
	for (var i = 0; i < fi.length; i++) {
		try {
			ctl = Ext.getCmp(fi[i].name);
			var val = (id == undefined ? '' : aktRec.get(fi[i].name));
			if (val == null)
				val = '';
			//if (fi[i].name === 'billede') {
			//	ctl.setSrc(val);
			//	//var visbil = false;
			//	//ctl.setVisible(visbil);
			//	ctl.setVisible(val != '');
			//}
			//else {
				switch (ctl.xtype) {
					case 'fcombo':
					case 'acombo':
						ctl.setOriginal(val);
						//console.log(aktPanel + ' -- ' + fi[i].name + ': ' + val);
						break;
					case 'image':
						ctl.setSrc(val);
						ctl.setVisible(val != '');
						break;
					default:
						ctl.setValue(val);
						//console.log(aktPanel + ' -- ' + fi[i].name + ': ' + val);
						break;
				}
			//}
		} catch (e) {
			if (e != "TypeError: Cannot read property 'xtype' of undefined")
				console.log(aktPanel + ' -- ' + fi[i].name + ': ' + e);
		}
	}

}