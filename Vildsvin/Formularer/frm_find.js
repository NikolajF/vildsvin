﻿function visFindVildsvin()
{
    //  showMap();

   	toemFormular('FindVildsvin_panel', 'visFindVildsvin');



    //   mapPanel.config.layers.data.afugle.setVisibility(true);


    Ext.define('mFindVildsvin', {
        extend: 'Ext.data.Model',
        fields: [
            'indberetning',
            'indberetning_status',
            'indberetning_skjul',
            'noteret',
            'an_navn',
            'set_tid',
            'an_vejnavn',
            'an_husnr',
            'an_postnr',
            'an_by',
            'an_telefon',
            'an_email',
            'billede',
            'antal',
            'fundtype',
            'setbeskriv',
            'setvejnavn',
            'sethusnr',
            'setpostnr',
            'setby',
            'setx',
            'sety',
            'fundsted',
			'indberetning_kommentar',
            'an_tid',
            'registrator',
            'registrator_firma',
            'vildsvin_id',
            'vildsvin_status',
            'resultat_tekst',
            'vildsvin_skjul',
            'tid_indsamlet',
            'opsamler',
            'afd',
            'x',
            'y',
            'tid_lab',
            'tid_slut',
            'vildsvin_kommentar',
            'journalnr_1',
            'tid_lab_1',
            'tid_slut_1',
            'asf',
            'csf',
            'aujeszkys',
            'journalnr_2',
            'trikiner',
            'tid_lab_2',
            'tid_slut_2'
        ],
        idProperty: 'vildsvin_id'
    });

    storeFindVildsvin = Ext.create('Ext.data.JsonStore', {
        storeId: 'st_FindVildsvin',
        //pageSize: 10,
        model: 'mFindVildsvin',
        remoteSort: false,
        autoLoad: false,
        proxy: {
            type: 'simpel',
            url: 'Webservices/svin_login.asmx/FindVildsvin'
        },
        sorters: [{
            property: 'vildsvin_id',
            direction: 'ASC'
        }],
        listeners: {
            load: function (store)
            {
                //console.log('load kaldt');
                // sæt de aktuelle filtre på Excel-udtræk
                Ext.get('btn_excel').down('a').dom.href = 'VisFil.ashx?type=xl_find&id=' + brugerid + '&value=' + store.proxy.extraParams.value + '&felt=' + store.proxy.extraParams.felt;

                if (store.getCount() > 0)
                    FindVildsvinegrid.getSelectionModel().select(0);
                else // tøm felterne
                {
					visFelter(storeFindVildsvin);
                }
            }
        }
    });
    storeAkt = storeFindVildsvin;

    var FindVildsvinegrid = Ext.create('Ext.grid.Panel', {
        layout: 'fit',
        id: 'FindVildsvinegrid',
        title: 'Vildsvin',
        autoHeight: true,
        autoWidth: true,
        style: 'margin-top: 10px;',
        //header: false,
        store: storeFindVildsvin,
        disableSelection: false,
        viewConfig: {
            id: 'gv',
            trackOver: false,
            stripeRows: false
        },
        // grid columns
        columns: {
            items: [{
                //id: 'indberetning',
                text: "Indberetning",
                tooltip: "Indberetning",
                dataIndex: 'indberetning',
                //flex: 1,
                width: 80
            }, {
                text: "Vildsvin ID",
                tooltip: "Vildsvin ID",
                dataIndex: 'vildsvin_id',
                width: 80,
                renderer: renderPositiv
            }, {
                text: "FVST journalnr.",
                tooltip: "FVST journalnr.",
                dataIndex: 'journalnr_2',
                width: 120,
                flex: 1
            }, {
                text: "LAB journalnr.",
                tooltip: "LAB journalnr.",
                dataIndex: 'journalnr_1',
                width: 120,
                flex: 1
            }]
        },
        // gridkol_slut
        selModel: Ext.create('Ext.selection.RowModel', {
            listeners: {
                select: {
                    fn: function (selModel, rec, index)
                    {
                        aktRec = rec;
						visFelter(storeFindVildsvin, aktRec.data.indberetning);

                        // skal vildsvine-felterne vises? levende eller død
                        var vis = aktRec.data.vildsvin_id > 0;
                        Ext.getCmp('akt_vildsvin').setVisible(vis);
                        Ext.getCmp('akt_vet').setVisible(vis);
                        Ext.getCmp('akt_fvst').setVisible(vis);
                    }
                }
            }
        }),
        //bbar: createZoombar()
        bbar: ['->', ExcelButtonCode()]
    });






    var FindVildsvin_panel = Ext.create('Ext.panel.Panel', {
        title: 'Find vildsvin og vis alle detaljer',
        id: 'FindVildsvin_panel',
        layout: 'anchor',
        anchor: '-20',
        autoScroll: true,
        bodyPadding: '10 10 10 10',
        autoWidth: true,
        autoHeight: false,
        renderTo: 'vestdiv',
        items: [{
            xtype: 'fieldset', // her er kun de redigerbare felter
            title: 'Skriv et kriterium og tryk Retur',
            id: 'find_fugl',
            bodyPadding: 10,
            autoWidth: true,
            autoHeight: true,
            style: 'margin-top: 10px',
            flex: 1,
            items: [{
                xtype: 'fccolumn',
                items: [{
                    xtype: 'textfield',
                    hideLabel: false,
                    fieldLabel: 'Indberetning',
                    id: 'find_indberetning',
                    margin: '0 3 0 0', // venstre
                    columnWidth: 0.5,
                    enableKeyEvents: true,
                    listeners: {
                        specialkey: function (field, e)
                        {
                            // e.HOME, e.END, e.PAGE_UP, e.PAGE_DOWN,
                            // e.TAB, e.ESC, arrow keys: e.LEFT, e.RIGHT, e.UP, e.DOWN
                            if (e.getKey() === e.ENTER)
                            {
                                FindVildsvin(field.value, 'indberetning');
                            }
                        }
                    }
                }, {
                    fieldLabel: 'Vildsvin ID',
                    xtype: 'textfield',
                    id: 'find_vildsvinid',
                    //margin: '0 0 0 3', // højre
                    columnWidth: 0.5,
                    enableKeyEvents: true,
                    listeners: {
                        specialkey: function (field, e)
                        {
                            if (e.getKey() === e.ENTER)
                            {
                                FindVildsvin(field.value, 'vildsvin_id');
                            }
                        }
                    }
                }]
            }, {
                xtype: 'fccolumn',
                items: [{
                    fieldLabel: 'FVST journalnr.',
                    xtype: 'textfield',
                    id: 'find_journalnr_2',
                    margin: '0 0 0 3', // højre
                    columnWidth: 0.5,
                    enableKeyEvents: true,
                    listeners: {
                        specialkey: function (field, e)
                        {
                            if (e.getKey() === e.ENTER)
                            {
                                FindVildsvin(field.value, 'journalnr_2');
                            }
                        }
                    }
                }, {
                    fieldLabel: 'LAB journalnr.',
                    xtype: 'textfield',
                    id: 'find_journalnr_1',
                    margin: '0 0 0 3', // højre
                    columnWidth: 0.5,
                    enableKeyEvents: true,
                    listeners: {
                        specialkey: function (field, e)
                        {
                            if (e.getKey() === e.ENTER)
                            {
                                FindVildsvin(field.value, 'journalnr_1');
                            }
                        }
                    }
                }]
            }, FindVildsvinegrid]
		}, {
				xtype: 'fieldset',
				title: 'Indberetning',
				margins: 6,
				autoWidth: true,
				autoHeight: true,
				flex: 1,
				items: [{
					xtype: 'fccolumn_ro',
					items: [{
						fieldLabel: 'Indberetning',
						//margin: '0 3 0 0', // venstre
						id: 'indberetning',
						columnWidth: 0.2
					}, {
						fieldLabel: 'Afdeling',
						//margin: '0 3 0 0', // venstre
						id: 'indberetning_status',
						columnWidth: 0.3
					}, {
						fieldLabel: 'Indberetning skjult',
						//margin: '0 3 0 0', // venstre
						id: 'indberetning_skjul',
						columnWidth: 0.3
					}, {
						fieldLabel: 'Noteret',
						id: 'noteret',
						//margin: '0 3 0 0', // venstre
						columnWidth: 0.2
					}]
				}]
			}, tegn_fs_anmelder(), tegn_fs_fund(false), tegn_fs_fotografi(), {
            xtype: 'fieldset',
            title: 'Oplysninger om registreringen',
            autoHeight: true,
            margins: '6',
            //anchor: '0',
            //flex: 1,
            items: [{
                xtype: 'fccolumn_ro',
                items: [{
                    fieldLabel: 'Anmeldt tidspunkt',
                    id: 'an_tid',
                    xtype: 'datefield',
                    format: 'd-m-Y H:i:s',
                    altFormats: 'd/m/Y H:i:s',
                    margin: '0 3 0 0', // venstre
                    columnWidth: 0.2
                }, {
                    fieldLabel: 'Registrator',
                    id: 'registrator',
                    //margin: '0 3 0 0', // venstre
                    columnWidth: 0.4
                }, {
                    fieldLabel: 'Afdeling',
                    id: 'registrator_firma',
                    margin: '0 0 0 3', // højre
                    columnWidth: 0.4
                }]
            }]
        }, {
            xtype: 'fieldset',
            title: 'Oplysninger om indsamling og analyse',
            id: 'akt_vildsvin',
            bodyPadding: 10,
            autoWidth: true,
            autoHeight: true,
            style: 'margin-top: 10px',
            flex: 1,
            items: [{
                xtype: 'fccolumn_ro',
                items: [{
                    hideLabel: false,
                    fieldLabel: 'Vildsvin ID',
                    id: 'vildsvin_id',
                    margin: '0 3 0 0', // venstre
                    columnWidth: 0.15
                }, {
                    hideLabel: false,
                    fieldLabel: 'Status',
                    id: 'vildsvin_status',
                    margin: '0 3 0 0', // venstre
                    columnWidth: 0.25
                }, {
                    hideLabel: false,
                    fieldLabel: 'Resultat',
                    id: 'resultat_tekst',
                    margin: '0 3 0 0', // venstre
                    columnWidth: 0.4
                }, {
                    fieldLabel: 'Skjul',
                    id: 'vildsvin_skjul',
                    //margin: '0 3 0 0', // venstre
                    columnWidth: 0.2
                }]
            }, {
                xtype: 'fccolumn_ro',
                items: [{
                    hideLabel: false,
                    fieldLabel: 'Indsamlet',
                    id: 'tid_indsamlet',
                    xtype: 'datefield',
                    format: 'd-m-Y',
                    altFormats: 'd/m/Y H:i:s',
                    margin: '0 3 0 0', // venstre
                    columnWidth: 0.2
                }, {
                    hideLabel: false,
                    fieldLabel: 'Opsamler',
                    id: 'opsamler',
                    margin: '0 3 0 0', // venstre
                    columnWidth: 0.4
                }, {
                    fieldLabel: 'Indsamlet af',
                    id: 'afd',
                    //margin: '0 3 0 0', // venstre
                    columnWidth: 0.4
                }]
            }, {
                xtype: 'fccolumn_ro',
                items: [{
                    hideLabel: false,
                    fieldLabel: 'Indsamlet X',
                    id: 'x',
                    margin: '0 3 0 0', // venstre
                    columnWidth: 0.2
                }, {
                    hideLabel: false,
                    fieldLabel: 'Indsamlet Y',
                    id: 'y',
                    margin: '0 3 0 0', // venstre
                    columnWidth: 0.2
                }, {
                    fieldLabel: 'Modtaget dato',
                    id: 'tid_lab',
                    xtype: 'datefield',
                    format: 'd-m-Y',
                    altFormats: 'd/m/Y H:i:s',
                    //margin: '0 0 0 3', // højre
                    columnWidth: 0.3
                }, {
                    fieldLabel: 'Afsluttet dato',
                    id: 'tid_slut',
                    xtype: 'datefield',
                    format: 'd-m-Y',
                    altFormats: 'd/m/Y H:i:s',
                    margin: '0 0 0 3', // højre
                    columnWidth: 0.3
                }]
            }, {
                xtype: 'fccolumn_ro',
                items: [{
                    xtype: 'textarea',
                    fieldLabel: 'Kommentar til dette vildsvin',
                    id: 'vildsvin_kommentar',
                    margin: '0',
                    columnWidth: 1,
                    //height: 20,
                    grow: true,
                    growMin: 30,
                    growMax: 300
                }]
            }]
        }, {
            xtype: 'fieldset',
            title: 'Analyse LAB',
            id: 'akt_vet',
            bodyPadding: 10,
            autoWidth: true,
            autoHeight: true,
            style: 'margin-top: 10px',
            flex: 1,
            items: [{
                xtype: 'fccolumn_ro',
                items: [{
                    xtype: 'textfield',
                    hideLabel: false,
                    fieldLabel: 'LAB journalnr.',
                    id: 'journalnr_1',
                    columnWidth: 0.4,
                    margin: '0 3 0 0' // venstre
                }, {
                    xtype: 'textfield',
                    hideLabel: false,
                    fieldLabel: 'Modtaget dato',
                    id: 'tid_lab_1',
                    columnWidth: 0.3
                }, {
                    xtype: 'textfield',
                    hideLabel: false,
                    fieldLabel: 'Afsluttet dato',
                    id: 'tid_slut_1',
                    margin: '0 0 0 3', // højre
                    columnWidth: 0.3
                }]
            }, {
                xtype: 'fccolumn_ro',
                items: [{
                    xtype: 'textfield',
                    hideLabel: false,
                    fieldLabel: 'Afrikansk svinepest',
                    id: 'asf',
                    columnWidth: 0.33
                }, {
                    xtype: 'textfield',
                    hideLabel: false,
                    fieldLabel: 'Klassisk svinepest',
                    id: 'csf',
                    columnWidth: 0.33
                }, {
                    xtype: 'textfield',
                    hideLabel: false,
                    fieldLabel: "Aujeszky's sygdom",
                    id: 'aujeszkys',
                    columnWidth: 0.34
                }]
            }]
        }, {
            xtype: 'fieldset',
            title: 'Analyse FVST Ringsted',
            id: 'akt_fvst',
            bodyPadding: 10,
            autoWidth: true,
            autoHeight: true,
            style: 'margin-top: 10px',
            flex: 1,
            items: [{
                xtype: 'fccolumn_ro',
                items: [{
                    xtype: 'textfield',
                    hideLabel: false,
                    fieldLabel: 'FVST journalnr.',
                    id: 'journalnr_2',
                    columnWidth: 0.5,
                    margin: '0 3 0 0' // venstre
                }, {
                    xtype: 'textfield',
                    hideLabel: false,
                    fieldLabel: 'Trikiner',
                    id: 'trikiner',
                    columnWidth: 0.5
                }]
            }, {
                xtype: 'fccolumn_ro',
                items: [{
                    xtype: 'textfield',
                    hideLabel: false,
                    fieldLabel: 'Modtaget dato',
                    id: 'tid_lab_2',
                    columnWidth: 0.5
                }, {
                    xtype: 'textfield',
                    hideLabel: false,
                    fieldLabel: 'Afsluttet dato',
                    id: 'tid_slut_2',
                    margin: '0 0 0 3', // højre
                    columnWidth: 0.5
                }]

            }]
        }]
    });
    retKolonner(FindVildsvinegrid, 3);

    function FindVildsvin(value, felt)
    {
        storeFindVildsvin.proxy.extraParams = { value: value, felt: felt };
        storeFindVildsvin.load();
        //Ext.Ajax.request({
        //	url: 'Webservices/svin_login.asmx/FindVildsvin',
        //	method: "POST",
        //	params: JSON.stringify({
        //		value: value, felt: felt
        //	}),
        //	success: function () {
        //		console.log("ok");
        //	},
        //	failure: function (response, opts) {
        //		console.log("failed");
        //	},
        //	headers: {
        //		'Content-Type': 'application/json'
        //	}
        //});

    }
}
//function visFind(id)
//{

//	var fi = storeFind.model.getFields();
//	for (var i = 0; i < fi.length; i++)
//	{
//		try
//		{
//			ctl = Ext.getCmp(fi[i].name);
//			var val = (id == undefined ? '' : aktRec.get(fi[i].name));
//			if (val == null)
//				val = '';
//			if (fi[i].name === 'billede')
//			{
//				ctl.setSrc(val);
//				//var visbil = false;
//				//ctl.setVisible(visbil);
//				ctl.setVisible(val != '');
//			}
//			else
//				ctl.setValue(val);
//		} catch (e)
//		{
//			console.log('Find -- ' + fi[i].name + ' findes ikke');
//		}
//	}
//}