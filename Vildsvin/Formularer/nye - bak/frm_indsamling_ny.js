﻿function visIndsamlinger() {
	showMap();
	toemFormular('Indsamlinger_panel', 'visIndsamlinger');


	Ext.define('mIndsamlinger', {
		extend: 'Ext.data.Model',
		fields: [
			{ name: 'id', type: 'string', defaultValue: '0' },
			{ name: 'afd', type: 'string', defaultValue: '0' },
			{ name: 'afd_ny', type: 'string', defaultValue: '0' },
			{ name: 'indberetning', type: 'string', defaultValue: '0' },
			{ name: 'postnr', type: 'string', defaultValue: '' },
			{ name: 'status', type: 'string', defaultValue: '0' },
			{ name: 'fundtype', type: 'string', defaultValue: '0' },
			{ name: 'setx', type: 'string', defaultValue: '0' },
			{ name: 'sety', type: 'string', defaultValue: '0' },
			{ name: 'vildsvinekommentar', type: 'string', defaultValue: '' },
			{ name: 'skjul', type: 'string', defaultValue: '0' },
			{ name: 'x', type: 'string', defaultValue: '0' },
			{ name: 'y', type: 'string', defaultValue: '0' },
			{ name: 'set_tid', type: 'string', defaultValue: '' },
            { name: 'tid_indsamlet', type: 'string', defaultValue: '' },
			//{ name: 'afhentningsadresse', type: 'string', defaultValue: '' },
			{ name: 'fundsted', type: 'string', defaultValue: '' },
			{ name: 'pdf', type: 'boolean', defaultValue: false },
			{ name: 'an_navn', type: 'string', defaultValue: '' },
			{ name: 'an_adresse', type: 'string', defaultValue: '' },
			{ name: 'an_telefon', type: 'string', defaultValue: '' },
            { name: 'an_email', type: 'string', defaultValue: '' },
			{ name: 'set_adresse', type: 'string', defaultValue: '' },
			{ name: 'kommentar', type: 'string', defaultValue: '' },
			{ name: 'billede', type: 'string', defaultValue: '' },
		    { name: 'opsamler', type: 'string', defaultValue: '' }
		],
		idProperty: 'id'
	});

	storeIndsamlinger = Ext.create('Ext.data.JsonStore', {
		storeId: 'st_Indsamlinger',
		pageSize: 10,
		model: 'mIndsamlinger',
		remoteSort: true,
		autoLoad: false,
		proxy: {
			type: 'gridmap',
			url: 'Webservices/svin_login.asmx/hentIndsamlinger'
		},
		sorters: [{
			property: 'id',
			direction: 'DESC'
		}],
		listeners: {
			load: function (store) {
				retLaastPost = false;
                mapPanel.redrawLayers('vildsvin_doede')
				aktRec = undefined;
				aiz = [0, 0, 0, 0];
				var xmin, xmax, ymin, ymax;

				// løb de nye poster igennem
				if (store.getCount() === 0) {
					//	Ext.getCmp('selectonmap').disable();
					Ext.getCmp('zoomliste').disable();
					Ext.getCmp('zoommarkeret').disable();
				} else {
					//	Ext.getCmp('selectonmap').enable();
					Ext.getCmp('zoomliste').enable();
					Ext.getCmp('zoommarkeret').enable();
					//nulstilling
					xmin = 0;
					xmax = xmin;
					ymin = 0;
					ymax = ymin;
					store.each(function (record) {
						if (record.data.setx != null && record.data.setx != '0') {
							if (xmin == 0) {
								xmin = Number(record.data.setx);
								xmax = xmin;
								ymin = Number(record.data.sety);
								ymax = ymin;
							} else {
								if (Number(record.data.setx) < xmin) xmin = Number(record.data.setx);
								if (Number(record.data.setx) > xmax) xmax = Number(record.data.setx);
								if (Number(record.data.sety) < ymin) ymin = Number(record.data.sety);
								if (Number(record.data.sety) > ymax) ymax = Number(record.data.sety);
							} // slut if 0
						}
					}); // slut each
					if ((xmax - xmin) < 200) {
						xmidt = (Number(xmax) + Number(xmin)) / 2;
						xmax = Number(xmidt) + 100;
						xmin = Number(xmidt - 100);
					}
					if ((ymax - ymin) < 200) {
						ymidt = (Number(ymax) + Number(ymin)) / 2;
						ymax = Number(ymidt) + 100;
						ymin = Number(ymidt - 100);
					}

					zmListe = [xmin, ymin, xmax, ymax];
					if (Ext.getCmp('zoomliste').pressed == true)
						mapPanel.zoomTo(zmListe);
					visIndsamletVildsvin();

				} // slut count
				// sæt de aktuelle filtre på Excel-udtræk
				Ext.get('btn_excel').down('a').dom.href = 'VisFil.ashx?type=xl_indsaml&id=' + brugerid + '&gridfilter=' + store.proxy.reader.rawData.d.filterkrit;
			}

		}
	});
	storeAkt = storeIndsamlinger;
    mapPanel.config.layers.data.vildsvin_doede.setVisibility(true);



	filters = Ext.create('Ext.ux.grid.FiltersFeature', {
		// encode and local configuration options defined previously for easier reuse
		encode: true, // json encode the filter query
		local: false // defaults to false (remote filtering)
	});
	var Indsamlinger_grid = Ext.create('Ext.grid.Panel', {
		layout: 'fit',
		id: 'Indsamlinger_grid',
		autoHeight: true,
		autoWidth: true,
		title: '',
		store: storeIndsamlinger,
		disableSelection: false,
		loadMask: true,
		features: [filters],
		forceFit: true,
		viewConfig: {
			id: 'vg',
			//trackOver: false,
			//stripeRows: true
			getRowClass: function (record, rowIndex, rowParams, store) {
				return ((record.get('status') == '1' || record.get('status') == '2' || record.get('status') == '3') && record.get('skjul') == '0' ? '' : 'readonly');
			}
		},
		// grid columns
		// specify any defaults for each column

		columns: {
			defaults: { renderer: renderTooltip },
			items: [{
				id: 'id',
				text: "ID",
				tooltip: "ID",
				dataIndex: 'id',
				width: frm_indsamling.Indsamlinger_grid.id.width,
				hidden: frm_indsamling.Indsamlinger_grid.id.hidden,
				//flex: 1,
				filterable: true,
				sortable: true,
				filter: { type: 'numeric' }
			}, {
				text: "Afdeling",
				tooltip: "Afdeling",
				dataIndex: 'afd',
				width: frm_indsamling.Indsamlinger_grid.afd.width,
				hidden: frm_indsamling.Indsamlinger_grid.afd.hidden,
				filterable: true,
				sortable: true,
				flex: 1,
				renderer: renderIndsamler,
				filter: {
					type: 'list',
					store: storeIndsamlere,
					idField: 'id',
					labelField: 'navn',
					value: ((firmatype == indsamlertype) ? firma : undefined),
					active: ((firmatype == indsamlertype) ? true : false)
				}
			}, {
				text: "Opsamler",
				tooltip: "Opsamler",
				dataIndex: 'opsamler',
				width: frm_indsamling.Indsamlinger_grid.opsamler.width,
				hidden: frm_indsamling.Indsamlinger_grid.opsamler.hidden,
				filterable: true,
				//flex: 1,
				sortable: true,
				filter: { type: 'string' }
			}, {
				text: "Postnr",
				tooltip: "Postnr",
				dataIndex: 'postnr',
				width: frm_indsamling.Indsamlinger_grid.postnr.width,
				hidden: frm_indsamling.Indsamlinger_grid.postnr.hidden,
				filterable: true,
				sortable: true,
				flex: 1,
				filter: { type: 'numeric' }
			}, {
				text: "Indberetning",
				tooltip: "Indberetning",
				dataIndex: 'indberetning',
				width: frm_indsamling.Indsamlinger_grid.indberetning.width,
				hidden: frm_indsamling.Indsamlinger_grid.indberetning.hidden,
				filterable: true,
				//flex: 1,
				sortable: true,
				filter: { type: 'numeric' }
            }, {
				text: "Fundtype",
                tooltip: "Fundtype",
                dataIndex: 'fundtype',
                width: frm_indsamling.Indsamlinger_grid.fundtype.width,
                hidden: frm_indsamling.Indsamlinger_grid.fundtype.hidden,
				filterable: true,
				flex: 1,
				sortable: true,
		        renderer: renderFundtyper,
                    filter: {
                        type: 'list',
                        store: storeFundtyperDoed,
                        idField: 'id',
                        labelField: 'navn'
                    }
			}, {
				text: "Set dato",
                    tooltip: "Set dato",
				dataIndex: 'set_tid',
				width: frm_indsamling.Indsamlinger_grid.set_tid.width,
				hidden: frm_indsamling.Indsamlinger_grid.set_tid.hidden,
				filterable: true,
				//flex: 1,
				sortable: true,
				renderer: renderDato,
				filter: { type: 'date' }
			}, {
				text: "Status",
				tooltip: "Status",
				dataIndex: 'status',
				width: frm_indsamling.Indsamlinger_grid.status.width,
				hidden: frm_indsamling.Indsamlinger_grid.status.hidden,
				filterable: true,
				sortable: true,
				flex: 1,
				renderer: renderStatus,
				filter: {
					type: 'list',
					store: storeStatus,
					idField: 'id',
					labelField: 'navn',
					value: ['1', '2', '3'],
					active: true
				}
			}, {
				text: "Vis / Skjul",
				tooltip: "Vis / Skjul",
				dataIndex: 'skjul',
				width: frm_indsamling.Indsamlinger_grid.skjul.width,
				hidden: frm_indsamling.Indsamlinger_grid.skjul.hidden,
				filterable: true,
				//flex: 1,
				sortable: true,
				renderer: renderVis,
				filter: {
					type: 'list',
					store: storeSkjul,
					idField: 'id',
					labelField: 'navn',
					value: '0',
					active: true
				}
			}]
		},
		// gridkol_slut
		selModel: Ext.create('Ext.selection.RowModel', {
			listeners: {
				select: {
					fn: function (selModel, rec, index) {
						retLaastPost = false;
						aktRec = rec;
						aktRk = index;
						aiz = [0, 0, 0, 0];
						aiz[0] = Number(rec.data.setx - 100);
						aiz[1] = Number(rec.data.sety - 100);
						aiz[2] = Number(Number(rec.data.setx) + 100);
						aiz[3] = Number(Number(rec.data.sety) + 100);

						if (Ext.getCmp('zoommarkeret').pressed == true && rec.data.setx != null && rec.data.setx != '0')
							mapPanel.zoomTo(aiz);


						// ??? sæt knapper aktive og inaktive

						mapPanel.clearLayers();
						mapPanel.addSelectedPoint(rec.data.setx, rec.data.sety);
						visIndsamletVildsvin(rec.data.id); //???
					}
				}
			}
		}),
		// paging bar on the bottom
		tbar: Ext.create('Ext.PagingToolbar', {
			store: storeIndsamlinger,
			displayInfo: true
		}),
		bbar: createZoombar(true)
	});

	// hent liste med indberetninger - resten udføres når listen er hentet og dernæst når der vælges i listen
	//var storeIndberetningIndsamling = Ext.create('Ext.data.JsonStore', {
	//	storeId: 'storeIndberetningIndsamling',
	//	model: 'id_navn',
	//	remoteSort: true,
	//	autoLoad: true,
	//	proxy: {
	//		type: 'simpel',
	//		extraParams: { liste: 'indberetninger' },
	//		url: 'Webservices/svin_login.asmx/hentListe'
	//	},
	//	//sorters: [{
	//	//    property: 'navn',
	//	//    direction: 'ASC'
	//	//}],
	//	listeners: {
	//		load: function (store) {
	//			var indbMenu = Ext.create('Ext.menu.Menu');
	//			store.each(function (record) {
	//				indbMenu.add({ xtype: 'menuitem', text: record.data.navn, id: 'indbMenu_' + record.data.id, handler: function () { tilfoejIndsamlerVildsvin(this.id) } });
	//			}, this);
	//			var btn = Ext.create('Ext.Button', {
	//				text: 'Tilføj vildsvin',
 //                   id: 'tilfoej_vildsvin',
	//				iconCls: 'icon-add',
 //                   tooltip: 'Vælg en indberetning at knytte det nye vildsvin til. Indberetninger med * bagefter er reserveret af dig.',
	//				menu: indbMenu
	//			});

	//			Indsamlinger_grid.dockedItems.items[2].add(btn)

	//		}
	//	}
	//});




	//	Indsamlinger_grid.bbar.add(['->', {
	//		text: 'Tilføj fugl',
	//		id: 'tilfoej_fugl',
	//		iconCls: 'icon-add',
	//		handler: function () { }
	//	}]);


	aktGrid = 'Indsamlinger_grid';

	Indsamlinger_grid.child('pagingtoolbar').add(['->', {
		text: 'Nulstil alle filtre',
		handler: function () {
			Indsamlinger_grid.filters.clearFilters();
			x1 = 0;
			y1 = 0;
			x2 = 0;
			y2 = 0;

		}
	}]);

	Ext.apply(Ext.form.field.VTypes, {
		nord: function (val, field) {
			var nordName = /^6[0-9]{6}$/;
			return nordName.test(val);
		},
		nordText: 'Nord skal udfyldes med et 7-cifret tal.',
		nordMask: /[0-9]/i
	});
	Ext.apply(Ext.form.field.VTypes, {
		ost: function (val, field) {
			var ostName = /^[4-9]{1}[0-9]{5}$/;
			return ostName.test(val);
		},
		ostText: 'Øst skal udfyldes med et 6-cifret tal.',
		ostMask: /[0-9]/i
	});


	var Indsamlinger_panel = Ext.create('Ext.panel.Panel', {
		title: 'Døde og skudte vildsvin',
		id: 'Indsamlinger_panel',
		layout: 'anchor',
		bodyPadding: '10 10 10 10',
		autoWidth: true,
		autoHeight: false,
		renderTo: 'vestdiv',
		items: [
			Indsamlinger_grid,
			{
				xtype: 'fieldset', // her er kun de redigerbare felter
                title: 'Redigér indsamlet vildsvin',
				id: 'akt_indsamling',
				bodyPadding: 10,
				autoWidth: true,
				autoHeight: true,
				style: 'margin-top: 10px',
				flex: 1,
				items: [{
					xtype: 'fccolumn',
					items: [{
						xtype: 'acombo',
						hideLabel: false,
						fieldLabel: 'Status',
						id: 'tx_status',
						store: storeStatusBRS,
						valueField: 'id',
						displayField: 'navn',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.5
                    }, {
						xtype: 'acombo',
						hideLabel: false,
						fieldLabel: 'Indsamlet af afdeling',
						id: 'tx_afdeling',
						store: storeIndsamlereA,
						valueField: 'id',
						displayField: 'navn',
						columnWidth: 0.5
					}]
				}, {
					xtype: 'fccolumn',
					items: [{
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Øst',
						id: 'tx_x',
						columnWidth: 0.5,
						allowBlank: false,
						vtype: 'ost',
						listeners: {
							blur: tegnFundsted
						}
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Nord',
						id: 'tx_y',
						margin: '0 0 0 3', // højre
						columnWidth: 0.5,
						allowBlank: false,
						vtype: 'nord',
						listeners: {
							blur: tegnFundsted
						}
					}]
				}, {
					xtype: 'toolbar',
					style: 'margin-top: 6px',
					items: [{
						xtype: 'button',
						text: 'Send til opsamlingshold',
						id: 'opsamlerMenu_0',
						style: 'margin-right: 15px',
						menu: opsamlerMenu,
						hidden: true
					}, {
						xtype: 'button',
						id: 'vis_pdf',
						tooltip: 'Vis PDF køreseddel',
						iconCls: 'icon-pdf',
						hidden: true,
						href: 'VisFil.ashx?type=pdf&id=0',
						htmp: 'VisFil.ashx?type=pdf&id=',
						target: '_blank',
						menu: {
							xtype: 'menu',
							items: [{
								xtype: 'menuitem',
								id: 'menuPDF',
								text: 'Opret køreseddel',
								handler: function () {
									Ext.Ajax.request({
										url: 'Webservices/svin_login.asmx/lavKoereseddel',
										params: JSON.stringify({
											indberetning: aktRec.data.indberetning
										}),
										headers: { 'Content-Type': 'application/json;charset=utf-8' },
										success: function (response, opts) {
											okbesked('Der er nu lavet en ny køreseddel');
										},
										failure: ajaxFailed
									});


								}
							}]
						}
					}, '-', {
						xtype: 'button',
						toggleGroup: 'geopunkt',
						enableToggle: true,
						pressed: true,
						tooltip: 'Skriv koordinater',
						id: 'btn_skriv',
						iconCls: 'icon-edit',
						handler: function () {
							//Ext.getCmp('tx_x').setValue(x);
							//Ext.getCmp('tx_y').setValue(y);
						}
					}, {
						xtype: 'button',
						toggleGroup: 'geopunkt',
						enableToggle: true,
						tooltip: 'Brug GPS',
						id: 'btn_gps',
						iconCls: 'icon-gps',
						listeners: {
							toggle: function (ctl, pressed) {
								if (pressed == true) {
									// kun hvis knappen er trykket ned
									ctl.btnInnerEl.setStyle({ color: '#600000', fontWeight: 'bold' });
									Ext.getCmp('ro_gps').show();
									Ext.getCmp('ro_gps2').show();
									gps_maal();

								}
								else {
									ctl.btnInnerEl.setStyle({ color: '#333333', fontWeight: 'normal' });
									Ext.getCmp('ro_gps').hide();
									Ext.getCmp('ro_gps2').hide();
								}
							}
						}
					}, {
						xtype: 'button',
						toggleGroup: 'geopunkt',
						enableToggle: true,
						tooltip: 'Marker fundsted på kortet',
						id: 'btn_mark',
						iconCls: 'icon-koordinatfanger',
						listeners: {
							toggle: function (ctl, pressed) {
								if (pressed == true) {
									// kun hvis knappen er trykket ned
									ctl.btnInnerEl.setStyle({ color: '#600000', fontWeight: 'bold' });
									DrawPoint = new OpenLayers.Control.DrawFeature(mapPanel.config.layers.selections.coordinateLayer, OpenLayers.Handler.Point);  //, { 'displayClass': 'olControlDrawBigaard' }
									DrawPoint.events.register('featureadded', this, function () {
										var x = arguments[0].feature.geometry.x;
										var y = arguments[0].feature.geometry.y;
										mapPanel.clearLayers();
										mapPanel.showPoint(x, y);
										Ext.getCmp('tx_x').setValue(Math.round(x));
										Ext.getCmp('tx_y').setValue(Math.round(y));
									});
									mapPanel.map.addControl(DrawPoint);
									DrawPoint.activate();
								}
								else {
									ctl.btnInnerEl.setStyle({ color: '#333333', fontWeight: 'normal' });
									DrawPoint.deactivate();
								}
							}
						}
					}, '->', {
						xtype: 'checkbox',
						boxLabel: 'Zone 33',
						id: 'menu_zone',
						//style: 'margin-right: 5px',
						style: 'margin-right: 35px; font-size: 11px !important; ',
						checked: false
					}, {
						xtype: 'button',
						text: 'Ret låst post',
						id: 'btn_laast',
						iconCls: 'icon-edit',
						style: 'margin-right: 15px',
						hidden: true,
						handler: function () {
							enableCtl(Ext.getCmp('akt_indsamling'), true);
							retLaastPost = true;
							//					        okbesked('Når du er færdig med at redigere den post, du lige har låst op, er det en god ide at trykke på knappen Opfrisk - ellers er det ikke sikkert at du kan se resultatet af din ændring.');
						}
					}, {
						xtype: 'button',
						text: 'Skjul',
						id: 'skjulmenu_0',
						iconCls: 'icon-remove',
						style: 'margin-right: 15px',
						menu: skjulMenu,
						hidden: (firmatype == '0' || firmatype == '4' ? false : true)
					}, {
						xtype: 'button',
						text: 'Udfør',
						id: 'gem_indsamling',
						iconCls: 'icon-ok',
						handler: function () {
							gemIndsamling(false);
						}
					}]
				}, {
					xtype: 'fccolumn_ro',
					id: 'ro_gps',
					hidden: true,
					style: 'margin-top: 6px',
					items: [{
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Øst',
						id: 'tx_gx',
						columnWidth: 0.3
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Nord',
						id: 'tx_gy',
						margin: '0 0 0 3', // højre
						columnWidth: 0.3
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Usikkerhed (m)',
						id: 'tx_gz',
						margin: '0 0 0 3', // højre
						columnWidth: 0.2
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Fejl',
						id: 'tx_fejl',
						margin: '0 0 0 3', // højre
						columnWidth: 0.2
					}]
				}, {
					xtype: 'toolbar',
					id: 'ro_gps2',
					hidden: true,
					style: 'margin-top: 6px',
					items: [{
						xtype: 'button',
						text: 'Ny GPS måling',
						id: 'btn_maal',
						handler: gps_maal
					}, '->', {
						xtype: 'button',
						text: 'Udfør',
						tooltip: 'Benyt GPS-målingen',
						id: 'btn_ok',
						handler: function () {
							Ext.getCmp('tx_x').setValue(Ext.getCmp('tx_gx').getValue());
							Ext.getCmp('tx_y').setValue(Ext.getCmp('tx_gy').getValue());
							Ext.getCmp('btn_gps').toggle();
						}
					}]
				}]
            }, {
                xtype: 'fieldset',
                title: 'Kommentar, kan redigeres selvom vildsvinet er låst',
                id: 'fs_kommentar',
                bodyPadding: 10,
                autoWidth: true,
                autoHeight: true,
                style: 'margin-top: 10px',
                flex: 1,
                items: [{
                    xtype: 'fccolumn',
                    items: [{
                        xtype: 'textarea',
                        hideLabel: true,
                        id: 'tx_vildsvinekommentar',
                        margin: '0',
                        columnWidth: 0.8,
                        grow: true,
                        growMin: 30,
                        growMax: 300
                    }, {
                        xtype: 'displayfield',
                        columnWidth: 0.02,
                        hideLabel: true,
                        value: ''

                    }, {
                        xtype: 'button',
                        text: 'Gem',
                        tooltip: 'Gem kommentar',
                        id: 'gem_kommentar',
                        iconCls: 'icon-ok',
                        columnWidth: 0.18,
                        handler: function () {
                            // kun hvis der er en post
                            if (aktRec != undefined) {
                                // send til serveren
                                var param = {
                                    fugl: aktRec.data.id,
                                    kommentar: Ext.getCmp('tx_vildsvinekommentar').getValue()
                                };
                                var params = Ext.JSON.encode(param);
                                Ext.Ajax.request({
                                    url: 'Webservices/svin_login.asmx/gemvildsvinekommentar',
                                    params: params,
                                    headers: { 'Content-Type': 'application/json;charset=utf-8' },
                                    success: function (response, opts) {
                                        var opdatvar = new Array();
                                        opdatvar.push('vildsvinekommentar');
                                        opdatvar.push(Ext.getCmp('tx_vildsvinekommentar').getValue());
                                        opdaterRaekke(opdatvar);
                                    },
                                    failure: ajaxFailed
                                });
                            }
                        }
                    }]
                }]
			}, {
				xtype: 'fieldset',
				title: 'Ekstra oplysninger',
				id: 'ekstra_indsamling',
				bodyPadding: 10,
				autoWidth: true,
				autoHeight: true,
				style: 'margin-top: 10px',
				flex: 1,
				items: [{
					xtype: 'fccolumn_ro',
					items: [{
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Indberetning',
						id: 'tx_indberetning',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.2
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Postnr',
						id: 'tx_postnr',
						columnWidth: 0.2
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Set øst',
						id: 'tx_setx',
						columnWidth: 0.3
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Set nord',
						id: 'tx_sety',
						margin: '0 0 0 3', // højre
						columnWidth: 0.3
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Set dato',
						id: 'tx_set_tid',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.3
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Indsamlet dato',
						id: 'tx_tid_indsamlet',
						margin: '0 0 0 3', // højre
						columnWidth: 0.3
                    }, {
                        xtype: 'fcombo',
                        hideLabel: false,
                        fieldLabel: 'Fundtype',
                        id: 'tx_fundtype',
                        store: storeFundtyperDoed,
                        valueField: 'id',
                        displayField: 'navn',
                        columnWidth: 0.4
                    }]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Anmelders navn',
						id: 'tx_an_navn',
						margin: '0 3 0 0', // venstre
						columnWidth: 1
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Anmelders adresse',
						id: 'tx_an_adresse',
						margin: '0 3 0 0', // venstre
						columnWidth: 1
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Anmelders telefon',
						id: 'tx_an_telefon',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.5
					}, {
						xtype: 'textfield',
						hideLabel: false,
                        fieldLabel: 'Email',
                        id: 'tx_an_email',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.5
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Fundets adresse',
						id: 'tx_set_adresse',
						margin: '0 3 0 0', // venstre
						columnWidth: 1
					}]
				//}, {
				//	xtype: 'fccolumn_ro',
				//	items: [{
				//		xtype: 'textfield',
				//		hideLabel: false,
				//		fieldLabel: 'Afhentningsadresse',
				//		id: 'tx_afhentningsadresse',
				//		margin: '0 3 0 0', // venstre
				//		columnWidth: 1
				//	}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Fundsted',
						id: 'tx_fundsted',
						margin: '0 3 0 0', // venstre
						columnWidth: 1
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						xtype: 'textarea',
						hideLabel: false,
						fieldLabel: 'Kommentar til indberetningen',
						id: 'tx_kommentar',
						margin: '0',
						columnWidth: 1,
						grow: true,
						growMin: 30,
						growMax: 300
					}]
				}, {
					xtype: 'fieldset',
					title: 'Fotografi',
					id: 'f_billede',
					bodyPadding: 10,
					autoWidth: true,
					autoHeight: true,
					hidden: true,
					style: 'margin-top: 10px',
					flex: 1,
					items: [{
						xtype: 'image',
						id: 'billede',
						src: '',
						hidden: false,
						shrinkWrap: true,
						width: '100%'
					}]
				}]
			}]
	});
	// vi skal sikre at de indledende filtre sættes
	//Indsamlinger_panel.doLayout();
	filters.createFilters();
	storeIndsamlinger.load();
	retKolonner(Indsamlinger_grid, "Indsamlinger_grid");

}


function visIndsamletVildsvin(id) {
	var redfugl = false;
	// tøm først alle felter
	Ext.getCmp('btn_laast').hide();

	Ext.getCmp('tx_indberetning').setValue('');
	Ext.getCmp('tx_postnr').setValue('');
	Ext.getCmp('tx_afdeling').setOriginal('');
	Ext.getCmp('tx_status').setOriginal('');
	Ext.getCmp('tx_fundtype').setValue('');
    Ext.getCmp('tx_fundtype').readOnly = true;
	Ext.getCmp('tx_x').setValue('');
	Ext.getCmp('tx_y').setValue('');
	Ext.getCmp('tx_setx').setValue('');
	Ext.getCmp('tx_sety').setValue('');
	Ext.getCmp('tx_set_tid').setValue('');
//	Ext.getCmp('tx_tid_fvst').setValue('');

	Ext.getCmp('tx_an_navn').setValue('');
	Ext.getCmp('tx_an_adresse').setValue('');
	Ext.getCmp('tx_an_telefon').setValue('');
    Ext.getCmp('tx_an_email').setValue('');
	Ext.getCmp('tx_set_adresse').setValue('');
	//Ext.getCmp('tx_afhentningsadresse').setValue('');
	Ext.getCmp('tx_fundsted').setValue('');
	Ext.getCmp('f_billede').setVisible(false);
	Ext.getCmp('opsamlerMenu_0').setVisible(false);
	Ext.getCmp('menu_zone').setValue(false);
	Ext.getCmp('vis_pdf').hide();
	Ext.getCmp('btn_skriv').hide();
	Ext.getCmp('btn_gps').hide();
	Ext.getCmp('btn_mark').hide();
	Ext.getCmp('tx_kommentar').setValue('');
	Ext.getCmp('tx_vildsvinekommentar').setValue('');
    Ext.getCmp('tilfoej_vildsvin').setDisabled(true);



    if (aktRec == undefined)
    {
        Ext.getCmp('akt_indsamling').setTitle('Intet aktuelt vildsvin');
    }
    else
    {
        // opdater listen med statusværdier til at have de rette aktive, baseret på aktuel status og fundtype
        /*
            afventer indsamling false
            reserveret false
                        
            indsamling udført true døde og ilanddrevne, false nedlagte - eller allerede valgt
            sendt til lab true døde og nedlagte, false ilanddrevne
            bortskaffet true ilanddrevne, ellers false
            Ikke fundet true døde og ilanddrevne, false nedlagte
            Kasseret true døde, ellers false
        */
        var a = storeStatusBRS.getById('3');
        a.set('aktiv', aktRec.data.fundtype >= 30 && aktRec.data.status != 3);
        a = storeStatusBRS.getById('4');
        a.set('aktiv', aktRec.data.fundtype < 40 && aktRec.data.status < 4);
        a = storeStatusBRS.getById('7');
        a.set('aktiv', aktRec.data.fundtype == 40 && aktRec.data.status < 4);
        a = storeStatusBRS.getById('91');
        a.set('aktiv', aktRec.data.fundtype >= 30 && aktRec.data.status < 4);
        a = storeStatusBRS.getById('92');
        a.set('aktiv', aktRec.data.fundtype == 30 && aktRec.data.status < 4);
        storeStatusBRS.sort();

        if (aktRec.data.skjul != '0')
        {
            Ext.getCmp('akt_indsamling').setTitle('Vis skjult indsamling (' + storeSkjul.getById(aktRec.data.skjul).data.navn + ')');
        }
        else
        {
            if (id == '0')
            {
                // ny fugl 
                // dette skal kun være aktivt for indsamlere, det klares med knappen
                redfugl = true;
                Ext.getCmp('akt_indsamling').setTitle('Nyt indsamlet vildsvin');
                Ext.getCmp('tx_afdeling').setOriginal(aktRec.data.afd);
            }
            else if ((aktRec.data.status == '1' || aktRec.data.status == '2' || aktRec.data.status == '3') && (firmatype == indsamlertype || firmatype == '0' || firmatype == '4'))
            {
                // eksisterende fugl, kan redigeres
                redfugl = true;
                // status = sendt til indsamling
                Ext.getCmp('akt_indsamling').setTitle('Redigér indsamlet vildsvin');
                Ext.getCmp('tx_afdeling').setOriginal(aktRec.data.afd);
                Ext.getCmp('opsamlerMenu_0').setVisible(aktRec.data.status == '1' && firmatype == indsamlertype);
                Ext.getCmp('btn_skriv').show();
                Ext.getCmp('btn_gps').show();
                Ext.getCmp('btn_mark').show();
                Ext.getCmp('tilfoej_vildsvin').setDisabled(false);
            }
            else
            {
                // status != sendt til indsamling
                Ext.getCmp('akt_indsamling').setTitle('Vis aktuelt vildsvin');
                Ext.getCmp('tx_afdeling').setOriginal(aktRec.data.afd);
            }
            Ext.getCmp('tx_indberetning').setValue(aktRec.data.indberetning);
            Ext.getCmp('tx_postnr').setValue(aktRec.data.postnr);
            Ext.getCmp('tx_status').setOriginal(aktRec.data.status);
            Ext.getCmp('tx_fundtype').setValue(aktRec.data.fundtype);
            Ext.getCmp('tx_fundtype').readOnly = (aktRec.data.id != '0');
            Ext.getCmp('tx_x').setValue(aktRec.data.x);
            Ext.getCmp('tx_y').setValue(aktRec.data.y);
            Ext.getCmp('tx_setx').setValue(aktRec.data.setx);
            Ext.getCmp('tx_sety').setValue(aktRec.data.sety);
            Ext.getCmp('tx_set_tid').setValue(aktRec.data.set_tid);
            //Ext.getCmp('tx_tid_fvst').setValue(aktRec.data.tid_fvst);
            Ext.getCmp('tx_an_navn').setValue(aktRec.data.an_navn);
            Ext.getCmp('tx_an_adresse').setValue(aktRec.data.an_adresse);
            Ext.getCmp('tx_an_telefon').setValue(aktRec.data.an_telefon);
            Ext.getCmp('tx_an_email').setValue(aktRec.data.an_email);
            Ext.getCmp('tx_set_adresse').setValue(aktRec.data.set_adresse);
            //Ext.getCmp('tx_afhentningsadresse').setValue(aktRec.data.afhentningsadresse);
            Ext.getCmp('tx_fundsted').setValue(aktRec.data.fundsted);
            Ext.getCmp('tx_kommentar').setValue(aktRec.data.kommentar);
            Ext.getCmp('tx_vildsvinekommentar').setValue(aktRec.data.vildsvinekommentar);
            //	Ext.getCmp('f_billede').setValue(aktRec.data.billede);
            Ext.getCmp('billede').setSrc(aktRec.data.billede);
            if (aktRec.data.billede != null && aktRec.data.billede != '')
                Ext.getCmp('f_billede').setVisible(true);
            if (aktRec.data.pdf)
            {
                Ext.getCmp('vis_pdf').show();
                Ext.getCmp('vis_pdf').href = Ext.getCmp('vis_pdf').htmp + aktRec.data.indberetning;
                Ext.getDom('vis_pdf' + '-btnEl').href = Ext.getCmp('vis_pdf').href;
                Ext.getCmp('vis_pdf').enable();
            }


            // sæt aktiv eller ej
            enableCtl(Ext.getCmp('akt_indsamling'), redfugl);
            // er der noget at låse op for dem der kan?
            if (redfugl == false && (firmatype == '0' || firmatype == '4'))
            {
                Ext.getCmp('btn_laast').show();
                Ext.getCmp('btn_laast').setDisabled(false);
            }


        }
    }
}

function tegnFundsted() {
	var cx = Ext.getCmp('tx_x');
	var cy = Ext.getCmp('tx_y');

	if (cx.isValid() == true && cy.isValid() == true) {
		mapPanel.showPoint(cx.getValue(), cy.getValue());

	}
}


function vaelgOpsamler(menu_nr) {
	var opsamler = menu_nr.substring(menu_nr.indexOf('_') + 1);
	Ext.Ajax.request({
		url: 'Webservices/svin_login.asmx/vaelgOpsamler',
		method: "POST",
		params: JSON.stringify({
			opsamler: opsamler,
			indberetning: aktRec.data.indberetning,
			pdf: aktRec.data.pdf
		}),
		success: function () {
			// genindlæs , der kan være rettet mange poster
			storeIndsamlinger.load();
		},
		failure: function (response, opts) {
			//console.log("failed");
			ajaxFailed(response, opts);
		},
		headers: { 'Content-Type': 'application/json' }
	});
}

//function tilfoejIndsamlerVildsvin(menu_nr) {
//	var indberetning = menu_nr.substring(menu_nr.indexOf('_') + 1);

//	var nr = storeAkt.getCount();
//	var ny = Ext.create('mIndsamlinger', {
//		id: '0',
//		afd: firma,
//        indberetning: indberetning,
//        status: 3
//	});
//	storeAkt.add(ny); // modellen, med standardværdier
//	// sæt fokus på den nye række
//	Ext.getCmp("Indsamlinger_grid").getView().select(nr);
//}
// ny variant, knytter til markerede indberetning
function tilfoejIndsamlerVildsvin()
{
    //var indberetning = menu_nr.substring(menu_nr.indexOf('_') + 1);

    var nr = storeAkt.getCount();
    var ny = Ext.create('mIndsamlinger', {
        id: '0',
        afd: aktRec.data.afd,
        indberetning: aktRec.data.indberetning,
        fundtype: aktRec.data.fundtype,
        status: 3
    });
    storeAkt.add(ny); // modellen, med standardværdier
    // sæt fokus på den nye række
    Ext.getCmp("Indsamlinger_grid").getView().select(nr);
}


function gemIndsamlingSpoerg(svar) {
	if (svar == 'yes') {
		gemIndsamling(true);
	}
}

function gemIndsamling(harSpurgt) {
	if (harSpurgt == undefined) harSpurgt = false;
	// opret et objekt med gemme-værdierne
	var g = {
		id: aktRec.data.id,
		indberetning: Ext.getCmp('tx_indberetning').getValue(),
		status: Ext.getCmp('tx_status').getValue(),
		afd: aktRec.data.afd,
		afd_ny: Ext.getCmp('tx_afdeling').getValue(),
		vildsvinekommentar: Ext.getCmp('tx_vildsvinekommentar').getValue(),
		skjul: aktRec.data.skjul,
		fundtype: Ext.getCmp('tx_fundtype').getValue(),
		x: Ext.getCmp('tx_x').getValue(),
		y: Ext.getCmp('tx_y').getValue(),
		zone33: (Ext.getCmp('menu_zone').getValue() == true ? '1' : '')
	};
	var fejltekst = '';
	var advarsel = '';

	// hvis dyret sendes til en anden indsamler, må intet andet være ændret - og det må ikke være et nyt dyret
	if (g.afd_ny != g.afd && g.afd != '0') {
		if (g.indberetning == '0') {
			fejlbox('Du kan ikke sende et vildsvin, du lige har tilføjet, til en anden indsamler.');
			return;
		}
		if (harSpurgt == false && (g.status != aktRec.data.status || g.fundtype != aktRec.data.fundtype || g.x != aktRec.data.x || g.y != aktRec.data.y)) {
            spoerg('Når du sender et vildsvin til en anden indsamler, kan du ikke gemme andre oplysninger om det, dvs ikke angive fx koordinater. Vil du fortsætte?', gemIndsamlingSpoerg);
			return;
		}
	}
		// hvis status er ikke indsamlet, må intet andet ændres
	else if (Number(g.status) > 90) {
		if (g.indberetning == '0') {
            fejlbox('Der er ingen grund til at tilføje et vildsvin, medmindre du har samlet den ind.');
			return;
		}
		if (harSpurgt == false && (g.fundtype != aktRec.data.fundtype || g.x != aktRec.data.x || g.y != aktRec.data.y)) {
            spoerg('Når et vildsvin ikke samles ind, kan du ikke gemme andre oplysninger om den, dvs ikke angive fx koordinater. Vil du fortsætte?', gemIndsamlingSpoerg);
			return;
		}
	}
	else if (harSpurgt == false) {
		// OK, nu er skjul og afdeling uændret - nu skal alle relevante felter være udfyldt
		// fundtype skal være udfyldt
		if (g.fundtype == '0')
			fejltekst += '\nFundtype skal udfyldes';
		if (g.status == '0')
			fejltekst += '\nStatus skal udfyldes';
		var afstand = 0.0;
		try {
			var x = Number(g.x);
			var y = Number(g.y);
			if ((x < 437000 || x > 902000 || y < 6043000 || y > 6405000) && g.zone33 == '' || (x < 0 || x > 515000 || y < 6040000 || y > 6410000) && g.zone33 == '1')// tjek at de ligger indenfor Danmarks boundingbox
				fejltekst += '\nKoordinater er ikke korrekt udfyldt - punktet ligger ikke i Danmark.';
			else if (g.id != '0' && g.zone33 == '') {
				afstand = Math.sqrt(Math.pow(x - Number(aktRec.data.setx), 2) + Math.pow(y - Number(aktRec.data.sety), 2));
				if (afstand > 1000)
					advarsel = 'Punktet ligger ' + afstand + ' meter fra det sted, der blev angivet i indberetningen.\nTryk OK, hvis du vil fortsætte.\nTryk Annullér, hvis du vil rette de indtastede koordinater.';
			}
		}
		catch (ex) {
			fejltekst += '\nKoordinater skal udfyldes - Øst skal være et 6-cifret tal og Nord skal være et 7-cifret tal.';
		}
	}
	if (fejltekst != '') {
		fejlbox(fejltekst);
		return;
	}

	if (advarsel != '') {
		if (confirm(advarsel) == false)
			return;
	}

	// send til serveren
	var param = { g: g };
	var params = Ext.JSON.encode(param);


	Ext.Ajax.request({
		url: 'Webservices/svin_login.asmx/gemIndsamling',
		params: params,
		headers: { 'Content-Type': 'application/json;charset=utf-8' },
		success: function (response, opts) {
			if (retLaastPost) {
				storeIndsamlinger.reload();
			}
			else {
				// ret kun den aktuelle række
				var ret = Ext.JSON.decode(response.responseText).d;
				var opdatvar = new Array();
				opdatvar.push('id');
				opdatvar.push(ret.id);
				opdatvar.push('fundtype');
				opdatvar.push(ret.fundtype);
				opdatvar.push('status');
				opdatvar.push(ret.status);
				//opdatvar.push('skjul');
				//opdatvar.push(ret.skjul);
				opdatvar.push('afd_ny');
				opdatvar.push(ret.afd_ny);
				opdatvar.push('afd');
				opdatvar.push(ret.afd);
				opdatvar.push('x');
				opdatvar.push(ret.x);
				opdatvar.push('y');
				opdatvar.push(ret.y);
				opdatvar.push('kommentar');
				opdatvar.push(ret.kommentar);
				opdatvar.push('vildsvinekommentar');
				opdatvar.push(ret.vildsvinekommentar);
				opdaterRaekke(opdatvar);
			}
		},
		failure: ajaxFailed
	});

}

function gps_error(err) {
	// afslut trykket, skriv fejlmeddelelse
	Ext.getCmp('btn_maal').setText('Ny GPS måling');
	Ext.getCmp('tx_fejl').setValue(err);

}
function gps_success(position) {
	Ext.getCmp('tx_fejl').setValue('');
	Ext.getCmp('btn_maal').setText('Ny GPS måling');
	// spørg på serveren om transformation
	Ext.Ajax.request({
		url: 'Webservices/svin_login.asmx/latlonUTM',
		method: "POST",
		params: JSON.stringify({
			lat: position.coords.latitude,
			lon: position.coords.longitude
		}),
		success: function (response, opts) {
			var ret = Ext.JSON.decode(response.responseText).d;
			mapPanel.clearLayers();
			mapPanel.showPoint(ret.x, ret.y);
			Ext.getCmp('tx_gx').setValue(ret.x);
			Ext.getCmp('tx_gy').setValue(ret.y);
			Ext.getCmp('tx_gz').setValue(position.coords.accuracy);
			var gps_punkt = [0, 0, 0, 0];
			gps_punkt[0] = Number(ret.x - 100);
			gps_punkt[1] = Number(ret.y - 100);
			gps_punkt[2] = Number(Number(ret.x) + 100);
			gps_punkt[3] = Number(Number(ret.y) + 100);
			mapPanel.zoomTo(gps_punkt);
		},
		failure: function (response, opts) {
			ajaxFailed(response, opts);
		},
		headers: { 'Content-Type': 'application/json' }
	});
}
function gps_maal() {
	navigator.geolocation.getCurrentPosition(gps_success, gps_error);
	Ext.getCmp('btn_maal').setText('Arbejder på at få en GPS måling ...');
}

