﻿
function visLogin()
{
    toemFormular('loginpanel', 'login');

	var loginpanel = Ext.create('Ext.panel.Panel', {
		title: 'Log ind...',
		layout: 'anchor',
		bodyPadding: 10,
		autoWidth: true,
        autoHeight: true,
		listeners: {
			specialkey: function (field, e) {
				if (e.getKey() == e.ENTER) {
					udfoer_login();
				}
			}
		},
        items: [{
            xtype: 'form',
            autoEl: {
                tag: 'form',
                method: 'POST',
                action: 'blank.html',
                target: 'submitTarget'
            },

            bodyPadding: 10,
            items: [{
                xtype: 'textfield',
                name: 'username',
                id: 'login-username',
                fieldLabel: 'Email-adresse',
                allowBlank: false,
                'inputAttrTpl': ['autocomplete="on"'],
                anchor: '0',
                flex: 1,
                tabIndex: 1,
            }, {
                xtype: 'textfield',
                inputType: 'password',
                name: 'password',
                id: 'login-password',
                fieldLabel: 'Adgangskode',
                allowBlank: false,
                'inputAttrTpl': ['autocomplete="on"'],
                    anchor: '0',
                    flex: 1,
                    tabIndex: 2,
                    listeners:
                        {
                            specialkey: function (field, e)
                            {
                                if (e.getKey() == e.ENTER)
                                {
                                    udfoer_login();
                                }
                            }
                        }
            }, {
                xtype: 'component',
                html: '<iframe id="submitTarget" name="submitTarget" style="display:none"></iframe>'
            }, {
                xtype: 'component',
                html: '<input type="submit" id="submitButton" style="display:none">'
            }],

            buttonAlign: 'center',

            buttons: [{
                itemId: 'loginButton',
                type: 'submit',
                text: 'Login',
                formBind: true,
                tabIndex: 3,
                handler: function ()
                {
                    Ext.getElementById('submitButton').click();
                    udfoer_login();
                }
            //}, {
            //    itemId: 'resetButton',
            //    text: 'Reset',
            //    handler: function (btn)
            //    {
            //        btn.up('form').getForm().reset();
            //    }
            }]
        //}]





//        items: [{
//		    xtype: 'textfield',
//		    fieldLabel: 'Email-adresse',
//			id: 'login-username',
//			anchor: '0',
//			flex: 1,
//			tabIndex: 1,
//		}, {
//			xtype: 'textfield',
//			fieldLabel: 'Adgangskode',
//			id: 'login-password',
//			anchor: '0',
//			flex: 1,
//			tabIndex: 2,
//			inputType: 'password',
//			allowBlank: false,
//			listeners:
//			{
//				specialkey: function (field, e) {
//					if (e.getKey() == e.ENTER) {
////						Ext.getCmp('btn-ok').fireEvent('click');
//						udfoer_login();
//					}
//				}
//			}
//		}],
//		buttonAlign: 'center',
		//buttons: [{
		////	text: 'Annullér',
		////	tabIndex: 6,
		////	handler: function () {
		////		//open('', '_self');
		////	}
		////}, {
		//    text: 'Login',
		//    id: 'btn-ok',
		//    tabIndex: 4,
		//    handler: udfoer_login
    	}],
		renderTo: 'vestdiv'
	});

	var mailpanel = Ext.create('Ext.panel.Panel', {
		title: 'Har du glemt din adgangskode?',
		layout: 'anchor',
		margin: '10 0 0 0',
		bodyPadding: 10,
		autoWidth: true,
		autoHeight: true,
//		height: 200,
		items: [{
			xtype: 'displayfield',
			hideLabel: true,
			value: 'Hvis du har glemt din adgangskode, kan du få den tilsendt ved at skrive din email-adresse i feltet ovenfor og trykke på knappen herunder. Det virker selvfølgelig kun, hvis du er oprettet som bruger af systemet.'
		}],
		buttonAlign: 'center',
		buttons: [{
			text: 'Send adgangskode i email',
			tabIndex: 4,
			handler: send_password
		}],
		renderTo: 'vestdiv'
	});
    Ext.getCmp('login-username').focus();
}

function udfoer_login(store) {
	var email = Ext.getCmp('login-username').getValue();
	var adgang = Ext.getCmp('login-password').getValue();
    //var email = Ext.getCmp('username').getValue();
    //var adgang = Ext.getCmp('password').getValue();

	
		if (adgang === '' || email === '') {
		fejlbox('Du skal angive din email-adresse og din adgangskode for kunne logge ind');
		return;
	}
	
	var param = {
		email: email,
		kendeord: adgang
	};
	var logindata = Ext.JSON.encode(param);


	Ext.Ajax.request(
	{
		url: 'Webservices/svin_login.asmx/Login',
		params: logindata,
		headers: {'Content-Type': 'application/json;charset=utf-8'},
		success: function (response, opts) {
			toemFormular('');
			laesBrugerrettigheder(response);
		},
		failure: ajaxFailed
	});
}

function send_password() {
	var email = Ext.getCmp('login-username').rawValue;
//    var email = Ext.getCmp('username').rawValue;

	if (email == null || email == '' || email.indexOf('@') < 0) {
		fejlbox('Du skal angive din email-adresse for at kunne få tilsendt din adgangskode.');
		return;
	}
	var param = {
		email: email
	};
	var logindata = Ext.JSON.encode(param);


	Ext.Ajax.request(
	{
		url: 'Webservices/svin_login.asmx/send_password',
		params: logindata,
		headers: {'Content-Type': 'application/json;charset=utf-8'},
		success: function (response, opts) {
				okbesked('Din adgangskode er nu sendt til den angivne email-adresse.', tomFunktion);
		},
		failure: ajaxFailed
	});
}
