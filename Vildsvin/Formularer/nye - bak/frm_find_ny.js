﻿function visFindFugl() {
	//  showMap();

	toemFormular('FindFugl_panel', 'visFindFugl');



	//   mapPanel.config.layers.data.afugle.setVisibility(true);


	Ext.define('mFugle', {
		extend: 'Ext.data.Model',
		fields: [
		'indberetning',
		//'indberetning_afd',
		'an_navn',
		'an_telefon',
		'an_vejnavn',
		'an_husnr',
		'an_postnr',
		'an_by',
		'an_tid',
		'set_tid',
		'setx',
		'sety',
		'setpostnr',
		'setbeskriv',
		'indberetning_status',
		'setvejnavn',
		'sethusnr',
		'setby',
		//'afhentningsadresse',
		//'afh_type',
		'fv_region',
		'fundsted',
		'registrator',
		'registrator_firma',
		'registrator_ret',
		'registrator_ret_firma',
		'rettet_tid',
		'visitator',
		'visitator_firma',
		'visiteret_tid',
		'reserveret_af_id',
		'billede',
		'indberetning_skjul',

		'fugleid',
		'fugl_afd',
		'journalnr',
		'fugl_status',
		'art',
		'art_praecis',
		'resultat_tekst',
		'fugl_skjul',
		'x',
		'y',
		'art_bem',
		'pcr_test_resultat',
		'virus_isolation',
		'virus_pathotype',
		'h',
		'n',
		'tid_indsamlet',
		'tid_lab',
		'tid_slut',
		'ring'

		],
		idProperty: 'fugleid'
	});

	storeFugle = Ext.create('Ext.data.JsonStore', {
		storeId: 'st_Fugle ',
		//pageSize: 10,
		model: 'mFugle',
		remoteSort: true,
		autoLoad: false,
		proxy: {
			type: 'simpel',
			url: 'Webservices/svin_login.asmx/findFugl'
		},
		sorters: [{
			property: 'fugleid',
			direction: 'ASC'
		}],
		listeners: {
			load: function (store) {
				//console.log('load kaldt');
				// sæt de aktuelle filtre på Excel-udtræk
				Ext.get('btn_excel').down('a').dom.href = 'VisFil.ashx?type=xl_find&id=' + brugerid;

                if (store.getCount() > 0)
                    findfuglegrid.getSelectionModel().select(0);
                else // tøm felterne
                {
                    var fi = store.model.getFields();
                    for (var i = 0; i < fi.length; i++)
                    { 
                        try {
                            ctl = Ext.getCmp(fi[i].name);
                            if (fi[i].name === 'billede') {
                                ctl.setSrc('');
                                var visbil = false;
                                ctl.setVisible(visbil);
                            }
                            else
                                ctl.setValue('');
                        } catch (e) {
                            console.log(fi[i].name + ' findes ikke');
                        }
                    }
                }
			}
		}
	});
	storeAkt = storeFugle;

	var findfuglegrid = Ext.create('Ext.grid.Panel', {
		layout: 'fit',
		id: 'findfuglegrid',
		title: 'Fundne fugle',
		autoHeight: true,
		autoWidth: true,
		style: 'margin-top: 10px;',
		//header: false,
		store: storeFugle,
		disableSelection: false,
		viewConfig: {
			id: 'gv',
			trackOver: false,
			stripeRows: false
		},
		// grid columns
		columns: {
			items: [{
				//id: 'indberetning',
				text: "Indberetning",
				tooltip: "Indberetning",
				dataIndex: 'indberetning',
				width: frm_find.findfuglegrid.indberetning.width,
				hidden: frm_find.findfuglegrid.indberetning.hidden,
				//flex: 1,
			}, {
				text: "FugleID",
				tooltip: "FugleID",
				dataIndex: 'fugleid',
				width: frm_find.findfuglegrid.fugleid.width,
				hidden: frm_find.findfuglegrid.fugleid.hidden,
			}, {
				text: "Journalnr",
				tooltip: "Journalnr",
				dataIndex: 'journalnr',
				width: frm_find.findfuglegrid.journalnr.width,
				hidden: frm_find.findfuglegrid.journalnr.hidden,
				flex: 1
			}]
		},
	    // gridkol_slut
		selModel: Ext.create('Ext.selection.RowModel', {
			listeners: {
				select: {
					fn: function (selModel, rec, index) {
						aktRec = rec;
						rec.fields.each(function (f) {
							try {
								ctl= Ext.getCmp(f.name);
								if (f.name === 'billede')
								{
									ctl.setSrc(aktRec.data[f.name]);
									var visbil = false;
									if (aktRec.data[f.name] !== null && aktRec.data[f.name] !== '')
										visbil = true;
									ctl.setVisible(visbil);
								}
								else
									ctl.setValue(aktRec.data[f.name]);
							} catch (e) {
								console.log(f.name + ' findes ikke');
							}
						});

					}
				}
			}
		}),
		//bbar: createZoombar()
		bbar: ['->', ExcelButtonCode()]
});






	var FindFugl_panel = Ext.create('Ext.panel.Panel', {
		title: 'Find fugl og vis alle detaljer',
		id: 'FindFugl_panel',
		layout: 'anchor',
		anchor: '-20',
		autoScroll: true,
		bodyPadding: '10 10 10 10',
		autoWidth: true,
		autoHeight: false,
		renderTo: 'vestdiv',
		items: [{
			xtype: 'fieldset', // her er kun de redigerbare felter
			title: 'Skriv et kriterium og tryk Retur',
			id: 'find_fugl',
			bodyPadding: 10,
			autoWidth: true,
			autoHeight: true,
			style: 'margin-top: 10px',
			flex: 1,
			items: [{
				xtype: 'fccolumn',
				items: [{
					xtype: 'textfield',
					hideLabel: false,
					fieldLabel: 'Indberetning',
					id: 'find_indberetning',
					margin: '0 3 0 0', // venstre
					columnWidth: 0.5,
					enableKeyEvents: true,
					listeners: {
						specialkey: function (field, e) {
							// e.HOME, e.END, e.PAGE_UP, e.PAGE_DOWN,
							// e.TAB, e.ESC, arrow keys: e.LEFT, e.RIGHT, e.UP, e.DOWN
							if (e.getKey() === e.ENTER) {
								findFugl(field.value, 'indberetning');
							}
						}
					}
				}, {
					fieldLabel: 'Fugleid',
					xtype: 'textfield',
					id: 'find_fugleid',
					//margin: '0 0 0 3', // højre
					columnWidth: 0.5,
					enableKeyEvents: true,
					listeners: {
						specialkey: function (field, e) {
							if (e.getKey() === e.ENTER) {
								findFugl(field.value, 'fugleid');
							}
						}
					}
				}]
			}, {
				xtype: 'fccolumn',
				items: [{
					fieldLabel: 'Journalnr',
					xtype: 'textfield',
					id: 'find_journalnr',
					margin: '0 0 0 3', // højre
					columnWidth: 0.5,
					enableKeyEvents: true,
					listeners: {
						specialkey: function (field, e) {
							if (e.getKey() === e.ENTER) {
								findFugl(field.value, 'journalnr');
							}
						}
					}
				}]
			}, findfuglegrid]
		}, {
			xtype: 'fieldset',
			title: 'Indberetning og fugl',
			margins: 6,
			autoWidth: true,
			autoHeight: true,
			flex: 1,
			items: [{
				xtype: 'fccolumn_ro',
				items: [{
					fieldLabel: 'Indberetning',
					//margin: '0 3 0 0', // venstre
					id: 'indberetning',
					columnWidth: 0.2
				}, {
					fieldLabel: 'Indberetning status',
					//margin: '0 3 0 0', // venstre
					id: 'indberetning_status',
					columnWidth: 0.3
				}, {
					fieldLabel: 'Indberetning skjult',
					//margin: '0 3 0 0', // venstre
					id: 'indberetning_skjul',
					columnWidth: 0.3
				}, {
					fieldLabel: 'Veterinærenhed',
					id: 'fv_region',
					//margin: '0 3 0 0', // venstre
					columnWidth: 0.2
				}]
			}, {
				xtype: 'fccolumn_ro',
				items: [{
					fieldLabel: 'Fugl',
					id: 'fugleid',
					//margin: '0 3 0 0', // højre
					columnWidth: 0.2
				}, {
					xtype: 'textfield',
					hideLabel: false,
					fieldLabel: 'Fugl status',
					id: 'fugl_status',
					margin: '0 3 0 0', // venstre
					columnWidth: 0.3
				}, {
					fieldLabel: 'Fugl skjult',
					id: 'fugl_skjul',
					//margin: '0 3 0 0', // højre
					columnWidth: 0.3
				}, {
					fieldLabel: 'Resultat',
					id: 'resultat_tekst',
					//margin: '0 3 0 0', // venstre
					columnWidth: 0.2
				}]
			}]
		}, {
			xtype: 'fieldset',
			title: 'Oplysninger om anmelder',
			margins: 6,
			autoWidth: true,
			autoHeight: true,
			flex: 1,
			items: [{
				xtype: 'fccolumn_ro',
				items: [{
					fieldLabel: 'Navn',
					id: 'an_navn',
					//margin: '0 3 0 0', // venstre
					columnWidth: 0.75
				}, {
					fieldLabel: 'Observeret dato',
					xtype: 'datefield',
					format: 'd-m-Y',
					altFormats: 'd/m/Y H:i:s',
					id: 'set_tid',
					flex: 1,
					//margin: '0 0 0 0', // højre
					columnWidth: 0.25
				}]
			}, {
				xtype: 'fccolumn_ro',
				items: [{
					fieldLabel: 'Vejnavn',
					//margin: '0 3 0 0', // venstre
					id: 'an_vejnavn',
					columnWidth: 0.35
				}, {
					fieldLabel: 'Husnr',
					id: 'an_husnr',
					//margin: '0 3 0 0', // højre
					columnWidth: 0.15
				}, {
					fieldLabel: 'Postnr',
					id: 'an_postnr',
					//margin: '0 3 0 0', // venstre
					columnWidth: 0.15
				}, {
					fieldLabel: 'Postdistrikt',
					id: 'an_by',
					flex: 1,
					//margin: '0 0 0 0', // højre
					columnWidth: 0.35
				}]
			}, {
				xtype: 'fccolumn_ro',
				items: [{
					fieldLabel: 'Telefon',
					id: 'an_telefon',
					//margin: '0 3 0 0', // venstre
					columnWidth: 0.2
				}]
			}]
		}, {
				xtype: 'fieldset',
				title: 'Fotografi',
				id: 'lb_billede',
				bodyPadding: 10,
				autoWidth: true,
				autoHeight: true,
				style: 'margin-top: 10px',
				flex: 1,
				items: [{
					xtype: 'image',
					id: 'billede',
					src: '',
					hidden: false,
					shrinkWrap: true,
					width: '100%'
				}]
			}, {
				xtype: 'fieldset',
				title: 'Oplysninger om fundstedet',
				autoHeight: true,
				margins: '6',
				//anchor: '0',
				//flex: 1,
				items: [{
					xtype: 'fccolumn_ro',
					items: [{
						fieldLabel: 'Vejnavn',
						id: 'setvejnavn',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.35
					}, {
						fieldLabel: 'Husnr',
						id: 'sethusnr',
						//margin: '0 3 0 0', // højre
						columnWidth: 0.15
					}, {
						fieldLabel: 'Postnr',
						id: 'setpostnr',
						//margin: '0 3 0 0', // venstre
						columnWidth: 0.15
					}, {
						fieldLabel: 'Postdistrikt',
						id: 'setby',
						margin: '0 0 0 3', // højre
						columnWidth: 0.35
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						fieldLabel: 'Øst',
						id: 'setx',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.5
					}, {
						fieldLabel: 'Nord',
						id: 'sety',
						margin: '0 0 0 3', // højre
						columnWidth: 0.5
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						xtype: 'textarea',
						fieldLabel: 'Beskrivelse af fundstedet - hvis det ikke blot er en adresse',
						id: 'fundsted',
						margin: '0', // højre
						columnWidth: 1,
						grow: true,
						growMin: 30,
						growMax: 300
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						xtype: 'textarea',
						fieldLabel: 'Beskrivelse af fundet - hvornår er fuglen død, er det nær tam-fjerkræ, beskyttelseszone, vådområde?',
						id: 'setbeskriv',
						margin: '0', // højre
						columnWidth: 1,
						grow: true,
						growMin: 30,
						growMax: 300
					}]
				}]
			//}, {
			//	xtype: 'fieldset',
			//	title: 'Oplysninger om afhentningsadresse',
			//	autoHeight: true,
			//	margins: '6',
			//	items: [{
			//		xtype: 'fccolumn_ro',
			//		items: [{
			//			xtype: 'textarea',
			//			fieldLabel: 'Afhentningsadresse',
			//			id: 'afhentningsadresse',
			//			hidden: true,
			//			margin: '0',
			//			columnWidth: 1,
			//			//height: 20,
			//			grow: true,
			//			growMin: 30,
			//			growMax: 300
			//		}]
			//	}]
			}, {
				xtype: 'fieldset',
				title: 'Oplysninger om registreringen',
				autoHeight: true,
				margins: '6',
				//anchor: '0',
				//flex: 1,
				items: [{
					xtype: 'fccolumn_ro',
					items: [{
						fieldLabel: 'Anmeldt tidspunkt',
						id: 'an_tid',
						xtype: 'datefield',
						format: 'd-m-Y H:i:s',
						altFormats: 'd/m/Y H:i:s',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.2
					}, {
						fieldLabel: 'Registrator',
						id: 'registrator',
						//margin: '0 3 0 0', // venstre
						columnWidth: 0.4
					}, {
						fieldLabel: 'Afdeling',
						id: 'registrator_firma',
						margin: '0 0 0 3', // højre
						columnWidth: 0.4
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						fieldLabel: 'Sidst rettet',
						id: 'rettet_tid',
						xtype: 'datefield',
						format: 'd-m-Y H:i:s',
						altFormats: 'd/m/Y H:i:s',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.2
					}, {
						fieldLabel: 'Registrator',
						id: 'registrator_ret',
						//margin: '0 3 0 0', // venstre
						columnWidth: 0.4
					}, {
						fieldLabel: 'Afdeling',
						id: 'registrator_ret_firma',
						margin: '0 0 0 3', // højre
						columnWidth: 0.4
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						fieldLabel: 'Visiteret dato',
						id: 'visiteret_tid',
						xtype: 'datefield',
						format: 'd-m-Y',
						altFormats: 'd/m/Y H:i:s',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.2
					}, {
						fieldLabel: 'Visitator',
						id: 'visitator',
						//margin: '0 3 0 0', // venstre
						columnWidth: 0.4
					}, {
						fieldLabel: 'Afdeling',
						id: 'visitator_firma',
						margin: '0 0 0 3', // højre
						columnWidth: 0.4
					}]
				}]
			}, {
				xtype: 'fieldset',
				title: 'Oplysninger om indsamling og analyse',
				id: 'akt_fugl',
				bodyPadding: 10,
				autoWidth: true,
				autoHeight: true,
				style: 'margin-top: 10px',
				flex: 1,
				items: [{
					xtype: 'fccolumn_ro',
					items: [{
						hideLabel: false,
						fieldLabel: 'Indsamlet',
						id: 'tid_indsamlet',
						xtype: 'datefield',
						format: 'd-m-Y',
						altFormats: 'd/m/Y H:i:s',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.2
					}, {
						hideLabel: false,
						fieldLabel: 'Opsamler',
						id: 'reserveret_af_id',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.4
					}, {
						fieldLabel: 'Indsamlet af',
						id: 'fugl_afd',
						//margin: '0 3 0 0', // venstre
						columnWidth: 0.4
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						hideLabel: false,
						fieldLabel: 'Indsamlet X',
						id: 'x',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.2
					}, {
						hideLabel: false,
						fieldLabel: 'Indsamlet Y',
						id: 'y',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.2
					}, {
						fieldLabel: 'Modtaget dato',
						id: 'tid_lab',
						xtype: 'datefield',
						format: 'd-m-Y',
						altFormats: 'd/m/Y H:i:s',
						//margin: '0 0 0 3', // højre
						columnWidth: 0.3
					}, {
						fieldLabel: 'Afsluttet dato',
						id: 'tid_slut',
						xtype: 'datefield',
						format: 'd-m-Y',
						altFormats: 'd/m/Y H:i:s',
						margin: '0 0 0 3', // højre
						columnWidth: 0.3
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Lab journalnr',
						id: 'journalnr',
						columnWidth: 0.4,
						margin: '0 3 0 0' // venstre
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Label',
						id: 'label',
						columnWidth: 0.3
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Ringmærke',
						id: 'ring',
						margin: '0 0 0 3', // højre
						columnWidth: 0.3
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Art angivet af indsamler',
						id: 'art',
						columnWidth: 0.5
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Art, EU-liste',
						id: 'art_praecis',
						columnWidth: 0.5
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						xtype: 'textarea',
						fieldLabel: 'Bemærkninger om fugleart (udfyld hvis artsangivelsen ovenfor er for præcis)',
						id: 'art_bem',
						margin: '0',
						columnWidth: 1,
						//height: 20,
						grow: true,
						growMin: 30,
						growMax: 300
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'PCR resultat',
						id: 'pcr_test_resultat',
						columnWidth: 0.5
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Virus isolation',
						id: 'virus_isolation',
						columnWidth: 0.5
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'Virus pathotype',
						id: 'virus_pathotype',
						columnWidth: 0.5
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'H subtype',
						id: 'h',
						columnWidth: 0.25
					}, {
						xtype: 'textfield',
						hideLabel: false,
						fieldLabel: 'N subtype',
						id: 'n',
						columnWidth: 0.25
					}]

			}]
		}]
	});
	retKolonner(findfuglegrid, "findfuglegrid");

	function findFugl(value, felt) {
		storeFugle.proxy.extraParams = { value: value, felt: felt };
		storeFugle.load();
		//Ext.Ajax.request({
		//	url: 'Webservices/svin_login.asmx/findFugl',
		//	method: "POST",
		//	params: JSON.stringify({
		//		value: value, felt: felt
		//	}),
		//	success: function () {
		//		console.log("ok");
		//	},
		//	failure: function (response, opts) {
		//		console.log("failed");
		//	},
		//	headers: {
		//		'Content-Type': 'application/json'
		//	}
		//});

	}
}

