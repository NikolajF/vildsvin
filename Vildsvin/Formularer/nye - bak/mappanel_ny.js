﻿/*
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 * 
 * Published under the BSD license.
 * See https://github.com/geoext/geoext2/blob/master/license.txt for the full
 * text of the license.
 */

//Ext.require([
//	'Ext.container.Viewport',
//	'Ext.window.MessageBox',
//	'GeoExt.panel.Map',
//	'GeoExt.Action'
//]);

function showMap() {
	if (document.getElementById('centerDiv').innerHTML != '') {
		// vis, hvis det er skjult ???
	    mapPanel.show();
	    //mapPanel.el.dom.style.display = '';
	    //mapPanel.setStyle('display', ''); //setVisibilityMode(Ext.Element.DISPLAY);
	    return;
	}

	Ext.get('centerDiv').createChild({
	    tag: 'div',
	    id: 'copyrighttext',
	    style: 'font-size:small; z-index: 1006; position: absolute; bottom: 3px; right: 5px;'
	});

	var layers = createLayers();
	var map = createMap();
	addLayers(map, layers);
	var controls = createControls(layers);
	// Add the controls to the map.
	addControls(map, controls);
	// Create the tools.
	var tools = createTools(map, controls, layers);


    //var toolbarItems = createActions(map, layers);

	mapPanel = Ext.create('GeoExt.panel.Map', {
		extent: '437000, 6043000, 902000, 6405000',
		width: centerPanel.el.dom.offsetWidth,
		height: centerPanel.el.dom.offsetHeight,
		config : {
				layers: layers,
						controls: controls,
						tools: tools
		},
		map: map,
				dockedItems: [{
					xtype: 'toolbar',
					dock: 'top',
					items: makeToolbar(tools)
				}],
		layout: 'fit',
		renderTo: 'centerDiv',

		// funktioner
		zoomTo: function (bbox) {
			if (bbox && bbox.length === 4) {
				this.map.zoomToExtent(new OpenLayers.Bounds(bbox[0], bbox[1], bbox[2], bbox[3]), false);
			}
		},
		zoomToBounds: function (bounds) {
			this.map.zoomToExtent(bounds, false);
		},
		clearLayers: function (lag) {
			if (lag == undefined) {
				this.config.layers.selections.SelectedPoint.destroyFeatures();
				this.config.layers.selections.coordinateLayer.destroyFeatures();
				this.config.layers.selections.awsLayer.destroyFeatures();
			}
			else {
				this.config.layers.selections[lag].destroyFeatures();
			}
		},
		redrawLayers: function (layer) {
			if (layer) {
				this.config.layers.data[layer].redraw(true);
			} else {
				for (layer in this.config.layers.data) {
					this.config.layers.data[layer].redraw(true);
				}
			}
		},
		addSelectedPoint: function (x, y) {
			var vector = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(x, y));
			this.config.layers.selections.SelectedPoint.addFeatures(vector);
		},
		zoomToAddressLevel: function (x, y, level) {
			// slet tidligere søg
			var point = new OpenLayers.LonLat(x, y);
			this.map.setCenter(point, level, false, true);
		},
		enableAreaSelector: function (callback, scope) {
			// Enable the area selector.
			this.config.tools.areaSelector.setDisabled(false);
			// Setup the area control.
			var selector = this.config.controls.areaSelector;
			var map = this.map;
			selector.onClick = function (event) {
				var value = {};
				var lonlat;
				if (event.x && event.y) {
					lonlat = map.getLonLatFromPixel(event);
					//				value = { lon: lonlat.lon, lat: lonlat.lat }
					value.left = lonlat.lon;
					value.bottom = lonlat.lat;
					value.right = lonlat.lon;
					value.top = lonlat.lat;
				} else {
					lonlat = map.getLonLatFromPixel({ x: event.left, y: event.bottom });
					value.left = lonlat.lon;
					value.bottom = lonlat.lat;
					lonlat = map.getLonLatFromPixel({ x: event.right, y: event.top });
					value.right = lonlat.lon;
					value.top = lonlat.lat;
				}
				// Return the value.
				callback.call(scope, value);
			};
			// Toggle on if not pressed.
			if (!this.config.tools.areaSelector.pressed) {
				this.config.tools.areaSelector.toggle();
			}
		},
		/**
		* Disable and turn off the area selector.
		*/
		disableAreaSelector: function () {
			// De-toggle if pressed.
			if (this.config.tools.areaSelector.pressed) {
				this.config.tools.areaSelector.toggle();
			}
			// Disable the control.
			this.config.tools.areaSelector.setDisabled(true);
		},
		showPoint: function (x, y, label) {
			// Create the point geometry.
			vector = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(x, y));
			if (label != undefined) {
				if (label == 'gl') {
					vector.style = {
						pointRadius: 8,
						graphicName: 'cross',
						fillColor: '#0000FF',
						fillOpacity: 1,
						strokeWidth: 1,
						strokeOpacity: 0,
						strokeColor: '#0000FF',
						strokeDashstyle: 'solid',
						externalGraphic: '',
						graphicWidth: 1,
						graphicHeight: 1
					};
				}
				else {
					vector.style = {
						//graphicName: 'square',
						strokeColor: "#FFFFFF",
						strokeOpacity: 1,
						strokeWidth: 1,
						fillColor: "#FFFFFF",
						fillOpacity: 1,
						pointRadius: 12,
						label: label, //label from attribute name
						labelXOffset: 0,
						labelYOffset: 0,
						fontColor: "black",
						fontSize: "12px",
						fontFamily: "Arial",
						fontWeight: "bold",
						labelAlign: "cm"
					};
				}
			}
			// Remove any previously added geometries.
			//this.clearLayers();
			// Add the vector to the selection layer.
			this.config.layers.selections.coordinateLayer.addFeatures(vector);
		}

	});

}
// oprettefunktioner
function createMap() {
	return new OpenLayers.Map('', {
	    fallThrough: true,
	    projection: 'EPSG:25832',
		units: 'm',
		maxExtent: new OpenLayers.Bounds(100000, 6000000, 1000000, 6500000),
		//minResolution: 0.1, //0.125,
		//maxResolution: 1000,
		minResolution: 0.05,
		maxResolution: 1638.40,
		restrictedExtent: new OpenLayers.Bounds(100000, 6000000, 1000000, 6500000),
		controls: [
			new OpenLayers.Control.PanZoomBar({ zoomWorldIcon: 'true' }),
			new OpenLayers.Control.ScaleLine({
				maxWidth: 500,
				topInUnits: 'm',
				topOutUnits: 'km',
				bottomInUnits: '',
				bottomOutUnits: '',
				geodesic: false
			}),
		]
	});
}
function createLayers() {
	var layers = {
		background: {
			kmsmap: new OpenLayers.Layer.WMS(
			'Kort',
			['https://a.kortforsyningen.kms.dk/service', 'https://b.kortforsyningen.kms.dk/service', 'https://c.kortforsyningen.kms.dk/service', 'https://d.kortforsyningen.kms.dk/service', 'https://e.kortforsyningen.kms.dk/service', 'https://f.kortforsyningen.kms.dk/service', 'https://g.kortforsyningen.kms.dk/service', 'https://h.kortforsyningen.kms.dk/service', 'https://i.kortforsyningen.kms.dk/service', 'https://j.kortforsyningen.kms.dk/service', 'https://k.kortforsyningen.kms.dk/service', 'https://l.kortforsyningen.kms.dk/service', 'https://m.kortforsyningen.kms.dk/service'],
					//'/kms.ashx',
			{
				ticket: kmsticket,
				service: 'WMS',
				format: 'image/jpeg',
				srs: 'EPSG:25832',
				request: 'GetMap',
				version: '1.1.1',
				servicename: 'topo_skaermkort',
				layers: 'dtk_skaermkort',
				tiled: true,
				tilesOrigin: '100000,6000000'
			}, 
			{
				displayInLayerSwitcher: true,
				buffer: 1,
				isBaseLayer: true,
				ratio: 0,
				singleTile: false,
				visibility: true,
				transitionEffect: 'resize',
				displayOutsideMaxExtent: true,
				copyright: 'Kortforsyningen.dk'
			}
		),
		grayscale: new OpenLayers.Layer.WMS(
			'Sort/hvidt kort',
			['https://a.kortforsyningen.kms.dk/service', 'https://b.kortforsyningen.kms.dk/service', 'https://c.kortforsyningen.kms.dk/service', 'https://d.kortforsyningen.kms.dk/service', 'https://e.kortforsyningen.kms.dk/service', 'https://f.kortforsyningen.kms.dk/service', 'https://g.kortforsyningen.kms.dk/service', 'https://h.kortforsyningen.kms.dk/service', 'https://i.kortforsyningen.kms.dk/service', 'https://j.kortforsyningen.kms.dk/service', 'https://k.kortforsyningen.kms.dk/service', 'https://l.kortforsyningen.kms.dk/service', 'https://m.kortforsyningen.kms.dk/service'],
			{
				ticket: kmsticket,
				service: 'WMS',
				format: 'image/jpeg',
				srs: 'EPSG:25832',
				request: 'GetMap',
				version: '1.1.1',
				servicename: 'topo_skaermkort',
				layers: 'dtk_skaermkort',
				manipulation: 'greyscale',
				tiled: true,
				tilesOrigin: '100000,6000000'
			},
			{
				displayInLayerSwitcher: true,
				buffer: 1,
				isBaseLayer: true,
				displayOutsideMaxExtent: true,
				transitionEffect: 'resize',
				visibility: true,
				singleTile: false,
				ratio: 0,
				copyright: 'Kortforsyningen.dk'
			}
		),
		ortho: new OpenLayers.Layer.WMS(
			'Foto',
			['https://a.kortforsyningen.kms.dk/service', 'https://b.kortforsyningen.kms.dk/service', 'https://c.kortforsyningen.kms.dk/service', 'https://d.kortforsyningen.kms.dk/service', 'https://e.kortforsyningen.kms.dk/service', 'https://f.kortforsyningen.kms.dk/service', 'https://g.kortforsyningen.kms.dk/service', 'https://h.kortforsyningen.kms.dk/service', 'https://i.kortforsyningen.kms.dk/service', 'https://j.kortforsyningen.kms.dk/service', 'https://k.kortforsyningen.kms.dk/service', 'https://l.kortforsyningen.kms.dk/service', 'https://m.kortforsyningen.kms.dk/service'],
			{
				ticket: kmsticket,
				service: 'WMS',
				layers: 'orto_foraar',
				servicename: 'orto_foraar',
				version: '1.1.1',
				request: 'GetMap',
				srs: 'EPSG:25832',
				format: 'image/jpeg',
				bgcolor: '0xFFFFFF',
				tiled: true,
				tilesOrigin: '100000,6000000'
			},
			{
				isBaseLayer: true,
				buffer: 1,
				displayInLayerSwitcher: true,
				ratio: 0,
				singleTile: false,
				visibility: true,
				transitionEffect: 'resize',
				displayOutsideMaxExtent: true,
				copyright: 'Kortforsyningen.dk'
			}
		),
		osm: new OpenLayers.Layer.WMS(
			'OpenStreetMap',
			'http://ows.terrestris.de/osm/service',
			{
			    //ticket: kmsticket,
			    service: 'WMS',
			    format: 'image/jpeg',
			    srs: 'EPSG:25832',
			    request: 'GetMap',
			    version: '1.1.1',
			    //servicename: 'topo_skaermkort',
			    layers: 'OSM-WMS'
			},
			{
			    displayInLayerSwitcher: true,
			    buffer: 1,
			    isBaseLayer: true,
			    displayOutsideMaxExtent: true,
			    transitionEffect: 'resize',
			    visibility: true,
			    ratio: 1,
			    singleTile: true,
			    copyright: 'OpenStreetMap.org / terrestris.de'
			}
		),
		blank: new OpenLayers.Layer(
			'Intet',
			{
				displayInLayerSwitcher: true,
				isBaseLayer: true,
				visibility: false,
				displayOutsideMaxExtent: false,
				singleTile: false,
				tiled: true,
				tilesOrigin: '100000,6000000',
				buffer: 0,
				ratio: 0,
                copyright: ''
			}
		)}
	};

	layers.data = {
		indberetninger: new OpenLayers.Layer.WMS(
			'Indberetninger',
			'/wms.ashx',
			{
			    transparent: true,
			    format_options: 'ANTIALIAS:NONE',
			    format: 'image/jpeg',
			    srs: 'EPSG:25832',
			    //styles: 'levende', 
			    request: 'GetMap',
			    version: '1.1.1',
			    layers: 'v_levende' 
			},
			{
			    displayInLayerSwitcher: true,
			    isBaseLayer: false,
			    transitionEffect: 'resize',
			    ratio: 1,
			    singleTile: true,
			    visibility: false,
                forceRule: true
			}
		),
		vildsvin_doede: new OpenLayers.Layer.WMS(
			'Døde',
			'/wms.ashx',
			{
			    transparent: true,
			    format_options: 'ANTIALIAS:NONE',
			    format: 'image/jpeg',
			    srs: 'EPSG:25832',
			    //styles: 'indberetninger', 
			    request: 'GetMap',
			    version: '1.1.1',
                layers: 'v_vildsvin_doede'
			},
			{
			    displayInLayerSwitcher: true,
			    isBaseLayer: false,
			    transitionEffect: 'resize',
			    ratio: 1,
			    singleTile: true,
			    visibility: false
			}
		),
		vildsvin_proevesvar: new OpenLayers.Layer.WMS(
			'Prøvesvar',
			'/wms.ashx',
			{
				transparent: true,
				format_options: 'ANTIALIAS:NONE',
				format: 'image/jpeg',
				srs: 'EPSG:25832',
				//styles: 'indberetninger', 
				request: 'GetMap',
				version: '1.1.1',
                layers: 'v_vildsvin_proevesvar'
			},
			{
				displayInLayerSwitcher: true,
				isBaseLayer: false,
				transitionEffect: 'resize',
				ratio: 1,
				singleTile: true,
				visibility: false
			}
		),
        vildsvin_off: new OpenLayers.Layer.WMS(
			'LAB analyser',
			'/wms.ashx',
			{
				transparent: true,
				format_options: 'ANTIALIAS:NONE',
				format: 'image/jpeg',
				srs: 'EPSG:25832',
				//styles: 'indberetninger', 
				request: 'GetMap',
				version: '1.1.1',
                layers: 'v_vildsvin_off',
			},
			{
				displayInLayerSwitcher: true,
				isBaseLayer: false,
				transitionEffect: 'resize',
				ratio: 1,
				singleTile: true,
				visibility: false
			}
		)	
	};

	layers.info = {};
	layers.selections = {};
	// Add coordinate layer.
	layers.selections.coordinateLayer = new OpenLayers.Layer.Vector(
		"Koordinater", {
			styleMap: new OpenLayers.StyleMap({
				'default': new OpenLayers.Style(
				null,
				{
					rules: [
						new OpenLayers.Rule(
						{
							symbolizer: {
								"Point": {
									pointRadius: 12,
									graphicName: 'cross',
									fillColor: '#0000FF',
									fillOpacity: 1,
									strokeWidth: 1,
									strokeOpacity: 0,
									strokeColor: '#0000FF',
									strokeDashstyle: 'solid',
									externalGraphic: '',
									graphicWidth: 1,
									graphicHeight: 1
								}
							}
						})
					]
				})
			})
			}, 
		{
			displayInLayerSwitcher: false,
			isBaseLayer: false
		}
	);
	//Add point info layer.
	layers.selections.SelectedPoint = new OpenLayers.Layer.Vector(
		"Markering", {
			styleMap: new OpenLayers.StyleMap({
				'default': new OpenLayers.Style(
				null,
				{
					rules: [
						new OpenLayers.Rule(
						{
							symbolizer: {
								"Point": {
									pointRadius: 16,
									graphicName: 'circle',
									fillColor: '#FF00FF',
									fillOpacity: 0,
									strokeWidth: 3,
									strokeOpacity: 1,
									strokeColor: '#FF0000',
									strokeDashstyle: 'solid',
									externalGraphic: '',
									graphicWidth: 1,
									graphicHeight: 1
								}
							}
								})
							]
						})
					})
				}, 
		{
			displayInLayerSwitcher: false,
			isBaseLayer: false
		}
	);
	// Add AWS layer.
	layers.selections.awsLayer = new OpenLayers.Layer.Vector(
	"Adressesøgning", {
		styleMap: new OpenLayers.StyleMap({
			'default': new OpenLayers.Style(
			null,
			{
				rules: [
					new OpenLayers.Rule(
					{
						symbolizer: {
							"Point": {
								pointRadius: 5,
								graphicName: 'circle',
								fillColor: '#0000FF',
								fillOpacity: 1,
								strokeWidth: 1,
								strokeOpacity: 1,
								strokeColor: '#0000FF',
								strokeDashstyle: 'solid',
								externalGraphic: '',
								graphicWidth: 1,
								graphicHeight: 1
							},
							'Polygon': {
								fillColor: '#EF9A00',
								fillOpacity: 0,
								strokeWidth: 2,
								strokeOpacity: 1,
								strokeColor: '#0000FF',
								strokeDashstyle: 'dash'
							}
						}
					})
				]
			})
		})
		}, 
		{
		displayInLayerSwitcher: false,
		isBaseLayer: false,
		visibility: true
		}
	);
	return layers;
}
function addLayers(map, layers){
	for (var layer in layers.background)
	{
		map.addLayer(layers.background[layer]);
	}
	for (layer in layers.data)
	{
		map.addLayer(layers.data[layer]);
	}
	for (layer in layers.udbrud)
	{
		map.addLayer(layers.udbrud[layer]);
	}
	for (layer in layers.info)
	{
		map.addLayer(layers.info[layer]);
	}
	for (layer in layers.selections)
	{
		map.addLayer(layers.selections[layer]);
		layers.selections[layer].addOptions({ 'displayInLayerSwitcher': false });
	}
}

function createControls(layers){
	return {
		zoomIn: new OpenLayers.Control.ZoomBox({ out: false }),
		zoomOut: new OpenLayers.Control.ZoomBox({ out: true }),
		navigation: new OpenLayers.Control.Navigation(),
		OverviewMap: new OpenLayers.Control.OverviewMap(
		{
			minRatio: 32,
			maxRatio: 64,
			minRectSize: 1,
			layers: [layers.background.kmsmap.clone()] ,
			maximized: false,
			size: new OpenLayers.Size(300,200),
			mapOptions: 
			{
				projection: 'EPSG:25832',
				units: 'm',
				maxExtent: new OpenLayers.Bounds(400000,6000000,900000,6420000)
			}
		})
	};
}

function addControls(map, controls) {
	for (control in controls)
	{
		map.addControl(controls[control]);
	}
}

function createTools(map, controls, layers) {
	var tools = {};

	tools.empty = '';
		tools.addressCombo = new GIS34AddressSearchCombo({
			//fieldLabel: ' ',
			valueNotFoundText: 'Den angivne adresse kunne ikke findes',
			listEmptyText: 'Der blev ikke fundet nogen adresser',
			loadingText: 'Begynd indtastning',
			emptyText: 'Søg adresse',
			anchor: '100%',
			filterid: 0,
			width: 200,
			maxrows: 10, //this.maxrows,
			layer: layers.selections.awsLayer
		});
		//tools.empty2 = '';

	tools.spacer = '';

	var items = [];
	var count = -1;
	for (var i in layers.background) {
		var item = items.push({
			itemIndex: count++,
			text: layers.background[i].name,
			xtype: 'menucheckitem'
		});
	}

	tools.BaselayerToggler = Ext.create('Ext.button.Cycle', {
		toggleGroup: 'baseLayerToggler',
		id: 'toolbar_item_BaseLayerToggler_3576',
		group: 'baseLayerToggler',
		tooltip: 'Klik på knappen for at skifte baggrundskort eller åbn menuen og vælg',
		showText: true,
		width: 120,
		prependText: '',
		menu: {
			items: items
			},
		//activeItem: items[4], // blank, det virker bare ikke
		changeHandler: function (btn, item) {
			var newBaseLayer = map.getLayersByName(item.text)[0];
			map.setBaseLayer(newBaseLayer);
		    // sæt den rette copyright
			var c = newBaseLayer.options.copyright;
			if (c != '')
			    c = 'Baggrundskort &#169 ' + c;
			document.getElementById('copyrighttext').innerHTML = c;

		}//,
//		listeners: {
//			render: function (btn) { // Dynamically add items (base layers)
//				btn.setActiveItem(btn.menu.items.items[0]);
//			}
//		}
	});

//	for (var i in layers.background) {
//		var item = tools.BaselayerToggler.menu.add({
//			checkHandler: tools.BaselayerToggler.btn.checkHandler,
//			group: 'layers',
//			itemIndex: tools.BaselayerToggler.btn.itemCount++,
//			scope: btn,
//			text: layers.background[i].name,
//			xtype: 'menucheckitem'
//		});
//	}

	//toolbarItems.push(toolbar_item_BaselayerToggler

	tools.fill = '->';
	tools.legend = Ext.create('Ext.Button', {
		tooltip: 'Signaturforklaring',
		iconCls: 'icon-legend',
		id: 'toolbar_item_Legend',
		enableToggle: true,
		allowDepress: true,
		//pressed: true,
		legendWindow: Ext.create('Ext.Window', {
			title: 'Signaturforklaring',
			id: 'toolbar_legend_window',
			closable: true,
			closeable: true,
			collapsible: true,
			closeAction: 'hide',
			shadow: false,
			width: 225,
			height: 300,
			layout: 'fit',
			style: { right: 10 },
			listeners: {
				hide: function ()
				{
					var buttonLegend = Ext.getCmp('toolbar_item_Legend');
					if (buttonLegend.pressed == true)
						buttonLegend.toggle();
				}
			},
			items: Ext.create('GeoExt.panel.Legend', {
				defaults: {
					labelCls: 'legendlayer',
					style: 'padding:5px'
				},
				bodyStyle: 'padding:5px',
				width: 200,
				filter: function (record) {
					return record.getLayer().isBaseLayer == false; // && record.getLayer().visibility == true
				},
				autoScroll: true
			})
		}),
		listeners: {
			toggle: function (button, pressed)
			{
				if (pressed) {
					button.legendWindow.show();
					button.legendWindow.alignTo(centerPanel, 'tr-tr', [0, 34]);
				} else {
					button.legendWindow.hide();
				}
			},
			scope: this
		}
	});
//	tools.help = new Ext.Button({
//		tooltip: 'Hjælp',
//		iconCls: 'icon-help',
//		id: 'toolbar_item_help',
//		handler: function ()
//		{
//			visHjaelp('#kortet');
//		}
//	});
	return tools;
}

function makeToolbar(tools) {
	var toolbar = new Ext.Toolbar({});
	for (var tool in tools) {
		//toolbar.addButton(tools[tool]);
		toolbar.add(tools[tool]);
	}
	return toolbar;
}



function createActions(map, layers) {
	var ctrl, action, actions = {}, toolbarItems=[];

	// ZoomToMaxExtent control, a "button" control
	action = Ext.create('GeoExt.Action', {
		control: new OpenLayers.Control.ZoomToMaxExtent(),
		map: map,
		text: "max extent",
		tooltip: "zoom to max extent"
	});
	actions["max_extent"] = action;
	toolbarItems.push(Ext.create('Ext.button.Button', action));
	toolbarItems.push("-");

	// Navigation control and DrawFeature controls
	// in the same toggle group
	action = Ext.create('GeoExt.Action', {
		text: "nav",
		control: new OpenLayers.Control.Navigation(),
		map: map,
		// button options
		toggleGroup: "draw",
		allowDepress: false,
		pressed: true,
		tooltip: "navigate",
		// check item options
		group: "draw",
		checked: true
	});
	actions["nav"] = action;
	toolbarItems.push(Ext.create('Ext.button.Button', action));

//				action = Ext.create('GeoExt.Action', {
//					text: "draw poly",
//					control: new OpenLayers.Control.DrawFeature(vector, OpenLayers.Handler.Polygon),
//					map: map,
//					// button options
//					toggleGroup: "draw",
//					allowDepress: false,
//					tooltip: "draw polygon",
//					// check item options
//					group: "draw"
//				});
//				actions["draw_poly"] = action;
//				toolbarItems.push(Ext.create('Ext.button.Button', action));

//				action = Ext.create('GeoExt.Action', {
//					text: "draw line",
//					control: new OpenLayers.Control.DrawFeature(vector, OpenLayers.Handler.Path),
//					map: map,
//					// button options
//					toggleGroup: "draw",
//					allowDepress: false,
//					tooltip: "draw line",
//					// check item options
//					group: "draw"
//				});
//				actions["draw_line"] = action;
//				toolbarItems.push(Ext.create('Ext.button.Button', action));
//				toolbarItems.push("-");

//				// SelectFeature control, a "toggle" control
//				action = Ext.create('GeoExt.Action', {
//					text: "select",
//					control: new OpenLayers.Control.SelectFeature(vector, {
//						type: OpenLayers.Control.TYPE_TOGGLE,
//						hover: true
//					}),
//					map: map,
//					// button options
//					enableToggle: true,
//					tooltip: "select feature"
//				});
//				actions["select"] = action;
//				toolbarItems.push(Ext.create('Ext.button.Button', action));
//				toolbarItems.push("-");

	// Navigation history - two "button" controls
	ctrl = new OpenLayers.Control.NavigationHistory();
	map.addControl(ctrl);

	action = Ext.create('GeoExt.Action', {
		text: "previous",
		control: ctrl.previous,
		disabled: true,
		tooltip: "previous in history"
	});
	actions["previous"] = action;
	toolbarItems.push(Ext.create('Ext.button.Button', action));

	action = Ext.create('GeoExt.Action', {
		text: "next",
		control: ctrl.next,
		disabled: true,
		tooltip: "next in history"
	});
	actions["next"] = action;
	toolbarItems.push(Ext.create('Ext.button.Button', action));
	toolbarItems.push("->");

	// Reuse the GeoExt.Action objects created above
	// as menu items
	toolbarItems.push({
		text: "menu",
		menu: Ext.create('Ext.menu.Menu', {
			items: [
			// ZoomToMaxExtent
		Ext.create('Ext.button.Button', actions["max_extent"]),
			// Nav
		Ext.create('Ext.menu.CheckItem', actions["nav"]),
			// Draw poly
		Ext.create('Ext.menu.CheckItem', actions["draw_poly"]),
			// Draw line
		Ext.create('Ext.menu.CheckItem', actions["draw_line"]),
			// Select control
		Ext.create('Ext.menu.CheckItem', actions["select"]),
			// Navigation history control
		Ext.create('Ext.button.Button', actions["previous"]),
		Ext.create('Ext.button.Button', actions["next"])
	]
		})
	});
	return toolbarItems;
}


