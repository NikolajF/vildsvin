﻿var DrawPoint;
var aInd;

//Ext.form.VTypes["emailVal"] = /^.+@.+\..+$/;
Ext.form.VTypes["emailVal"] = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

Ext.form.VTypes["emailText"] = 'Ikke en gyldig email-adresse.  Skal indeholde mindst et navn før @ og mindst to navne efter @. Navnene skal adskilles af punktum.';
Ext.form.VTypes["email"] = function (v)
{
    return Ext.form.VTypes["emailVal"].test(v);
}
Ext.form.VTypes["telefonVal"] = /^(00[0-9]{6,}|\+[0-9]{6,}|[2-9][0-9]{7})$/;
Ext.form.VTypes["telefonMask"] = /[0-9 +]/;
Ext.form.VTypes["telefonText"] = 'Telefonnummeret er ikke gyldigt.  Skal være på formen "12 34 56 78" eller "+45 12 34 56 78" eller "0045 12 34 56 78" (mellemrum kan udelades).';
Ext.form.VTypes["telefon"] = function (v)
{
    return Ext.form.VTypes["telefonVal"].test(v.replace(/ /g, ""));
}


function indberetningsformular(indberetningsid) {

	showMap();
	toemFormular('telefon_indberetning', 'indberetningsformular');

	redJournr();

	DrawPoint = new OpenLayers.Control.DrawFeature(mapPanel.config.layers.selections.coordinateLayer, OpenLayers.Handler.Point);  //, { 'displayClass': 'olControlDrawBigaard' }
	DrawPoint.events.register('featureadded', this, function () {
		var x = arguments[0].feature.geometry.x;
		var y = arguments[0].feature.geometry.y;
		mapPanel.clearLayers();
		//			if (mapPanel.map.resolution > 1) {
		//				fejlbox("Du skal zoome længere ind, før du kan placere bigården - kun de fire øverste trin på zoom-stigen er tilladt.");
		//				Ext.getCmp('gx').setValue('');
		//				Ext.getCmp('gy').setValue('');
		//				return;
		//			}
		mapPanel.showPoint(x, y);
		Ext.getCmp('t_setx').setValue(x);
		Ext.getCmp('t_sety').setValue(y);
		// ??? hent fundstedets nærmeste adresse fra vores service
		// indlæsning af data
//		var param = { x: x, y: y };
	//	var params = Ext.JSON.encode(param);


		Ext.Ajax.request({
//			url: 'http://kortforsyningen.kms.dk/?servicename=RestGeokeys_v2&method=nadresse&geop=' + x + ',' + y + '&hits=1&geometry=true&ticket=' + kmsticket,
			url: 'KF_Proxy.ashx?servicename=RestGeokeys_v2&method=nadresse&geop=' + x + ',' + y + '&hits=1&geometry=true&ticket=' + kmsticket,
			headers: { 'Content-Type': 'application/json;charset=utf-8' },
			success: function (response, opts) {
				var adr = Ext.JSON.decode(response.responseText).features[0].properties;

				// fyld formularen
				Ext.getCmp('s_setvejnavn').setValue(adr.vej_navn);
				Ext.getCmp('s_sethusnr').setValue(adr.husnr);
				Ext.getCmp('t_setpostnr').setValue(adr.postdistrikt_kode);
				Ext.getCmp('s_setby').setValue(adr.postdistrikt_navn);
				// tag hensyn til eksisterende indhold
				var v = Ext.getCmp('s_fundsted').getValue();
				if (v != '') {// tom
					var bg = v.lastIndexOf('Nærmeste adresse');
					if (bg >= 0) {
						v = v.substring(0, bg);
						var slut = v.indexOf(' meter.') + 9;
						v = v.substring(slut);
					}
				}
				Ext.getCmp('s_fundsted').setValue('Nærmeste adresse - afstand ' + adr.afstand_afstand + ' meter.\n' + v);
			},
			failure: ajaxFailed
		});

		/*
			{
			 "type": "FeatureCollection",
			 "crs": {
			  "type": "name",
			  "properties": {"name": "EPSG:25832"}
			 },
			 "features": [{
			  "type": "Feature",
			  "properties": {
			   "husnr": "69",
			   "kommune_kode": "0746",
			   "kommune_navn": "Skanderborg Kommune",
			   "kommune_href": "http:\/\/kortforsyningen.kms.dk\/?servicename=RestGeokeys_v2&method=kommune&komkode=0746",
			   "vej_kode": "0793",
			   "vej_navn": "Ravnsøvej",
			   "vej_href": "http:\/\/kortforsyningen.kms.dk\/?servicename=RestGeokeys_v2&method=vej&vejkode=0793",
			   "postdistrikt_kode": "8670",
			   "postdistrikt_navn": "Låsby",
			   "postdistrikt_href": "http:\/\/kortforsyningen.kms.dk\/?servicename=RestGeokeys_v2&method=postdistrikt&postnr=8670",
			   "afstand_enhed": "meter",
			   "afstand_afstand": "90"
			  },
			  "geometry": {
			   "type": "POINT",
			   "coordinates": [
				550085.8,
				6221029.94
			   ]
			  }
			 }]
			}		
		*/





	});
	mapPanel.map.addControl(DrawPoint);
	DrawPoint.activate();
	
	// ??? husk at afmelde eventet igen, når formularen lukkes - koden her er fra bierne og med et andet event, dette afmeldes faktisk slet ikke
	//	mapPanel.map.events.unregister('featureadded');
	// DrawPoint.deactivate();

	////vTypes
	////Ext.form.VTypes["cpr"] = /^\[0-9]{6} |[0-9]{6}-[0-9]{4}$/;
	//Ext.apply(Ext.form.field.VTypes, {

	//	//  vtype validation function
	//	cpr: function (val, field) {
	//		var cprName = /^[0-9]{6}$|^[0-9]{6}-[0-9]{4}$/;
	//		if (val.length >=6) {
	//			var dt = Ext.Date.parse(val.substring(0, 6), 'dmy');
	//			if (dt == undefined || Ext.Date.format(dt, 'dmy') != val.substring(0, 6))
	//				return false;
	//		}
	//		return cprName.test(val);
	//	},
	//	// vtype Text property to display error Text
	//	// when the validation function returns false
	//	cprText: 'Udfyld enten kun de første 6 cifre af CPR-nummeret eller både de første 6, bindestregen og de sidste 4 - eller lad feltet være tomt.',
	//	// vtype Mask property for keystroke filter mask
	//	cprMask: /[0-9-]/i

	//});

//	Ext.apply(Ext.form.VTypes, {
//	'journrText': 'Ikke et gyldigt journalnummer. Skal være i formatet xxxx-xx-xxxxx-xx.',
//	'journrMask': /[\-\0-9]/,
//	'journrRe': /^[0-9]{4}[-]{1}[0-9]{2}[-]{1}[0-9]{5}[-]{1}[0-9]{2}$/,
//	'journr': function (v) {
//		return this.journrRe.test(v);
//	}
//	});

	var bx = vestPanel.getBox();

	var indberetningspanel = Ext.create('Ext.panel.Panel', {
		title: indberetningsid == -1 ? 'Ny leverance af vildsvin til LAB' : indberetningsid == 0 ? 'Ny telefonisk indberetning' : 'Indberetning nr ' + indberetningsid,
		id: 'telefon_indberetning',
		layout: 'anchor',
		bodyPadding: '10 10 10 10',
		autoWidth: true,
		autoHeight: false,
		height: bx.height,
		autoScroll: true,


		buttons: [{
			text: 'Hjælp',
			iconCls: 'icon-help',
			//				hidden : hjaelp == '' ? true : false,
			hidden: true,
			handler: function () {
				visHjaelp(hjaelp);
			}
		}, '->', {
			text: 'Annullér',
			iconCls: 'icon-cancel',
			handler: function () {
				if (indberetningsid == -1) {
					// returner til lab-liste
					visLabListe();
				}
				else {
					// luk formularen
					VisLevende();
				}
			}
		}, {
			text: 'OK',
			iconCls: 'icon-ok',
			style: 'margin-right: 25px;',
			handler: function () {
				// valider formularen
				var fejltekst = '';
				for (var prop in aInd) {
					switch (prop) {
						// først dem der skal springes over							     
						case '__type':
						case 't_id':
					    case 't_registrator':
					    case 't_afd':
					    case 't_status':
					    case 't_fv_region':
					    case 't_registrator_ret':
					    case 'v_visiteret_tid':
					    case 'l_billede':
					        break;
						//case 'c_afh_type':
						//    if (indberetningsid == -1)
						//    {
						//        aInd.c_afh_type = '3';
						//        aInd.s_afhentningsadresse = 'Sendt direkte til Lab';
						//    }
						//    else
						//    {
						//        for (var i = 1; i <= 3; i++) {
						//		    if (Ext.getCmp('c_afh_type_' + i).checked == true) {
						//		        aInd.c_afh_type = i + '';
						//		        // skriv i afhentningadresse
						//		        if (i == 1) // 
						//		            aInd.s_afhentningsadresse = 'Fundstedet';
						//		        else if (i == 2)
						//		            aInd.s_afhentningsadresse = 'Anmelders adresse';

						//		        break;
						//		    }
						//	    }
						//    }
						//	break;
						default:
							try {
								var c = Ext.getCmp(prop);
								if (c == undefined)
									window.console.log('Undefined prop: ' + prop);
								else {
									if (c.isValid() == false) {//c.isDisabled() == false && c.readOnly == false && 
										switch (prop) {
											// enkelte skal ikke blot have fieldLabel							     
											case 's_an_vejnavn':
												if (aInd.t_afd != 10) // dette er kun et problem, hvis anmeldelsen ikke kommer fra BorgerTip app
													fejltekst += '\nAnmelders adresse';
												break;
											case 's_setvejnavn':
												fejltekst += '\nFundets adresse';
												break;
											case 't_setx':
												fejltekst += '\nFundstedets koordinater - klik i kortet for at sætte dem';
												break;
											default:
												fejltekst += '\n' + c.fieldLabel;
												break;
										}
									}
									else {
										aInd[prop] = c.getValue();
										if (aInd[prop] == null)
											aInd[prop] = '';
									}
								}

							}
							catch (ex) {
								window.console.log('prop=' + prop + '; værdi=' + aInd[prop] + '; fejl=' + ex);
								//if (prop == 'Titel')
								//Ext.getCmp(prop).setTitle(aInd[prop]);
							}
							break;
					}
                }
                if (adresseValideret == false)
                    fejltekst += '\nAnmelders adresse er ikke valideret';


				if (fejltekst != '') {
					// ??? skal være fejlbox
					alert('Du kan ikke gemme indberetningen, før disse felter er blevet rettet til noget gyldigt:' + fejltekst);
					return;
				}
				// tilføj id til nye poster
				aInd.t_id = indberetningsid;

				// send til serveren
				var param = { t: aInd };
				var params = Ext.JSON.encode(param);


				Ext.Ajax.request({
					url: 'Webservices/svin_login.asmx/gemTelefonIndberetning',
					params: params,
					headers: { 'Content-Type': 'application/json;charset=utf-8' },
					success: function (response, opts) {
						if (indberetningsid == -1) {
							// returner til lab-liste
							visLabListe();
						}
						else // returner til Levende, nej aldrig det, måske indsamling
						{
						// luk formularen
                            toemFormular('', 'indberetningsformular');
						}
					},
					failure: ajaxFailed
				});
			}

		}],
		items: [{
			xtype: 'fieldset',
			title: 'Oplysninger om anmelder',
			margins: 6,
			autoWidth: true,
			autoHeight: true,
			flex: 1,
			items: [{
				xtype: 'fccolumn',
				items: [{
					fieldLabel: 'Navn',
					id: 's_an_navn',
					//margin: '0 3 0 0', // venstre
					columnWidth: 1,
					allowBlank: false,
					enableKeyEvents: true,
					listeners: {
						keyup: function (ctl, e, eOpts) {
							var bg = ctl.selectionStart;
							window.console.log('start=' + bg);
						}
					}
				}]
			}, {
			    xtype: 'fccolumn',
			    items: [{
			        fieldLabel: 'Telefon',
			        id: 's_an_telefon',
			        //margin: '0 3 0 0', // venstre
                    vtype: 'telefon',
			        columnWidth: 0.5
			    }, {
			        fieldLabel: 'Email',
			        id: 's_an_email',
                    vtype: 'email',
			        //margin: '0 3 0 0', // højre
			        columnWidth: 0.5
			    }]
			//}, {
			//	xtype: 'fccolumn',
			//	items: [{
			//		fieldLabel: 'Søg anmelders adresse',
			//		id: 'adresse',
			//		//margin: '0 0 0 0',
			//		columnWidth: 1,
			//		xtype: 'adrsearch',
			//		forceHouseNumber: true,
			//		allowBlank: false,
			//		//vType: 'adresse',
			//		listeners: {
			//			searchFinished: {
			//				fn: function (responseType, vejnavn, husnummer, postnummer, postdistrikt, stednavn, x, y) {
			//					//an address
			//					if (responseType === 0) {
			//						Ext.getCmp('s_an_vejnavn').setValue(vejnavn);
			//						Ext.getCmp('s_an_husnr').setValue(husnummer);
			//						Ext.getCmp('t_an_postnr').setValue(postnummer);
			//						Ext.getCmp('s_an_by').setValue(postdistrikt);
			//						mapPanel.zoomToAddressLevel(x, y, 11);
			//					}
			//					//a street
			//					else {
			//						Ext.getCmp('s_an_vejnavn').setValue('');
			//						Ext.getCmp('s_an_husnr').setValue('');
			//						Ext.getCmp('t_an_postnr').setValue('');
			//						Ext.getCmp('s_an_by').setValue('');
			//					}
			//				}
			//			}
			//		}

			//	}]
			//}, {
			//	xtype: 'fccolumn_ro',
			//	items: [{
			//		fieldLabel: 'Vejnavn',
			//		allowBlank: false,
			//		//margin: '0 3 0 0', // venstre
			//		id: 's_an_vejnavn',
			//		columnWidth: 0.35
			//	}, {
			//		fieldLabel: 'Husnr',
			//		id: 's_an_husnr',
			//		//margin: '0 3 0 0', // højre
			//		columnWidth: 0.15
			//	}, {
			//		fieldLabel: 'Postnr',
			//		id: 't_an_postnr',
			//		//margin: '0 3 0 0', // venstre
			//		columnWidth: 0.15
			//	}, {
			//		fieldLabel: 'Postdistrikt',
			//		id: 's_an_by',
			//		flex: 1,
			//		//margin: '0 0 0 0', // højre
			//		columnWidth: 0.35
			//	}]
			//}]
                }, {
                    xtype: 'fccolumn',
                    items: [{
                        fieldLabel: 'Vejnavn',
                        allowBlank: false,
                        //margin: '0 3 0 0', // venstre
                        id: 's_an_vejnavn',
                        columnWidth: 0.35,
                        qtipText: 'Udfyld Vejnavn, Husnr og Postnr og tryk Retur i et af felterne for at validere adressen.',
                        //enableKeyEvents: true,
                        listeners: {
                            change: function (fld, newValue, oldValue, opts)
                            {
                                adresseValideret = false;
                            }, 
                            specialkey: function (field, e)
                            {
                                if (e.getKey() === e.ENTER)
                                    findAdresse();
                            }
                        }

                    }, {
                        fieldLabel: 'Husnr',
                        id: 's_an_husnr',
                        //margin: '0 3 0 0', // højre
                            columnWidth: 0.15,
                            qtipText: 'Udfyld Vejnavn, Husnr og Postnr og tryk Retur i et af felterne for at validere adressen.',
                            enableKeyEvents: true,
                            listeners: {
                                change: function (fld, newValue, oldValue, opts)
                                {
                                    adresseValideret = false;
                                }, 
                                specialkey: function (field, e)
                                {
                                    if (e.getKey() === e.ENTER)
                                        findAdresse();
                                }
                            }
                    }, {
                        fieldLabel: 'Postnr',
                        id: 't_an_postnr',
                        //margin: '0 3 0 0', // venstre
                            columnWidth: 0.15,
                            qtipText: 'Udfyld Vejnavn, Husnr og Postnr og tryk Retur i et af felterne for at validere adressen.',
                            enableKeyEvents: true,
                            listeners: {
                                change: function (fld, newValue, oldValue, opts)
                                {
                                    adresseValideret = false;
                                }, 
                                specialkey: function (field, e)
                                {
                                    if (e.getKey() === e.ENTER)
                                        findAdresse();
                                }
                            }
                    }, {
                        fieldLabel: 'Postdistrikt',
                        id: 's_an_by',
                        flex: 1,
                        //margin: '0 0 0 0', // højre
                        columnWidth: 0.35,
                            enableKeyEvents: true,
                            listeners: {
                                change: function (fld, newValue, oldValue, opts)
                                {
                                    adresseValideret = false;
                                }
                            }
                    }]
                }]
			}, {
				xtype: 'fieldset',
				title: 'Oplysninger om fundet',
				autoHeight: true,
				margins: '6',
				//anchor: '0',
				//flex: 1,
				items: [{
					xtype: 'fccolumn',
					items: [{
					    xtype: 'fcombo',
					    fieldLabel: 'Fundets art',
						id: 't_fundtype',
						margin: '0 3 0 0', // venstre
					    store: storeFundtyper,
					    valueField: 'id',
					    displayField: 'navn',
					    allowBlank: false,
					    columnWidth: 0.6
					}, {
						fieldLabel: 'Antal',
						id: 't_antal',
						//margin: '0 3 0 0', // højre
                        allowBlank: false,
						columnWidth: 0.15
					}, {
					    fieldLabel: 'Observeret dato',
					    xtype: 'datefield',
					    id: 's_set_tid',
					    flex: 1,
					    //margin: '0 0 0 0', // højre
					    columnWidth: 0.25,
					    allowBlank: false,
					    maxValue: new Date(),
					    format: 'd-m-Y',
					    altFormats: 'Y-m-d H:i:s|D M Y H:i:s|d-m-Y|d-m-y|Y-m-d|j-n-y|j-n-Y|j-m-y|j-m-Y|d-n-y|d-n-Y'
					}]
				}, {
					xtype: 'fccolumn',
					items: [{
						xtype: 'textarea',
						fieldLabel: 'Beskrivelse af fundet',
						id: 's_setbeskriv',
						margin: '0', // højre
						columnWidth: 1,
						grow: true,
						growMin: 30,
						growMax: 300
					}]
				}]
			}, {
			    xtype: 'fieldset',
			    title: 'Fotografi',
			    id: 'fl_billede',
			    bodyPadding: 10,
			    autoWidth: true,
			    autoHeight: true,
			    style: 'margin-top: 10px',
			    flex: 1,
			    items: [{
			        xtype: 'image',
			        id: 'l_billede',
			        src: '',
			        hidden: false,
			        shrinkWrap: true,
			        width: '100%'
			    }]
			}, {
			    xtype: 'fieldset',
			    title: 'Oplysninger om fundstedet',
			    autoHeight: true,
			    margins: '6',
			    //anchor: '0',
			    //flex: 1,
			    items: [{
			        xtype: 'fccolumn_ro',
			        items: [{
			            fieldLabel: 'Vejnavn',
			            allowBlank: false,
			            id: 's_setvejnavn',
			            margin: '0 3 0 0', // venstre
			            columnWidth: 0.35
			        }, {
			            fieldLabel: 'Husnr',
			            id: 's_sethusnr',
			            //margin: '0 3 0 0', // højre
			            columnWidth: 0.15
			        }, {
			            fieldLabel: 'Postnr',
			            id: 't_setpostnr',
			            //margin: '0 3 0 0', // venstre
			            columnWidth: 0.15
			        }, {
			            fieldLabel: 'Postdistrikt',
			            id: 's_setby',
			            margin: '0 0 0 3', // højre
			            columnWidth: 0.35
			        }]
			    }, {
			        xtype: 'fccolumn_ro',
			        items: [{
			            fieldLabel: 'Øst',
			            allowBlank: false,
			            id: 't_setx',
			            margin: '0 3 0 0', // venstre
			            columnWidth: 0.5
			        }, {
			            fieldLabel: 'Nord',
			            id: 't_sety',
			            margin: '0 0 0 3', // højre
			            columnWidth: 0.5
			        }]
			    }, {
			        xtype: 'fccolumn',
			        items: [{
			            xtype: 'textarea',
			            fieldLabel: 'Beskrivelse af fundstedet - hvis det ikke blot er en adresse',
			            id: 's_fundsted',
			            margin: '0', // højre
			            columnWidth: 1,
			            grow: true,
			            growMin: 30,
			            growMax: 300
			        }]
			    }]
			//}, {
			//	xtype: 'fieldset',
			//	title: 'Oplysninger om afhentningsadresse',
			//	autoHeight: true,
			//	margins: '6',
			//	hidden: indberetningsid ==-1,
			//	//anchor: '0',
			//	//flex: 1,
			//	items: [{
			//		xtype: 'fccolumn',
			//		items: [{
			//			xtype: 'radiogroup',
			//			fieldLabel: 'Hvor skal fundet afhentes?',
			//			columns: 3,
			//			items: [
			//				{ boxLabel: 'Fundstedet', name: 'c_afh_type', id: 'c_afh_type_1', inputValue: '1', checked: true },
			//				{ boxLabel: 'Anmelders adresse', name: 'c_afh_type', id: 'c_afh_type_2', inputValue: '2' },
			//				{ boxLabel: 'Andet', name: 'c_afh_type', id: 'c_afh_type_3', inputValue: '3' }
			//			],
			//			id: 't_afhentgruppe',
			//			margin: '0',
			//			columnWidth: 1,
			//			listeners:
			//			{
			//				change: function (group, selected, previous) {
			//					if (selected.c_afh_type == '3') {
			//						Ext.getCmp('s_afhentningsadresse').show();
			//						Ext.getCmp('s_afhentningsadresse').allowBlank = false;
			//					}
			//					else {
			//						Ext.getCmp('s_afhentningsadresse').hide();
			//						Ext.getCmp('s_afhentningsadresse').allowBlank = true;
			//					}
			//				}
			//			}
			//		}]
			//	}, {
			//		xtype: 'fccolumn',
			//		items: [{
			//			xtype: 'textarea',
			//			fieldLabel: 'Afhentningsadresse',
			//			id: 's_afhentningsadresse',
			//			hidden: true,
			//			margin: '0',
			//			columnWidth: 1,
			//			//height: 20,
			//			grow: true,
			//			growMin: 30,
			//			growMax: 300
			//		}]
			//	}]
			}, {
				xtype: 'fieldset',
				title: 'Oplysninger om registreringen',
				autoHeight: true,
				margins: '6',
				//anchor: '0',
				//flex: 1,
				items: [{
					xtype: 'fccolumn_ro',
					items: [{
						fieldLabel: 'Anmeldt dato',
						id: 's_an_tid',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.2
					}, {
						fieldLabel: 'Registrator',
						id: 'v_brugernavn',
						//margin: '0 3 0 0', // venstre
						columnWidth: 0.4
					}, {
						fieldLabel: 'Afdeling',
						id: 'v_firmanavn',
						margin: '0 0 0 3', // højre
						columnWidth: 0.4
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						fieldLabel: 'Sidst rettet dato',
						id: 's_rettet_tid',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.2
					}, {
						fieldLabel: 'Registrator',
						id: 'v_rettet_navn',
						//margin: '0 3 0 0', // venstre
						columnWidth: 0.4
					}, {
						fieldLabel: 'Afdeling',
						id: 'v_rettet_firma',
						margin: '0 0 0 3', // højre
						columnWidth: 0.4
					}]
				}, {
					xtype: 'fccolumn_ro',
					items: [{
						fieldLabel: 'Visiteret dato',
						id: 's_visiteret_tid',
						margin: '0 3 0 0', // venstre
						columnWidth: 0.2
					}, {
						fieldLabel: 'Visitator',
						id: 'v_visitator_navn',
						//margin: '0 3 0 0', // venstre
						columnWidth: 0.4
					}, {
						fieldLabel: 'Afdeling',
						id: 'v_visitator_firma',
						margin: '0 0 0 3', // højre
						columnWidth: 0.4
					}]
				}]
			}],
		renderTo: 'vestdiv' //indberetningsid == 0 ? 'vestdiv' : 'vestdiv' //undefined
	});



	// indlæsning af data
	var param = {id: indberetningsid};
	var params = Ext.JSON.encode(param);


	Ext.Ajax.request({
		url: 'Webservices/svin_login.asmx/hentTelefonIndberetning',
		params: params,
		headers: { 'Content-Type': 'application/json;charset=utf-8' },
		success: function (response, opts) {
			aInd = Ext.JSON.decode(response.responseText).d;
			//tegning af kortet
			//mapPanel.config.layers.data.indberetninger.setVisibility(true);
			if (indberetningsid > 0) {
				mapPanel.zoomToAddressLevel(aInd.t_setx, aInd.t_sety, 11);
				mapPanel.addSelectedPoint(aInd.t_setx, aInd.t_sety);
            }
			// fyld formularen
			var ro = (aInd.v_visitator_navn == '' || aInd.v_visitator_navn == null) ? false : true;

			var c = Ext.getCmp('telefon_indberetning');
			if (ro)
			{
			    c.query('.fieldset,.grid').forEach(function (c) { c.setDisabled(true); });
			}
			else
			{ c.enable(); }
			for (var prop in aInd) {
				switch (prop) {
					// først dem der skal springes over      
					case '__type':
					case 't_id':
						break;
					//case 'c_afh_type':
					//    if (aInd[prop] == null) // standardværdien
					//        Ext.getCmp('c_afh_type_1').setValue(true);
					//    else
					//        Ext.getCmp('c_afh_type_' + aInd[prop]).setValue(true);
					//	//						Ext.getCmp('t_afhentgruppe').setValue((aInd[prop] == '1'), (aInd[prop] == '2'), (aInd[prop] == '3'));
					//	//						// vis tekstfeltet, hvis værdien er 3
					//	//						if (aInd[prop] == '3') {
					//	//							Ext.getCmp('s_afhentningsadresse').show();
					//	//							Ext.getCmp('s_afhentningsadresse').allowBlank = false;
					//	//						}
					//	break;
					case 'l_billede':
						Ext.getCmp(prop).setSrc(aInd[prop]);
						var visbil = false;
						if (aInd[prop] != null && aInd[prop] != '')
							visbil = true;
						Ext.getCmp('f' + prop).setVisible(visbil);
						break;
					default:
						try {
							c = Ext.getCmp(prop);
							if (c != undefined)
								c.setValue(aInd[prop]);

						}
						catch (ex) {
							window.console.log('prop=' + prop + '; værdi=' + aInd[prop] + '; fejl=' + ex);
							//if (prop == 'Titel')
							//Ext.getCmp(prop).setTitle(aInd[prop]);
						}
						break;
				}
			}
		},
		failure: ajaxFailed
	});
}

var adresseValideret = false;
function findAdresse()
{
    // er alle oplysninger på plads?
    var vej = Ext.getCmp('s_an_vejnavn').getValue().trim();
    var husnr = Ext.getCmp('s_an_husnr').getValue().toUpperCase().trim();
    var postnr = Ext.getCmp('t_an_postnr').getValue().trim();
    if (vej == '' || husnr == '' || postnr == '')
    {
        fejlbox("Felterne Vejnavn, Husnr og Postnr skal udfyldes.")
        return false;
    }


    Ext.Ajax.request({
        url: 'KF_Proxy.ashx?servicename=RestGeokeys_v2&method=adresse&vejnavn=' + vej + '&husnr=' + husnr + '&postnr=' + postnr + '&hits=1&geometry=true&ticket=' + kmsticket,
        headers: { 'Content-Type': 'application/json;charset=utf-8' },
        success: function (response, opts)
        {
            try
            {
                var adr = Ext.JSON.decode(response.responseText).features[0].properties;
                // fyld formularen
                Ext.getCmp('s_an_vejnavn').setValue(adr.vej_navn);
                Ext.getCmp('s_an_husnr').setValue(adr.husnr);
                Ext.getCmp('t_an_postnr').setValue(adr.postdistrikt_kode);
                Ext.getCmp('s_an_by').setValue(adr.postdistrikt_navn);
                // zoom til adressen
                var g = Ext.JSON.decode(response.responseText).features[0].geometry.coordinates;
                mapPanel.zoomToAddressLevel(g[0], g[1], 11);
                adresseValideret = true;
            }
            catch (ex)
            {
                adresseValideret = false;
                fejlbox("Beklager, Kortforsyningen kunne ikke finde den indtastede adresse.")
                return false;
            }
        },
        failure: ajaxFailed
    });

}

