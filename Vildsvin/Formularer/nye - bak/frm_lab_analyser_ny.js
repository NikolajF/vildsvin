﻿function visLabAnalyser() {
	showMap();
	toemFormular('lab_analyser_panel', 'visLabAnalyser');
	// logos
	//	var lablogos = '<table width="100%"><tr><td align="center"><a href="http://www.brs.dk"><img src="images/logos/BRS_logo.png" alt="Beredskabsstyrelsens logo med link"></a></td><td align="center" ><a href="http://zoologi.snm.ku.dk/"><img src="images/logos/zm_logo.png" alt="Zoologisk Museums logo med link"></a></td><td align="center" ><a href="http://www.dmu.dk"><img src="images/logos/dmuaulogo.gif" alt="Dammarks Miljøundersøgelsers logo med link"></a></td></tr></table>';


	//	document.getElementById('vestliste').innerHTML = '<div id="lablogos"></div>';

	/*
		statisk: hvor mange vildsvin er undersøgt i år
			hvornår er sidste opdatering af databasen
		find først årene og læg dem i comboboxen
		vælg det seneste år i comboboxen - så kører resten af indlæsningen derfra
	
	*/
	var aarListe = [];
	var aars_antal = ''; 
	var aktaar = '';
	var valgtAar = '';
	var sidst_opdateret = '';
	Ext.Ajax.request({
		url: 'Webservices/svin_login.asmx/LabAnalyserAar',
		headers: { 'Content-Type': "application/json" },
		method: 'POST',
		async: false,
		success: function (response, opts) {
			var obj = Ext.decode(response.responseText);
			var a = obj.d.aar.split(';');
			for (var i = 0; i < a.length; i++) {
				aarListe.push([a[i], a[i]]);
			}
			sidst_opdateret = obj.d.SidstRettet;
			valgtAar = obj.d.valgtaar;
			aktaar = obj.d.aktaar;
			aars_antal = obj.d.aktaar_antal;
			//mapPanel.config.layers.data.lab_analyser.mergeNewParams({ CQL_FILTER: 'aar=' + valgtAar });
            mapPanel.redrawLayers('vildsvin_off');
		},
		failure: function (response, opts) {
		}
	});

	//	Ext.define('aarModel', {
	//		extend: 'Ext.data.Model',
	//		fields: ['id'],
	//		idProperty: 'id'
	//	});

	var storeAar = Ext.create('Ext.data.ArrayStore', {
		model: Ext.define('ComboModel', {
			extend: 'Ext.data.Model',
			fields: ['id', 'text'],
			idProperty: 'id'
		}),
		data: aarListe,
		sorters: [{
			property: 'id',
			direction: 'ASC'
		}]
	});

	Ext.define('LabAnalyser', {
		extend: 'Ext.data.Model',
		fields: [
    	    'id',
		    'pos',
		    'tilstand',
		    'fundtype',
		    'journalnr_1',
            'journalnr_2',
		    'resultat',
		    'indsamler',
		    'kommune',
		    'x',
		    'y',
		    'set_tid',
//		    'tid_indsamlet',
            'tid_lab',
		    'tid_slut',
		    'set_adresse',
		    'fundsted',
            'indberetning',
            'billede',
            'asf',
            'csf',
            'trikiner',
            'aujeszkys'
		],
		idProperty: 'id'
	});

	var store = Ext.create('Ext.data.JsonStore', {
		storeId: 'laban',
		pageSize: 10,
		model: 'LabAnalyser',
		remoteSort: true,
		proxy: {
			type: 'gridmap',
			extraParams: { aar: valgtAar + '' },
			url: 'Webservices/svin_login.asmx/visLabAnalyser'
		}, 
		sorters: [{
			property: 'dato',
			direction: 'DESC'
		}],
		listeners: {
			load: function (store) {
                mapPanel.redrawLayers('vildsvin_off');
				Ext.getCmp('off_grid').setTitle('De enkelte vildsvin, der er analyseret på laboratoriet og fundet i ' + valgtAar);
				aktRec = undefined;
				aiz = [0, 0, 0, 0];
				var xmin, xmax, ymin, ymax;

				// løb de nye poster igennem
				if (store.getCount() == 0) {
					//Ext.getCmp('selectonmap').disable();
					Ext.getCmp('zoomliste').disable();
					Ext.getCmp('zoommarkeret').disable();
				} else {
					//Ext.getCmp('selectonmap').enable();
					Ext.getCmp('zoomliste').enable();
					Ext.getCmp('zoommarkeret').enable();
					//  nulstilling
					xmin = 0;
					xmax = xmin;
					ymin = 0;
					ymax = ymin;
					store.each(function (record) {
						if (record.data.x != null && record.data.x != '0') {
							if (xmin == 0) {
								xmin = Number(record.data.x);
								xmax = xmin;
								ymin = Number(record.data.y);
								ymax = ymin;
							} else {
								if (Number(record.data.x) < xmin) xmin = Number(record.data.x);
								if (Number(record.data.x) > xmax) xmax = Number(record.data.x);
								if (Number(record.data.y) < ymin) ymin = Number(record.data.y);
								if (Number(record.data.y) > ymax) ymax = Number(record.data.y);
							} // slut if 0
						}
					}); // slut each
					if ((xmax - xmin) < 200) {
						xmidt = (Number(xmax) + Number(xmin)) / 2;
						xmax = Number(xmidt) + 100;
						xmin = Number(xmidt - 100);
					}
					if ((ymax - ymin) < 200) {
						ymidt = (Number(ymax) + Number(ymin)) / 2;
						ymax = Number(ymidt) + 100;
						ymin = Number(ymidt - 100);
					}

					zmListe = [xmin, ymin, xmax, ymax];
                    mapPanel.config.layers.data.vildsvin_off.setVisibility(true);
					if (Ext.getCmp('zoomliste').pressed == true) mapPanel.zoomTo(zmListe);
				} // slut der er poster
			    // tøm de ekstra felter
				Ext.getCmp('tx_tid_lab').setValue('');
				Ext.getCmp('tx_tid_slut').setValue('');
				Ext.getCmp('tx_set_adresse').setValue('');
				Ext.getCmp('tx_fundsted').setValue('');
                Ext.getCmp('tx_asf').setValue('');
                Ext.getCmp('tx_csf').setValue('');
                Ext.getCmp('tx_trikiner').setValue('');
                Ext.getCmp('tx_aujeszkys').setValue('');
				Ext.getCmp('billede').setSrc('');
				// sæt de aktuelle filtre på Excel-udtræk
				if (brugerid > 0)
					Ext.get('btn_excel').down('a').dom.href = 'VisFil.ashx?type=xl_off&id=' + brugerid + '&aar=' + valgtAar + '';

			}

		}
	});
	storeAkt = store;
    mapPanel.config.layers.data.vildsvin_off.setVisibility(true);

    var filters = Ext.create('Ext.ux.grid.FiltersFeature', {
        // encode and local configuration options defined previously for easier reuse
        encode: true, // json encode the filter query
        local: false   // defaults to false (remote filtering)
    });

	var ana_grid = Ext.create('Ext.grid.Panel', {
	    layout: 'fit',
	    id: 'off_grid',
	    autoHeight: true,
	    autoWidth: true,
	    title: 'Lab analyser i år',
	    store: store,
	    disableSelection: false,
	    loadMask: true,
	    features: [filters],
	    viewConfig: {
	        id: 'gv',
	        autoFill: true,
	        getRowClass: function (record, rowIndex, rowParams, store) {
	            return (record.get('pos') == '1' ? 'fejl' : 'readonly');
	        }
	    },


	    // grid columns
	    // specify any defaults for each column
	    columns: {
	        defaults: { renderer: renderTooltip },
	        items: [{
	            id: 'id',
	            text: "ID",
                tooltip: "ID",
	            dataIndex: 'id',
	            width: frm_lab_analyser.ana_grid.id.width,
	            hidden: frm_lab_analyser.ana_grid.id.hidden,
	            //flex: 1,
	        }, {
	            text: "Indberetning",
	            tooltip: "Indberetning",
	            dataIndex: 'indberetning',
	            width: frm_lab_analyser.ana_grid.indberetning.width,
	            hidden: frm_lab_analyser.ana_grid.indberetning.hidden,
	            filterable: true,
	            sortable: true,
	            //flex: 1,
	            renderer: renderTooltip,
	            filter: { type: 'numeric' }
	        }, {
	            text: "Fundtype",
                tooltip: "Fundtype",
                dataIndex: 'fundtype',
                width: frm_lab_analyser.ana_grid.fundtype.width,
                hidden: frm_lab_analyser.ana_grid.fundtype.hidden,
	            filterable: true,
	            sortable: true,
	            //flex: 1,
                renderer: renderFundtyper,
                filter: {
                    type: 'list',
                    store: storeFundtyper,
                    idField: 'id',
                    labelField: 'navn'
                }
	        }, {
	            text: "LAB Journalnr",
                    tooltip: "LAB Journalnr",
	            dataIndex: 'journalnr_1',
	            width: frm_lab_analyser.ana_grid.journalnr_1.width,
	            hidden: frm_lab_analyser.ana_grid.journalnr_1.hidden,
	            filterable: true,
	            sortable: true,
	            //flex: 1,
	            filter: { type: 'string' }
	        }, {
                text: "FVST Journalnr",
                    tooltip: "FVST Journalnr",
                dataIndex: 'journalnr_2',
                width: frm_lab_analyser.ana_grid.journalnr_2.width,
                hidden: frm_lab_analyser.ana_grid.journalnr_2.hidden,
                filterable: true,
                sortable: true,
                //flex: 1,
                filter: { type: 'string' }
            }, {
	            text: "Kommune",
	            tooltip: "Kommune",
	            dataIndex: 'kommune',
	            width: frm_lab_analyser.ana_grid.kommune.width,
	            hidden: frm_lab_analyser.ana_grid.kommune.hidden,
	            filterable: true,
	            sortable: true,
	            //flex: 1,
	            filter: { type: 'string' }
	        }, {
	            text: "Funddato",
	            tooltip: "Funddato",
	            dataIndex: 'set_tid',
	            width: frm_lab_analyser.ana_grid.set_tid.width,
	            hidden: frm_lab_analyser.ana_grid.set_tid.hidden,
	            filterable: true,
	            sortable: true,
	            renderer: renderDato,
	            filter: { type: 'date' }
	        }, {
	            text: "Resultat",
	            tooltip: "Resultat",
	            dataIndex: 'resultat',
	            width: frm_lab_analyser.ana_grid.resultat.width,
	            hidden: frm_lab_analyser.ana_grid.resultat.hidden,
	            filterable: true,
	            sortable: true,
	            //flex: 1,
	            filter: { type: 'string' }
	        }]
	    },
	    // gridkol_slut
	    selModel: Ext.create('Ext.selection.RowModel', {
	        listeners: {
	            select: {
	                fn: function (selModel, rec, index) {
	                    aktRec = rec;
	                    aktRk = index;
	                    aiz = [0, 0, 0, 0];
	                    aiz[0] = Number(rec.data.x - 100);
	                    aiz[1] = Number(rec.data.y - 100);
	                    aiz[2] = Number(Number(rec.data.x) + 100);
	                    aiz[3] = Number(Number(rec.data.y) + 100);

	                    if (Ext.getCmp('zoommarkeret').pressed == true && rec.data.x != null && rec.data.x != '0')
	                        mapPanel.zoomTo(aiz);
	                    mapPanel.clearLayers();
	                    mapPanel.addSelectedPoint(rec.data.x, rec.data.y);
	                    // ekstra felter
                        Ext.getCmp('tx_set_tid').setValue(rec.data.set_tid);
	                    Ext.getCmp('tx_tid_lab').setValue(rec.data.tid_lab);
	                    Ext.getCmp('tx_tid_slut').setValue(rec.data.tid_slut);
	                    if (brugerid != '0')
	                    {
							Ext.getCmp('tx_set_adresse').setValue(rec.data.set_adresse);
							Ext.getCmp('tx_fundsted').setValue(rec.data.fundsted);
	                    	// billedet
							Ext.getCmp('billede').setSrc(rec.data.billede);
							var visbil = false;
							if (rec.data.billede != null && rec.data.billede != '')
								visbil = true;
							Ext.getCmp('f_billede').setVisible(visbil);
						}
                        Ext.getCmp('tx_asf').setValue(rec.data.asf);
                        Ext.getCmp('tx_csf').setValue(rec.data.csf);
                        Ext.getCmp('tx_trikiner').setValue(rec.data.trikiner);
                        Ext.getCmp('tx_aujeszkys').setValue(rec.data.aujeszkys);
	                }
	            }
	        }
	    }),
	    // paging bar on the bottom
	    tbar: Ext.create('Ext.PagingToolbar', {
	        store: store,
	        displayInfo: true
	    }),
	    bbar: createZoombar(true)
	});
	ana_grid.child('pagingtoolbar').add([
	'->',
	{
	    text: 'Nulstil alle filtre',
	    handler: function () {
	        ana_grid.filters.clearFilters();
	        x1 = 0;
	        y1 = 0;
	        x2 = 0;
	        y2 = 0;
	    }
	}]);


	var lab_analyser_panel = Ext.create('Ext.panel.Panel', {
		title: 'Prøveresultater for vildsvin',
		id: 'lab_analyser_panel',
		layout: 'anchor',
	    anchor: '-20',
		bodyPadding: '10 10 10 10',
		autoWidth: true,
		autoHeight: false,
		autoScroll: true,
		renderTo: 'vestdiv',
		items: [{
			xtype: 'fieldset',
			title: 'Vælg år',
			bodyPadding: 10,
			autoWidth: true,
			autoHeight: true,
			collapsible: true,
			flex: 1,
			items: [{
				xtype: 'displayfield',
				fieldLabel: '',
				autoHeight: true,
				hideLabel: true,
				value: 'På denne side vises resultatet af de prøver, som Veterinærinstituttet har udført eller er ved at udføre, samt fund af levende vildsvin og spor efter vildsvin, som ikke kan undersøges i laboratoriet. '
			}, {
				xtype: 'displayfield',
				autoHeight: true,
				hideLabel: true,
				value: 'Der er til dato set eller undersøgt ' + aars_antal + ' vildsvin, som er fundet i ' + aktaar + '.'
			}, {
				xtype: 'displayfield',
				autoHeight: true,
				hideLabel: true,
				value: 'Tabellen er senest opdateret den ' + sidst_opdateret + '.'
			}, {
				xtype: 'displayfield',
				autoHeight: true,
				hideLabel: true,
				value: 'Opsummeringen viser som standard det seneste år, der findes analyserede prøver fra - men du kan vælge et eller flere tidligere år i listen herunder.'
			}, {
				xtype: 'fcombo',
				id: 'aarcombo',
				hideLabel: false,
				multiSelect: true,
				fieldLabel: 'Vis oplysninger for årene',
				labelWidth: 150,
				labelSeparator: '',
				labelStyle: 'padding: 0;',
				store: storeAar,
				valueField: 'id',
				displayField: 'text',
				value: valgtAar.split(','),
				listeners: {
					select: function fn(combo, records, eOpts) {
					    valgtAar = combo.value.toString()
					    store.proxy.extraParams.aar = valgtAar;
						store.loadPage(1);
					}
				}
			}]
        }, 
        ana_grid, 
        {
			xtype: 'fieldset',
			title: 'Ekstra oplysninger om det markerede vildsvn',
			id: 'ekstra_lab_analyse',
			bodyPadding: 10,
			autoWidth: true,
			autoHeight: true,
			style: 'margin-top: 10px',
			flex: 1,
			items: [{
			    xtype: 'fccolumn_ro',
			    items: [{
				    xtype: 'textfield',
				    hideLabel: false,
				    fieldLabel: 'Set dato',
				    id: 'tx_set_tid',
				    margin: '0 3 0 0', // venstre
				    columnWidth: 0.33
			    }, {
		            xtype: 'textfield',
		            hideLabel: false,
		            fieldLabel: 'Modtaget lab',
		            id: 'tx_tid_lab',
		            margin: '0 3 0 0', // venstre
		            columnWidth: 0.33
		        }, {
		            xtype: 'textfield',
		            hideLabel: false,
		            fieldLabel: 'Analyse afsluttet',
		            id: 'tx_tid_slut',
		            margin: '0 0 0 3', // højre
		            columnWidth: 0.34
		        }]
		    }, {
		        xtype: 'fccolumn_ro',
		        items: [{
                    xtype: 'fcombo',
		            hideLabel: false,
		            fieldLabel: 'Afrikansk svinepest',
		            id: 'tx_asf',
                    store: storeResultat,
                    valueField: 'id',
                    displayField: 'navn',
		            columnWidth: 0.5
		        }, {
                        xtype: 'fcombo',
		            hideLabel: false,
		            fieldLabel: 'Centraleuropæisk svinepest',
		            id: 'tx_csf',
                        store: storeResultat,
                        valueField: 'id',
                        displayField: 'navn',
		            columnWidth: 0.5
		        }]
		    }, {
		        xtype: 'fccolumn_ro',
		        items: [{
                    xtype: 'fcombo',
		            hideLabel: false,
		            fieldLabel: 'Trikiner',
                    id: 'tx_trikiner',
                    store: storeResultat,
                    valueField: 'id',
                    displayField: 'navn',
		            columnWidth: 0.5
		        }, {
                    xtype: 'fcombo',
		            hideLabel: false,
                    fieldLabel: 'Aujeszkys sygdom',
                    id: 'tx_aujeszkys',
                    store: storeResultat,
                    valueField: 'id',
                    displayField: 'navn',
		            columnWidth: 0.5
		        }]
		    }, {
		        xtype: 'fccolumn_ro',
		        hidden: (brugerid == '0'),
		        items: [{
				    xtype: 'textfield',
				    hideLabel: false,
				    fieldLabel: 'Fundets adresse',
				    id: 'tx_set_adresse',
				    margin: '0 3 0 0', // venstre
				    columnWidth: 1,
			    }]
		    }, {
			    xtype: 'fccolumn_ro',
			    hidden: (brugerid == '0'),
			    items: [{
				    xtype: 'textfield',
				    hideLabel: false,
				    fieldLabel: 'Fundsted',
				    id: 'tx_fundsted',
				    margin: '0 3 0 0', // venstre
				    columnWidth: 1
			    }]
		    }]
		}, {
		    xtype: 'fieldset',
		    hidden: (brugerid == '0'),
		    title: 'Fotografi',
		    id: 'f_billede',
		    bodyPadding: 10,
		    autoWidth: true,
		    autoHeight: true,
		    hidden: true,
		    style: 'margin-top: 10px',
		    flex: 1,
		    items: [{
		        xtype: 'image',
		        id: 'billede',
		        src: '',
		        hidden: false,
		        shrinkWrap: true,
		        width: '100%'
		    }]
		}]
	});
	//store.loadPage(1);
	var ac = Ext.getCmp('aarcombo');
	ac.fireEvent('select', ac); // , ac, records, eOpts) {
	var btn =Ext.getCmp('toolbar_item_Legend');
	if (!btn.pressed) btn.toggle();

	retKolonner(ana_grid, "ana_grid");
}


function renderNul(val)
{
    if (val == '0')
        val = '';
    return val;
}
