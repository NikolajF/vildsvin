﻿
Le34AddressSearchLocal = Ext.extend(Ext.Window, {
    layout: 'fit',
    width: 370,
    height: 150,
    resizable: true,
    closable: true,
    closeAction: 'hide',
    autoScroll: false,
    collapsible: true,
    title: 'Adressesøgning',
    addressLabel: 'Indtast adresse',
    addressNotFound: 'Den angivne adresse kunne ikke findes.',
    addressListEmpty: 'Der blev ikke fundet nogen adresser.',
    addressListEmptyText: 'Begynd indtastning',
    addressListLoadingText: 'Indlæser...',
    plain: true,

    initComponent: function () {

        var addressCombo = new GIS34AddressSearchCombo({
            fieldLabel: this.addressLabel,
            valueNotFoundText: this.addressNotFound,
            listEmptyText: this.addressListEmpty,
            loadingText: this.addressListLoadingText,
            emptyText: this.addressListEmptyText,
            anchor: '100%',
            filterid: 0,
            maxrows: 50,//this.maxrows,
            layer: this.layer //layers.selections.awsLayer
        });

        this.controls = {
            addressCombo: addressCombo
        };

        this.items = [
            {
                xtype: 'panel',
                layout: 'form',
                plain: true,
                bodyStyle: 'padding: 10px;',
                items:
                [
                    {
                        xtype: 'fieldset',
                        title: 'Eksempel',
                        items: [
                        {
                            xtype: 'box',
                            html: '<i>energivej 34, 2750 ballerup</i>'
                        }]
                    },
                    addressCombo
                ]
            }
        ];

        // Call parent (required)
        Le34AddressSearchLocal.superclass.initComponent.apply(this, arguments);
    },

    afterRender: function (component) {
        Le34AddressSearchLocal.superclass.afterRender.call(this);
    },
    hide: function (component) {
        Le34AddressSearchLocal.superclass.hide.call(this);
        if (Ext.getCmp(this.toolbaritem).pressed) {
            Ext.getCmp(this.toolbaritem).toggle();
        }
        //visibillity
        this.layer.setVisibility(false);
    },
    show: function (component) {
        Le34AddressSearchLocal.superclass.show.call(this);
        this.alignTo(Ext.getBody(), 't-t', [-5, 45]);
        this.layer.setVisibility(true);
        this.controls.addressCombo.focus('', 1000);
    }
});