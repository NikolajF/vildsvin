﻿
Le34AddressSearch = Ext.extend(Ext.Window, {
	//style: 'background-color: transparent;',
	//bodyStyle: 'background-color: transparent; border: 0px solid transparent;',
	id: 'LE34AddressSearch',
	layout: 'fit',
	width: 370,
	height: 180,
	resizable: false,
	closable: true,
	closeAction: 'hide',
	autoScroll: false,
	collapsible: true,
	title: 'Adressesøgning',
	addressLabel: 'Indtast adresse',
	addressNotFound: 'Den angivne adresse kunne ikke findes.',
	addressListEmpty: 'Der blev ikke fundet nogen adresser.',
	addressListEmptyText: 'Begynd indtastning',
	addressListLoadingText: 'Indlæser...',
	plain: true,

	initComponent: function()
	{

		var awsStore = new Ext.data.ArrayStore(
        {
        	fields: ['id', 'displayName', 'x', 'y', 'isValidated', 'streetName', 'streetBuildingIdentifier', 'hasStreetBuildingIdentifier', 'postCodeIdentifier', 'x1', 'y1', 'x2', 'y2'],
        	data: []
        });
		var config = {
			awsStore: awsStore,
			items:
            [
                {
                	xtype: 'panel',
                	layout: 'form',
                	plain: true,
                	bodyStyle: 'padding: 10px;',
                	items:
                    [
                        {
                        	xtype: 'fieldset',
                        	title: 'Eksempler',
                        	items: [
                            {
                            	xtype: 'box',
                            	html: '<i>energivej 34, 2750 ballerup</i> &nbsp; viser adressen med blå prik<br><br><i>energivej, 2750 ballerup</i> &nbsp; viser hele vejen uden prik'
}]
                        }, {
                        	xtype: 'combo',
                        	id: 'le34-address-search-combo',
                        	fieldLabel: this.addressLabel,
                        	displayField: 'displayName',
                        	valueNotFoundText: this.addressNotFound,
                        	listEmptyText: this.addressListEmpty,
                        	loadingText: this.addressListloadingText,
                        	emptyText: this.addressListEmptyText,
                        	hideTrigger: true,
                        	enableKeyEvents: true,
                        	forceSelection: true,
                        	anchor: '100%',
                        	mode: 'local',
                        	store: awsStore,
                        	autoSelect: true,
                        	listeners:
                            {
                            	keyup: function(textfield, event)
                            	{
                            		// hvis listen har et enkelt element, er det ellers umuligt at pile ned og vælge dette element - dette hjælper
                            		if (event.button == 39 && awsStore.getCount() == 1) // pil ned
                            		{
                            			Ext.getCmp('le34-address-search-combo').select(0, true);
                            		}
                            		else if (textfield.lastQuery != textfield.getRawValue())
                            		{
                            			this.requestSearch(textfield.getRawValue());
                            		}
                            	},

                            	select: function(combo, record, index)
                            	{
                            		// The selected value might be an alias for a group of sub-addresses.
                            		// If so, a new request is needed.
                            		if (record.get('isValidated') === false)
                            		{
                            			// hvis der er et nummer, skal vi også søge efter det
                            			if (record.get('hasStreetBuildingIdentifier') == true && record.data.displayName.indexOf(' ' + record.get('streetBuildingIdentifier') + ',') > 0)
                            			{
                            				this.requestSearch(combo.getRawValue());
                            			}
                            			else
                            			{
                            				var zBListe = [record.data.x1, record.data.y1, record.data.x2, record.data.y2];
                            				// slet evt punkt
                            				mapPanel.clearLayers('awsLayer');
                            				mapPanel.zoomTo(zBListe);
                            			}
                            			//this.hentAdresser(record.data.streetName, record.data.postCodeIdentifier);
                            		}
                            		else
                            		{
                            			mapPanel.zoomToAddress(record.data.x, record.data.y);
                            		}
                            		combo.lastQuery = record.data.displayName;
                            		//                            		else
                            		//                            		{
                            		//                            			combo.lastQuery = record.data.displayName;
                            		//                            		}
                            	},
                            	scope: this
                            }
                        }
                    ]
                }
            ]
		};


		Ext.apply(this, Ext.apply(this.initialConfig, config));

		// Config object has already been applied to 'this' so properties can 
		// be overriden here or new properties (e.g. items, tools, buttons) 
		// can be added, eg:

		// Call parent (required)
		Le34AddressSearch.superclass.initComponent.apply(this, arguments);
	},

	hentAdresser: function(vejnavn, postnr)
	{
		Ext.Ajax.request({
			url: '/Webservices/bier_lister.asmx/adresserVej',
			scope: this,
			jsonData:
              {
              	vejnavn: vejnavn,
              	postnr: postnr
              },
			method: 'POST',
			success: function(response, opts)
			{
				//				response = Ext.decode(response.responseText).d;
				//			response = Ext.util.JSON.decode(response);
				response = Ext.util.JSON.decode(response.responseText).d;
				this.fillSearchResultsList(response);
			},
			failure: function(response, opts)
			{
				Ext.MessageBox.show({
					msg: 'Der er opstået en uventet fejl i kommunikationen med Kort og Matrikelstyrelsen.',
					buttons: Ext.MessageBox.OK,
					icon: Ext.MessageBox.ERROR
				});
			}
		});
	},

	requestSearch: function(searchTerm)
	{
		if (searchTerm && searchTerm.length > 2)
		{
			Ext.Ajax.request({
				url: '/Webservices/bier_lister.asmx/getKMSList',
				scope: this,
				jsonData:
                {
                	userinput: searchTerm
                },
				method: 'POST',
				success: function(response, opts)
				{
					response = Ext.decode(response.responseText).d;
					response = Ext.util.JSON.decode(response);
					this.fillSearchResults(response, opts);
				},
				failure: function(response, opts)
				{
					Ext.MessageBox.show({
						msg: 'Der er opstået en uventet fejl i kommunikationen med Kort og Matrikelstyrelsen.',
						buttons: Ext.MessageBox.OK,
						icon: Ext.MessageBox.ERROR
					});
				}
			});
		}
	},

	fillSearchResults: function(results, opts)
	{
		var combo = Ext.getCmp('le34-address-search-combo');

		combo.store.removeAll();
		var recordType = combo.store.recordType;

		for (var index = 0; index < results.length; index++)
		{
			var result = results[index];
			// Check if the coordinate is within this case.
			//if (result.x && result.x >= map.maxExtent.left && result.x <= map.maxExtent.right && result.y && result.y >= map.maxExtent.bottom && result.y <= map.maxExtent.top) {
			var record = new recordType({
				id: index,
				displayName: result.displayName,
				x: result.x || 0,
				y: result.y || 0,
				x1: result.x1 || 0,
				y1: result.y1 || 0,
				x2: result.x2 || 0,
				y2: result.y2 || 0,
				isValidated: result.isValidated || false,
				streetName: result.streetName,
				streetBuildingIdentifier: result.streetBuildingIdentifier,
				hasStreetBuildingIdentifier: result.hasStreetBuildingIdentifier,
				postCodeIdentifier: result.postCodeIdentifier
			});
			combo.store.add(record);
			//}
		}

		//if(results.length > 1) {
		combo.expand();
		combo.restrictHeight();
		//}
		if (results.length == 1)
		{
			// hvis der er et gyldigt husnummer, så zoom automatisk
			if (results[0].isValidated == true && results[0].streetBuildingIdentifier > 0)
			{
				// hvis der er tale om første nummer på vejen, efter at der kun er valg en vej, så undertryk zoom
				if (opts.jsonData.userinput == results[0].displayName)
					mapPanel.zoomToAddress(results[0].x, results[0].y);
			}
		}
	},

	fillSearchResultsList: function(results)
	{
		var combo = Ext.getCmp('le34-address-search-combo');

		combo.store.removeAll();
		var recordType = combo.store.recordType;

		for (var index = 0; index < results.Count; index++)
		{
			var result = results.List[index];
			// Check if the coordinate is within this case.
			//if (result.x && result.x >= map.maxExtent.left && result.x <= map.maxExtent.right && result.y && result.y >= map.maxExtent.bottom && result.y <= map.maxExtent.top) {
			var record = new recordType({
				id: index,
				displayName: result.displayName,
				x: result.x || 0,
				y: result.y || 0,
				isValidated: result.isValidated || false,
				streetName: result.streetName,
				streetBuildingIdentifier: result.streetBuildingIdentifier,
				hasStreetBuildingIdentifier: result.hasStreetBuildingIdentifier,
				postCodeIdentifier: result.postCodeIdentifier
			});
			combo.store.add(record);
			//}
		}

		//if(results.length > 1) {
		combo.expand();
		combo.restrictHeight();
		//}
	},

	afterRender: function(component)
	{
		Le34AddressSearch.superclass.afterRender.call(this);
		//load the municipality store when the tool is activated
		//Ext.getCmp('le34-address-search-combo').getStore().load();
	},
	hide: function(component)
	{
		Le34AddressSearch.superclass.hide.call(this);
		if (Ext.getCmp(this.toolbaritem).pressed)
		{
			Ext.getCmp(this.toolbaritem).toggle();
		}
		//delete the feature in the layer
		//this.layer.destroyFeatures();

		//visibillity
		this.layer.setVisibility(false);
	},
	show: function(component)
	{
		Le34AddressSearch.superclass.show.call(this);
		this.layer.setVisibility(true);
	},


	zoomToAddress: function(x, y)
	{
		mapPanel.zoomToAddress(x, y);
		/*        var pointGeom = new OpenLayers.Geometry.Point(x, y);
		var pointFeature = new OpenLayers.Feature.Vector(pointGeom);
		this.layer.addFeatures([pointFeature]);
		var point = new OpenLayers.LonLat(x, y);
		mapPanel.setCenter(point, (mapPanel.getZoomForResolution(mapPanel.minResolution)) - 2);
		*/
	}

});