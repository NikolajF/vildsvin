﻿Ext.define('Adresser', {
	extend: 'Ext.data.Model',
	fields: [
        'Id',
        'Identification',
        'Type',
        'Geometry'
    ],
	idProperty: 'id'
});


GIS34AddressSearchCombo = Ext.define('GIS34AddressSearchCombo', {
    extend: 'Ext.form.ComboBox',
    alias: 'widget.adrsearch',
    forceHouseNumber: false,
    //fieldLabel: 'Indtast adresse',
    valueNotFoundText: 'Den angivne adresse kunne ikke findes.',
    listEmptyText: 'Der blev ikke fundet nogen adresser.',
    loadingText: 'Indlæser...',
    emptyText: 'Begynd indtastning...',
    minChars: 2,
    queryDelay: 50,
    //listWidth: 300,
    //tpl: '<tpl for="."><div class="x-combo-list-item">{Identification}</div></tpl>',
    //anchor: '100%',
    typeAhead: false,
    hideTrigger: true,
    enableKeyEvents: true,
    displayField: 'Identification',
    queryMode: 'remote',
    // The filterid defines which addresses to query, defaults to 0 (DK).
    filterid: 0,
    // The maximum number of rows in the responses, defaults to 30.
    maxrows: 30,
    initComponent: function () {
        this.addEvents('searchFinished');

        this.store = Ext.create('Ext.data.JsonStore', {
            storeId: 'storeAdresser',
            model: 'Adresser',
            remoteSort: true,
            proxy: {
                type: 'ajax',
                headers: { 'Content-Type': "application/json" },
                url: '/Webservices/LE34AddressSearchWS.asmx/getLE34List',
                actionMethods: { create: 'POST', read: 'POST', update: 'POST', destroy: 'POST' },
                extraParams: {
                    query: '',
                    epsg: 25832,
                    filterID: this.filterid,
                    limit: this.maxrows,
                    mun: 0,
                    street: 0
                },
                encode: true,
                idIndex: 0,
                root: 'd.Result',
                totalProperty: 'd.Count',
                idProperty: 'Id',

                reader: {
                    type: 'json',
                    root: 'd.Result',
                    totalProperty: 'd.Count',
                    encode: true

                },
                writer: {
                    type: 'json',
                    encode: true
                },
                doRequest: function (operation, callback, scope) {
                    var writer = this.getWriter(),
					request = this.buildRequest(operation, callback, scope);

                    if (operation.allowWrite()) {
                        request = writer.write(request);
                    }

                    var pr = Ext.JSON.encode(operation.request.params);
                    operation.request.params = pr;

                    Ext.apply(request, {
                        headers: this.headers,
                        timeout: this.timeout,
                        scope: this,
                        callback: this.createRequestCallback(request, operation, callback, scope),
                        method: this.getMethod(request)//,
                        //disableCaching: false // explicitly set it to false, ServerProxy handles caching
                    });
                    Ext.Ajax.request(request);
                    return request;
                },
                // sends single sort as multi parameter
                simpleSortMode: true
            },
            listeners: {
                dataChanged: function (store) {
                    var markerRække = -1;
                },
                exception: function (proxy, type, action, options, response, arg) {
                    var responseObject = Ext.util.JSON.decode(response.responseText);
                    //Poseidon.Exception.AjaxExceptionHandler(responseObject);
                    fejlbox(responseObject.Message);
                }
            }

        });
        //        this.storeHouse = Ext.create('Ext.data.JsonStore', {
        //            storeId: 'storeHusnumre',
        //            model: 'Adresser',
        //            remoteSort: true,
        //            proxy: {
        //                type: 'ajax',
        //                headers: { 'Content-Type': "application/json" },
        //                url: '/Webservices/LE34AddressSearchWS.asmx/getHouseList',
        //                actionMethods: { create: 'POST', read: 'POST', update: 'POST', destroy: 'POST' },
        //                extraParams: {
        //                    mun: 0,
        //                    street: 0,
        //                    epsg: 25832,
        //                    filterID: this.filterid
        //                },
        //                encode: true,
        //                idIndex: 0,
        //                root: 'd.Result',
        //                totalProperty: 'd.Count',
        //                idProperty: 'Id',

        //                reader: {
        //                    type: 'json',
        //                    root: 'd.Result',
        //                    totalProperty: 'd.Count',
        //                    encode: true

        //                },
        //                writer: {
        //                    type: 'json',
        //                    encode: true
        //                },
        //                doRequest: function (operation, callback, scope) {
        //                    var writer = this.getWriter(),
        //					request = this.buildRequest(operation, callback, scope);

        //                    if (operation.allowWrite()) {
        //                        request = writer.write(request);
        //                    }

        //                    var pr = Ext.JSON.encode(operation.request.params);
        //                    operation.request.params = pr;

        //                    Ext.apply(request, {
        //                        headers: this.headers,
        //                        timeout: this.timeout,
        //                        scope: this,
        //                        callback: this.createRequestCallback(request, operation, callback, scope),
        //                        method: this.getMethod(request)//,
        //                        //disableCaching: false // explicitly set it to false, ServerProxy handles caching
        //                    });
        //                    Ext.Ajax.request(request);
        //                    return request;
        //                },
        //                // sends single sort as multi parameter
        //                simpleSortMode: true
        //            },
        //            listeners: {
        //                dataChanged: function (store) {
        //                    var markerRække = -1;
        //                },
        //                exception: function (proxy, type, action, options, response, arg) {
        //                    var responseObject = Ext.util.JSON.decode(response.responseText);
        //                    //Poseidon.Exception.AjaxExceptionHandler(responseObject);
        //                    fejlbox(responseObject.Message);
        //                }
        //            }

        //        });

        // Call parent (required)
        GIS34AddressSearchCombo.superclass.initComponent.apply(this, arguments);

        this.on('keyup', function (textfield, event) {
            var userinput = textfield.getRawValue();
            ////var epsg = mapPanel.getProjection().substring(mapPanel.getProjection().indexOf(":") + 1, mapPanel.getProjection().length);
            this.store.proxy.extraParams.query = userinput;
            url: '/Webservices/LE34AddressSearchWS.asmx/getLE34List',
            //var p = { query: userinput, epsg: '25832', filterID: this.filterid + '', limit: this.maxrows + '' };
            //this.store.params = Ext.JSON.stringify(p); // Ext.JSON.encode(p);
            //var params = Ext.JSON.encode();
			this.store.load();
            // var dumm = 'd';
            //this.store.proxy.conn.jsonData = { 'query': userinput, 'epsg': 25832, 'filterID': this.filterid, 'limit': this.maxrows };
            /*
            // hvis listen har et enkelt element, er det ellers umuligt at pile ned og vælge dette element - dette hjælper
            if (event.button == 39 && this.store.getCount() == 1) // pil ned
            {
            //GIS34AddressSearchCombo
            this.select(0, true);
            }
            else //if (textfield.lastQuery != textfield.getRawValue()) 
            {
            this.store.proxy.conn.jsonData = { 'query': userinput, 'epsg': 25832, 'filterID': this.filterid, 'limit': this.maxrows }
            ; //this.requestSearch(textfield.getRawValue());
            }
            */
        });

        this.on('select', function (combo, records, index) {
            var r = records[0];

            //if (this.forceHouseNumber) {
                var komma = r.get('Identification').indexOf(',');
                combo.focus();
                combo.selectText(komma, komma);
            //}
            this.zoom(r.get('Geometry'), r.get('Identification'), r.get('Type'));
            combo.lastQuery = r.get('Identification');
        });
        //        this.on('select', function (combo, records, index) {
        //            var r = records[0];
        //            if (this.forceHouseNumber == true) { 
        //                if (r.get('Type') == 1) {
        //                    this.selectedStreet = r.get('Identification');
        //                    var houseCrit = r.get('Id').split('_');
        //                    this.store.proxy.url = '/Webservices/LE34AddressSearchWS.asmx/getHouseList';
        //                    this.store.proxy.extraParams.mun = houseCrit[0];
        //                    this.store.proxy.extraParams.street = houseCrit[1];
        //                    this.store.load();
        //                }
        //                else if (r.get('Type') == 3) {
        //                    var komma = this.selectedStreet.indexOf(',');
        //                    var combinedStreet = this.selectedStreet.substring(0, komma) + ' ' + r.get('Identification') + this.selectedStreet.substring(komma);
        //                    this.setValue(combinedStreet);
        //                }
        //            }
        //            this.zoom(r.get('Geometry'), r.get('Identification'), r.get('Type'));
        //            combo.lastQuery = r.get('Identification');
        //        });

        //		this.on('searchFinished', function(combo, record, index) {
        //			var geoJsonReader = new OpenLayers.Format.GeoJSON();
        //			var features = geoJsonReader.read(geometry);
        //			geoJsonReader.destroy();
        //	
        //			if (features) {
        //				//an address
        //				if (responseType === 0) {
        //					mapPanel.zoomToAddressLevel(features[0].geometry.x, features[0].geometry.y);
        //				}
        //				//a street
        //				else {
        //					//mapPanel.zoomToExtent(bounds);
        //					bounds = features[0].geometry.getBounds();
        //					mapPanel.zoomToBounds(bounds);
        //				}
        //				//}
        //				//else {
        //				//  Ext.MessageBox.show({ title: 'Information', msg: displayName + ' er uden for kortområdet!', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.INFO });
        //				//}
        //			}
        //		});

    },

    zoom: function (geometry, displayName, responseType) {
        var geoJsonReader = new OpenLayers.Format.GeoJSON();
        var features = geoJsonReader.read(geometry);
        geoJsonReader.destroy();
        var bounds;

        if (this.layer) {
            mapPanel.clearLayers('awsLayer');
            //        if (this.layer) {
            //            this.layer.destroyFeatures();
            //        }

            if (features) {
                //var maxExtent = mapPanel.getMaxExtent();
                //if (maxExtent.left < bounds.left && maxExtent.right > bounds.right && maxExtent.bottom < bounds.bottom && maxExtent.top > bounds.top) {
                if (this.layer) {
                    this.layer.addFeatures(features);
                }
                //an address
                if (responseType === 0) {
                    //if (features[0].geometry.CLASS_NAME === 'OpenLayers.Geometry.Point') {
                    //var point = new OpenLayers.LonLat(features[0].geometry.x, features[0].geometry.y);
                    //mapPanel.zoomToAddressLevel(point);
                    mapPanel.zoomToAddressLevel(features[0].geometry.x, features[0].geometry.y);
                    //mapPanel.setCenter(point, (mapPanel.getZoomForResolution(mapPanel.minResolution)) - 4);
                }
                //a street
                else {
                    //mapPanel.zoomToExtent(bounds);
                    bounds = features[0].geometry.getBounds();
                    mapPanel.zoomToBounds(bounds);
                }
                //}
                //else {
                //  Ext.MessageBox.show({ title: 'Information', msg: displayName + ' er uden for kortområdet!', buttons: Ext.MessageBox.OK, icon: Ext.MessageBox.INFO });
                //}
            }
        } else {
            // split the attributes into proper fields
            var vejnavn = '';
            var husnummer = '';
            var postnummer = '';
            var postdistrikt = '';
            var stednavn = '';
            var x = '';
            var y = '';
            if (responseType == 0) {
                var felter = displayName.split(',');
                var skil = felter[0].lastIndexOf(' ');
                husnummer = felter[0].substring(skil + 1);
                vejnavn = felter[0].substring(0, skil);

                felter[1] = felter[1].substring(1);
                skil = felter[1].lastIndexOf(' (');
                if (skil > 0) {
                    stednavn = felter[1].substring(skil + 2);
                    stednavn = stednavn.substring(0, stednavn.length - 1);
                    felter[1] = felter[1].substring(0, skil);
                }
                skil = felter[1].indexOf(' ');
                postnummer = felter[1].substring(0, skil);
                postdistrikt = felter[1].substring(skil + 1);
                x = features[0].geometry.x;
                y = features[0].geometry.y;
                //"Hølkenhavevej 3G, 8300 Odder (Hølken)"
            }
            this.fireEvent('searchFinished', responseType, vejnavn, husnummer, postnummer, postdistrikt, stednavn, x, y);
        }
    }
});