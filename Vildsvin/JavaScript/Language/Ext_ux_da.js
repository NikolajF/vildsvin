﻿/*************************** Tilføjelser og rettelser af Nikolaj Frandsen, 2011 */

if (Ext.ux.grid.FiltersFeature)
{
	Ext.apply(Ext.ux.grid.FiltersFeature.prototype, {
		menuFilterText: 'Udvælgelse'
	});
}

if (Ext.ux.grid.filter.DateFilter)
{
	Ext.apply(Ext.ux.grid.filter.DateFilter.prototype, {
		afterText: 'Efter',
		beforeText: 'Før',
		onText: 'Præcis',
		dateFormat: 'd-m-Y'
	});
}



/*
if (Ext.ux.grid.filter.Filter)
{
Ext.apply(Ext.ux.grid.filter.Filter.prototype, {
emptyText: 'Indtast kriterium ...'
});
}

if (Ext.ux.grid.filter.NumericFilter)
{
Ext.apply(Ext.ux.grid.filter.NumericFilter.prototype, {
emptyText: 'Indtast kriterium ...'
});
}

if (Ext.ux.grid.filter.StringFilter)
{
Ext.apply(Ext.ux.grid.filter.StringFilter.prototype, {
emptyText: 'Indtast kriterium ...'
});
}
*/

if (Ext.ux.grid.FiltersFeature)
{
	DateFilterMenuItems = ['Før', 'Efter', '-', 'Lig'];
	//NumericFilterMenuItems = ['Under', 'Over', '-', 'Lig'];
	NumericFilterMenuItemCfgs = {
		emptyText: 'Indtast kriterium...'//,
		//selectOnFocus: true,
		//width: 125
	};

	if (Ext.ux.grid.filter.BooleanFilter)
	{
		Ext.apply(Ext.ux.grid.filter.BooleanFilter.prototype, {
			yesText: 'Ja',
			noText: 'Nej'
		});
	}

//    if (Ext.ux.grid.filter.NumericFilter) {
//        Ext.apply(Ext.ux.grid.filter.NumericFilter.prototype, {
//            gtText: 'Over',
//            ltText: 'Under',
//            eqText: 'Lig'
//        });
//    }

    if (Ext.ux.grid.filter.StringFilter)
	{
		Ext.apply(Ext.ux.grid.filter.BooleanFilter.prototype, {
		emptyText: 'Indtast kriterium...'
		});
	}

	if (Ext.ux.grid.filter.NumericFilter)
	{
		Ext.apply(Ext.ux.grid.filter.NumericFilter.prototype, {
			menuItemCfgs: NumericFilterMenuItemCfgs
		});
	}
}


 