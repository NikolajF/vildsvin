﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vildsvin.AddressService {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SearchResponse", Namespace="http://schemas.datacontract.org/2004/07/LE34.AddressService")]
    [System.SerializableAttribute()]
    public partial class SearchResponse : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int CountField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private Vildsvin.AddressService.SearchResult[] ResultField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int StatusCodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string StatusTextField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Count {
            get {
                return this.CountField;
            }
            set {
                if ((this.CountField.Equals(value) != true)) {
                    this.CountField = value;
                    this.RaisePropertyChanged("Count");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public Vildsvin.AddressService.SearchResult[] Result {
            get {
                return this.ResultField;
            }
            set {
                if ((object.ReferenceEquals(this.ResultField, value) != true)) {
                    this.ResultField = value;
                    this.RaisePropertyChanged("Result");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int StatusCode {
            get {
                return this.StatusCodeField;
            }
            set {
                if ((this.StatusCodeField.Equals(value) != true)) {
                    this.StatusCodeField = value;
                    this.RaisePropertyChanged("StatusCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StatusText {
            get {
                return this.StatusTextField;
            }
            set {
                if ((object.ReferenceEquals(this.StatusTextField, value) != true)) {
                    this.StatusTextField = value;
                    this.RaisePropertyChanged("StatusText");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SearchResult", Namespace="http://schemas.datacontract.org/2004/07/LE34.AddressService.BL")]
    [System.SerializableAttribute()]
    public partial class SearchResult : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string GeometryField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string IdentificationField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int TypeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Geometry {
            get {
                return this.GeometryField;
            }
            set {
                if ((object.ReferenceEquals(this.GeometryField, value) != true)) {
                    this.GeometryField = value;
                    this.RaisePropertyChanged("Geometry");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Id {
            get {
                return this.IdField;
            }
            set {
                if ((object.ReferenceEquals(this.IdField, value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Identification {
            get {
                return this.IdentificationField;
            }
            set {
                if ((object.ReferenceEquals(this.IdentificationField, value) != true)) {
                    this.IdentificationField = value;
                    this.RaisePropertyChanged("Identification");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Type {
            get {
                return this.TypeField;
            }
            set {
                if ((this.TypeField.Equals(value) != true)) {
                    this.TypeField = value;
                    this.RaisePropertyChanged("Type");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SearchResponseParsed", Namespace="http://schemas.datacontract.org/2004/07/LE34.AddressService")]
    [System.SerializableAttribute()]
    public partial class SearchResponseParsed : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int CountField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private Vildsvin.AddressService.SearchResultParsed[] ResultField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int StatusCodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string StatusTextField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Count {
            get {
                return this.CountField;
            }
            set {
                if ((this.CountField.Equals(value) != true)) {
                    this.CountField = value;
                    this.RaisePropertyChanged("Count");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public Vildsvin.AddressService.SearchResultParsed[] Result {
            get {
                return this.ResultField;
            }
            set {
                if ((object.ReferenceEquals(this.ResultField, value) != true)) {
                    this.ResultField = value;
                    this.RaisePropertyChanged("Result");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int StatusCode {
            get {
                return this.StatusCodeField;
            }
            set {
                if ((this.StatusCodeField.Equals(value) != true)) {
                    this.StatusCodeField = value;
                    this.RaisePropertyChanged("StatusCode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StatusText {
            get {
                return this.StatusTextField;
            }
            set {
                if ((object.ReferenceEquals(this.StatusTextField, value) != true)) {
                    this.StatusTextField = value;
                    this.RaisePropertyChanged("StatusText");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SearchResultParsed", Namespace="http://schemas.datacontract.org/2004/07/LE34.AddressService.BL")]
    [System.SerializableAttribute()]
    public partial class SearchResultParsed : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string DistrictField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string EastingField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string HouseNumberField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string IdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MunicipalityField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string NorthingField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PlaceNameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string StreetField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int TypeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int ZipCodeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string District {
            get {
                return this.DistrictField;
            }
            set {
                if ((object.ReferenceEquals(this.DistrictField, value) != true)) {
                    this.DistrictField = value;
                    this.RaisePropertyChanged("District");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Easting {
            get {
                return this.EastingField;
            }
            set {
                if ((object.ReferenceEquals(this.EastingField, value) != true)) {
                    this.EastingField = value;
                    this.RaisePropertyChanged("Easting");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string HouseNumber {
            get {
                return this.HouseNumberField;
            }
            set {
                if ((object.ReferenceEquals(this.HouseNumberField, value) != true)) {
                    this.HouseNumberField = value;
                    this.RaisePropertyChanged("HouseNumber");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Id {
            get {
                return this.IdField;
            }
            set {
                if ((object.ReferenceEquals(this.IdField, value) != true)) {
                    this.IdField = value;
                    this.RaisePropertyChanged("Id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Municipality {
            get {
                return this.MunicipalityField;
            }
            set {
                if ((object.ReferenceEquals(this.MunicipalityField, value) != true)) {
                    this.MunicipalityField = value;
                    this.RaisePropertyChanged("Municipality");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Northing {
            get {
                return this.NorthingField;
            }
            set {
                if ((object.ReferenceEquals(this.NorthingField, value) != true)) {
                    this.NorthingField = value;
                    this.RaisePropertyChanged("Northing");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PlaceName {
            get {
                return this.PlaceNameField;
            }
            set {
                if ((object.ReferenceEquals(this.PlaceNameField, value) != true)) {
                    this.PlaceNameField = value;
                    this.RaisePropertyChanged("PlaceName");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Street {
            get {
                return this.StreetField;
            }
            set {
                if ((object.ReferenceEquals(this.StreetField, value) != true)) {
                    this.StreetField = value;
                    this.RaisePropertyChanged("Street");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Type {
            get {
                return this.TypeField;
            }
            set {
                if ((this.TypeField.Equals(value) != true)) {
                    this.TypeField = value;
                    this.RaisePropertyChanged("Type");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ZipCode {
            get {
                return this.ZipCodeField;
            }
            set {
                if ((this.ZipCodeField.Equals(value) != true)) {
                    this.ZipCodeField = value;
                    this.RaisePropertyChanged("ZipCode");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="http://manser.gis34.dk/address", ConfigurationName="AddressService.IAddressService")]
    public interface IAddressService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://manser.gis34.dk/address/IAddressService/SearchHouseNumbers", ReplyAction="http://manser.gis34.dk/address/IAddressService/SearchHouseNumbersResponse")]
        Vildsvin.AddressService.SearchResponse SearchHouseNumbers(int municipality_code, int street_code, int epsg, int filter_code);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://manser.gis34.dk/address/IAddressService/Search", ReplyAction="http://manser.gis34.dk/address/IAddressService/SearchResponse")]
        Vildsvin.AddressService.SearchResponse Search(string query, int epsg, int filter_code, int limit);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://manser.gis34.dk/address/IAddressService/SearchParsed", ReplyAction="http://manser.gis34.dk/address/IAddressService/SearchParsedResponse")]
        Vildsvin.AddressService.SearchResponseParsed SearchParsed(string street, string housenumber, int zipcode, string district, int epsg, int filter_code, int limit);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IAddressServiceChannel : Vildsvin.AddressService.IAddressService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class AddressServiceClient : System.ServiceModel.ClientBase<Vildsvin.AddressService.IAddressService>, Vildsvin.AddressService.IAddressService {
        
        public AddressServiceClient() {
        }
        
        public AddressServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public AddressServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AddressServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AddressServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public Vildsvin.AddressService.SearchResponse SearchHouseNumbers(int municipality_code, int street_code, int epsg, int filter_code) {
            return base.Channel.SearchHouseNumbers(municipality_code, street_code, epsg, filter_code);
        }
        
        public Vildsvin.AddressService.SearchResponse Search(string query, int epsg, int filter_code, int limit) {
            return base.Channel.Search(query, epsg, filter_code, limit);
        }
        
        public Vildsvin.AddressService.SearchResponseParsed SearchParsed(string street, string housenumber, int zipcode, string district, int epsg, int filter_code, int limit) {
            return base.Channel.SearchParsed(street, housenumber, zipcode, district, epsg, filter_code, limit);
        }
    }
}
