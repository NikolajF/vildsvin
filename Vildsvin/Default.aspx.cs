﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Vildsvin;
using System.Configuration;
using System.Net;
using System.IO;

namespace Vildsvin
{
    public partial class Default : System.Web.UI.Page
    {
		string svin_ticket = Guid.NewGuid().ToString(); // + "-34h78g1";
		private ScriptManager scriptManager;


        private void skrivInfo()
        {
            System.Web.HttpBrowserCapabilities browser = Request.Browser;
            string browserinfo = "browsertype ='" + browser.Type + "', browser ='" + browser.Browser + "', browserversion='" + browser.Version + "'" +
                ", platform ='" + browser.Platform + "', cookies ='" + browser.Cookies + "', mobil ='" + browser.IsMobileDevice + "'" +
                ", javascript = '" + browser.EcmaScriptVersion.ToString() + "', javascript_version = '" + browser["JavaScriptVersion"] + "', w3c_dom_version = '" + browser.W3CDomVersion + "'";

            System.Diagnostics.Debug.Print(Request.ServerVariables["HTTP_ACCEPT_LANGUAGE"].ToString());
        }


        protected void Page_Load(object sender, EventArgs e)
		{
            //skrivInfo();
            HttpCookie cookie = new HttpCookie("svin_ticket");
            cookie.Value = svin_ticket;
            Response.Cookies.Add(cookie);
			bool NetworkMap = Convert.ToBoolean(ConfigurationManager.AppSettings["NetworkMap"]);
			get_gst_ticket(NetworkMap);

			// skriv i databasen
			svin_util ut = new svin_util(true);
            //svin_ticket = 
            ut.tjekBruger("start", "", "", svin_ticket);
            // skriv brugervariable på klient-siden

            // tjek browser-type
            System.Web.HttpBrowserCapabilities browser = Request.Browser;
            //string s = "Browser Capabilities\n"
            //    + "Type = " + browser.Type + "\n"
            //    + "Name = " + browser.Browser + "\n"
            //    + "Version = " + browser.Version + "\n"
            //    + "Major Version = " + browser.MajorVersion + "\n"
            //    + "Minor Version = " + browser.MinorVersion + "\n"
            //    + "Platform = " + browser.Platform + "\n"
            //    + "Is Beta = " + browser.Beta + "\n"
            //    + "Is Crawler = " + browser.Crawler + "\n"
            //    + "Is AOL = " + browser.AOL + "\n"
            //    + "Is Win16 = " + browser.Win16 + "\n"
            //    + "Is Win32 = " + browser.Win32 + "\n"
            //    + "Supports Frames = " + browser.Frames + "\n"
            //    + "Supports Tables = " + browser.Tables + "\n"
            //    + "Supports Cookies = " + browser.Cookies + "\n"
            //    + "Supports VBScript = " + browser.VBScript + "\n"
            //    + "Supports JavaScript = " +
            //        browser.EcmaScriptVersion.ToString() + "\n"
            //    + "Supports Java Applets = " + browser.JavaApplets + "\n"
            //    + "Supports ActiveX Controls = " + browser.ActiveXControls
            //          + "\n"
            //    + "Supports JavaScript Version = " +
            //        browser["JavaScriptVersion"] + "\n";

            //this.Page.ClientScript.RegisterClientScriptBlock(typeof(string), "Browsertype", s, true);

        }

        protected override void OnLoad(EventArgs e)
        {
            scriptManager = new ScriptManager();

            base.OnLoad(e);
        }

        private void LoadScripts()
        {
            //this.Page.ClientScript.RegisterClientScriptInclude("OpenLayers", ResolveUrl("~/Libraries/OpenLayers-2.12/OpenLayers.js")); //runtime
//			this.Page.ClientScript.RegisterClientScriptInclude("OpenLayers", ResolveUrl("~/Libraries/OpenLayers-2.12/OpenLayers.debug.js")); //runtime
			this.Page.ClientScript.RegisterClientScriptInclude("OpenLayers", ResolveUrl("~/Libraries/OpenLayers-2.13/OpenLayers.debug.js")); //runtime
			// ext-base skal være debug, der er egne rettelser i den
            //this.Page.ClientScript.RegisterClientScriptInclude("ExtBase", ResolveUrl("~/Libraries/Ext/adapter/ext/ext-base-debug.js"));
            //this.Page.ClientScript.RegisterClientScriptInclude("ExtBase", ResolveUrl("~/Libraries/Ext/adapter/ext/ext-base.js"));
            //this.Page.ClientScript.RegisterClientScriptInclude("ExtAll", ResolveUrl("~/Libraries/Ext/ext-all.js")); 
            this.Page.ClientScript.RegisterClientScriptInclude("ExtAll", ResolveUrl("~/Libraries/Ext 41/ext-all-debug-w-comments.js"));
			this.Page.ClientScript.RegisterClientScriptInclude("UxCheckColumn", ResolveUrl("~/Libraries/Ext 41/examples/ux/CheckColumn.js"));
			this.Page.ClientScript.RegisterClientScriptInclude("UxPreviewPlugin", ResolveUrl("~/Libraries/Ext 41/examples/ux/PreviewPlugin.js"));
            this.Page.ClientScript.RegisterClientScriptInclude("ExtLangDa", ResolveUrl("~/Libraries/Ext 41/locale/ext-lang-da.js"));

			// proj4s til transformation af projektioner
			this.Page.ClientScript.RegisterClientScriptInclude("proj1", "https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.3.12/proj4-src.js");
			this.Page.ClientScript.RegisterClientScriptInclude("proj2", "https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.3.12/proj4.js");

			//geoext
			this.Page.ClientScript.RegisterClientScriptInclude("GeoExt", ResolveUrl("~/Libraries/GeoExt2/src/GeoExt/Loader.js"));
			//this.Page.ClientScript.RegisterClientScriptInclude("GeoExt", ResolveUrl("~/Libraries/GeoExt2/src/GeoExt/GeoExt.js"));
			//this.Page.ClientScript.RegisterClientScriptInclude("GeoExt_Action", ResolveUrl("~/Libraries/GeoExt2/src/GeoExt/Action.js"));
			//this.Page.ClientScript.RegisterClientScriptInclude("GeoExt_Map", ResolveUrl("~/Libraries/GeoExt2/src/GeoExt/panel/Map.js"));
			//this.Page.ClientScript.RegisterClientScriptInclude("GeoExt_Legend", ResolveUrl("~/Libraries/GeoExt2/src/GeoExt/panel/Legend.js"));
			//this.Page.ClientScript.RegisterClientScriptInclude("GeoExt_LayerLegend", ResolveUrl("~/Libraries/GeoExt2/src/GeoExt/container/LayerLegend.js"));
			//this.Page.ClientScript.RegisterClientScriptInclude("GeoExt_LayerStore", ResolveUrl("~/Libraries/GeoExt2/src/GeoExt/data/LayerStore.js"));

			// benyttet af login dialog
			//this.Page.ClientScript.RegisterClientScriptInclude("bootstrap", ResolveUrl("~/Libraries/Ext 41/bootstrap.js"));

            this.Page.ClientScript.RegisterClientScriptInclude("UxGridFilter", ResolveUrl("~/Libraries/Ext 41/examples/ux/grid/FiltersFeature.js"));
            this.Page.ClientScript.RegisterClientScriptInclude("Filter", ResolveUrl("~/Libraries/Ext 41/examples/ux/grid/filter/Filter.js"));
            this.Page.ClientScript.RegisterClientScriptInclude("StringFilter", ResolveUrl("~/Libraries/Ext 41/examples/ux/grid/filter/StringFilter.js"));
            this.Page.ClientScript.RegisterClientScriptInclude("DateFilter", ResolveUrl("~/Libraries/Ext 41/examples/ux/grid/filter/DateFilter.js"));
			//this.Page.ClientScript.RegisterClientScriptInclude("DateTimeFilter", ResolveUrl("~/Libraries/Ext 41/examples/ux/grid/filter/DateTimeFilter.js")); //kun i 4.2
			this.Page.ClientScript.RegisterClientScriptInclude("BooleanFilter", ResolveUrl("~/Libraries/Ext 41/examples/ux/grid/filter/BooleanFilter.js"));
            this.Page.ClientScript.RegisterClientScriptInclude("ListFilter", ResolveUrl("~/Libraries/Ext 41/examples/ux/grid/filter/ListFilter.js"));
            this.Page.ClientScript.RegisterClientScriptInclude("NumericFilter", ResolveUrl("~/Libraries/Ext 41/examples/ux/grid/filter/NumericFilter.js"));
            this.Page.ClientScript.RegisterClientScriptInclude("ListMenu", ResolveUrl("~/Libraries/Ext 41/examples/ux/grid/menu/ListMenu.js"));
            this.Page.ClientScript.RegisterClientScriptInclude("RangeMenu", ResolveUrl("~/Libraries/Ext 41/examples/ux/grid/menu/RangeMenu.js"));
			// dansk oversættelse til gridfilter
			this.Page.ClientScript.RegisterClientScriptInclude("UxLangDa", ResolveUrl("~/JavaScript/Language/Ext_ux_da.js"));
			//encryption
			//this.Page.ClientScript.RegisterClientScriptInclude("encrypt", ResolveUrl("~/Libraries/sjcl.js"));
			//this.Page.ClientScript.RegisterClientScriptInclude("encrypt", ResolveUrl("~/Libraries/encryption.js"));
			this.Page.ClientScript.RegisterClientScriptInclude("encrypt", ResolveUrl("~/Libraries/aes.js"));

			this.Page.ClientScript.RegisterClientScriptInclude("AdresseCombo", ResolveUrl("~/JavaScript/GIS34AddressSearchCombo.js"));
			this.Page.ClientScript.RegisterClientScriptInclude("AdresseSoegning", ResolveUrl("~/JavaScript/LE34AddressSearchLocal.js"));
         
            this.Page.ClientScript.RegisterClientScriptInclude("Menuer", ResolveUrl("~/Formularer/menu.js"));
			this.Page.ClientScript.RegisterClientScriptInclude("Kontroller", ResolveUrl("~/Formularer/kontroller.js"));
			this.Page.ClientScript.RegisterClientScriptInclude("mappanel", ResolveUrl("~/Formularer/mappanel.js"));
			//this.Page.ClientScript.RegisterClientScriptInclude("Opslag", ResolveUrl("~/Formularer/opslag.js"));
			this.Page.ClientScript.RegisterClientScriptInclude("Login", ResolveUrl("~/Formularer/frm_login.js"));
			this.Page.ClientScript.RegisterClientScriptInclude("indberetning_telefon", ResolveUrl("~/Formularer/indberetning_telefon.js"));
			this.Page.ClientScript.RegisterClientScriptInclude("Levende og spor", ResolveUrl("~/Formularer/frm_levende.js"));
            this.Page.ClientScript.RegisterClientScriptInclude("Indsamlinger", ResolveUrl("~/Formularer/frm_indsamling.js"));
			this.Page.ClientScript.RegisterClientScriptInclude("frm_lab", ResolveUrl("~/Formularer/frm_lab.js"));
			this.Page.ClientScript.RegisterClientScriptInclude("Analyser", ResolveUrl("~/Formularer/frm_lab_analyser.js"));
			this.Page.ClientScript.RegisterClientScriptInclude("Brugere", ResolveUrl("~/Formularer/frm_brugere.js"));
            this.Page.ClientScript.RegisterClientScriptInclude("bredder", ResolveUrl("~/Formularer/dlg_bredder.js"));
            this.Page.ClientScript.RegisterClientScriptInclude("Forside", ResolveUrl("~/Formularer/frm_forside.js"));
			this.Page.ClientScript.RegisterClientScriptInclude("FindSvin", ResolveUrl("~/Formularer/frm_find.js"));

			// login dialog           
			//this.Page.ClientScript.RegisterClientScriptInclude("IconCombo", ResolveUrl("~/logindialog/IconCombo.js"));
			//this.Page.ClientScript.RegisterClientScriptInclude("overrides", ResolveUrl("~/logindialog/overrides.js"));
			//this.Page.ClientScript.RegisterClientScriptInclude("LoginDialog", ResolveUrl("~/logindialog/LoginDialog.js"));


			/*
                        this.Page.ClientScript.RegisterClientScriptInclude("GeoExt", ResolveUrl("~/Libraries/GeoExt/GeoExt.js"));
                        // ux-all skal være debug, der er egne rettelser i den
                        this.Page.ClientScript.RegisterClientScriptInclude("ExtUx", ResolveUrl("~/Libraries/Ext 34/ux/examples/ux-all-debug.js"));
                        this.Page.ClientScript.RegisterClientScriptInclude("ExtLangDa", ResolveUrl("~/Libraries/Ext 34/src/locale/ext-lang-da.js"));
                        // egne tilføjelser til oversættelsen
                        this.Page.ClientScript.RegisterClientScriptInclude("Ext_ux_bier_da", ResolveUrl("~/JavaScript/Language/Ext_ux_bier_da.js"));


             */
		}
		protected override void OnPreRender(EventArgs e)
        {
            LoadScripts();
            base.OnPreRender(e);
        }

		private void get_gst_ticket(bool benyt)
		{
			HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create("http://kortforsyningen.kms.dk/?request=GetTicket&login=" + ConfigurationManager.AppSettings["KMSlogin"] + "&password=" + ConfigurationManager.AppSettings["KMSpassword"]);
			string ticket = "var kmsticket='";
			if (benyt)
			{
				try
				{
				Stream res = req.GetResponse().GetResponseStream();
				StreamReader reader = new StreamReader(res);
				ticket += reader.ReadToEnd() + "';";
			}
			catch
			{
				ticket += "';";
			}
			}
			else
				ticket += "';";
			this.Page.ClientScript.RegisterClientScriptBlock(typeof(string), "KMS ticket", ticket, true);
		
		}
    }
}