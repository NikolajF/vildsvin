﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//using System.Collections;
using System.Configuration;
//using System.Data;
//using System.IO;
//using System.Text;
//using System.Net;
//using System.Web.Services;
//using System.Web.Services.Protocols;
using System.Web.SessionState;

namespace Vildsvin
{
	/// <summary>
	/// Summary description for KF_Proxy
	/// </summary>
	public class KF_Proxy : IHttpHandler, IRequiresSessionState
	{

		public void ProcessRequest(HttpContext context)
		{
//			context.Response.ContentType = "text/plain";
	//		context.Response.Write("Hello World");
			string KMSServerUrl = ConfigurationManager.AppSettings["KMSServerUrl"];
			string url = KMSServerUrl + context.Request.Url.Query;
			ProxyService.ProxyRequest(ref context, url, null);
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}



