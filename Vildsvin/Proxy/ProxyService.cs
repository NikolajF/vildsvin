﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Configuration;

/// <summary>
/// Common Proxy service class. Handles image and text requests
/// </summary>
public class ProxyService
{
    /// <summary>
    /// Proxy the request given by the url parameter
    /// </summary>
    /// <param name="context">An http context to be used for the web request. 
    /// The request of the context may contain data to post during the proxy request.
    /// The parameter is a "ref" parameter and the response of the context will be the response of the url request
    /// </param>
    /// <param name="url">url to request</param>
    public static void ProxyRequest(ref HttpContext context, string url, string cqlfilter=null)
    {
        try
        {
            WebRequest request = WebRequest.Create(url);
            //WebRequest request = WebRequest.Create(url + "?" + cqlfilter);
            request.Credentials = CredentialCache.DefaultCredentials;
			request.Method = "POST";
			request.ContentType ="application/x-www-form-urlencoded";
            if (cqlfilter != null)
            {
                byte[] buffer = Encoding.UTF8.GetBytes(cqlfilter);
                Stream reqstr = request.GetRequestStream();
                reqstr.Write(buffer, 0, buffer.Length);
                reqstr.Close();
            }


            // Get the response and content-type
            WebResponse response = request.GetResponse();
            Stream stream = response.GetResponseStream();

            //context.Response.Cache.SetCacheability(HttpCacheability.Private);

            // Set expires to the past if no expires has been given.
            if (response.Headers["Expires"] == null)
            {
                response.Headers["Expires"] = (new DateTime(1970, 1, 1)).ToString();
                context.Response.CacheControl = null;                
            }

            DateTime expires = DateTime.Parse(response.Headers["Expires"].ToString());
            TimeSpan duration = new TimeSpan(0); ;
            if (expires > DateTime.Now)
            {
                duration = expires.Subtract(DateTime.Now);
            }

            context.Response.Cache.SetExpires(expires);            
            context.Response.Cache.SetMaxAge(duration);

            if (response.ContentType.ToLower().StartsWith("image") || response.ContentType == "")
            {
                // "IMAGE" proxy               
                context.Response.ContentType = response.ContentType;

                Image image = Image.FromStream(stream);
                ImageFormat format = image.RawFormat;

                MemoryStream ms = new MemoryStream();
                image.Save(ms, format);
                ms.WriteTo(context.Response.OutputStream);
            }
            else
            {
                // "TEXT" proxy
                // Always read and write stream as UTF-8, i.e. the external WFS/WMS services must use UTF-8 encoding
                context.Response.ContentType = response.ContentType.Replace("iso-8859-1", "utf-8");
                context.Response.Charset = "UTF-8";// ((HttpWebResponse)response).CharacterSet;
                context.Response.ContentEncoding = Encoding.GetEncoding(context.Response.Charset);

                // Choose encoding based on correspondance in the URL. Very naive implementation!!!
                Encoding currentEncoding;
                //if (url.IndexOf("kort.plansystem.dk") >= 0)
                if (url.IndexOf("wfs.plansystem.dk") >= 0)
                {
                    currentEncoding = Encoding.GetEncoding("ISO-8859-1");
                }
                else
                {
                    // Use UTF-8 as default encoding.
                    currentEncoding = Encoding.UTF8;
                }

                // Open the stream using a StreamReader.
                StreamReader reader;
                // Depending on the encoding, the correct scheme is chosen.
                if (currentEncoding == Encoding.GetEncoding("ISO-8859-1"))
                    reader = new StreamReader(stream, Encoding.GetEncoding("ISO-8859-1"));
                else // UTF-8
                    reader = new StreamReader(stream, context.Response.ContentEncoding); // Default chosen by viskort.dk
                
                // Read the stream and close the reader.
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                responseFromServer = responseFromServer.Replace(ConfigurationManager.AppSettings["WFSfra"], ConfigurationManager.AppSettings["WFStil"]);
                // Convert encoding if necessary.
                if (currentEncoding == Encoding.GetEncoding("ISO-8859-1"))
                {
                    Encoding targetEncoding = Encoding.UTF8;
                    byte[] currentEncodingBytes = currentEncoding.GetBytes(responseFromServer);
                    byte[] targetEncodingBytes = Encoding.Convert(currentEncoding, targetEncoding, currentEncodingBytes);
                    responseFromServer = targetEncoding.GetString(targetEncodingBytes);
                }
                System.Diagnostics.Debug.Print("SVAR: " + responseFromServer + "\r\n");

                // Write the response to the original request
                context.Response.Write(responseFromServer);

                context.Response.Flush();
            }
            
            stream.Close();
            response.Close();
        }
        catch (WebException ex)
        {
            // In case of a web exception the server error (code=500) is returned.
			context.Response.Write(ex.Message); 
			context.Response.StatusCode = 500;
            return;
        }
    }

}
