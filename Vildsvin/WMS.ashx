﻿<%@ WebHandler Language="C#" Class="WMS" %>

using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.SessionState;

using LE34.Data.Access;

/// <summary>
/// A proxy class to proxy HTTP calls to a local WMS server installation, e.g. GeoServer
/// </summary>
public class WMS : IHttpHandler, IRequiresSessionState
{
    /// <summary>
    /// Implements the IHttpHandler.ProcessRequest
    /// WMSServerUrl configured in the web.config file is used as the destination host url
    /// </summary>
    /// <param name="context"></param>
    public void ProcessRequest(HttpContext context)
    {                
		// oversæt GET til POST
		string WMSServerUrl = ConfigurationManager.AppSettings["WMSServerUrl"];
		// sæt CQL_FILTER ud fra den sidst udførte forespørgsel til grid		
		HttpCookie cookie = HttpContext.Current.Request.Cookies["svin_ticket"];
		string svin_cookie = cookie.Value;
		
        DataBase db = new DataBase("", ConfigurationManager.ConnectionStrings["svin_npgsql"].ConnectionString);
		string cq = null;
		try
		{
			cq = db.SingleSelect<string>("select kort_filter from vildsvin.log_stat where cookie ='" + svin_cookie + "'");
		}
		catch { }
        db.Close();
		string url = WMSServerUrl;
		string parm = context.Request.Url.Query.Substring(1);
		if (cq != null && cq != "nej")
		{
			parm = "CQL_FILTER=" + cq +"&" + parm;
		}
		//else
			//cq = context.Request.Url.Query;
	
		ProxyService.ProxyRequest(ref context, url, parm);
    }
    
    /// <summary>
    /// Implements IHttpHandler.IsReusable
    /// Returns false
    /// </summary>
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}