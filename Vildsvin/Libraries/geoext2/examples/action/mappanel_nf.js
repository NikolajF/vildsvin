/*
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 * 
 * Published under the BSD license.
 * See https://github.com/geoext/geoext2/blob/master/license.txt for the full
 * text of the license.
 */
var kmsticket='4cb5e33be229004e6048dc7d63a62a57';
var mapPanel;
Ext.require([
    'Ext.container.Viewport',
    'Ext.window.MessageBox',
    'GeoExt.panel.Map',
    'GeoExt.Action'
]);

Ext.application({
    name: 'ActionExample',
    launch: function(){
		//http://kortforsyningen.kms.dk/service?service=META&request=GetTicket&login=moenatlas&password=moenatlas
		var layers = createLayers();
		// Create the map.
		var map = createMap();
		// Add the layers to the map.
		addLayers(map, layers);

        // var map = new OpenLayers.Map({});
        // map.addControl(new OpenLayers.Control.LayerSwitcher());
        // var wms = new OpenLayers.Layer.WMS(
            // "OpenLayers WMS",
            // "http://vmap0.tiles.osgeo.org/wms/vmap0?",
            // {layers: 'basic'}
        // );

        // var vector = new OpenLayers.Layer.Vector("vector");
        // map.addLayers([wms, vector]);
        
        var ctrl, toolbarItems = [], action, actions = {};
        
        // ZoomToMaxExtent control, a "button" control
        action = Ext.create('GeoExt.Action', {
            control: new OpenLayers.Control.ZoomToMaxExtent(),
            map: map,
            text: "max extent",
            tooltip: "zoom to max extent"
        });
        actions["max_extent"] = action;
        toolbarItems.push(Ext.create('Ext.button.Button', action));
        toolbarItems.push("-");
        
        // Navigation control and DrawFeature controls
        // in the same toggle group
        action = Ext.create('GeoExt.Action', {
            text: "nav",
            control: new OpenLayers.Control.Navigation(),
            map: map,
            // button options
            toggleGroup: "draw",
            allowDepress: false,
            pressed: true,
            tooltip: "navigate",
            // check item options
            group: "draw",
            checked: true
        });
        actions["nav"] = action;
        toolbarItems.push(Ext.create('Ext.button.Button', action));
        
        action = Ext.create('GeoExt.Action', {
            text: "draw poly",
            control: new OpenLayers.Control.DrawFeature(layers.selections.SelectedPoint, OpenLayers.Handler.Polygon),
            map: map,
            // button options
            toggleGroup: "draw",
            allowDepress: false,
            tooltip: "draw polygon",
            // check item options
            group: "draw"
        });
        actions["draw_poly"] = action;
        toolbarItems.push(Ext.create('Ext.button.Button', action));
        
        action = Ext.create('GeoExt.Action', {
            text: "draw line",
            control: new OpenLayers.Control.DrawFeature(layers.selections.SelectedPoint, OpenLayers.Handler.Path),
            map: map,
            // button options
            toggleGroup: "draw",
            allowDepress: false,
            tooltip: "draw line",
            // check item options
            group: "draw"
        });
        actions["draw_line"] = action;
        toolbarItems.push(Ext.create('Ext.button.Button', action));
        toolbarItems.push("-");
        
        // SelectFeature control, a "toggle" control
        action = Ext.create('GeoExt.Action', {
            text: "select",
            control: new OpenLayers.Control.SelectFeature(layers.selections.SelectedPoint, {
                type: OpenLayers.Control.TYPE_TOGGLE,
                hover: true
            }),
            map: map,
            // button options
            enableToggle: true,
            tooltip: "select feature"
        });
        actions["select"] = action;
        toolbarItems.push(Ext.create('Ext.button.Button', action));
        toolbarItems.push("-");
        
        // Navigation history - two "button" controls
        ctrl = new OpenLayers.Control.NavigationHistory();
        map.addControl(ctrl);
        
        action = Ext.create('GeoExt.Action', {
            text: "previous",
            control: ctrl.previous,
            disabled: true,
            tooltip: "previous in history"
        });
        actions["previous"] = action;
        toolbarItems.push(Ext.create('Ext.button.Button', action));
        
        action = Ext.create('GeoExt.Action', {
            text: "next",
            control: ctrl.next,
            disabled: true,
            tooltip: "next in history"
        });
        actions["next"] = action;
        toolbarItems.push(Ext.create('Ext.button.Button', action));
        toolbarItems.push("->");
        
        // Reuse the GeoExt.Action objects created above
        // as menu items
        toolbarItems.push({
            text: "menu",
            menu: Ext.create('Ext.menu.Menu', {
                items: [
                    // ZoomToMaxExtent
                    Ext.create('Ext.button.Button', actions["max_extent"]),
                    // Nav
                    Ext.create('Ext.menu.CheckItem', actions["nav"]),
                    // Draw poly
                    Ext.create('Ext.menu.CheckItem', actions["draw_poly"]),
                    // Draw line
                    Ext.create('Ext.menu.CheckItem', actions["draw_line"]),
                    // Select control
                    Ext.create('Ext.menu.CheckItem', actions["select"]),
                    // Navigation history control
                    Ext.create('Ext.button.Button', actions["previous"]), 
                    Ext.create('Ext.button.Button', actions["next"])
                ]
            })
        });
        
        mapPanel = Ext.create('GeoExt.panel.Map', {
            //title: 'Using GeoExt.Action instances in various places',
            map: map,
			extent: '437000, 6043000, 902000, 6405000',
			config: {
				//map: map,
//				config: {
					layers: layers//,
//					controls: controls,
//					tools: tools
	//			}//,
//				tbar: this.makeToolbar(tools)
			},
			addSelectedPoint: function (x, y) {
				var vector = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Point(x, y));
				this.config.layers.selections.SelectedPoint.addFeatures(vector);
			},
			zoomToAddressLevel: function(x, y, level) {
				// slet tidligere søgning
				var point = new OpenLayers.LonLat(x, y);
				this.map.setCenter(point, level, false, true);
			}

			//Ext.apply(this, config);
            // dockedItems: [{
                // xtype: 'toolbar',
                // dock: 'top',
                // items: toolbarItems
            // }]
        });
        
        
        Ext.create('Ext.container.Viewport', {
            layout: 'fit',
            items: [mapPanel]
        });
        
		mapPanel.addSelectedPoint(610000,6100000);
		mapPanel.zoomToAddressLevel(610000,6100000,11);
    }
});


function createMap() {
	return new OpenLayers.Map('', {
		projection: 'EPSG:25832',
		units: 'm',
		maxExtent: new OpenLayers.Bounds(100000, 6000000, 1000000, 6500000),
		//minResolution: 0.1, //0.125,
		//maxResolution: 1000,
		minResolution: 0.05,
		maxResolution: 1638.40,
		restrictedExtent: new OpenLayers.Bounds(100000, 6000000, 1000000, 6500000),
		controls: [
			new OpenLayers.Control.PanZoomBar({ zoomWorldIcon: 'true' }),
			new OpenLayers.Control.ScaleLine({
				maxWidth: 500,
				topInUnits: 'm',
				topOutUnits: 'km',
				bottomInUnits: '',
				bottomOutUnits: '',
				geodesic: false
			}),
			new OpenLayers.Control.LayerSwitcher()
		]
	});
}

function createLayers() {
	var layers = {
		background: {
			kmsmap: new OpenLayers.Layer.WMS(
			'Kort',
			['http://a.kortforsyningen.kms.dk/service', 'http://b.kortforsyningen.kms.dk/service', 'http://c.kortforsyningen.kms.dk/service', 'http://d.kortforsyningen.kms.dk/service', 'http://e.kortforsyningen.kms.dk/service', 'http://f.kortforsyningen.kms.dk/service', 'http://g.kortforsyningen.kms.dk/service', 'http://h.kortforsyningen.kms.dk/service', 'http://i.kortforsyningen.kms.dk/service', 'http://j.kortforsyningen.kms.dk/service', 'http://k.kortforsyningen.kms.dk/service', 'http://l.kortforsyningen.kms.dk/service', 'http://m.kortforsyningen.kms.dk/service'],
					//'/kms.ashx',
			{
				ticket: kmsticket,
				service: 'wms',
				format: 'image/jpeg',
				srs: 'EPSG:25832',
				request: 'GetMap',
				version: '1.1.1',
				servicename: 'topo_skaermkort',
				layers: 'dtk_skaermkort',
				tiled: true,
				tilesOrigin: '100000,6000000'
			}, 
			{
				displayInLayerSwitcher: true,
				buffer: 1,
				isBaseLayer: true,
				ratio: 0,
				singleTile: false,
				visibility: true,
				transitionEffect: 'resize',
				displayOutsideMaxExtent: true
			}
		),
		grayscale: new OpenLayers.Layer.WMS(
			'Sort/hvidt kort',
			['http://a.kortforsyningen.kms.dk/service', 'http://b.kortforsyningen.kms.dk/service', 'http://c.kortforsyningen.kms.dk/service', 'http://d.kortforsyningen.kms.dk/service', 'http://e.kortforsyningen.kms.dk/service', 'http://f.kortforsyningen.kms.dk/service', 'http://g.kortforsyningen.kms.dk/service', 'http://h.kortforsyningen.kms.dk/service', 'http://i.kortforsyningen.kms.dk/service', 'http://j.kortforsyningen.kms.dk/service', 'http://k.kortforsyningen.kms.dk/service', 'http://l.kortforsyningen.kms.dk/service', 'http://m.kortforsyningen.kms.dk/service'],
			{
				ticket: kmsticket,
				service: 'wms',
				format: 'image/jpeg',
				srs: 'EPSG:25832',
				request: 'GetMap',
				version: '1.1.1',
				servicename: 'topo_skaermkort',
				layers: 'dtk_skaermkort',
				manipulation: 'greyscale',
				tiled: true,
				tilesOrigin: '100000,6000000'
			},
			{
				displayInLayerSwitcher: true,
				buffer: 1,
				isBaseLayer: true,
				displayOutsideMaxExtent: true,
				transitionEffect: 'resize',
				visibility: true,
				singleTile: false,
				ratio: 0
			}
		),
		ortho: new OpenLayers.Layer.WMS(
			'Foto',
			['http://a.kortforsyningen.kms.dk/service', 'http://b.kortforsyningen.kms.dk/service', 'http://c.kortforsyningen.kms.dk/service', 'http://d.kortforsyningen.kms.dk/service', 'http://e.kortforsyningen.kms.dk/service', 'http://f.kortforsyningen.kms.dk/service', 'http://g.kortforsyningen.kms.dk/service', 'http://h.kortforsyningen.kms.dk/service', 'http://i.kortforsyningen.kms.dk/service', 'http://j.kortforsyningen.kms.dk/service', 'http://k.kortforsyningen.kms.dk/service', 'http://l.kortforsyningen.kms.dk/service', 'http://m.kortforsyningen.kms.dk/service'],
			{
				ticket: kmsticket,
				service: 'wms',
				layers: 'orto_foraar',
				servicename: 'orto_foraar',
				version: '1.1.1',
				request: 'GetMap',
				srs: 'EPSG:25832',
				format: 'image/jpeg',
				bgcolor: '0xFFFFFF',
				tiled: true,
				tilesOrigin: '100000,6000000'
			},
			{
				isBaseLayer: true,
				buffer: 1,
				displayInLayerSwitcher: true,
				ratio: 0,
				singleTile: false,
				visibility: true,
				transitionEffect: 'resize',
				displayOutsideMaxExtent: true
			}
		),
		blank: new OpenLayers.Layer(
			'Intet',
			{
				displayInLayerSwitcher: true,
				isBaseLayer: true,
				visibility: false,
				displayOutsideMaxExtent: false,
				singleTile: false,
				tiled: true,
				tilesOrigin: '100000,6000000',
				buffer: 0,
				ratio: 0
			}
		)}
	};

	layers.data = {};

	layers.info = {};
	layers.selections = {};
	// Add coordinate layer.
	layers.selections.coordinateLayer = new OpenLayers.Layer.Vector(
		"Koordinater",
		{
			styleMap: new OpenLayers.StyleMap({
				'default': new OpenLayers.Style(
				null,
				{
					rules: [
						new OpenLayers.Rule(
						{
							symbolizer: {
								"Point": {
									pointRadius: 12,
									graphicName: 'cross',
									fillColor: '#0000FF',
									fillOpacity: 1,
									strokeWidth: 1,
									strokeOpacity: 0,
									strokeColor: '#0000FF',
									strokeDashstyle: 'solid',
									externalGraphic: '',
									graphicWidth: 1,
									graphicHeight: 1
								}
							}
						})
					]
				})
			})
			}, 
		{
			displayInLayerSwitcher: false,
			isBaseLayer: false
		}
	);
	//Add point info layer.
	layers.selections.SelectedPoint = new OpenLayers.Layer.Vector(
		"vector",
		{
			styleMap: new OpenLayers.StyleMap({
				'default': new OpenLayers.Style(
				null,
				{
					rules: [
						new OpenLayers.Rule(
						{
							symbolizer: {
								"Point": {
									pointRadius: 15,
									graphicName: 'circle',
									fillColor: '#FF00FF',
									fillOpacity: 0,
									strokeWidth: 1,
									strokeOpacity: 1,
									strokeColor: '#FF0000',
									strokeDashstyle: 'solid',
									externalGraphic: '',
									graphicWidth: 1,
									graphicHeight: 1
								}
							}
								})
							]
						})
					})
				}, 
				{
					displayInLayerSwitcher: false,
					isBaseLayer: false
				}
	);
	// Add AWS layer.
	layers.selections.awsLayer = new OpenLayers.Layer.Vector(
	"Adressesøgning",
	{
		styleMap: new OpenLayers.StyleMap({
			'default': new OpenLayers.Style(
			null,
			{
				rules: [
					new OpenLayers.Rule(
					{
						symbolizer: {
							"Point": {
								pointRadius: 5,
								graphicName: 'circle',
								fillColor: '#0000FF',
								fillOpacity: 1,
								strokeWidth: 1,
								strokeOpacity: 1,
								strokeColor: '#0000FF',
								strokeDashstyle: 'solid',
								externalGraphic: '',
								graphicWidth: 1,
								graphicHeight: 1
							},
							'Polygon': {
								fillColor: '#EF9A00',
								fillOpacity: 0,
								strokeWidth: 2,
								strokeOpacity: 1,
								strokeColor: '#0000FF',
								strokeDashstyle: 'dash'
							}
						}
					})
				]
			})
		})
		}, {
		displayInLayerSwitcher: false,
		isBaseLayer: false,
		visibility: true
		}
	);
	return layers;
}

function addLayers(map, layers){
	for (layer in layers.background)
	{
		map.addLayer(layers.background[layer]);
	}
	for (layer in layers.data)
	{
		map.addLayer(layers.data[layer]);
	}
	for (layer in layers.udbrud)
	{
		map.addLayer(layers.udbrud[layer]);
	}
	for (layer in layers.info)
	{
		map.addLayer(layers.info[layer]);
	}
	for (layer in layers.selections)
	{
		map.addLayer(layers.selections[layer]);
		layers.selections[layer].addOptions({ 'displayInLayerSwitcher': false });
	}
}


