﻿--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.6
-- Dumped by pg_dump version 9.6.6

-- Started on 2018-09-09 18:32:23

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = vildsvin, pg_catalog;

--
-- TOC entry 4993 (class 0 OID 49077582)
-- Dependencies: 664
-- Data for Name: indstillinger_grids; Type: TABLE DATA; Schema: vildsvin; Owner: postgres
--

INSERT INTO indstillinger_grids VALUES (1, 2, 'Brugere_grid', 1, NULL, true);
INSERT INTO indstillinger_grids VALUES (2, 3, 'findfuglegrid', 1, NULL, true);
INSERT INTO indstillinger_grids VALUES (3, 5, 'Indsamlinger_grid', 1, NULL, true);
INSERT INTO indstillinger_grids VALUES (4, 6, 'LabListe_grid', 1, NULL, true);
INSERT INTO indstillinger_grids VALUES (5, 7, 'ana_grid', 1, NULL, true);
INSERT INTO indstillinger_grids VALUES (6, 8, 'LevendeGrid', 1, NULL, true);


--
-- TOC entry 5005 (class 0 OID 0)
-- Dependencies: 663
-- Name: indstillinger_grids_id_seq; Type: SEQUENCE SET; Schema: vildsvin; Owner: postgres
--

SELECT pg_catalog.setval('indstillinger_grids_id_seq', 7, true);


--
-- TOC entry 4995 (class 0 OID 49077594)
-- Dependencies: 666
-- Data for Name: indstillinger_kolonner; Type: TABLE DATA; Schema: vildsvin; Owner: postgres
--

INSERT INTO indstillinger_kolonner VALUES (1, 2, 1, 1, 'id', 'ID', 40, true, true);
INSERT INTO indstillinger_kolonner VALUES (2, 2, 1, 2, 'navn', 'Navn', 120, false, true);
INSERT INTO indstillinger_kolonner VALUES (3, 2, 1, 3, 'email', 'Email', 80, false, true);
INSERT INTO indstillinger_kolonner VALUES (4, 2, 1, 4, 'aktiv', 'Status', 40, false, true);
INSERT INTO indstillinger_kolonner VALUES (5, 2, 1, 5, 'firmanavn', 'Firmanavn', 60, false, true);
INSERT INTO indstillinger_kolonner VALUES (6, 2, 1, 6, 'firmaaktiv', 'Firma status', 40, false, true);
INSERT INTO indstillinger_kolonner VALUES (7, 3, 2, 1, 'indberetning', 'Indberetning', 50, false, false);
INSERT INTO indstillinger_kolonner VALUES (8, 3, 2, 2, 'fugleid', 'FugleID', 50, false, false);
INSERT INTO indstillinger_kolonner VALUES (9, 3, 2, 3, 'journalnr', 'Journalnr', 120, false, false);
INSERT INTO indstillinger_kolonner VALUES (10, 5, 3, 1, 'id', 'ID', 40, true, false);
INSERT INTO indstillinger_kolonner VALUES (11, 5, 3, 2, 'afd', 'Afdeling', 120, false, false);
INSERT INTO indstillinger_kolonner VALUES (12, 5, 3, 3, 'opsamler', 'Opsamler', 80, false, false);
INSERT INTO indstillinger_kolonner VALUES (13, 5, 3, 4, 'postnr', 'Postnr', 60, true, false);
INSERT INTO indstillinger_kolonner VALUES (14, 5, 3, 5, 'indberetning', 'Indberetning', 40, false, false);
INSERT INTO indstillinger_kolonner VALUES (15, 5, 3, 6, 'fundtype', 'Fundtype', 100, false, false);
INSERT INTO indstillinger_kolonner VALUES (16, 5, 3, 7, 'set_tid', 'Set dato', 80, false, false);
INSERT INTO indstillinger_kolonner VALUES (17, 5, 3, 8, 'status', 'Status', 80, false, false);
INSERT INTO indstillinger_kolonner VALUES (18, 5, 3, 9, 'skjul', 'Vis / Skjul', 60, true, false);
INSERT INTO indstillinger_kolonner VALUES (19, 6, 4, 1, 'id', 'ID', 40, true, false);
INSERT INTO indstillinger_kolonner VALUES (20, 6, 4, 2, 'afd', 'Indsamler', 120, false, false);
INSERT INTO indstillinger_kolonner VALUES (21, 6, 4, 3, 'indberetning', 'Indberetning', 40, false, false);
INSERT INTO indstillinger_kolonner VALUES (22, 6, 4, 4, 'tid_lab', 'Modtaget dato', 80, true, false);
INSERT INTO indstillinger_kolonner VALUES (23, 6, 4, 5, 'fundtype', 'Fundtype', 100, false, false);
INSERT INTO indstillinger_kolonner VALUES (24, 6, 4, 6, 'journalnr_1', 'Lab J nr.', 100, false, false);
INSERT INTO indstillinger_kolonner VALUES (25, 6, 4, 7, 'journalnr_2', 'FVST J nr.', 100, false, false);
INSERT INTO indstillinger_kolonner VALUES (26, 6, 4, 8, 'status', 'Status', 80, false, false);
INSERT INTO indstillinger_kolonner VALUES (27, 6, 4, 9, 'resultat_tekst', 'Resultat', 100, false, false);
INSERT INTO indstillinger_kolonner VALUES (28, 6, 4, 10, 'skjul', 'Vis / Skjul', 60, true, false);
INSERT INTO indstillinger_kolonner VALUES (29, 7, 5, 1, 'id', 'ID', 40, true, false);
INSERT INTO indstillinger_kolonner VALUES (30, 7, 5, 2, 'indberetning', 'Indberetning', 60, false, false);
INSERT INTO indstillinger_kolonner VALUES (31, 7, 5, 3, 'fundtype', 'Fundtype', 100, false, false);
INSERT INTO indstillinger_kolonner VALUES (32, 7, 5, 4, 'journalnr_1', 'LAB Journalnr', 120, false, false);
INSERT INTO indstillinger_kolonner VALUES (33, 7, 5, 5, 'journalnr_2', 'FVST Journalnr', 120, false, false);
INSERT INTO indstillinger_kolonner VALUES (34, 7, 5, 6, 'kommune', 'Kommune', 120, false, false);
INSERT INTO indstillinger_kolonner VALUES (35, 7, 5, 7, 'set_tid', 'Funddato', 120, false, false);
INSERT INTO indstillinger_kolonner VALUES (36, 7, 5, 8, 'resultat', 'Resultat', 120, false, false);
INSERT INTO indstillinger_kolonner VALUES (37, 8, 6, 1, 'id', 'Indberetning', 40, false, false);
INSERT INTO indstillinger_kolonner VALUES (38, 8, 6, 2, 'antal', 'Antal', 30, false, false);
INSERT INTO indstillinger_kolonner VALUES (39, 8, 6, 3, 'fundtype', 'Fundtype', 80, false, false);
INSERT INTO indstillinger_kolonner VALUES (40, 8, 6, 4, 'afd', 'Registrator', 100, true, false);
INSERT INTO indstillinger_kolonner VALUES (41, 8, 6, 5, 'status', 'Enhed', 100, false, false);
INSERT INTO indstillinger_kolonner VALUES (42, 8, 6, 6, 'reserveret_af_id', 'Noteret', 50, false, false);
INSERT INTO indstillinger_kolonner VALUES (43, 8, 6, 7, 'setpostnr', 'Postnr', 40, false, false);
INSERT INTO indstillinger_kolonner VALUES (44, 8, 6, 8, 'set_tid', 'Fund dato', 80, false, false);
INSERT INTO indstillinger_kolonner VALUES (45, 8, 6, 9, 'skjul', 'Vis / Skjul', 60, true, false);


--
-- TOC entry 5006 (class 0 OID 0)
-- Dependencies: 665
-- Name: indstillinger_kolonner_id_seq; Type: SEQUENCE SET; Schema: vildsvin; Owner: postgres
--

SELECT pg_catalog.setval('indstillinger_kolonner_id_seq', 45, true);


--
-- TOC entry 4997 (class 0 OID 49077608)
-- Dependencies: 668
-- Data for Name: indstillinger_sider; Type: TABLE DATA; Schema: vildsvin; Owner: postgres
--

INSERT INTO indstillinger_sider VALUES (10, 'indberetning_telefon.js', NULL, NULL, 0, true, false, 600, true, NULL);
INSERT INTO indstillinger_sider VALUES (4, 'frm_forside.js', NULL, NULL, 0, true, false, 600, true, NULL);
INSERT INTO indstillinger_sider VALUES (11, 'kontroller.js', NULL, NULL, 0, true, false, 600, true, NULL);
INSERT INTO indstillinger_sider VALUES (12, 'mappanel.js', NULL, NULL, 0, true, false, 600, true, NULL);
INSERT INTO indstillinger_sider VALUES (13, 'menu.js', NULL, NULL, 0, true, false, 600, true, NULL);
INSERT INTO indstillinger_sider VALUES (9, 'frm_login.js', NULL, NULL, 0, true, false, 600, true, NULL);
INSERT INTO indstillinger_sider VALUES (5, 'frm_indsamling.js', 'Døde og skudte', 3, 2, false, false, 600, true, 'visIndsamlinger');
INSERT INTO indstillinger_sider VALUES (6, 'frm_lab.js', 'Prøvesvar', 4, 3, false, false, 600, true, 'visLabListe');
INSERT INTO indstillinger_sider VALUES (7, 'frm_lab_analyser.js', 'Undersøgte vildsvin', 6, 0, false, false, 600, true, 'visLabAnalyser');
INSERT INTO indstillinger_sider VALUES (8, 'frm_levende.js', 'Levende og spor', 2, 11, false, false, 600, true, 'visLevende');
INSERT INTO indstillinger_sider VALUES (3, 'frm_find.js', 'Find vildsvin', 5, 0, false, false, 600, true, 'visFindVildsvin');
INSERT INTO indstillinger_sider VALUES (1, 'dlg_bredder.js', NULL, NULL, 0, true, true, 600, true, NULL);
INSERT INTO indstillinger_sider VALUES (2, 'frm_brugere.js', 'Administration af brugere', 1, 4, false, true, 600, false, 'visBrugere');


--
-- TOC entry 5007 (class 0 OID 0)
-- Dependencies: 667
-- Name: indstillinger_sider_id_seq; Type: SEQUENCE SET; Schema: vildsvin; Owner: postgres
--

SELECT pg_catalog.setval('indstillinger_sider_id_seq', 15, true);


-- Completed on 2018-09-09 18:32:24

--
-- PostgreSQL database dump complete
--


-- EFTERRRETTELSER FRM_FIND
-- find skal ikke ses af ikke-login
UPDATE vildsvin.indstillinger_sider set firma=-1 where id=3;
/*
id	side	grid	raekkefoelge	dataindex	titel	bredde	hidden	opdateret
7	3	2	1	indberetning	Indberetning	50	FALSE	FALSE
8	3	2	2	fugleid	FugleID	50	FALSE	FALSE
9	3	2	3	journalnr	Journalnr	120	FALSE	FALSE

*/

update vildsvin.indstillinger_kolonner set bredde=80 where id =7;
update vildsvin.indstillinger_kolonner set bredde=80, dataindex='vildsvin_id', titel='Vildsvin ID' where id =8;
update vildsvin.indstillinger_kolonner set dataindex='journalnr_2', titel='FVST journalnr' where id =9;
INSERT INTO vildsvin.indstillinger_kolonner(side, grid, raekkefoelge, dataindex, titel, bredde, hidden, opdateret) VALUES (3, 2, 4, 'journalnr_1', 'VET Journalnr', 120, false, false);


